<?php

//load theme name from db to session['theme']

class Theme
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();

        $theme = $this->CI->db->query("SELECT value FROM settings WHERE name='theme'")->row()->value;
        $this->CI->session->set_userdata('theme', $theme);

        $language = $this->CI->db->query("SELECT value FROM settings WHERE name='language'")->row()->value;
        $this->CI->session->set_userdata('language', $language);
    }
}
