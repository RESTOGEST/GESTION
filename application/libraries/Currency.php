<?php

class Currency
{
    protected $CI;

    public function format($amount)
    {
        $this->CI = &get_instance();

        $sql = "SELECT value FROM settings WHERE name='currency_sl'";

        $a = $this->CI->db->query($sql)->row_array();

        if ($a) {
            $currency_sl = $a['value'];
        } else {
            $currency_sl = '';
        }

        $b = $this->CI->db->query("SELECT value FROM settings WHERE"
            . " name='currency_sr'")->row_array();

        if ($b) {
            $currency_sr = $b['value'];
        } else {
            $currency_sr = '';
        }

        if ($amount) {
            return $currency_sl.$amount.$currency_sr;
        } else {
            return $currency_sl.'0'.$currency_sr;
        }
    }
}
