<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CustomerModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function existsEmail($email)
    {
        $email_count = $this->db->get_where(
            'users',
            array('email' => $email)
        )->num_rows();

        return $email_count;
    }

    public function changeEmail()
    {
        $data = array('email' => $this->input->post('email', true));
        $condition = array('id' => userdata());

        return $this->db->update('users', $data, $condition);
    }

    public function createUser()
    {
        $user_details = array(
            'name' => $this->input->post('name', true),
            'email' => $this->input->post('email', true),
            'telephone' => $this->input->post('telephone', true),
            'password' => md5($this->input->post('pass1', true)),
            'details' => $this->input->post('details', true),
            'register_time' => strtotime(date('d F Y g:i a')),
            'ip_address' => $this->input->server('REMOTE_ADDR'),
            'status' => '1',
        );

        return $this->db->insert('users', $user_details);
    }

    public function updateUser()
    {
        if ($this->input->post('banned', true) == 1) {
            $status = 0;
        } else {
            $status = 1;
        }

        $user_details = array(
            'name' => $this->input->post('name', true),
            'details' => $this->input->post('details', true),
            'email' => $this->input->post('email', true),
            'telephone' => $this->input->post('telephone', true),
            'department' => $this->input->post('department', true),
            'status' => $status,
        );

        return $this->db->update('users', $user_details,
                array('id' => $this->input->post('user_id', true)));
    }

    public function userData($username)
    {
        return $this->db->get_where(
            'users',
            array('email' => $username)
        )->row();
    }

    public function checkUserDetail()
    {
        $username = $this->input->post('email', true);
        $password = $this->input->post('password', true);

        $userdata = $this->user_data($username);

        if ($userdata->email == $username &&
            $userdata->password == md5($password) &&
            $userdata->status == 1) {

            return true;
            
        } else {
            return false;
        }
    }

    public function createLostpwCode()
    {
        $email = $this->input->post('email', true);

        $lostpw_code = md5(microtime().'-'.rand(10, 10000).'-'.$email);

        $this->db->update(
            'users',
            array('lostpw' => $lostpw_code),
            array('email' => $this->input->post('email', true))
        );

        return $lostpw_code;
    }

    public function checkCode($email, $code)
    {
        return $this->db->get_where(
            'users',
            array('email' => $email, 'lostpw' => $code)
        )->num_rows();
    }

    public function createNewPassword($email)
    {
        $new_password = substr(
            strtoupper(md5(microtime().'-'.rand(100, 10000))),
            0,
            6
        );

        $updated = $this->db->update(
            'users',
            array('password' => md5($new_password), 'lostpw' => ''),
            array('email' => $email)
        );

        if ($updated) {
            return $new_password;
        }
    }

    public function checkPassword()
    {
        $where = array(
            'email' => userdata('email'),
            'password' => md5($this->input->post('currentpass', true))
        );

        $this->db->where($where);

        return $this->db->get('users')->num_rows();
    }

    public function passwordUpdate()
    {
        return $this->db->update(
            'users',
            array('password' => md5($this->input->post('pass1', true))),
            array('email' => userdata('email'))
        );
    }

    public function userList(
        $limit = 10,
        $offset = 0,
        $filter_banned = '',
        $sort = 'name',
        $order = 'DESC',
        $q = ''
    ) {
        $arr_sort = array(
            'name',
            'email',
            'register_time',
            'status',
        );

        if (in_array($sort, $arr_sort)) {
            $this->db->order_by($sort, $order);
        }

        if ($q) {
            $this->db->like('name', $q);
        }

        //assigned to agent 
        if (userdata('department') > 1) {
            $this->db->where('agent_id', $this->session->userdata('user_id'));
        }
        
        if ($filter_banned) {
            
            $this->db->where(
                'status = 0 AND department = 0 AND users.id !=',
                $this->session->userdata('user_id')
            ); // ID 14 : First Admin user. You can't delete.
            
        } else {

            $this->db->where(
                'department = 0 AND users.id !=',
                $this->session->userdata('user_id')
            ); // ID 14 : First Admin user. You can't delete.
            
        }

        $this->db->select('users.id,users.status, users.name, users.email,'
            . ' users.register_time');

        return $this->db->get('users', $limit, $offset)->result();
    }

    public function totalUsers($filter_banned = '', $q = '')
    {
        if ($q) {
            $this->db->like('name', $q);
        }

        //assigned to agent 
        if (userdata('department') > 1) {
            $this->db->where('agent_id', $this->session->userdata('user_id'));
        }
        
        if ($filter_banned) {
            
            $this->db->where(
                'status = 0 AND department = 0 AND users.id !=',
                $this->session->userdata('user_id')
            ); // ID 14 : First Admin user. You can't delete.

        } else {

            $this->db->where(
                'department = 0 AND users.id !=',
                $this->session->userdata('user_id')
            ); // ID 14 : First Admin user. You can't delete.
            
        }

        return $this->db->count_all_results('users');
    }

    public function departmentList()
    {
        return $this->db->get('departments')->result();
    }

    public function getUser($user_id)
    {
        return $this->db->get_where('users', array('id' => $user_id))->row();
    }

    public function checkUserId($user_id)
    {
        return $this->db->get_where(
            'users',
            array('id' => $user_id)
        )->num_rows();
    }

    public function delete($user_id)
    {
        if ($this->db->delete('users', array('id' => $user_id))) {
            
            $this->db->delete('tickets', array('user_id' => $user_id));
            $this->db->delete('messages', array('user_id' => $user_id));

            return true;
        }
    }
}
