<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class TicketModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getServiceName($service_id)
    {
        $row = $this->db->query('select * from services WHERE service_id="'.$service_id.'"')->row_array();
        
        if ($row) {
            return $row['title'];
        } 
    }
    
    public function getProductName($product_id)
    {
        $row = $this->db->query('select * from products WHERE product_id="'.$product_id.'"')->row_array();
        
        if ($row) {
            return $row['title'];
        } 
    }
    
    public function services() 
    {
        return $this->db->query('select * from services')->result_array();
    }
    
    public function products()
    {
        return $this->db->query('select * from products')->result_array();
    }
    
    public function ticketAssign()
    {
        $this->db->set('agent_id', $this->input->post('agent_id', true));
        $this->db->where('id', $this->input->post('ticket_id', true));
        $this->db->update('tickets');

        return $this->db->get_where(
            'users',
            array('id' => $this->input->post('agent_id', true))
        )->row()->name;
    }

    public function departments()
    {
        return $this->db->get('departments')->result();
    }

    public function checkDepartmentId($id)
    {
        return $this->db->get_where('departments', array('id' => $id))
            ->num_rows();
    }

    /*  All pending tickets
     *  e.g., status = 2
     * 
     */
    public function pendingTickets($data = array())
    {
        //for customers
        if (userdata('department') == 0) {
            
            $where = 'ticket_status = 2 AND user_id = '
                .$this->session->userdata('user_id');
            
            $this->db->order_by('tickets.id', 'desc');

            //for admin
        } elseif (userdata('department') == 1) {

            /* filter
              - deprtment
              - agent
              - customer name
              - customer email */

            if ($data['filter_department']) {
                $this->db->where('tickets.department_id',
                    $data['filter_department']);
            }

            if ($data['filter_agent']) {
                $this->db->where('tickets.agent_id', $data['filter_agent']);
            }

            if ($data['filter_customer_name']) {
                $this->db->where(
                    'customer.name',
                    $data['filter_customer_name']
                );
            }

            if ($data['filter_customer_email']) {
                $this->db->where(
                    'customer.email',
                    $data['filter_customer_email']
                );
            }

            $where = 'ticket_status = 2';

            //department
        } else {
            
            $where = 'ticket_status = 2 AND tickets.agent_id = '
                .$this->session->userdata('user_id');

            if ($data['filter_customer_name']) {
                $this->db->where(
                    'customer.name',
                    $data['filter_customer_name']
                );
            }

            if ($data['filter_customer_email']) {
                $this->db->where(
                    'customer.email',
                    $data['filter_customer_email']
                );
            }
        }

        $this->db->order_by('tickets.priority', 'desc');
        $this->db->where($where, null, false);

        $this->db->select('tickets.priority, tickets.modified_time,'
            . 'customer.name as customer, users.name, tickets.id,'
            . ' tickets.department_id, tickets.modified_time,'
            . 'tickets.created_time, tickets.title, tickets.ticket_status,'
            . ' departments.department_name');

        $this->db->from('tickets');
        
        $this->db->join(
            'departments',
            'tickets.department_id = departments.id',
            'left'
        );
        
        $this->db->join('users', 'users.id = tickets.agent_id', 'left');

        $this->db->join(
            '`users` customer', 'customer.id = tickets.user_id',
            'left'
        );

        return $this->db->get()->result();
    }

    /*  All open tickets 
     *  e.g., status = 1
     */
    public function openTickets($data = array())
    {
        //for customers
        if (userdata('department') == 0) {

            $where = 'ticket_status = 1 AND user_id = '
                .$this->session->userdata('user_id');

            //for admin
        } elseif (userdata('department') == 1) {

            /* filter
              - deprtment
              - agent
              - customer name
              - customer email */

            if ($data['filter_department']) {
                $this->db->where('tickets.department_id',
                    $data['filter_department']);
            }

            if ($data['filter_agent']) {
                $this->db->where('tickets.agent_id', $data['filter_agent']);
            }

            if ($data['filter_customer_name']) {
                $this->db->where(
                    'customer.name',
                    $data['filter_customer_name']
                );
            }

            if ($data['filter_customer_email']) {
                $this->db->where(
                    'customer.email',
                    $data['filter_customer_email']
                );
            }

            $where = 'ticket_status = 1';

            //department
        } else {

            $where = ' ticket_status = 1 AND tickets.agent_id = '
                .$this->session->userdata('user_id');

            /* filter
              - customer name
              - customer email */

            if ($data['filter_customer_name']) {

                $this->db->where(
                    'customer.name',
                    $data['filter_customer_name']
                );

            }

            if ($data['filter_customer_email']) {

                $this->db->where(
                    'customer.email',
                    $data['filter_customer_email']
                );
            }
        }

        $this->db->order_by('tickets.priority', 'desc');
        $this->db->where($where, null, false);

        $this->db->select('tickets.modified_time,tickets.priority, '
            . 'customer.name as customer, users.name, tickets.id,'
            . ' tickets.department_id, tickets.modified_time,'
            . 'tickets.created_time, tickets.title, tickets.ticket_status,'
            . ' departments.department_name');
        
        $this->db->from('tickets');

        $this->db->join(
            'departments',
            'tickets.department_id = departments.id',
            'left'
        );
        $this->db->join('users', 'users.id = tickets.agent_id', 'left');

        $this->db->join(
            '`users` customer', 'customer.id = tickets.user_id',
            'left'
        );

        return $this->db->get()->result();
    }

    /*  All close tickets 
     *  e.g., status = 3
     */
    public function otherTickets($start, $end, $data = array())
    {
        //for customer 
        if (userdata('department') == 0) {
            
            $where = 'tickets.ticket_status = 3 AND tickets.user_id = '
                .$this->session->userdata('user_id');

            //for admin                   
        } elseif (userdata('department') == 1) {
            
            if ($data['filter_department']) {
                
                $this->db->where(
                    'tickets.department_id',
                    $data['filter_department']
                );

            }

            if ($data['filter_agent']) {
                $this->db->where('tickets.agent_id', $data['filter_agent']);
            }

            if ($data['filter_customer_name']) {
                $this->db->where(
                    'customer.name',
                    $data['filter_customer_name']
                );
            }

            if ($data['filter_customer_email']) {
                $this->db->where(
                    'customer.email',
                    $data['filter_customer_email']
                );
            }

            $where = 'tickets.ticket_status = 3';

            //for agents
        } else {
            
            if ($data['filter_customer_name']) {

                $this->db->where(
                    'customer.name',
                    $data['filter_customer_name']
                );
            }

            if ($data['filter_customer_email']) {
                $this->db->where(
                    'customer.email',
                    $data['filter_customer_email']
                );
            }

            $where = 'ticket_status = 3 AND tickets.agent_id = '
                .$this->session->userdata('user_id');
            
        }

        $this->db->where($where, null, false);
        $this->db->order_by('tickets.priority', 'desc');
        $this->db->limit($end, $start);

        $this->db->select('customer.name as customer, tickets.priority,'
            . ' tickets.modified_time, users.name, tickets.id,'
            . ' tickets.department_id, tickets.created_time, tickets.title, '
            . 'tickets.ticket_status, departments.department_name');

        $this->db->from('tickets');
        
        $this->db->join(
            'departments',
            'tickets.department_id = departments.id',
            'left'
        );

        $this->db->join('users', 'users.id = tickets.agent_id', 'left');
        
        $this->db->join(
            '`users` customer',
            'customer.id = tickets.user_id',
            'left'
        );

        return $this->db->get()->result();
    }

    public function getAgentName($agent_id)
    {
        $row = $this->db->get_where('users', array('id' => $agent_id))
            ->row_array();
        
        if ($row) {
            return $row['name'];
        }
    }

    public function totalTickets($data)
    {
        if (userdata('department') == 0) {

            $where = array(
                'ticket_status' => 3,
                'user_id' => $this->session->userdata('user_id')
            );

            $this->db->where($where);

            return $this->db->get('tickets')->num_rows();

            //for admin 
        } elseif (userdata('department') == 1) {
            
            $this->db->where(array('ticket_status' => 3));

            if ($data['filter_department']) {
                $this->db->where(
                    'tickets.department_id',
                    $data['filter_department']
                );
            }

            if ($data['filter_agent']) {
                $this->db->where('tickets.agent_id', $data['filter_agent']);
            }

            if ($data['filter_customer_name']) {
                $this->db->where(
                    'customer.name',
                    $data['filter_customer_name']
                );
            }

            if ($data['filter_customer_email']) {
                $this->db->where(
                    'customer.email',
                    $data['filter_customer_email']
                );
            }

            $this->db->join(
                '`users` customer',
                'customer.id = tickets.user_id',
                'left'
            );

            return $this->db->get('tickets')->num_rows();

            //for agent
        } else {

            $where = array(
                'ticket_status' => 3,
                'tickets.agent_id' => $this->session->userdata('user_id')
            );

            $this->db->where($where);

            if ($data['filter_customer_name']) {
                
                $this->db->where(
                    'customer.name',
                    $data['filter_customer_name']
                );

            }

            if ($data['filter_customer_email']) {

                $this->db->where(
                    'customer.email',
                    $data['filter_customer_email']
                );
            }

            $this->db->join(
                '`users` customer',
                'customer.id = tickets.user_id',
                'left'
            );

            return $this->db->get('tickets')->num_rows();
        }
    }

    public function createTicket()
    {
        $department = $this->input->post('department', true);
        
        //get agent of ticket department with minimum ticket assigned 
        $agent_row = $this->db->query("select id, (select count(*) from tickets 
            where agent_id = u.id) as ticket_assigned from users u WHERE
            u.department = '".$department."' order by ticket_assigned
            ASC")->row_array();
        
        if ($agent_row) {
            $agent_id = $agent_row['id'];
        } else {
            $agent_id = 0;
        }
        
        $data = array(
            'department_id' => $this->input->post('department', true),
            'user_id' => $this->session->userdata('user_id'),
            'product_id' => $this->input->post('product_id', true),
            'service_id' => $this->input->post('service_id', true),
            'order_id' => $this->input->post('order_id', true),
            'agent_id' => $agent_id,
            'title' => $this->input->post('title', true),
            'ftp_db_access' => $this->input->post('ftp_db_access', true),
            'priority' => $this->input->post('priority', true),
            'created_time' => strtotime(date('d F Y g:i a')),
            'modified_time' => strtotime(date('d F Y g:i a')),
            'ip_address' => $this->input->server('REMOTE_ADDR'),
            'secret' => rand(1111111111, 9999999999),
            'ticket_status' => 2,
        );
        $this->db->insert('tickets', $data);

        $ticket_id = $this->db->insert_id();

        $message_data = array(
            'ticket_id' => $ticket_id,
            'files' => $this->input->post('files', true),
            'user_id' => $this->session->userdata('user_id'),
            'message' => $this->input->post('message'),//strip_tags('<a><b><i><u><ul><li><ol><pre>'),                
            'ip_address' => $this->input->server('REMOTE_ADDR'),
            'created_time' => strtotime(date('d F Y g:i a')),
        );

        $this->db->insert('messages', $message_data);

        //Send to customer 
        $subject = config('new_ticket_subject');
        $template = config('new_ticket_template');

        $search = array(
            '{{site_name}}',
            '{{ticket_id}}',
            '{{title}}',
            '{{user_id}}',
            '{{message}}',
            '{{ip_addresss}}',
            '{{link}}',
        );

        $replace = array(
            config('site_name'),
            $ticket_id,
            $this->input->post('title', true),
            $this->session->userdata('user_id'),
            $this->input->post('message'),//strip_tags('<a><b><i><u><ul><li><ol><pre>')
            $this->input->server('REMOTE_ADDR'),
            site_url('tickets/mail_link/'.$ticket_id.'/'.$data['secret']),
        );

        //get users email
        $email = $this->db->get_where(
            'users',
            array('id' => $this->session->userdata('user_id'))
        )->row()->email;

        sendNotice(
            $email,
            str_replace($search, $replace, $subject),
            str_replace($search, $replace, $template)
        );

        return $ticket_id;
    }

    public function newMessage()
    {
        //$msg_text =  ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]
        //", "<a href=\"\\0\" target=\"_blank\">\\0</a>"
        //, $this->input->post('message'));

        $message = array(
            'ticket_id' => $this->input->post('ticket_id', true),
            'user_id' => $this->session->userdata('user_id'),
            'files' => $this->input->post('files', true),
            'message' => $this->input->post('message'),//strip_tags(  '<a><b><i><u><ul><li><ol><pre>'),              
            'ip_address' => $this->input->server('REMOTE_ADDR'),
            'created_time' => strtotime(date('d F Y g:i a')),
        );

        $this->db->insert('messages', $message);
        $insert_id = $this->db->insert_id();

        //$this->transfer_attachments($this->input->post('ticket_id'),
        // $insert_id);

        if (userdata('department') != 0) {
            $new_status = 1;
            $this->db->update('tickets', array('ticket_status' => 1),
              array('id' => $this->input->post('ticket_id')));
        } else {
            $new_status = 2;
            $this->db->update('tickets', array('ticket_status' => 2),
            array('id' => $this->input->post('ticket_id')));
        }

        //open to pending 
        //close to pending
        //clsoe to open
        //pending to open
        //pending to close
        //open to closed 

        $sql = 'select * from tickets WHERE id = "'
            .$this->input->post('ticket_id', true).'"';

        $ticket_info = $this->db->query($sql)->row_array();

        $old_status = $ticket_info['ticket_status'];

        if ($old_status != $new_status) {
            $arr_status = array(
                '1' => $this->lang->line('open'),
                '2' => $this->lang->line('pending'),
                '3' => $this->lang->line('close'),
            );

            $search = array(
                '{{ticket_id}}',
                '{{message}}',
                '{{time}}',
                '{{old_status}}',
                '{{new_status}}',
                '{{link}}',
                '{{site_name}}',
            );

            $link = site_url('tickets/mail_link/'
                .$this->input->post('ticket_id', true).'/'.$ticket_info['secret']);

            $replace = array(
                $this->input->post('ticket_id', true),
                //strip_tags(   '<a><b><i><u><ul><li><ol><pre>'
                $this->input->post('message', true),
                date('d F Y g:i a'),
                $arr_status[$old_status],
                $arr_status[$new_status],
                $link,
                config('site_name'),
            );

            $subject = str_replace(
                $search,
                $replace,
                config('ticket_status_change_subject')
            );

            $template = str_replace(
                $search,
                $replace,
                config('ticket_status_change_template')
            );

            $customer_id = $ticket_info['user_id'];

            $email = $this->db->get_where(
                'users',
                array('id' => $customer_id)
            )->row()->email;

            sendNotice($email, $subject, $template);
        }

        //update modified time
        $this->db->set('modified_time', strtotime(date('d F Y g:i a')));
        $this->db->where('id', $this->input->post('ticket_id', true));
        $this->db->update('tickets');

        return $insert_id;
    }

    public function transferAttachments($ticket_id, $message_id)
    {
        $attach_count = $this->db->get_where(
            'attachments_tmp',
            array('ticket_id' => $ticket_id)
        )->num_rows();

        if ($attach_count > 0) {

            $attachments = $this->db->get_where(
                'attachments_tmp',
                array('ticket_id' => $ticket_id)
            )->result();

            $attach_data = array();

            foreach ($attachments as $attach) {
                $attach_data[] = $attach->file_name;
            }

            $updated = $this->db->update(
                'messages',
                array('files' => json_encode($attach_data)),
                array('id' => $message_id)
            );

            if ($updated) {
                $this->db->delete(
                    'attachments_tmp',
                    array('ticket_id' => $ticket_id)
                );
            }
        }
    }

    public function existsTicket($ticket_id)
    {
        // if customer
        if (userdata('department') == 0) {

            $condition = array(
                'id' => $ticket_id,
                'user_id' => $this->session->userdata('user_id')
            );
            
        } else {
            $condition = array('id' => $ticket_id);
        }

        return $this->db->get_where('tickets', $condition)->num_rows();
    }

    public function ticketDetails($ticket_id)
    {
        // if customer
        if (userdata('department') == 0) {
            
            $condition = array(
                'tickets.id' => $ticket_id,
                'tickets.user_id' => $this->session->userdata('user_id')
            );

        } else {
            
            // if support team
            $condition = array('tickets.id' => $ticket_id);
        }

        $this->db->where($condition);
        
        $this->db->select('tickets.order_id, tickets.product_id, tickets.service_id, tickets.id, tickets.ftp_db_access, tickets.modified_time, '
            . 'tickets.department_id, tickets.title, tickets.priority, '
            . 'tickets.created_time, tickets.ticket_status, users.details,'
            . ' users.id as user_id, users.name, users.email, ');

        $this->db->from('tickets');
        
        $this->db->join('users', 'tickets.user_id = users.id');

        return $this->db->get()->row();
    }
    
    //update ticket ftp_db_access 
    public function updateAccess()
    {
        $this->db->set('ftp_db_access', $this->input->post('ftp_db_access'));
        $this->db->where('id', $this->input->post('id', true));
        
        //department login 
        if(userdata('department') > 0) {
            $this->db->where('agent_id', userdata('id')); //tickets assigned to login user 
            
        //customer login     
        } elseif (userdata('department') == 0) {
            $this->db->where('user_id', userdata('id')); //ticket generated by me 
        }
        
        $this->db->update('tickets');
    }
    
    //update customer details 
    public function updateDetails()
    { 
        $this->db->set('details', $this->input->post('details'));
                
        if(userdata('department') > 0){
            $this->db->where('id', $this->input->post('user_id', true));
        }else{
            $this->db->where('id', userdata('id'));
        }
        
        $this->db->update('users');
    }
    
    public function ticketMessages($ticket_id)
    {
        $condition = array('messages.ticket_id' => $ticket_id);
        $this->db->where($condition);
        $this->db->order_by('messages.id', 'asc');
        $this->db->select('*');
        $this->db->from('messages');
        $this->db->join('users', 'messages.user_id = users.id');

        return $this->db->get()->result();
    }

    public function getDepartmentName($department_id)
    {
        $department_detail = $this->db->get_where(
            'departments',
            array('id' => $department_id)
        )->row_array();
        
        if ($department_detail) {
            return $department_detail['department_name'];
        }
    }

    public function deleteTicket($ticket_id)
    {
        if (userdata('department') == 0) {

            $where = array(
                'id' => $ticket_id,
                'user_id' => $this->session->userdata('user_id')
            );

            $this->db->delete('tickets', $where);

            $where = array(
                'ticket_id' => $ticket_id,
                'user_id' => $this->session->userdata('user_id')
            );

            $this->db->delete('messages', $where);
            
        } else {

            $this->db->delete('tickets', array('id' => $ticket_id));
            $this->db->delete('messages', array('ticket_id' => $ticket_id));
            
        }

        return true;
    }

    public function closeTicket($ticket_id)
    {
        $sql = 'select * from tickets WHERE id = "'.$ticket_id.'"';

        $ticket_info = $this->db->query($sql)->row_array();

        $new_status = 3;

        $old_status = $ticket_info['ticket_status'];

        if ($old_status != $new_status) {
            
            $arr_status = array(
                '1' => $this->lang->line('open'),
                '2' => $this->lang->line('pending'),
                '3' => $this->lang->line('close'),
            );

            $search = array(
                '{{ticket_id}}',
                '{{old_status}}',
                '{{new_status}}',
                '{{link}}',
                '{{site_name}}',
            );

            $link = site_url('tickets/mail_link/'
                .$this->input->post('ticket_id', true).'/'
                .$ticket_info['secret']);

            $replace = array(
                $this->input->post('ticket_id', true),
                $arr_status[$old_status],
                $arr_status[$new_status],
                $link,
                config('site_name'),
            );

            $subject = str_replace(
                $search,
                $replace,
                config('ticket_status_change_subject')
            );

            $template = str_replace(
                $search,
                $replace,
                config('ticket_status_change_template')
            );

            $customer_id = $ticket_info['user_id'];

            $email = $this->db->get_where(
                'users',
                array('id' => $customer_id)
            )->row()->email;

            sendNotice($email, $subject, $template);
        }

        if (userdata('department') == 0) {

            $where = array(
                'id' => $ticket_id,
                'user_id' => $this->session->userdata('user_id')
            );

            $data = array('ticket_status' => 3);

            return $this->db->update('tickets', $data, $where);
            
        } else {
            
            return $this->db->update(
                'tickets',
                array('ticket_status' => 3),
                array('id' => $ticket_id)
            );
            
        }
    }

    public function userdataWithId($user_id)
    {
        return $this->db->get_where('users', array('id' => $user_id))->row();
    }

    public function messageDetails($message_id)
    {
        return $this->db->get_where('messages', array('id' => $message_id))
            ->row();
    }

    public function getAttachmentsTmp($ticket_id = 0)
    {
        $where = array(
            'ticket_id' => $ticket_id,
            'user_id' => $this->session->userdata('user_id')
        );

        return $this->db->get_where('attachments_tmp', $where)->result();
    }

    public function saveFile($file_data)
    {
        return $this->db->insert('attachments_tmp', $file_data);
    }

    public function existsFile($file_name)
    {
        return $this->db->get_where(
            'attachments_tmp',
            array('file_name' => $file_name)
        )->num_rows();
    }

    public function deleteFile($file_name)
    {
        return $this->db->delete(
            'attachments_tmp',
            array('file_name' => $file_name)
        );
    }
}
