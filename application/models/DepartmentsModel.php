<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class DepartmentsModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function create()
    {
        $department_details = array(
            'department_name' => $this->input->post('name', true)
        );

        return $this->db->insert('departments', $department_details);
    }

    public function update()
    {
        $department_details = array(
            'department_name' => $this->input->post('name', true)
        );

        return $this->db->update(
            'departments', $department_details,
            array('id' => $this->input->post('department_id', true))
        );
    }

    public function departmentList()
    {
        return $this->db->get('departments')->result();
    }

    public function getDepartment($department_id)
    {
        return $this->db->get_where(
            'departments',
            array('id' => $department_id)
        )->row();
    }

    public function checkDepartmentId($department_id)
    {
        return $this->db->get_where(
            'departments',
            array('id' => $department_id)
        )->num_rows();
    }

    public function delete($department_id)
    {
        $this->db->delete('departments', array('id' => $department_id));

        $tickets = $this->db->get_where(
            'tickets',
            array('department_id' => $department_id)
        )->result();

        if (!empty($tickets)) {
            foreach ($tickets as $ticket) {
                $this->db->delete('tickets', array('id' => $ticket->id));

                $this->db->delete(
                    'messages',
                    array('ticket_id' => $ticket->id)
                );
            }
        }

        return true;
    }

    public function existDepName($name)
    {
        return $this->db->get_where(
            'departments',
            array('department_name' => $name)
        )->num_rows();
    }
}
