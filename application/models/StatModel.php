<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class StatModel extends CI_Model
{
    public function totalUsers()
    {
        $this->db->where('department!=0', null, false);

        return $this->db->count_all_results('users');
    }

    public function totalCustomers()
    {
        $this->db->where('department', 0);

        return $this->db->count_all_results('users');
    }

    public function totalDepartments()
    {
        return $this->db->count_all('departments');
    }

    public function totalOpenTickets()
    {
        //if agent get asigned tickets only
        if(userdata('department') > 1) {
            $this->db->where('agent_id', userdata('id'));
        }
        
        $this->db->where('ticket_status', 1);
        return $this->db->count_all_results('tickets');
    }

    public function totalPendingTickets()
    {
        //if agent get asigned tickets only
        if(userdata('department') > 1) {
            $this->db->where('agent_id', userdata('id'));
        }
        
        $this->db->where('ticket_status', 2);

        return $this->db->count_all_results('tickets');
    }

    public function totalCloseTickets()
    {
        //if agent get asigned tickets only
        if(userdata('department') > 1) {
            $this->db->where('agent_id', userdata('id'));
        }
        
        $this->db->where('ticket_status', 3);

        return $this->db->count_all_results('tickets');
    }

    public function totalTickets()
    {
        //if agent get asigned tickets only
        if(userdata('department') > 1) {
            $this->db->where('agent_id', userdata('id'));
            return $this->db->count_all_results('tickets');
        }else{
            return $this->db->count_all('tickets');
        }        
    }

    public function totalMessages()
    {
        return $this->db->count_all('messages');
    }

    public function totalBannedCustomers()
    {
        $this->db->where('status', 0);
        $this->db->where('department', 0);

        return $this->db->count_all_results('users');
    }

    public function totalBannedUsers()
    {
        $this->db->where('status', 0);

        return $this->db->count_all_results('users');
    }

    public function getGraph($range = 'month')
    {
        $data = array();

        switch ($range) {
            case 'week':
                //foreach day in week
                $date_start = strtotime('-'.date('w').' days');

                for ($i = 0; $i < 7; ++$i) {
                    $date = date('Y-m-d', $date_start + ($i * 86400));

                    //total tickets 
                    $sql  = 'SELECT COUNT(*) AS total FROM `tickets` ';
                    $sql .= 'WHERE DATE(FROM_UNIXTIME(created_time)) = '.$this->db->escape($date);
                    
                    if(userdata('department') > 1) {
                        $sql .= ' AND agent_id="' .  userdata('id') . '" ';
                    }
                    
                    $sql .= 'GROUP BY DATE(FROM_UNIXTIME(created_time))';
                            
                    $row = $this->db->query($sql)->row_array();
                    
                    //closed tickets 
                    $sql1  = 'SELECT COUNT(*) AS total FROM `tickets` ';
                    $sql1 .= 'WHERE ticket_status=3 ';
                    $sql1 .= 'AND DATE(FROM_UNIXTIME(created_time)) = '.$this->db->escape($date).' ';
                    
                    if(userdata('department') > 1) {
                        $sql1 .= ' AND agent_id="' .  userdata('id') . '" ';
                    }
                    
                    $sql1 .= 'GROUP BY DATE(FROM_UNIXTIME(created_time))';
                            
                    $row1 = $this->db->query($sql1)->row_array();

                    if (!isset($row['total'])) {
                        $row['total'] = 0;
                    }
                    if (!isset($row1['total'])) {
                        $row1['total'] = 0;
                    }

                    $data[] = array(
                        'time' => date('D', strtotime($date)),
                        'total' => $row['total'],
                        'closed' => $row1['total'],
                    );
                }

                break;
            case 'month':
                //foreach day in month
                for ($i = 1; $i <= date('t'); ++$i) {
                    $date = date('Y').'-'.date('m').'-'.$i;

                    $sql  = 'SELECT COUNT(*) AS total FROM `tickets` ';
                    $sql .= 'WHERE DATE(FROM_UNIXTIME(created_time)) = '.$this->db->escape($date).' ';
                    
                    if(userdata('department') > 1) {
                        $sql .= ' AND agent_id="' .  userdata('id') . '" ';
                    }
                    
                    $sql .= 'GROUP BY DAY(FROM_UNIXTIME(created_time))';
                            
                    $row = $this->db->query($sql)->row_array();
                    
                    $sql1  = 'SELECT COUNT(*) AS total FROM `tickets` ';
                    $sql1 .= 'WHERE ticket_status=3 AND DATE(FROM_UNIXTIME(created_time)) = ' .$this->db->escape($date).' ';
                    
                    if(userdata('department') > 1) {
                        $sql1 .= ' AND agent_id="' .  userdata('id') . '" ';
                    }
                    
                    $sql1 .= 'GROUP BY DAY(FROM_UNIXTIME(created_time))';
                            
                    $row1 = $this->db->query($sql1)->row_array();

                    if (!isset($row['total'])) {
                        $row['total'] = 0;
                    }
                    if (!isset($row1['total'])) {
                        $row1['total'] = 0;
                    }

                    $data[] = array(
                        'time' => date('j', strtotime($date)),
                        'total' => $row['total'],
                        'closed' => $row1['total'],
                    );
                }
                break;
            case 'year':
                //foreach month 
                for ($i = 1; $i <= 12; ++$i) {

                    $sql  = 'SELECT COUNT(*) AS total FROM `tickets` ';
                    $sql .= 'WHERE YEAR(FROM_UNIXTIME(created_time)) = '.date('Y').' ';
                    $sql .= 'AND MONTH(FROM_UNIXTIME(created_time)) = "'.$i.'" ';
                    
                    if(userdata('department') > 1) {
                        $sql .= ' AND agent_id="' .  userdata('id') . '" ';
                    }
                    
                    $sql .= 'GROUP BY MONTH(FROM_UNIXTIME(created_time))';
                                                
                    $row = $this->db->query($sql)->row_array();
                    
                    $sql1  = 'SELECT COUNT(*) AS total FROM `tickets` ';
                    $sql1 .= 'WHERE ticket_status=3 ';
                    $sql1 .= 'AND YEAR(FROM_UNIXTIME(created_time)) = '.date('Y')." ";
                    $sql1 .= 'AND MONTH(FROM_UNIXTIME(created_time)) = "'.$i.'" ';
                    
                    if(userdata('department') > 1) {
                        $sql1 .= ' AND agent_id="' .  userdata('id') . '" ';
                    }
                    
                    $sql1 .= "GROUP BY MONTH(FROM_UNIXTIME(created_time))";
                            
                    $row1 = $this->db->query($sql1)->row_array();

                    if (!isset($row['total'])) {
                        $row['total'] = 0;
                    }
                    if (!isset($row1['total'])) {
                        $row1['total'] = 0;
                    }

                    $data[] = array(
                        'time' => date('M', mktime(0, 0, 0, $i, 1, date('Y'))),
                        'total' => $row['total'],
                        'closed' => $row1['total'],
                    );
                }
                break;
            case 'today':
                //foreach hour 
                for ($i = 0; $i < 24; ++$i) {
                    
                    $sql  = "SELECT COUNT(*) AS total FROM `tickets` ";
                    $sql .= "Where (DATE(FROM_UNIXTIME(created_time)) = DATE(NOW()) ";
                    $sql .= "AND HOUR(FROM_UNIXTIME(created_time)) = '".(int) $i."') ";
                    
                    if(userdata('department') > 1) {
                        $sql .= ' AND agent_id="' .  userdata('id') . '" ';
                    }
                    
                    $sql .= "GROUP BY HOUR(FROM_UNIXTIME(created_time)) ";
                    $sql .= "ORDER BY FROM_UNIXTIME(created_time) ASC";
                    
                    $row = $this->db->query($sql)->row_array();

                    $sql1  = "SELECT COUNT(*) AS total FROM `tickets` ";
                    $sql1 .= "Where ticket_status=3 ";
                    $sql1 .= "AND (DATE(FROM_UNIXTIME(created_time)) = DATE(NOW()) ";
                    $sql1 .= "AND HOUR(FROM_UNIXTIME(created_time)) = '".(int) $i."') ";
                    
                    if(userdata('department') > 1) {
                        $sql1 .= ' AND agent_id="' .  userdata('id') . '" ';
                    }
                    
                    $sql1 .= "GROUP BY HOUR(FROM_UNIXTIME(created_time))";
                    $sql1 .= "ORDER BY FROM_UNIXTIME(created_time) ASC";
                            
                    $row1 = $this->db->query($sql1)->row_array();

                    if (!isset($row['total'])) {
                        $row['total'] = 0;
                    }
                    
                    if (!isset($row1['total'])) {
                        $row1['total'] = 0;
                    }

                    $data[] = array(
                        'time' => date(
                            'H',
                            mktime($i, 0, 0, date('n'), date('j'), date('Y'))
                         ),
                        'total' => $row['total'],
                        'closed' => $row1['total'],
                    );
                }
                break;
            default:
                break;
        }
        return $data;
    }
}
