<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class UserModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function existsEmail($email)
    {
        $email_count = $this->db->get_where('users', array('email' => $email))
            ->num_rows();

        return $email_count;
    }

    public function changeEmail()
    {
        $data = array('email' => $this->input->post('email', true));
        $condition = array('id' => userdata());

        return $this->db->update('users', $data, $condition);
    }

    public function resetPwd()
    {
        $this->db->set('password', md5($this->input->post('password', true)));
        $this->db->where('id', $this->input->post('user_id', true));
        $this->db->update('users');

        return true;
    }

    public function createUser()
    {
        $user_details = array(
            'name' => $this->input->post('name', true),
            'email' => $this->input->post('email', true),
            'telephone' => $this->input->post('telephone', true),
            'password' => md5($this->input->post('pass1', true)),
            'department' => $this->input->post('department', true),
            'details' => $this->input->post('details', true),
            'register_time' => strtotime(date('d F Y g:i a')),
            'ip_address' => $this->input->server('REMOTE_ADDR'),
            'status' => '1',
        );

        return $this->db->insert('users', $user_details);
    }

    public function updateUser()
    {
        if ($this->input->post('banned', true) == 1) {
            $status = 0;
        } else {
            $status = 1;
        }

        $user_details = array(
            'name' => $this->input->post('name', true),
            'details' => $this->input->post('details', true),
            'email' => $this->input->post('email', true),
            'telephone' => $this->input->post('telephone', true),
            'department' => $this->input->post('department', true),
            'status' => $status,
        );

        return $this->db->update(
            'users',
            $user_details,
            array('id' => $this->input->post('user_id', true))
        );
    }

    public function userData($username)
    {
        return $this->db->get_where(
            'users',
            array('email' => $username)
        )->row();
    }

    public function checkUserDetail()
    {
        $username = $this->input->post('email', true);
        $password = $this->input->post('password', true);

        $userdata = $this->userData($username);

        if ($userdata->email == $username
            && $userdata->password == md5($password)
            && $userdata->status == 1) {
            
            return true;
            
        } else {
            return false;
        }
    }

    public function createLostpwCode()
    {
        $email = $this->input->post('email', true);

        $lostpw_code = md5(microtime().'-'.rand(10, 10000).'-'.$email);

        $this->db->update(
            'users',
            array('lostpw' => $lostpw_code),
            array('email' => $this->input->post('email', true))
        );

        return $lostpw_code;
    }

    public function checkCode($email, $code)
    {
        return $this->db->get_where(
            'users',
            array('email' => $email, 'lostpw' => $code)
        )->num_rows();
    }

    public function createNewPassword($email)
    {
        $new_password = substr(
            strtoupper(md5(microtime().'-'.rand(100, 10000))),
            0,
            6
        );

        $is_updated = $this->db->update(
            'users',
            array('password' => md5($new_password), 'lostpw' => ''),
            array('email' => $email)
        );

        if ($is_updated) {
            return $new_password;
        }
    }

    public function checkPassword()
    {
        return $this->db->get_where(
            'users',
            array(
                'email' => userdata('email'),
                'password' => md5($this->input->post('currentpass', true))
            )
        )->num_rows();
    }

    public function passwordUpdate()
    {
        return $this->db->update(
            'users',
            array('password' => md5($this->input->post('pass1', true))),
            array('email' => userdata('email'))
        );
    }

    public function userList(
        $limit = 10,
        $offset = 0,
        $sort = 'users.name',
        $order = 'DESC',
        $q = ''
    ) {
        
        $arr_sort = array(
            'users.name',
            'users.email',
            'users.register_time',
            'users.status',
            'departments.department_name',
        );

        if (in_array($sort, $arr_sort)) {
            $this->db->order_by($sort, $order);
        }

        if ($q) {
            $this->db->like('name', $q);
        }

        $this->db->order_by('users.id', 'desc');
        
        $this->db->where(
            'users.department !=0 AND users.id !=',
            $this->session->userdata('user_id')
        ); // ID 14 : First Admin user. You can't delete.
        
        $this->db->select('users.id,users.status, users.name, users.email, '
            . 'users.department, users.register_time, '
            . 'departments.department_name');

        $this->db->join(
            'departments',
            'users.department = departments.id',
            'left'
        );

        return $this->db->get('users', $limit, $offset)->result();
    }

    public function totalUsers($q = false)
    {
        if ($q) {
            $this->db->like('name', $q);
        }

        $this->db->where(
            'users.department !=0 AND users.id !=',
            $this->session->userdata('user_id')
        ); // ID 14 : First Admin user. You can't delete.
        
        return $this->db->count_all_results('users');
    }

    public function departmentList()
    {
        return $this->db->get('departments')->result();
    }

    public function getUser($user_id)
    {
        return $this->db->get_where('users', array('id' => $user_id))->row();
    }

    public function checkUserId($user_id)
    {
        return $this->db->get_where('users', array('id' => $user_id))
            ->num_rows();
    }

    public function delete($user_id)
    {
        $deleted = $this->db->delete('users', array('id' => $user_id));

        if ($deleted) {  // Delete user
            $this->db->delete('tickets', array('user_id' => $user_id));
            $this->db->delete('messages', array('user_id' => $user_id));

            return true;
        }
    }

    public function getSettings()
    {
        return $this->db->get('settings')->result();
    }

    public function authSettings()
    {
        $data['authorize_test'] = $this->input->post('authorize_test', true);
        $data['auth_trans_key'] = $this->input->post('auth_trans_key', true);
        $data['auth_login_id'] = $this->input->post('auth_login_id', true);

        foreach ($data as $key => $value) {
            $this->db->delete('settings', array('name' => $key));
            $this->db->insert('settings',
                array('name' => $key, 'value' => $value));
        }

        return true;
    }

    public function updateMailTemplate()
    {
        $data = array(
            'new_ticket_subject' => 
                $this->input->post('new_ticket_subject', true),
            'new_ticket_template' => 
                $this->input->post('new_ticket_template', true),
            'ticket_status_change_subject' =>
                $this->input->post('ticket_status_change_subject', true),
            'ticket_status_change_template' =>
                $this->input->post('ticket_status_change_template', true),
        );

        foreach ($data as $key => $value) {
            $this->db->delete('settings', array('name' => $key));

            $this->db->insert(
                'settings',
                array('name' => $key, 'value' => $value)
            );
        }

        return true;
    }

    public function envatoSettings()
    {
        $data = array(
            'envato_app_id' => $this->input->post('envato_app_id', true),
            'envato_username' => $this->input->post('envato_username', true),
        );

        foreach ($data as $key => $value) {
            $this->db->delete('settings', array('name' => $key));

            $this->db->insert(
                'settings',
                array('name' => $key, 'value' => $value)
            );
        }

        return true;
    }

    public function paypalSettings()
    {
        $data['paypal_test'] = $this->input->post('paypal_test', true);
        $data['paypal_signature'] = $this->input->post('paypal_signature', true);
        $data['paypal_username'] = $this->input->post('paypal_username', true);
        $data['paypal_password'] = $this->input->post('paypal_password', true);

        foreach ($data as $key => $value) {
            $this->db->delete('settings', array('name' => $key));

            $this->db->insert(
                'settings',
                array('name' => $key, 'value' => $value)
            );
        }

        return true;
    }

    public function smtpSettings()
    {
        $data['smtp_protocol'] = $this->input->post('smtp_protocol', true);
        $data['smtp_host'] = $this->input->post('smtp_host', true);
        $data['smtp_user'] = $this->input->post('smtp_user', true);
        $data['smtp_pass'] = $this->input->post('smtp_pass', true);
        $data['smtp_port'] = $this->input->post('smtp_port', true);
        $data['smtp_timeout'] = $this->input->post('smtp_timeout', true);

        foreach ($data as $key => $value) {
            $this->db->delete('settings', array('name' => $key));
            
            $this->db->insert(
                'settings',
                array('name' => $key, 'value' => $value)
            );
        }

        return true;
    }

    public function chatSettings()
    {
        $data = array(
            'online_check_time' =>
                $this->input->post('online_check_time', true),
            'online_update_time' =>
                $this->input->post('online_update_time', true),
            'message_check_time' =>
                $this->input->post('message_check_time', true),
            'notification_check_time' =>
                $this->input->post('notification_check_time', true),
        );

        foreach ($data as $key => $value) {
            $this->db->delete('settings', array('name' => $key));
            $this->db->insert(
                'settings',
                array('name' => $key, 'value' => $value)
            );
        }

        return true;
    }

    public function uploadSettings()
    {
        $data = array(
            'allowed_extensions' =>
                $this->input->post('allowed_extensions', true),
            'max_upload_file_size' =>
               $this->input->post('max_upload_file_size', true),
            'max_upload_files' => $this->input->post('max_upload_files', true)
        );

        foreach ($data as $key => $value) {
            
            $this->db->delete('settings', array('name' => $key));

            $this->db->insert(
                'settings',
                array('name' => $key, 'value' => $value)
            );
        }

        return true;
    }

    public function generalSettings()
    {
        $data = array(
            'site_name' => $this->input->post('site_name', true),
            'site_email' => $this->input->post('site_email', true),
            'tickets_per_page' => $this->input->post('tickets_per_page', true),
            'language' => $this->input->post('language', true),
            'currency_sl' => $this->input->post('currency_sl', true),
            'currency_sr' => $this->input->post('currency_sr', true),
        );

        foreach ($data as $key => $value) {
            
            $this->db->delete('settings', array('name' => $key));

            $this->db->insert(
                'settings',
                array('name' => $key, 'value' => $value)
            );
        }

        return true;
    }
}
