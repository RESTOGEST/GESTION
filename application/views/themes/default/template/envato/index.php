<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-leaf"></i>
        <?= $this->lang->line('envato') ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="message_wrapper">
        <?php if($msg){ ?>
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $msg ?>
            </div>
        <?php } ?>
    </div>

    <?php echo form_open(
        'envato/varify',
        'id="envato_form" method="post"'
      );
    ?> 

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= $this->lang->line('varify_purchse_code') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label><?= $this->lang->line('buyer') ?></label>
                    <input type="text" class="form-control" name="buyer" />
                </div>
                <div class="form-group">
                    <label><?= $this->lang->line('purchase_code') ?></label>
                    <input type="text" class="form-control" name="code" />
                </div>
            </div>
            <div class="box-footer">
                <button type="button" class="btn-varify-envato btn btn-primary"><?= $this->lang->line('submit') ?></button>
            </div>
        </div>
    </form>
</section>

<?= Modules::run('footer/footer/index') ?>

<script>
    $(function(){
       $('.envato').addClass('active');
    });

    $('.btn-varify-envato').click(function(){
       $this = $(this);
       $this.attr('disabled','disabled');

       $.post('envato/varifyPost',$('#envato_form').serialize(), function(data){
           var data = JSON.parse(data);
           if(data.status){
               $html  = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
               $html += data.msg;
               $html += '</div>';
               $('.message_wrapper').html($html);
               $('#envato_form input').val('');
           }else{
               $html  = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
               $html += data.msg;
               $html += '</div>';
               $('.message_wrapper').html($html);
           }
           $this.removeAttr('disabled');
       });
    });
</script>
