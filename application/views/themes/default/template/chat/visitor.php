<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header)
<section class="content-header">
    <h1>
        <i class="fa fa-user"></i>
        <?php echo $this->lang->line('visitors'); ?>
    </h1>
</section>
-->
 
<!-- Main content -->
<section class="content">    
    <div class="col-lg-3">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= $this->lang->line('online_visitors') ?></h3>

                <div class="pull-right box-tools">                
                    <button data-original-title="<?= $this->lang->line('collapse') ?>" style="margin-right: 5px;" title="" data-toggle="tooltip" data-widget="collapse" class="btn bg-teal btn-sm"><i class="fa fa-minus"></i></button>
                    <button data-original-title="<?= $this->lang->line('close') ?>" data-toggle="tooltip" data-widget="remove" class="btn bg-teal btn-sm"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <p id="no_visitors" <?php if($visitors){ ?>style="display: none;"<?php } ?>>
                    <?= $this->lang->line('no_online_visitors_to_list') ?>
                </p>            

                <ul class="list-unstyled" id="tbl_visitors" <?php if(!$visitors){ ?>style="display: none;"<?php } ?>>
                        <?php foreach($visitors as $row) { ?>
                        <li>       
                            <a onclick="start_chat(<?= $row['id'] ?>);" rel="tooltip" data-toggle="tooltip"
                               data-html="true" data-title="
                                <ul class=list-unstyled>
                                <li>
                                    <b>Email:</b> <?= $row['email'] ?>
                                </li>
                                <li>
                                    <b>Telephone: </b><?= $row['telephone'] ?>
                                </li>
                                <?php if ($row['chat_response'] == 2) { ?>
                                <li>
                                    <b>Response</b>
                                    <i class='fa fa-thumbs-down'></i>                                    
                                </li>    
                                <?php } else if ($row['chat_response'] == 1) { ?>
                                <li>
                                    <b>Response</b>
                                    <i class='fa fa-thumbs-up'></i>
                                </li>    
                                <?php } ?>
                                <li>
                                    <b>Active chat:</b> <?= $row['total_active_chat'] ?>                                    
                                </li>
                                </ul>">
                                <?= $row['name'] ?>
                            </a>
                        </li>
                        <?php } ?>
                    </tbody>    
                </table>
            </div><!-- END .box -->
        </div>    
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= $this->lang->line('online_agents') ?></h3>            
                <div class="pull-right box-tools">                
                    <button data-original-title="<?= $this->lang->line('collapse') ?>" style="margin-right: 5px;" title="" data-toggle="tooltip" data-widget="collapse" class="btn bg-teal btn-sm"><i class="fa fa-minus"></i></button>
                    <button data-original-title="<?= $this->lang->line('close') ?>" data-toggle="tooltip" data-widget="remove" class="btn bg-teal btn-sm"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <p id='no_agents' <?php if($agents){ ?>style="display: none;"<?php } ?>>
                    <?= $this->lang->line('no_online_agents_to_list') ?>
                </p>

                <ul class="list-unstyled" id="tbl_agents" <?php if(!$agents){ ?>style="display: none;"<?php } ?>>
                    <?php foreach($agents as $row) { ?>
                    <li>
                        <a onclick="start_chat(<?= $row['id'] ?>);" rel="tooltip" data-toggle="tooltip"
                           data-html="true" data-title="
                            <ul class=list-unstyled>
                            <li>
                                <b>Email:</b> <?= $row['email'] ?>
                            </li>
                            <li>
                                <b>Telephone: </b><?= $row['telephone'] ?>
                            </li>
                            <?php if ($row['chat_response'] == 2) { ?>
                            <li>
                                <b>Response</b>
                                <i class='fa fa-thumbs-down'></i>                                    
                            </li>    
                            <?php } else if ($row['chat_response'] == 1) { ?>
                            <li>
                                <b>Response</b>
                                <i class='fa fa-thumbs-up'></i>
                            </li>    
                            <?php } ?>
                            <li>
                                <b>Active chat:</b> <?= $row['total_active_chat'] ?>                                    
                            </li>
                            </ul>">
                            <?= $row['name'] ?>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div><!-- END .box -->
        </div>
    </div>
    <div class="col-lg-9">
        <div id="live-chat-ui-wrapper" class="nav-tabs-custom">
            <ul class="nav nav-tabs ui-sortable-handle">
                <!--
                <li>
                    <a data-toggle='tab' href="#tabs-1">
                    </a>
                </li>
                -->
            </ul>
            <div class="tab-content no-padding">                    
                <!--
                <div id="tabs-1" class="tab-pane">
                </div>
                -->
            </div>
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<script>

function refresh_online_users(){
    $.get('chat/visitorAjax', function(data){
       var data = JSON.parse(data);

        if(data.visitors){
            $('#tbl_visitors').html(data.visitors);
            $('#tbl_visitors').show();        
            $('#no_visitors').hide();
            
            $('#tbl_visitors a').tooltip();
            
        }else{
            $('#tbl_visitors').hide();        
            $('#no_visitors').show();
        }
    
        if(data.agents){
           $('#tbl_agents').html(data.agents);   
           $('#tbl_agents').show();
           $('#no_agents').hide();
           
           $('#tbl_agents a').tooltip();
           
        }else{
            $('#tbl_agents').hide();        
            $('#no_agents').show();
        }
    });
}

setInterval(function(){
    refresh_online_users();
},<?= config('online_update_time') ?> + 200);


</script>

<script>
    $(function(){
        $('.visitor').addClass('active');
        $('.all_chats').addClass('active');
    });
</script>
