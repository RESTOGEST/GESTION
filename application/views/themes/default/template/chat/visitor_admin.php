<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-user"></i>
        <?php echo $this->lang->line('visitors'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?= $this->lang->line('online_visitors') ?></h3>
            
            <div class="pull-right box-tools">                
                <button data-original-title="<?= $this->lang->line('collapse') ?>" style="margin-right: 5px;" title="" data-toggle="tooltip" data-widget="collapse" class="btn bg-teal btn-sm"><i class="fa fa-minus"></i></button>
                <button data-original-title="<?= $this->lang->line('close') ?>" data-toggle="tooltip" data-widget="remove" class="btn bg-teal btn-sm"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            
            <p id="no_visitors" <?php if($visitors){ ?>style="display: none;"<?php } ?>>
                <?= $this->lang->line('no_online_visitors_to_list') ?>
            </p>            
                
            <table class="table-striped table" id="tbl_visitors" <?php if(!$visitors){ ?>style="display: none;"<?php } ?>>
                <thead>
                    <tr>
                        <th><?= $this->lang->line('visitor') ?></th>
                        <th><?= $this->lang->line('email') ?></th>
                        <th><?= $this->lang->line('telephone') ?></th>
                        <th><?= $this->lang->line('response') ?></th>
                        <th><?= $this->lang->line('total_active_chats') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($visitors as $row) { ?>
                    <tr>
                        <td>
                            <?= $row['name'] ?>
                        </td>
                        <td><?= $row['email'] ?></td>
                        <td><?= $row['telephone'] ?></td>                        
                        <td>
                            <?php if($row['chat_response'] == 2){ ?>
                                <i class="fa fa-thumbs-down"></i>
                            <?php }else if($row['chat_response'] == 1){ ?>
                                <i class="fa fa-thumbs-up"></i>
                            <?php }else{ ?>
                                -
                            <?php } ?>
                        </td>
                        <td><?= $row['total_active_chat'] ?></td>
                    </tr>
                    <?php } ?>
                </tbody>    
            </table>
        </div><!-- END .box -->
    </div>
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?= $this->lang->line('online_agents') ?></h3>            
            <div class="pull-right box-tools">                
                <button data-original-title="<?= $this->lang->line('collapse') ?>" style="margin-right: 5px;" title="" data-toggle="tooltip" data-widget="collapse" class="btn bg-teal btn-sm"><i class="fa fa-minus"></i></button>
                <button data-original-title="<?= $this->lang->line('close') ?>" data-toggle="tooltip" data-widget="remove" class="btn bg-teal btn-sm"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            
            <p id='no_agents' <?php if($agents){ ?>style="display: none;"<?php } ?>>
                <?= $this->lang->line('no_online_agents_to_list') ?>
            </p>
            
                
            <table class="table-striped table" id="tbl_agents" <?php if(!$agents){ ?>style="display: none;"<?php } ?>>
                <thead>
                    <tr>
                        <th><?= $this->lang->line('agent') ?></th>
                        <th><?= $this->lang->line('email') ?></th>
                        <th><?= $this->lang->line('telephone') ?></th>
                        <th><?= $this->lang->line('response') ?></th>
                        <th><?= $this->lang->line('total_active_chats') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($agents as $row) { ?>
                    <tr>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['email'] ?></td>
                        <td><?= $row['telephone'] ?></td>                        
                        <td>
                            <?php if($row['chat_response'] == 2){ ?>
                                <i class="fa fa-thumbs-down"></i>
                            <?php }else if($row['chat_response'] == 1){ ?>
                                <i class="fa fa-thumbs-up"></i>
                            <?php }else{ ?>
                                -
                            <?php } ?>
                        </td>
                        <td><?= $row['total_active_chat'] ?></td>
                    </tr>
                    <?php } ?>
                </tbody>    
            </table>
        </div><!-- END .box -->
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<link rel="stylesheet" type="text/css" href="application/views/themes/default/css/jquery-ui/smoothness/jquery-ui.css" />
<script src="application/views/themes/default/js/plugins/jQueryUI/jquery-ui-1.10.3.min.js"></script>

<script>
$('input[name="customer"]').autocomplete({
    source: function(request, response) {
        $.ajax({
            url: 'chat/customerAutocomplete/' +  encodeURIComponent(request.term),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.name,
                        value: item.id,
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        $('input[name="customer"]').val(ui.item.label);
        $('input[name="customer_id"]').val(ui.item.value);
        return false;
    },
    focus: function(event, ui) {
        return false;
    }
});

$('input[name="agent"]').autocomplete({
    source: function(request, response) {
        $.ajax({
            url: 'chat/agentAutocomplete/' +  encodeURIComponent(request.term),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.name,
                        value: item.id,
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        $('input[name="agent"]').val(ui.item.label);
        $('input[name="agent_id"]').val(ui.item.value);
        return false;
    },
    focus: function(event, ui) {
        return false;
    }
});

function refresh_online_users(){
    $.get('chat/visitorAdminAjax', function(data){
       var data = JSON.parse(data);

        if(data.visitors){
            $('#tbl_visitors tbody').html(data.visitors);
            $('#tbl_visitors').show();        
            $('#no_visitors').hide();
        }else{
            $('#tbl_visitors').hide();        
            $('#no_visitors').show();
        }
    
        if(data.agents){
           $('#tbl_agents tbody').html(data.agents);   
           $('#tbl_agents').show();
           $('#no_agents').hide();
        }else{
            $('#tbl_agents').hide();        
            $('#no_agents').show();
        }
    });
}

setInterval(function(){
    refresh_online_users();
},<?= config('online_update_time') ?> + 200);

</script>

<script>
    $(function(){
        $('.visitor').addClass('active');
        $('.all_chats').addClass('active');
    });
</script>
