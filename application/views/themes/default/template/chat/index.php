<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-clock-o"></i>
        <?php echo $this->lang->line('chat_history'); ?>
        
        <?php if($rating == 1){ ?>
            <div class="feedback_wrapper pull-right"> Feedback &nbsp;:&nbsp; <i class="fa fa-thumbs-up"></i> &nbsp; &nbsp; &nbsp; </div>
        <?php }else if($rating == 2){ ?>
            <div class="feedback_wrapper pull-right"> Feedback &nbsp;:&nbsp; <i class="fa fa-thumbs-down"></i> &nbsp; &nbsp; &nbsp; </div>
        <?php } ?>
        
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Filter</h3>
        </div>
        <div class="box-body">
            
            <?php echo form_open(
                    'chat/index',
                    'method="get" class="form form-inline" id="chat_filter_form"'
                  );
            ?>
                
            <?php if(userdata('department') == 1) { ?>
            
                <div class="form-group">
                    <label>Agent:&nbsp;</label>
                    <input type="text" id="agent" class="form-control" value="<?= $agent ?>" name="agent" placeholder="<?= $this->lang->line('agent') ?>" />
                    <input type="hidden" name="agent_id" value="<?= $agent_id ?>"  />
                </div>
            
            <?php } ?>
            
                &nbsp;&nbsp;&nbsp;&nbsp;
                
                <div class="form-group">
                    <label>Customer: &nbsp;</label>
                    <input type="text" id="customer" class="form-control" value="<?= $customer ?>" name="customer" placeholder="<?= $this->lang->line('customer') ?>" />
                    <input type="hidden" name="customer_id" value="<?= $customer_id ?>"  />
                </div>
               
                &nbsp;&nbsp;
                
                <div class="form-group">
                <button class="btn btn-primary">
                    Filter &nbsp; <i class="fa fa-filter"></i>
                </button>
                </div>
            </form>
        </div>
    </div>
    
    
    <div class="box">
        <div class="box-body chat" id="chat-box">

            <table class="table-striped table-hover table">
                <thead>
                    <tr>
                        <td>From</td>
                        <td>To</td>
                        <td>Department</td>
                        <td>Response</td>
                        <td>
                            <span class="pull-right">Timestamp</span>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($rows as $row) { ?>
                    <tr style="cursor: pointer;" onclick="location = '<?= $row["action"] ?>';">
                        <td>
                            <div class="item" style="margin-bottom: 0">
                                <?php if($row['from_department'] == 0){ ?>
                                    <img class="online" alt="user image" src="images/customer-user.png" />
                                <?php }else{ ?>
                                    <img class="online" alt="user image" src="images/support-user.png" />
                                <?php } ?>
                                <p class="message">
                                    <a class="name"> 
                                       <?= $row['from_name'] ?>
                                    </a>
                                    <?= $row['msg'] ?>
                                </p>
                            </div>
                        </td>
                        <td>
                            <div class="item" style="margin-bottom: 0">
                                <?php if($row['to_department'] == 0){ ?>
                                    <img class="online" alt="user image" src="images/customer-user.png" />
                                <?php }else{ ?>
                                    <img class="online" alt="user image" src="images/support-user.png" />
                                <?php } ?>
                                <p class="message">
                                    <a class="name">
                                       <?= $row['to_name'] ?>
                                    </a>
                                </p>
                            </div>
                        </td>
                        <td><?= $row['department'] ?></td>
                        <td>
                            <?php if($row['chat_response'] == 2){ ?>
                                <i class="fa fa-thumbs-down"></i>
                            <?php }else if($row['chat_response'] == 1){ ?>
                                <i class="fa fa-thumbs-up"></i>
                            <?php }else{ ?>
                                -
                            <?php } ?>
                        </td>
                        <td>
                            <small class="text-muted pull-right">
                                <i class="fa fa-clock-o"></i>
                                <?= date('d F Y, g:i a', strtotime($row['timestamp'])) ?>
                            </small>
                        </td>                    
                    </tr>
                    <?php } ?>
                </tbody>    
            </table>

            <div class="pagination">
                <ul class="pagination">
                    <?php echo $links; ?>
                </ul>
            </div>

        </div><!-- END .box -->
    </section>

    <?= Modules::run('footer/footer/index') ?>

<link rel="stylesheet" type="text/css" href="application/views/themes/default/css/jquery-ui/smoothness/jquery-ui.css" />
<script src="application/views/themes/default/js/plugins/jQueryUI/jquery-ui-1.10.3.min.js"></script>

<script>
$('input[name="customer"]').autocomplete({
    source: function(request, response) {
        $.ajax({
            url: 'chat/customerAutocomplete/' +  encodeURIComponent(request.term),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.name,
                        value: item.id,
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        $('input[name="customer"]').val(ui.item.label);
        $('input[name="customer_id"]').val(ui.item.value);
        return false;
    },
    focus: function(event, ui) {
        return false;
    }
});

$('input[name="agent"]').autocomplete({
    source: function(request, response) {
        $.ajax({
            url: 'chat/agentAutocomplete/' +  encodeURIComponent(request.term),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.name,
                        value: item.id,
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        $('input[name="agent"]').val(ui.item.label);
        $('input[name="agent_id"]').val(ui.item.value);
        return false;
    },
    focus: function(event, ui) {
        return false;
    }
});
</script>

<script>
    $(function(){
        $('.chat_history').addClass('active');
        $('.all_chats').addClass('active');
    });
</script>
