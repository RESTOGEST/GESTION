<?= Modules::run('header/header/index') ?>

<section class="content-header">
    <h1>
        Chat Details
    </h1>    
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-6">

            <div class="box">
                <div class="box-body">
                    <?php 
                    foreach($rows as $row){ 
                        if($row['from_department'] == 0){
                    ?>

                        <!-- Message. Default to the left -->
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left"><?php echo $row['from_name']; ?></span>
                                <span class="direct-chat-timestamp pull-right"><?php echo date('d F Y g:i a', strtotime($row['timestamp'])); ?></span>
                            </div><!-- /.direct-chat-info -->
                            <img alt="message user image" src="<?php echo base_url(); ?>images/customer-user.png" class="direct-chat-img"><!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                                <?php echo $row['msg']; ?>
                            </div><!-- /.direct-chat-text -->
                        </div><!-- /.direct-chat-msg -->

                        <?php 
                        }else{
                        ?>

                        <!-- Message to the right -->
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right"><?php echo $row['from_name']; ?></span>
                                <span class="direct-chat-timestamp pull-left"><?php echo date('d F Y g:i a', strtotime($row['timestamp'])); ?></span>
                            </div><!-- /.direct-chat-info -->
                            <img alt="message user image" src="<?php echo base_url(); ?>images/support-user.png" class="direct-chat-img"><!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                                <?php echo $row['msg']; ?>
                            </div><!-- /.direct-chat-text -->
                        </div><!-- /.direct-chat-msg -->        
                    <?php   
                        }
                    }
                    ?>
                        
                        
                    <div class="clearfix"></div>

                    <ul style="padding-left: 11px;" class="pagination">
                        <?= $links ?>
                    </ul>

                </div>                
            </div>
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>