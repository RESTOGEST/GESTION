<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-users"></i>
        <?php echo $this->lang->line('customers'); ?>
        
        <?php if(userdata('department') == 1){ ?>
        <div class="pull-right">
            <a href="<?php echo base_url('customer/create'); ?>" class="btn btn-primary">
              <i class="fa fa-plus"></i>
                <?php echo $this->lang->line('add_new_customer'); ?>
            </a>
        </div>
        <?php } ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('customers'); ?></h3>        
            <div class="pull-right box-tools">                
                <button class="btn bg-teal btn-sm" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>    
        <div class="box-body table-responsive">

                <?php echo form_open(
                    'customer/customerList',
                    'class="form form-inline ticket_filter_form"'
                  );
                ?>

                <input type="hidden" name="sort" value="<?= $sort ?>" />
                <input type="hidden" name="order" value="<?= $order ?>" />
                
                <div class="form-group">
                    <input placeholder="<?= $this->lang->line('enter_customer_name')  ?>" value="<?= $q ?>" name="q" class="form-control" />
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-search"></i>
                        <?= $this->lang->line('search')  ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
    
    <div class="box">
        <div class="box-body table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <th width="15%" class="sorting">
                        <?php if ($sort == 'name') { ?>
                            <a class="<?php echo strtolower($order); ?>" href="<?= $sort_name ?>">
                                <?php echo $this->lang->line('name'); ?>
                            </a>
                        <?php }else{ ?>
                            <a href="<?= $sort_name ?>">
                                <?php echo $this->lang->line('name'); ?>
                            </a>
                        <?php } ?>
                    </th>
                    <th class="sorting" width="15%">
                        <?php if ($sort == 'email') { ?>
                            <a class="<?php echo strtolower($order); ?>" href="<?= $sort_email ?>">
                                <?php echo $this->lang->line('email'); ?>
                            </a>
                        <?php }else{ ?>
                            <a href="<?= $sort_email ?>">
                                <?php echo $this->lang->line('email'); ?>
                            </a>
                        <?php } ?>                            
                    </th>
                    <th class="sorting" width="18%">
                        <?php if ($sort == 'register_time') { ?>
                            <a class="<?php echo strtolower($order); ?>" href="<?= $sort_register_time ?>">
                                <?php echo $this->lang->line('register_time'); ?>
                            </a>
                        <?php }else{ ?>
                            <a href="<?= $sort_register_time ?>">
                                <?php echo $this->lang->line('register_time'); ?>
                            </a>
                        <?php } ?>                               
                    </th>
                    <th class="sorting" width="10%">
                        <?php if ($sort == 'status') { ?>
                            <a class="<?php echo strtolower($order); ?>" href="<?= $sort_status ?>">
                                <?php echo $this->lang->line('status'); ?>
                            </a>
                        <?php }else{ ?>
                            <a href="<?= $sort_status ?>">
                                <?php echo $this->lang->line('status'); ?>
                            </a>
                        <?php } ?>                                                           
                    </th>
                    <th style="text-align: right;" width="24%"><?php echo $this->lang->line('options'); ?></th>
                </thead>
                <tbody>
                <?php if( ! empty($users) ){?>
                <?php foreach( $users as $user){ ?>
                 <tr id="user_id_<?php echo $user->id; ?>">
                        <td><?php echo $user->name; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td><?php echo date('d F Y g:i a',$user->register_time); ?></td>
                        <td>
                            <?php if($user->status){ ?>
                                <label class="label label-info">Active</label>
                            <?php }else{ ?>
                                <label class="label label-danger">Banned</label>
                            <?php } ?>
                        </td>
                        <td align="right">
                            
                            <?php if(userdata('department') == 1){ ?>
                            <a class="btn bg-gray btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('login'); ?>" href="<?php echo base_url('user/loginAs/'.$user->id); ?>">
                                <i class="fa fa-lock"></i>
                            </a>&nbsp;&nbsp;
                            
                            <a class="btn bg-blue btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('password'); ?>"   href="<?php echo base_url('customer/resetPwd/'.$user->id); ?>">
                                <i class="fa fa-key"></i>
                            </a>&nbsp;&nbsp;
                            
                            <?php } ?>
                            
                            <a class="btn bg-orange btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('details'); ?>"   href="<?php echo base_url('customer/update/'.$user->id); ?>">
                                <i class="fa fa-file-text-o"></i>
                            </a>&nbsp;&nbsp;
                            
                            <?php if(userdata('department') == 1){ ?>
                            <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('delete'); ?>"   href="javascript:void(0)" onclick="delete_user(<?php echo $user->id; ?>)">
                                <i class="fa fa-trash"></i>
                            </a>
                            <?php } ?>
                        </td>
                 </tr>
                <?php } ?>
                <?php } else { ?> <tr><td colspan="5"><h2><?php echo $this->lang->line('any_customer'); ?></h2></td></tr> <?php } ?>
                </tbody>
            </table>
            <div class="pagination">
                <ul class="pagination">
                    <?php echo $links; ?>
                </ul>
            </div>
        </div>
    </div><!-- END .box -->
</section>

<?= Modules::run('footer/footer/index') ?>

<script>

$(function(){
   $('.customer_list').addClass('active');
});

function delete_user( user_id ){
    //return confirm('Are you sure?');
    $.ajax({
        type: "GET",
        url: "<?php echo base_url('user/delete' ); ?>/" + user_id,
        success: function(msg){
            $('#user_id_' + user_id).fadeOut('normal');
        }
    });
 }
 </script>
