
<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('update_customer'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('update_customer'); ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">

            <div id="ajax"></div>

            <?php echo form_open(
                    '',
                    'id="updateform"'
                  );
                ?>

                <input type="hidden" name="user_id" value="<?php echo $user->id; ?>" />

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('name_lastname'); ?></label>
                    <input type="text" name="name" maxlength="50" value="<?php echo $user->name; ?>" class="form-control" />
                </div>

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('email'); ?></label>
                    <input type="text" name="email" value="<?php echo $user->email; ?>"  maxlength="50" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('telephone'); ?></label>
                    <input type="text" name="telephone" value="<?php echo $user->telephone; ?>"  maxlength="50" class="form-control" />
                </div>
                
                <input type="hidden" name="department" value="0" />                        

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('details'); ?></label>
                    <textarea name="details" class="form-control"><?= $user->details ?></textarea>
                </div>
                
                <div class="checkbox">
                  <label>
                    <input name="banned" <?php if ($user->status == 0) echo 'checked="checked"'; ?> value="1" type="checkbox"><?php echo $this->lang->line('banned'); ?> ? 
                  </label>
                </div>

                <div id="submitbutton" class="box-footer">
                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('update_customer'); ?>" onclick="update_user()" class="btn btn-primary" />
                </div>	
            </form>
        </div>
    </div><!-- END .box -->
</section>

<?= Modules::run('footer/footer/index') ?>

<script>
    $(function(){
       $('.customer_list').addClass('active'); 
    });
    
    function update_user(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('customer/updateProcess'); ?>",
            data: $("#updateform").serialize(),
            beforeSend: function(msg) {
                $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />');
            },
            success: function(msg){
                
                data = JSON.parse(msg);
                
                $('body,html').animate({scrollTop: 0}, 200);
                $("#ajax").html(data.html);
                $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('update_user'); ?>" onclick="update_user()" class="btn btn-primary" />');
            }
        });
    }
</script>





