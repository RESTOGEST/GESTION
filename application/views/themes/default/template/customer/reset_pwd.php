<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('reset_pwd'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div id="ajax"><?php
        if ($this->session->flashdata('message')) {
            echo $this->session->flashdata('message');
        }
        ?></div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('reset_pwd'); ?></h3>
        </div><!-- /.box-header -->

        <div class="box-body table-responsive">

            <div id="ajax"></div>

            <?php echo form_open(
                '',
                'id="createform"'
              );
            ?>
                
                <input  type="hidden" name="user_id" value="<?= $user_id ?>" />
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('password'); ?></label>
                    <input type="password" name="password" maxlength="50" value="" class="form-control" />
                </div>

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('confirm_password'); ?></label>
                    <input type="password" name="confirm_password" maxlength="50" value="" class="form-control" />
                </div>
           
             <div id="submitbutton" class="box-footer">
                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('submit'); ?>" onclick="reset_pwd()" class="btn btn-primary" />
                </div>	
            </form>
        </div>
    </div><!-- END .box -->
</section>

<?= Modules::run('footer/footer/index') ?>

<script>
    $(function(){
       $('.customer_list').addClass('active'); 
    });
    
    function reset_pwd()
    {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('user/resetPwdProcess'); ?>",
            data: $("#createform").serialize(),
            beforeSend: function(msg) {
                $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />');
            },
            success: function(msg){
                
                var data = JSON.parse(msg);
                
                $('body,html').animate({scrollTop: 0}, 200);
                $("#ajax").html(data.msg);                
                $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('submit'); ?>" onclick="reset_pwd()" class="btn btn-primary" />');
                
                $('#createform')[0].reset();
            }
        });
    }
</script>




