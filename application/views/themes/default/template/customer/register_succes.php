<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('open_tickets'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <?php echo $this->lang->line('register_succes_msg1'); ?>
            </h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <p class="text"><?php echo vlang('register_succes_msg2',array($name)); ?> </p>
            <br>
            <input type="button" class="btn btn-primary" onclick="location.href='<?php echo base_url('tickets/create'); ?>'" value="<?php echo $this->lang->line('create_a_new_ticket'); ?>" />
        </div><!-- END .box-body -->
    </div><!-- END .box -->
</section><!-- END .section -->

<?= Modules::run('footer/footer/index') ?>