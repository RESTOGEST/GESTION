<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $this->lang->line('add_notice') ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?php if($msg){ ?>
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= $msg ?>
        </div>
    <?php } ?>

    <?php echo form_open(
        'notice/insert',
        'method="post"'
      );
    ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= $this->lang->line('add_notice') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label><?= $this->lang->line('user_dep') ?></label>
                    <select class="form-control" id="department" name="department_id">
                        <option value="0">Customer</option>
                        <option value="1">Team Leader/Manager/Supervisor</option>
                        <?php
                        if (!empty($departments)) {
                            foreach ($departments as $department) {
                                echo '<option value="' . $department['id'] . '">' . $department['department_name'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group" id="user_wrapper">
                    <label>
                        <?= $this->lang->line('user') ?>
                        <small style="color:#777777">(Leaving blank will display notice to all users in defined department)</small>
                        &nbsp;<span style="color:red;" class="user_error"></span>
                    </label>
                    
                    <input placeholder="<?= $this->lang->line('search_user') ?>" type="text" class="form-control" name="user" value="<?= set_value('user') ?>" />
                    
                    <table id="user_list" class="table table-striped">
                        <?php foreach($users as $user){ ?>
                        <tr>
                            <td>
                                <input type="hidden" name="user_ids[]" value="<?= $user['id'] ?>" />
                                <?= $user['name'] ?>
                            </td>
                            <td>
                                <button type="button" class="remove btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                    
                    
                </div>

                <div class="form-group">
                    <label><?= $this->lang->line('content') ?></label>
                    <textarea class="form-control" name="content"><?= set_value('content') ?></textarea>
                </div>

                <div class="form-group">
                    <label><?= $this->lang->line('sort_order') ?></label>
                    <input type="text" class="form-control" value="<?= set_value('sort_order') ?>" name="sort_order" />
                </div>
            </div>
            <div class="box-footer">
                <button type="sumit" class="btn btn-primary"><?= $this->lang->line('submit') ?></button>
            </div>
        </div>
    </form>
</section>

<?= Modules::run('footer/footer/index') ?>

<script src="application/views/themes/default/js/plugins/jQueryUI/jquery-ui-1.10.3.min.js"></script>
<link rel="stylesheet" href="application/views/themes/default/css/jquery-ui/smoothness/jquery.ui.theme.css" />
<link rel="stylesheet" href="application/views/themes/default/css/jquery-ui/smoothness/jquery-ui.css" />

<script>

$(function(){
   $('.notice_list').addClass('active');
});

//autocomplete job
$("input[name='user']").autocomplete({
    source: function(request, response) {
            $.ajax({
                url: '<?= base_url() ?>notice/userAutocomplete/' +  encodeURIComponent(request.term)+'/'+ encodeURIComponent($('select[name="department_id"]').val()),
                dataType: 'json',
                success: function(json) {
                    if(json.length != 0){
                        $('.user_error').html('');
                        response($.map(json, function(item) {
                            return {
                                label: item.name,
                                value: item.id,
                            }
                        }));
                    }else{
                        $('.user_error').html('<?= $this->lang->line("no_user_found") ?>');
                    }
                }
            });
    },
    select: function(event, ui) {
           
            $html  = '<tr>';
            $html += '  <td>';
            $html += '    <input type="hidden" name="user_ids[]" value="'+ui.item.value+'" />';
            $html +=      ui.item.label;
            $html += '  </td>'; 
            $html += '  <td>'; 
            $html += '    <button type="button" class="remove btn btn-sm btn-danger">';
            $html += '      <i class="fa fa-trash"></i>';
            $html += '    </button>';
            $html += '  </td>';
            $html += '</tr>';
                            
            $('#user_list').append($html);
            
            return false;
    },
    focus: function(event, ui) {
            return false;
    }
});

</script>
