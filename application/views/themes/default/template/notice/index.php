<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      <i class="fa fa-exclamation-triangle"></i>
        <?= $this->lang->line('notice') ?>
        <?php if(userdata('department')==1){ ?>
        <div class="box-tools pull-right">
            <a class="btn btn-primary" href="<?= site_url('notice/insert') ?>">
                <i class="fa fa-plus"></i> <?= $this->lang->line('add_new') ?>
            </a>
        </div>
        <?php } ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?= $msg ?>

    <div class="box">
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="10%"><?= $this->lang->line('department') ?></th>
                        <th width="10%" align="left"><?= $this->lang->line('user') ?></th>
                        <th width="65%" align="left"><?= $this->lang->line('content') ?></th>
                        <th style="text-align: right;" width="15%"><?= $this->lang->line('options') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($rows as $row){ ?>
                        <tr id="item_<?= $row['notice_id'] ?>">
                            <td align="left">
                                <?php if($row['department_id']==0) echo $this->lang->line('customer') ?>
                                <?php if($row['department_id']==1) echo $this->lang->line('team_leader') ?>
                                <?= $row['department'] ?>

                            </td>
                            <td align="left">
                                <?php 
                                if($row['users']){ 
                                    foreach($row['users'] as $user){
                                        echo '<span class="badge">'.$user['name'].'</span>';
                                    }                                    
                                } ?>
                            </td>
                            <td align="left"><?= $row['content'] ?></td>
                            <td align="right">
                                
                                <a class="btn bg-orange btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('update'); ?>" href="notice/edit/<?= $row['notice_id'] ?>">
                                    <i class="fa fa-pencil"></i> 
                                </a>&nbsp;&nbsp;
                                
                                <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('delete'); ?>"   onclick="delete_department(<?= $row['notice_id'] ?>)" href="javascript:void(0)">
                                    <i class="fa fa-trash"></i>
                                </a>
                                
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <div class="pagination_wrapper">
                <ul class="pagination pull-right">
                    <?php echo $links; ?>
                </ul>
            </div>
            
            <div class="clearfix"></div>
            
        </div>
    </div><!-- END .box -->
</section>

<?= Modules::run('footer/footer/index') ?>

<script>

$(function(){
   $('.notice_list').addClass('active');
});

function delete_department(id){
    if(confirm('Are you sure?')){
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('notice/delete' ); ?>/" + id,
            success: function(msg)
            {
               $('#item_' + id).fadeOut('normal');
            }
        });
    }
}

</script>
