<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-users"></i>
        <?php echo $this->lang->line('departments'); ?>
        <div class="pull-right">
            <a href="<?php echo base_url('departments/create'); ?>" class="btn btn-primary">
              <i class="fa fa-plus"></i>
                <?php echo $this->lang->line('create_a_new_department'); ?>
            </a>
        </div>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?php echo $this->session->flashdata('message'); ?>

    <div class="box">
        <div class="box-body table-responsive">
            <table class="table table-hover">
                <thead>
		<thead>
                    <tr>
                        <th width="10%">ID</th>
                        <th width="70%" align="left"><?php echo $this->lang->line('department_name'); ?></th>
                        <th style="text-align: right;" width="20%"><?php echo $this->lang->line('options'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($departments)) { ?>
                        <?php foreach ($departments as $department) { ?>
                            <tr id="department_id_<?php echo $department->id; ?>">
                                <td><?php echo $department->id; ?></td>
                                <td align="left"><?php echo $department->department_name; ?></td>
                                <td align="right">
                                    <a class="btn bg-orange btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('update'); ?>" href="<?php echo base_url('departments/update/' . $department->id); ?>">
                                        <i class="fa fa-pencil"></i> 
                                    </a>&nbsp;&nbsp;
                                    <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('delete'); ?>"   href="javascript:void(0)" onclick="delete_department(<?php echo $department->id; ?>)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<script>

$(function(){
    $('.departments').addClass('active');
});

function delete_department(department_id){

    //return confirm('Are you sure?');

    $.ajax({
        type: "GET",
        url: "<?php echo base_url('departments/delete' ); ?>/" + department_id,
        success: function(result){            
            var data = JSON.parse(result);            
	    if(data.msg == 'deleted' ){
                $('#department_id_' + department_id).fadeOut('normal');
            }
        }
    });
 }
 </script>
