<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('create_a_new_department'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <div id="ajax">            
        <?php echo $this->session->flashdata('message'); ?>
    </div>    
    
    <div class="box">
        <div class="box-body table-responsive">
            

            <?php echo form_open(
                '',
                'id="createform"'
              );
            ?>
        
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('department_name'); ?></label>
                    <input type="text" name="name" maxlength="50" value=""  class="form-control" />
                </div>
                <div class="box-footer">
                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('create_a_new_department'); ?>" onclick="create_department()" class="btn btn-primary" />
                </div>	
            </form>
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<script>
    
$(function(){
    $('.departments').addClass('active');
}); 

function create_department(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('departments/createProcess'); ?>",
        data: $("#createform").serialize(),
        beforeSend : function(msg){ 
            $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); 
        },
        success: function(msg){
            
            var data = JSON.parse(msg);
            
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(data.msg); 
            $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('create_a_new_department'); ?>" onclick="create_department()" class="stbutton"/>');            
        }
    });
 }
 </script>

       