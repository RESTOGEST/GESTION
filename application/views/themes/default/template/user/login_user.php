<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?= $this->lang->line('log_in') ?> | <?= $site_name ?></title>
    <base href="<?= base_url() ?>">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Custome -->
    <link href="application/views/themes/default/css/custome.css?v=1" rel="stylesheet" type="text/css" />
    <!-- Bootstrap 3.3.2 -->
    <link href="application/views/themes/default/css/bootstrap.min.css?v=1" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css?v=1" rel="stylesheet" type="text/css" />
    <link href="application/views/themes/default/css/A1support.min.css?v=1" rel="stylesheet" type="text/css" />
    <!-- Custome -->
    <link href="application/views/themes/default/css/custome.css?v=1" rel="stylesheet" type="text/css" />        
    <!-- Chat -->
    <link href="application/views/themes/default/css/chat_customer.css?v=1" rel="stylesheet" type="text/css" />
        
    <link rel="icon" type="image/png" href="images/favicon.png"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a>
           <img src="<?php echo base_url(); ?>images/logo.png" alt="Logo" title="Krafty Support"/>
        </a>
      </div><!-- /.login-logo -->

      <div id="ajax"><?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?></div>

      <div class="login-box-body">
        <p class="login-box-msg"><?= $this->lang->line('login_to_support_desk') ?></p>

        <?php echo form_open(
                'users/login',
                'id="loginform" method="post"'
              );
        ?>

          <div class="form-group has-feedback">
             <input type="text" class="form-control" name="email" placeholder="<?php echo $this->lang->line('email'); ?>"/>
             <span style="z-index: 0;" class="fa fa-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
             <input type="password" class="form-control" name="password" placeholder="<?php echo $this->lang->line('password'); ?>"/>
             <span style="z-index: 0;" class="fa fa-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="button" onclick="login_user()" class="btn btn-primary btn-block btn-lg"><?= $this->lang->line('login') ?></button>
            </div><!-- /.col -->
          </div>
          <hr />
          <div class="row">
              <div class="col-xs-6">
                <a href="<?= site_url('user/lostpassword') ?>">
                    <?= $this->lang->line('i_forgot_my_password') ?>
                </a>
              </div>
              <div class="col-xs-6 text-right">
                <a href="<?= site_url('user/create') ?>">
                    <?= $this->lang->line('create_account') ?>
                </a>
              </div><!-- /.col -->
          </div>
          <hr />
          <center><h4>OR</h4></center>
          <hr />
          <div class="row">
              <div class="col-xs-6">
                <a class="btn btn-block btn-success btn-lg" href="<?= site_url('faq') ?>">
                    <i class="fa fa-question" style="padding: 0 !important;"></i> &nbsp;
                    <?= $this->lang->line('browse_faq') ?>
                </a>
              </div>
              <div class="col-xs-6">
                <a class="btn-modal btn btn-block btn-warning btn-lg" data-toggle="modal" href="#modal-start-chat">
                    <i class="fa fa-comments-o" style="padding: 0 !important;"></i> &nbsp;
                    <?= $this->lang->line('start_chat') ?>
                </a>
              </div>
          </div>
        </form>
      </div><!-- /.login-box-body -->
      <hr />
      <center>&copy; Copyright <?php echo date("Y"); ?> - <strong>A1 Support Desk v<?= VERSION ?></strong></center>
    </div><!-- /.login-box -->
    
    <div id="live-chat-ui-wrapper"></div>

    <div id="modal_agent_offline_alert" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a data-dismiss="modal" class="close">×</a>
                    <h4 class="modal-title"><?= $this->lang->line('agent_offline_title') ?></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <?= $this->lang->line('agent_offline_body') ?>
                    </p>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn_agent_offline_alert_yes btn-primary" value="Yes">
                    <a onclick="$('#modal_agent_offline_alert').remove();" class="btn-remove-model btn btn-default"><?= $this->lang->line('cancel') ?></a>
                </div>
            </div>
        </div>
    </div>

    <!-- start chat model -->
    <div id="modal-start-chat" class="modal fade in" aria-hidden="false">
        <div class="login-box modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><?= $this->lang->line('start_chat') ?></h4>
                </div>
                <div class="modal-body">

                    <?php echo form_open(
                            '',
                            'id="chat_form"'
                          );
                    ?>

                        <div class="form-group required">
                            <input type="text" required="" class="form-control" placeholder="Name" value="" name="name" />
                        </div>
                        <div class="form-group required">
                            <input type="text" class="form-control" placeholder="Email" name="email" />
                        </div>

                        <input type="button" value="Start" id="btn-start-chat-submit" class="btn btn-block btn-warning" />
                    </form>
                    
                    <div class="clearfix"></div>

                    <p></p>
                    
                    <style>
                    .modal-dialog{
                        width:360px;
                        margin-top: 100px;
                    }
                    </style></div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <!-- jQuery 2.1.3 -->
    <script src="application/views/themes/default/js/plugins/jQuery/jQuery-2.1.3.min.js?v=1"></script>

    <!-- Bootstrap 3.3.2 JS -->
    <script src="application/views/themes/default/js/bootstrap.min.js?v=1" type="text/javascript"></script>

  </body>
</html>

<script src="application/views/themes/default/js/moment.min.js?v=1" type="text/javascript"></script>
<script src="application/views/themes/default/js/livestamp.js?v=1" type="text/javascript"></script>    

<!-- Chat -->
<script src="application/views/themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js?v=1"></script>
<script src="application/views/themes/default/js/plugins/slimscroll/jquery.slimscroll.init.js?v=1"></script>

<?php if(!$timezone) { ?>   
<script>    
$(document).ready(function() {
    var time = new Date();
    $.ajax({
        type: "GET",
        url: "chat/timezone",
        data: 'value='+ parseInt(time.getTimezoneOffset()),
        success: function(){
            location.reload();
        }
    });
});        
</script>
<?php } ?>

<!-- language variables -->
<script>
    $play_sound = "<?= userdata('play_sound') ?>";
    $chat_error_msg = "<?= $this->lang->line('chat_error_msg') ?>";
    $no_agent_found = "<?= $this->lang->line('no_agent_found') ?>";
    $message_check_time = <?php echo config('message_check_time'); ?>;
    $online_update_time = <?php echo config('online_update_time'); ?>;
    $notification_check_time = <?php echo config('notification_check_time'); ?>;
</script>


<script src="application/views/themes/default/js/chat.js?v=1"></script>

<script type="text/javascript">

        $chat_status = 0;
        
        setInterval(function(){
            $.get('chat/iAmOnline/'+$('.live-chat-ui').length);
        }, $online_update_time);

        function check_notification(){
            $.get('chat/checkNotification',function(data){
                var data = JSON.parse(data);

                if(data.status==1){
                    $old_count = $('.chat-notification .chat-count').html();

                    if($play_sound && data.count > 0 && $old_count != data.count){ //alert only first time
                        $('#chatAudio')[0].play();
                    }

                    $('#chat-notification-list').html(data.html);
                    $('.chat-notification .chat-count').html(data.count);

                    if(data.count > 0){
                        $('.chat-notification a').addClass('noti_active');
                    }else{
                        $('.chat-notification a').removeClass('noti_active');
                    }

                    //init_scroll();
                    popup_notification(data.rows);
                }
            });
        }

        function start_chat($receiver_id) {
            $.get('chat/startChat/'+$receiver_id, function(data) {
                var data = JSON.parse(data);
                if ($('#live-chat-ui-' + $receiver_id).length == 0) {
                    $('#live-chat-ui-wrapper').append(data.html);
                    init_scroll();
                    reposition_chat();
                } else {
                    $('#live-chat-ui-' + $receiver_id + ' input').trigger('focus');
                }

                //tooltip
                $("[data-toggle='tooltip']").tooltip();
                $('#customer_start_chat').hide();
            });
        }
        
        //Chat close 
        $(function() {
            $(document).delegate('.chat-close','click', function(e) {
                e.preventDefault();
                $(this).parent().parent().fadeOut(300).remove();
                reposition_chat();

                $receiver_id = $(this).parent().parent().find('input[name="receiver_id"]').val();                    

                $.get('chat/leaveChatroom/'+$receiver_id);//leave chatroom
                $.post('chat/setNotified', $(this).parent().parent().find('form').serialize());

                return false;
            });
        });
        
        $('.btn-modal').click(function(){
            $('#modal-start-chat input[type="text"]').val('');
        });
        
        //chat response
        $(document).delegate('.like_unlike_wrapper div','click', function(){
            $this = $(this);
            $response = $(this).attr('data-response');
            $agent = $(this).attr('data-agent'); 
            $.get('chat/response/'+$response+'/'+$agent,function(){
                $this.parent().find('.active').removeClass('active');
                $this.addClass('active');
            });
            
            //send like message 
            $form = $this.parents('form.send_message');            
            $input = $form.find('input[name="msg"]');
            
            $input.val($this.html());
            
            //send i liked the chat 
            $.post('chat/sendMessage', $form.serialize(), function(data) {
               
                var data = JSON.parse(data);
                $this.parents('.live-chat-ui').find('.chat-history').append(data.html);

                //scroll to bottom        
                $this.parents('.chat').find('.chat-history').slimScroll({scrollTo: 10000000});
            });

            $input.val('');            
        });

        $('#btn-start-chat-submit').click(function(){
            $.post('<?= site_url("chat/startOfflineChat") ?>',$('#chat_form').serialize(), function(data){
                    var data = JSON.parse(data);
                    
                    //if success start chat else show error message     
                    if(data.status){
                        $('#live-chat-ui-wrapper').html(data.html);//append replaced with html to allow only one chat 
                        $chat_status = 1;
                        $('#modal-start-chat .close').trigger('click');
                        init_scroll();
                    }else{
                        $('#modal-start-chat p').html(data.msg);
                    }
            });
        });
        
        function start_offline_chat($receiver_id){
            $.post('<?= site_url("chat/startOfflineChat2") ?>/'+$receiver_id,function(data){
                    var data = JSON.parse(data);
                    
                    //if success start chat else show error message     
                    if(data.status){
                        $('#live-chat-ui-wrapper').html(data.html);//append replaced with html to allow only one chat 
                        $chat_status = 1;
                        $('#modal-start-chat .close').trigger('click');
                        init_scroll();
                    }else{
                        $('#modal-start-chat p').html(data.msg);
                    }
            });
        }

</script>

<script>
 
 $(document).keypress(function(e){     
     if (e.which == 13 && !$('#chat_form input:focus').length && !$("input[name='msg']").is(":focus")) {
         login_user();
     }    
 });
  
 function login_user(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/loginProcess'); ?>",
        data: $("#loginform").serialize(),
        beforeSend : function(msg){ $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg){
	    $('body,html').animate({ scrollTop: 0 }, 200);
            
            var data = JSON.parse(msg);
            
            if(data.msg.substring(1,7) != 'script'){
                $("#ajax").html(data.msg);
                $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('login'); ?>" onclick="login_user()" class="stbutton"/>');
            }else{
                $("#ajax").html(data.msg);
            }
        }
    });
}

$(function(){ 
    //start all chats added in user_chatroom 
    <?php 

    $found = 0;

    foreach($chats as $chat){ 
        if($chat['receiver_id'] != $user_id){ 
            $found = 1;
    ?>                        
            if($('#live-chat-ui-<?= $chat["receiver_id"] ?>').length == 0){    
                start_chat(<?= $chat["receiver_id"] ?>);
            }
    <?php                             
        } else {
            $found = 1;
    ?>
            if($('#live-chat-ui-<?= $chat["user_id"] ?>').length == 0){    
                start_chat(<?= $chat["user_id"] ?>);
            }
    <?php         
        }
    } ?>
});
 </script>
