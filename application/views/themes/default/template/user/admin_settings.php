
<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-cog"></i>
        <?php echo $this->lang->line('admin_settings'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div id="ajax"><?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?></div>

    <div class="box">
        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs ui-sortable-handle">
                    <li class="active"><a data-toggle='tab' href="#tabs-1"><?php echo $this->lang->line('general'); ?></a></li>
                    <li><a data-toggle='tab' href="#tabs-2"><?php echo $this->lang->line('email'); ?></a></li>
                    <li><a data-toggle='tab' href="#tabs-3"><?php echo $this->lang->line('password'); ?></a></li>
                    <li><a data-toggle='tab' href="#tabs-4"><?php echo $this->lang->line('upload'); ?></a></li>
                    <li><a data-toggle='tab' href="#tabs-5"><?php echo $this->lang->line('chat'); ?></a></li>
                    <li><a data-toggle='tab' href="#tabs-6"><?php echo $this->lang->line('smtp'); ?></a></li>
                    <li><a data-toggle='tab' href="#tabs-7"><?php echo $this->lang->line('paypal'); ?></a></li>
                    <li><a data-toggle='tab' href="#tabs-8"><?php echo $this->lang->line('auth_settings'); ?></a></li>
                    <li><a data-toggle='tab' href="#tabs-9"><?php echo $this->lang->line('envato'); ?></a></li>
                </ul>
                <div class="tab-content">
                    
                    <div id="tabs-9" class="tab-pane">

                        <?php echo form_open(
                            '',
                            'id="envato_settings"'
                          );
                        ?>
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('envato_app_id') ?></label>
                                <input class="form-control" value="<?= config('envato_app_id') ?>" name="envato_app_id" />
                            </div>
                            
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('envato_username') ?></label>
                                <input class="form-control" value="<?= config('envato_username') ?>" name="envato_username" />
                            </div>
                            <div class="box-footer">
                                <div id="btn_envato_wrapper">
                                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="envato_settings()" class="btn btn-primary"/>
                                </div>
                            </div>                            
                        </form>
                    </div>
                            
                    <div id="tabs-8" class="tab-pane">
                        <?php echo form_open(
                            '',
                            'id="auth_settings"'
                          );
                        ?>
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('is_in_test_mode') ?></label>
                                <select class="form-control" name="authorize_test">
                                    <?php if (config('authorize_test')) { ?>
                                        <option value="1" selected=""><?= $this->lang->line('yes') ?></option>
                                        <option value="0"><?= $this->lang->line('no') ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?= $this->lang->line('yes') ?></option>
                                        <option selected="" value="0"><?= $this->lang->line('no') ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('transaction_key') ?></label>
                                <input class="form-control" value="<?= config('auth_trans_key') ?>" name="auth_trans_key" />
                            </div>
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('login_id') ?></label>
                                <input class="form-control" value="<?= config('auth_login_id') ?>" name="auth_login_id" />
                            </div>
                            <div class="box-footer">
                                <div id="btn_auth_wrapper">
                                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="auth_settings()" class="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    </div><!-- END tab -->
                    
                    <div id="tabs-7" class="tab-pane">
                        <?php echo form_open(
                            '',
                            'id="paypal_settings"'
                          );
                        ?>
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('is_in_test_mode') ?></label>
                                <select class="form-control" name="paypal_test">
                                    <?php if (config('paypal_test')) { ?>
                                        <option value="1" selected=""><?= $this->lang->line('yes') ?></option>
                                        <option value="0"><?= $this->lang->line('no') ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?= $this->lang->line('yes') ?></option>
                                        <option selected="" value="0"><?= $this->lang->line('no') ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('signature') ?></label>
                                <input class="form-control" value="<?= config('paypal_signature') ?>" name="paypal_signature" />
                            </div>
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('username') ?></label>
                                <input class="form-control" value="<?= config('paypal_username') ?>" name="paypal_username" />
                            </div>
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('password') ?></label>
                                <input class="form-control" value="<?= config('paypal_password') ?>" name="paypal_password" />
                            </div>
                            <div class="box-footer">
                                <div id="btn_paypal_wrapper">
                                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="paypal_settings()" class="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    </div><!-- END tab -->
                    
                    <div id="tabs-6" class="tab-pane">
                        
                    <?php echo form_open(
                            '',
                            'id="smtp_settings"'
                          );
                    ?>

                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('smtp_protocol'); ?></label>
                            <select name="smtp_protocol" class="form-control">                                
                                <?php if(config('smtp_protocol') == 'mail'){ ?>
                                <option selected value="mail">mail</option>
                                <option value="smtp">smtp</option>
                                <?php }else{ ?>
                                <option value="mail">mail</option>
                                <option selected value="smtp">smtp</option>
                                <?php } ?>
                            </select>                            
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('smtp_host'); ?></label>
                            <input type="text" name="smtp_host" value="<?php echo config('smtp_host'); ?>" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('smtp_user'); ?></label>
                            <input type="text" name="smtp_user" value="<?php echo config('smtp_user'); ?>" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('smtp_pass'); ?></label>
                            <input type="text" name="smtp_pass" value="<?php echo config('smtp_pass'); ?>" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('smtp_port'); ?></label>
                            <input type="text" name="smtp_port" value="<?php echo config('smtp_port'); ?>" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('smtp_timeout'); ?></label>
                            <input type="text" name="smtp_timeout" value="<?php echo config('smtp_timeout'); ?>" class="form-control" />
                        </div>
                         <div class="box-footer">
                            <div id="btn_smtp_wrapper">
                                <input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="smtp_settings()" class="btn btn-primary"/>
                            </div>
                        </div>
                    </form>
                    </div><!-- END tab -->
    
                    <div id="tabs-1" class="tab-pane active">

                    <?php echo form_open(
                            '',
                            'id="general_settings"'
                          );
                    ?>

                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('site_name'); ?></label>
                            <input type="text" name="site_name" value="<?php echo config('site_name'); ?>" class="form-control" />
                            <p class="help-block"><?php echo $this->lang->line('site_name_desc'); ?></p>
                        </div>
                        
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('currency'); ?></label>
                            <input type="text" name="currency" value="<?php echo config('currency'); ?>" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('currency_sl'); ?></label>
                            <input type="text" name="currency_sl" value="<?php echo config('currency_sl'); ?>" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('currency_sr'); ?></label>
                            <input type="text" name="currency_sr" value="<?php echo config('currency_sr'); ?>" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('site_email'); ?></label>
                            <input type="text" name="site_email" value="<?php echo config('site_email'); ?>" class="form-control" />
                            <p class="help-block"><?php echo $this->lang->line('site_email_desc'); ?></p>
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('tickets_per_page'); ?></label>
                            <input type="text" name="tickets_per_page" value="<?php echo config('tickets_per_page'); ?>" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('language'); ?></label>
                            <select name="language" class="form-control">
                            <?php
                            foreach($languages as $l){
                                
                                $l = substr($l, 0, -1);
                                
                                if($l == $language){ ?>
                                <option selected="" value="<?= $l; ?>"><?= ucfirst($l) ?></option>
                            <?php }else{ ?>
                                <option value="<?= $l ?>"><?= ucfirst($l) ?></option>
                            <?php }
                            } ?>
                            </select>
                        </div>
                        <div class="box-footer">
                            <input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="general_settings()" class="btn btn-primary" />
                        </div>
                    </form>
                </div><!-- END #tab-1 -->

                <div id="tabs-2" class="tab-pane">

                    <?php echo form_open(
                            '',
                            'id="change_email"'
                          );
                    ?>

                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('email'); ?></label>
                            <input type="text" name="email" value="<?php echo $user->email; ?>"  maxlength="50" class="form-control"/>
                        </div>
                        <div class="box-footer">
                            <input type="button" name="button" id="button" value="<?php echo $this->lang->line('change_email'); ?>" onclick="change_email()" class="btn btn-primary"/>
                        </div>
                    </form>
                </div><!-- END #tab-2 -->

                <div id="tabs-3" class="tab-pane">

                    <?php echo form_open(
                            '',
                            'id="change_password"'
                          );
                    ?>

                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('current_password'); ?></label>
                            <input type="password" name="currentpass" value=""  maxlength="20" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('new_password'); ?></label>
                            <input type="password" name="pass1" value=""  maxlength="20" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('retype_new_password'); ?></label>
                            <input type="password" name="pass2" value="" maxlength="20" class="form-control"/>
                        </div>
                        <div class="box-footer">
                            <input type="button" name="button" id="button" value="<?php echo $this->lang->line('change_password'); ?>" onclick="change_password()" class="btn btn-primary"/>
                        </div>
                    </form>
                </div><!-- END #tab-3 -->

                <div id="tabs-4" class="tab-pane">

                    <?php echo form_open(
                            '',
                            'id="upload_settings"'
                          );
                    ?>

                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('allowed_files'); ?></label>
                            <input type="text" name="allowed_extensions" value="<?php echo config('allowed_extensions'); ?>"  class="form-control"/>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="stlabel"><?php echo $this->lang->line('max_upload_files'); ?></label>
                            <select name="max_upload_files" class="form-control">
                                <option value="1" <?php if (config('max_upload_files') == 1) {
                                echo 'selected="selected"';
                            } ?>>1</option>
                                <option value="2" <?php if (config('max_upload_files') == 2) {
                                echo 'selected="selected"';
                            } ?>>2</option>
                                <option value="3" <?php if (config('max_upload_files') == 3) {
                                echo 'selected="selected"';
                            } ?>>3</option>
                                <option value="4" <?php if (config('max_upload_files') == 4) {
                                echo 'selected="selected"';
                            } ?>>4</option>
                                <option value="5" <?php if (config('max_upload_files') == 5) {
                                echo 'selected="selected"';
                            } ?>>5</option>
                                <option value="6" <?php if (config('max_upload_files') == 6) {
                                echo 'selected="selected"';
                            } ?>>6</option>
                                <option value="7" <?php if (config('max_upload_files') == 7) {
                                echo 'selected="selected"';
                            } ?>>7</option>
                                <option value="8" <?php if (config('max_upload_files') == 8) {
                                echo 'selected="selected"';
                            } ?>>8</option>
                                <option value="9" <?php if (config('max_upload_files') == 9) {
                                echo 'selected="selected"';
                            } ?>>9</option>
                                <option value="10" <?php if (config('max_upload_files') == 10) {
                                echo 'selected="selected"';
                            } ?>>10</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label class="stlabel"><?php echo $this->lang->line('max_upload_file_size'); ?></label>
                            <select name="max_upload_file_size" class="form-control">
                                <option value="1000" <?php if (config('max_upload_file_size') == 1000) {
                                echo 'selected="selected"';
                            } ?>>1 MB</option>
                                <option value="2000" <?php if (config('max_upload_file_size') == 2000) {
                                echo 'selected="selected"';
                            } ?>>2 MB</option>
                                <option value="3000" <?php if (config('max_upload_file_size') == 3000) {
                                echo 'selected="selected"';
                            } ?>>3 MB</option>
                                <option value="4000" <?php if (config('max_upload_file_size') == 4000) {
                                echo 'selected="selected"';
                            } ?>>4 MB</option>
                                <option value="5000" <?php if (config('max_upload_file_size') == 5000) {
                                echo 'selected="selected"';
                            } ?>>5 MB</option>
                                <option value="6000" <?php if (config('max_upload_file_size') == 6000) {
                                echo 'selected="selected"';
                            } ?>>6 MB</option>
                                <option value="7000" <?php if (config('max_upload_file_size') == 7000) {
                                echo 'selected="selected"';
                            } ?>>7 MB</option>
                                <option value="8000" <?php if (config('max_upload_file_size') == 8000) {
    echo 'selected="selected"';
} ?>>8 MB</option>
                                <option value="9000" <?php if (config('max_upload_file_size') == 9000) {
    echo 'selected="selected"';
} ?>>9 MB</option>
                                <option value="10000" <?php if (config('max_upload_file_size') == 10000) {
    echo 'selected="selected"';
} ?>>10 MB</option>
                            </select>
                        </div>
                        <div class="box-footer">
                            <input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="upload_settings()" class="btn btn-primary"/>
                        </div>
                    </form>
                </div><!-- END #tab-4 -->

                <div id="tabs-5" class="tab-pane">

                    <?php echo form_open(
                            '',
                            'id="chat_settings"'
                          );
                    ?>

                        <div class="form-group">
                            <label class="stlabel"><?= $this->lang->line('online_check_time') ?></label>
                            <input type="text" name="online_check_time" value="<?= config('online_check_time'); ?>" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?= $this->lang->line('online_update_time') ?></label>
                            <input type="text" name="online_update_time" value="<?= config('online_update_time'); ?>" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?= $this->lang->line('message_check_time') ?></label>
                            <input type="text" name="message_check_time" value="<?= config('message_check_time'); ?>" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label class="stlabel"><?= $this->lang->line('notification_check_time') ?></label>
                            <input type="text" name="notification_check_time" value="<?= config('notification_check_time'); ?>" class="form-control"/>
                        </div>
                        <div class="box-footer">
                            <div id="btn_chat_wrapper">
                                <input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="chat_settings()" class="btn btn-primary"/>
                            </div>
                        </div>
                    </form>
                </div><!-- END #tab-5 -->
            </div><!-- END #tabs -->

        </div><!-- END .box-body -->
    </div><!-- END .box -->
</section>

<?= Modules::run('footer/footer/index') ?>

<link rel="stylesheet" type="text/css" href="application/views/themes/default/css/jquery-ui/smoothness/jquery.ui.theme.css" />
<script src="application/views/themes/default/js/plugins/jQueryUI/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<script>
$(function(){
    $('.settings').addClass('active');
});

function auth_settings(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/authSettings'); ?>",
        data: $("#auth_settings").serialize(),
        beforeSend : function(msg){ $("#btn_auth_wrapper").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
	    $("#btn_auth_wrapper").html('<input type="button" name="button" id="button" value="<?= $this->lang->line('save') ?>" onclick="auth_settings()" class="btn btn-primary"/>');
        }
    });
}

function envato_settings(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/envatoSettings'); ?>",
        data: $("#envato_settings").serialize(),
        beforeSend : function(msg){ $("#btn_envato_wrapper").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
	    $("#btn_envato_wrapper").html('<input type="button" name="button" id="button" value="<?= $this->lang->line('save') ?>" onclick="envato_settings()" class="btn btn-primary"/>');
        }
    });
}

function paypal_settings(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/paypalSettings'); ?>",
        data: $("#paypal_settings").serialize(),
        beforeSend : function(msg){ $("#btn_payapal_wrapper").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
	    $("#btn_paypal_wrapper").html('<input type="button" name="button" id="button" value="<?= $this->lang->line('save') ?>" onclick="paypal_settings()" class="btn btn-primary"/>');
        }
    });
}
 
function smtp_settings(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/smtpSettings'); ?>",
        data: $("#smtp_settings").serialize(),
        beforeSend : function(msg){ $("#btn_smtp_wrapper").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
	    $("#btn_smtp_wrapper").html('<input type="button" name="button" id="button" value="<?= $this->lang->line('save') ?>" onclick="smtp_settings()" class="btn btn-primary"/>');
        }
    });
}
 
function chat_settings(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/chatSettings'); ?>",
        data: $("#chat_settings").serialize(),
        beforeSend : function(msg){ $("#btn_chat_wrapper").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
	    $("#btn_chat_wrapper").html('<input type="button" name="button" id="button" value="<?= $this->lang->line('save') ?>" onclick="chat_settings()" class="btn btn-primary"/>');
        }
    });
 }

 function change_email()
 {
	$.ajax({
        type: "POST",
        url: "<?php echo base_url('user/changeEmail'); ?>",
        data: $("#change_email").serialize(),
        beforeSend : function(msg){ $("#submitbutton1").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
            $("#submitbutton1").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('change_email'); ?>" onclick="change_email()" class="btn btn-primary"/>');
        }
    });
 }

 function change_password()
 {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/changePassword'); ?>",
        data: $("#change_password").serialize(),
        beforeSend : function(msg){ $("#submitbutton2").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            var data = JSON.parse(msg);
            
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(data.msg);
            $("#submitbutton2").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('change_password'); ?>" onclick="change_password()" class="btn btn-primary" />');
        }
    });
 }

 function upload_settings()
 {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/uploadSettings'); ?>",
        data: $("#upload_settings").serialize(),
        beforeSend : function(msg){ $("#submitbutton3").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
	    $("#submitbutton3").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('Save'); ?>" onclick="upload_settings()" class="btn btn-primary"/>');
        }
    });
 }

 function general_settings()
 {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/generalSettings'); ?>",
        data: $("#general_settings").serialize(),
        beforeSend : function(msg){ $("#submitbutton4").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
	    $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
	    $("#submitbutton4").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="general_settings()" class="btn btn-primary"/>');
        }
    });
 }
 </script>
