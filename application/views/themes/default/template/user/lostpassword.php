<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?= $this->lang->line('forgott_password') ?> | <?= $site_name ?></title>
    <base href="<?= base_url() ?>">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Custome -->
    <link href="application/views/themes/default/css/custome.css?v=1" rel="stylesheet" type="text/css" />
    <!-- Bootstrap 3.3.2 -->
    <link href="application/views/themes/default/css/bootstrap.min.css?v=1" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css?v=1" rel="stylesheet" type="text/css" />
    <link href="application/views/themes/default/css/A1support.min.css?v=1" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="images/favicon.png"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a>
            <img src="<?php echo base_url(); ?>images/logo.png" alt="Logo" title="A1 Support Desk"/>
        </a>
      </div><!-- /.login-logo -->

        <div id="ajax"></div>

        <div class="login-box-body">
            <p class="login-box-msg"><?= $this->lang->line('recover_password') ?></p>

            <?php echo form_open('', 'id="lostpassword"'); ?>

                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="email" placeholder="<?php echo $this->lang->line('email'); ?>"/>
                    <span class="fa fa-envelope form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <a class="btn btn-primary btn-block" href="<?= site_url('user/login') ?>"><?= $this->lang->line('back_to_login') ?></a><br>
                    </div><!-- /.col -->
                    <div class="col-xs-4 pull-right">
                      <button type="button" onclick="lostpw()" class="btn btn-primary btn-block"><?php echo $this->lang->line('send'); ?></button>
                    </div><!-- /.col -->
                </div>
            </form>
        </div>
        <hr />
        <center>&copy; Copyright <?php echo date("Y"); ?> - <strong>A1 Support Desk v<?= VERSION ?></strong></center>
    </div>
  </body>
</html>

<!-- jQuery 2.1.3 -->
<script src="application/views/themes/default/js/plugins/jQuery/jQuery-2.1.3.min.js"></script>

<!-- Bootstrap 3.3.2 JS -->
<script src="application/views/themes/default/js/bootstrap.min.js" type="text/javascript"></script>

<script src="application/views/themes/default/js/moment.min.js" type="text/javascript"></script>
<script src="application/views/themes/default/js/livestamp.js" type="text/javascript"></script>    

<script>

 $(document).keypress(function(e) {
    if(e.which == 13) {
        lostpw();
        return false;
    }
 });

 function lostpw(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/lostpasswordProcess'); ?>",
        data: $("#lostpassword").serialize(),
        beforeSend : function(msg){ $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg){
            
            var data = JSON.parse(msg);
            
            $('body,html').animate({ 
                scrollTop: 0
            }, 200);
            
            $("#ajax").html(data.msg);
            $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('send'); ?>" onclick="lostpw()" class="stbutton"/>');
        }
    });
 }
 </script>
