
<?= Modules::run('header/header/index') ?>

<?php echo form_open('', 'id="form_update_mail_template"'); ?>
    
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-envelope"></i>
        <?php echo $this->lang->line('mail_templates'); ?>
        
        <div id="btn_wrapper" class="pull-right">
            <input type="button" name="button" id="button" value="<?php echo $this->lang->line('save'); ?>" onclick="update_mail_template()" class="btn btn-primary"/>
        </div>

    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div id="ajax"><?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?></div>

    <div class="row">
        <div class="col-lg-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?= $this->lang->line('new_ticket_mail_for_customer') ?>
                    </h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('new_ticket_subject') ?></label>
                                <input class="form-control" value="<?= config('new_ticket_subject') ?>" name="new_ticket_subject" />
                            </div>

                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('new_ticket_template') ?></label>
                                <textarea id="new_ticket_template" name="new_ticket_template" class="ckeditor"><?= config('new_ticket_template') ?></textarea>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <h3 class="box-title">
                                <?= $this->lang->line('available_variables') ?>
                            </h3>
                            {{site_name}}<br />
                            {{ticket_id}}<br />
                            {{title}}<br />
                            {{user_id}}<br />
                            {{message}}<br />
                            {{ip_addresss}}<br />
                            {{link}}<br />

                        </div>
                    </div>
                </div><!-- END .box-body -->
            </div><!-- END .box -->
        </div>
        <div class="col-lg-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?= $this->lang->line('ticket_status_change_mail') ?>
                    </h3>
                </div>
                <div class="box-body">
                     <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('ticket_status_change_subject') ?></label>
                                <input class="form-control" value="<?= config('ticket_status_change_subject') ?>" name="ticket_status_change_subject" />
                            </div>

                            <div class="form-group">
                                <label for="inputSuccess" class="control-label"><?= $this->lang->line('ticket_status_change_template') ?></label>
                                <textarea id='ticket_status_change_template' name="ticket_status_change_template" class="ckeditor"><?= config('ticket_status_change_template') ?></textarea>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <h3 class="box-title">
                                <?= $this->lang->line('available_variables') ?>
                            </h3>
                            {{site_name}}<br />
                            {{ticket_id}}<br />
                            {{old_status}}<br />
                            {{new_status}}<br />
                            {{link}}<br />
                        </div>
                    </div>
                </div>
            </div>     
        </div>
    </div>    
</section>

</form>

<?= Modules::run('footer/footer/index') ?>

<script src="<?= base_url() ?>application/third_party/ckeditor/ckeditor.js"></script>

<script>
$(function(){
    $('.sidebar-menu .settings').first().addClass('active');
    $('.sidebar-menu .mail_template').addClass('active');
});

function update_mail_template(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/updateMailTemplate'); ?>",
        data: {
            'csrf_a1support' : $('input[name="csrf_a1support"]').val(),
            'new_ticket_template' : CKEDITOR.instances['new_ticket_template'].getData(),
            'new_ticket_subject' : $('input[name="new_ticket_subject"]').val(),
            'ticket_status_change_subject' : $('input[name="ticket_status_change_subject"]').val(),
            'ticket_status_change_template': CKEDITOR.instances['ticket_status_change_template'].getData(),
            
        },
        beforeSend : function(msg){ $("#btn_wrapper").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(msg);
	    $("#btn_wrapper").html('<input type="button" name="button" id="button" value="<?= $this->lang->line('save') ?>" onclick="update_mail_template()" class="btn btn-primary"/>');
        }
    });
}
 </script>
