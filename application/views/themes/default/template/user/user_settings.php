<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('settings'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
        
    <div id="ajax">
        <?php  if ($this->session->flashdata('message')) {
                    echo $this->session->flashdata('message');
                } ?>
    </div>

    <div class="col-md-6">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('change_password'); ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">

            <?php echo form_open('', 'id="settings"'); ?>

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('current_password'); ?></label>
                    <input type="password" name="currentpass" value=""  maxlength="20" class="form-control" />
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('new_password'); ?></label>
                    <input type="password" name="pass1" value=""  maxlength="20" class="form-control" />
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('retype_new_password'); ?></label>
                    <input type="password" name="pass2" value="" maxlength="20" class="form-control" />
                </div>
                <div class="box-footer">
                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('update'); ?>" onclick="update()" class="btn btn-primary" />
                </div>
            </form>
        </div><!-- END .box-body -->
    </div><!-- END .box -->
    </div>
    
    <div class="col-md-6">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('update_details'); ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">

            <?php echo form_open('', 'id="details"'); ?>
            
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('details'); ?></label>
                    <textarea id="details" name="details" class="ckeditor"><?= $user->details ?></textarea>
                </div>
                <div class="box-footer">
                    <input type="button" name="button" id="btn_update_details" value="<?php echo $this->lang->line('update'); ?>" onclick="update_details()" class="btn btn-primary" />
                </div>
            </form>
        </div><!-- END .box-body -->
    </div><!-- END .box -->
    </div>
</section><!-- End Ticket Form -->

<?= Modules::run('footer/footer/index') ?>

<script src="application/third_party/ckeditor/ckeditor.js"></script>

<script>

 $(function(){
    $('.settings').addClass('active');
 });

function update_details(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/updateDetails'); ?>",
        data: { 'details':CKEDITOR.instances['details'].getData() },
        beforeSend : function(msg){ 
            $("#btn_update_details").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />');
        },
        success: function(msg)
        {
            var data = JSON.parse(msg);
            
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(data.msg);
            $("#btn_update_details").html('<input type="button" name="button" id="btn_update_details" value="<?php echo $this->lang->line('update'); ?>" onclick="update_details()" class="stbutton"/>');
        }
    });
 }
 
function update(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/changePassword'); ?>",
        data: $("#settings").serialize(),
        beforeSend : function(msg){ 
            $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />');
        },
        success: function(msg)
        {
            var data = JSON.parse(msg);
            
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(data.msg);
            $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('update'); ?>" onclick="update()" class="stbutton"/>');
        }
    });
 }
 </script>
