<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('users'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div id="ajax"><?php
        if ($this->session->flashdata('message')) {
            echo $this->session->flashdata('message');
        }
        ?></div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('create_user'); ?></h3>
        </div><!-- /.box-header -->

        <div class="box-body table-responsive">

            <div id="ajax"></div>

                <?php echo form_open(
                        '',
                        'id="createform"'
                      );
                ?>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('name_lastname'); ?></label>
                    <input type="text" name="name" maxlength="50" value="" class="form-control" />
                </div>

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('email'); ?></label>
                    <input type="text" name="email" value=""  maxlength="50" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('telephone'); ?></label>
                    <input type="text" name="telephone" value=""  maxlength="50" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('password'); ?></label>
                    <input type="text" name="pass1" value=""  maxlength="50" class="form-control" />
                </div>

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('user_dep'); ?></label>
                    <select name="department" id="department" class="form-control">
                        <!--
                        <option value="0">Customer</option>                        
                        -->
                        <option value="1"><?= $this->lang->line('team_leader') ?></option>
                                                
                        <?php
                        if (!empty($departments)) {
                            foreach ($departments as $department) {
                                echo '<option value="' . $department->id . '">' . $department->department_name . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('details'); ?></label>
                    <textarea name="details" class="form-control"></textarea>
                </div>
                
                <div id="submitbutton" class="box-footer">
                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('submit'); ?>" onclick="create_user()" class="btn btn-primary" />
                </div>	
            </form>
        </div>
    </div><!-- END .box -->
</section>

<?= Modules::run('footer/footer/index') ?>

<script>
    $(function(){
       $('.user_list').addClass('active'); 
    });
    
    function create_user()
    {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('user/addProcess'); ?>",
            data: $("#createform").serialize(),
            beforeSend: function(msg) {
                $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />');
            },
            success: function(msg){
                
                var data = JSON.parse(msg);
                
                $('body,html').animate({scrollTop: 0}, 200);
                $("#ajax").html(data.msg);                
                $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('submit'); ?>" onclick="create_user()" class="btn btn-primary" />');
            }
        });
    }
</script>





