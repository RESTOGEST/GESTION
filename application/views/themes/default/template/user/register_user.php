<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?= $this->lang->line('sign_up') ?> | <?= $site_name ?></title>
    <base href="<?= base_url() ?>">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Custome -->
    <link href="application/views/themes/default/css/custome.css?v=1" rel="stylesheet" type="text/css" />
    <!-- Bootstrap 3.3.2 -->
    <link href="application/views/themes/default/css/bootstrap.min.css?v=1" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css?v=1" rel="stylesheet" type="text/css" />
    <link href="application/views/themes/default/css/A1support.min.css?v=1" rel="stylesheet" type="text/css" />

    <link rel="icon" type="image/png" href="images/favicon.png"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a>
            <img src="<?php echo base_url(); ?>images/logo.png" alt="Logo" title="Krafty Support"/>
        </a>
      </div><!-- /.login-logo -->

      <div id="ajax"><?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?></div>

      <div class="login-box-body">
          
        <p class="login-box-msg"><?= $this->lang->line('create_account_to_start') ?></p>
        
        <?php echo form_open('', 'id="createform"'); ?>

          <div class="form-group">
              <input type="text" name="name" value="" maxlength="50"  class="form-control" placeholder="<?php echo $this->lang->line('name_lastname'); ?>" />
          </div>
          <div class="form-group">
              <input type="text" name="email" value=""  maxlength="50" class="form-control" placeholder="<?php echo $this->lang->line('email'); ?>" />
          </div>
          <div class="form-group">
              <input type="password" name="pass1" value=""  maxlength="20" class="form-control" placeholder="<?php echo $this->lang->line('password'); ?>" />
  	  </div>
          <div class="form-group required">
            <input type="text" class="form-control" placeholder="TELEPHONE" name="telephone" />
          </div>
          <div class="row">
            <div class="col-xs-12">
                <input type="button" name="button" id="button" value="<?php echo $this->lang->line('create_a_new_account_button'); ?>" onclick="create_user()" class="btn btn-primary btn-block btn-lg" />
            </div>
          </div>
          <hr />
          <div class="row">
              <div class="col-xs-6">
                <a href="<?= site_url('user/lostpassword') ?>">
                    <?= $this->lang->line('i_forgot_my_password') ?>
                </a>
              </div>
              <div class="col-xs-6 text-right">
                  <a href="<?= site_url('user/login') ?>"><?= $this->lang->line('already_registered') ?></a><br>
              </div><!-- /.col -->
          </div>
       
	</form>
     </div><!-- /.login-box-body -->
      <hr />
      <center>&copy; Copyright <?php echo date("Y"); ?> - <strong>A1 Support Desk v<?= VERSION ?></strong></center>
    </div><!-- /.login-box -->
      
    <div id="live-chat-ui-wrapper"></div>
    
    <!-- start chat model -->
    <div id="modal-start-chat" class="modal fade in" aria-hidden="false">
        <div class="login-box modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title"><?= $this->lang->line('start_chat') ?></h4>
                </div>
                <div class="modal-body">
                    
                    <?php echo form_open('', 'id="chat_form"'); ?>
                    
                        <div class="form-group required">
                            <input type="text" required="" class="form-control" placeholder="NAME" value="" name="name" />
                        </div>
                        <div class="form-group required">
                            <input type="text" class="form-control" placeholder="EMAIL ADDRESS" name="email" />
                        </div>
                        <div class="form-group required">
                            <input type="text" class="form-control" placeholder="TELEPHONE" name="telephone" />
                        </div>
                        <input type="button" value="Submit" id="btn-start-chat-submit" class="btn btn-default" />
                    </form>
                    
                    <div class="clearfix"></div>

                    <p></p>
                    
                    <style>
                    .modal-dialog{
                        width:360px;
                        margin-top: 100px;
                    }
                    </style></div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <!-- jQuery 2.1.3 -->
    <script src="application/views/themes/default/js/plugins/jQuery/jQuery-2.1.3.min.js"></script>

    <!-- Bootstrap 3.3.2 JS -->
    <script src="application/views/themes/default/js/bootstrap.min.js" type="text/javascript"></script>

  </body>
</html>

<script src="application/views/themes/default/js/moment.min.js" type="text/javascript"></script>
<script src="application/views/themes/default/js/livestamp.js" type="text/javascript"></script>    

<script>

 $(document).keypress(function(e) {
    if(e.which == 13) {
        create_user();
    }
 });

function create_user(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('user/createProcess'); ?>",
        data: $("#createform").serialize(),
        beforeSend : function(msg){ $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); },
        success: function(msg)
        {
            $('body,html').animate({ scrollTop: 0 }, 200);
            
            var data = JSON.parse(msg);
            
            if(data.msg.substring(1,7) != 'script'){
                $("#ajax").html(data.msg);
                $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('create_a_new_account_button'); ?>" onclick="create_user()" class="stbutton"/>');
            }else{
                $("#ajax").html(data.msg);
            }
        }
    });
 }
 </script>
