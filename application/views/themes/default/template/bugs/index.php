<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-bug"></i>
        <?= $this->lang->line('bugs') ?>
        <div class="box-tools pull-right">
            <a class="btn btn-primary" href="<?= site_url('bugs/insert') ?>">
                <i class="fa fa-plus"></i> &nbsp;<?= $this->lang->line('request_bug') ?>
            </a>
        </div>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?= $msg ?>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <?= $this->lang->line('search_bugs') ?>
            </h3>
            <div class="pull-right box-tools">                
                <button class="btn bg-teal btn-smm" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>    
        <div class="box-body table-responsive">

            <?php echo form_open(
                    'bugs/index',
                    'class="form form-inline ticket_filter_form"'
                  );
            ?>
                
                <input type="hidden" name="sort" value="<?= $sort ?>" />
                <input type="hidden" name="order" value="<?= $order ?>" />
                
                <div class="form-group">
                    <label><?= $this->lang->line('title')  ?></label><br />
                    <input value="<?= $title ?>" name="title" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label><?= $this->lang->line('product')  ?></label><br />
                    <input value="<?= $product ?>" name="product" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label><?= $this->lang->line('status')  ?></label><br />
                    <select name="status" class="form-control">
                        <option value="">All</option>
                        
                        <?php if($status){ ?>                            
                            <option selected="selected" value="1">Complete</option>
                        <?php }else{ ?>
                            <option value="1">Complete</option>
                        <?php } ?>
                            
                        <?php if($status != "" && $status == 0){ ?>
                            <option selected="selected" value="0">Incomplete</option>
                        <?php }else{ ?>
                            <option value="0">Incomplete</option>
                        <?php } ?>    
                    </select>
                </div>
                
                <div class="form-group">
                    <label>&nbsp;</label><br />
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-search"></i>
                        <?= $this->lang->line('search')  ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
    
    <div class="box">
        <div class="box-body table-responsive">
            <table class="table table-services table-hover">
                <thead>
		<thead>
                    <tr>
                         <th class="sorting" align="left">
                            <?php if ($sort == 'b.title') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_title ?>">
                                    <?php echo $this->lang->line('title'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_title ?>">
                                    <?php echo $this->lang->line('title'); ?>
                                </a>
                            <?php } ?>
                        </th>
                        
                        <th class="sorting">
                            <?php if ($sort == 'p.title') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_product ?>">
                                    <?php echo $this->lang->line('product'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_product ?>">
                                    <?php echo $this->lang->line('product'); ?>
                                </a>
                            <?php } ?>
                        </th>
                        
                        <th class="sorting">
                            <?php if ($sort == 'b.votes') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_votes ?>">
                                    <?php echo $this->lang->line('votes'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_votes ?>">
                                    <?php echo $this->lang->line('votes'); ?>
                                </a>
                            <?php } ?>
                        </th>
                        
                        <th class="sorting">
                            <?php if ($sort == 'b.status') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_status ?>">
                                    <?php echo $this->lang->line('status'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_status ?>">
                                    <?php echo $this->lang->line('status'); ?>
                                </a>
                            <?php } ?>
                        </th>
                        
                        <th class="sorting">
                            <?php if ($sort == 'b.date_added') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_date_added ?>">
                                    <?php echo $this->lang->line('date_added'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_date_added ?>">
                                    <?php echo $this->lang->line('date_added'); ?>
                                </a>
                            <?php } ?>
                        </th>
                        
                        <th style="text-align: right;" align="right" width="20%"><a><?php echo $this->lang->line('options'); ?></a></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($rows as $row){ ?>
                    <tr id="item_<?= $row['bug_id'] ?>">
                        <td width="25%">
                            <a href="<?= site_url('bugs/view/'.$row['bug_id']) ?>">
                                <?= $row['title'] ?>
                            </a>
                        </td>
                        <td width="15%"><?= $row['product'] ?></td>
                        <td width="7%"><?= $row['votes'] ?></td>
                        <td width="8%">
                            <?php if($row['status']){ ?>
                                <label class="label label-success"><?= $this->lang->line('solved') ?></label>
                            <?php }else{ ?>
                                <label class="label label-danger"><?= $this->lang->line('unsolved') ?></label>
                            <?php } ?>    
                        </td>
                        <td width="10%">
                            <?= date('d M Y', strtotime($row['date_added'])) ?>
                        </td>
                        <td width="25%" align="right">
                            
                          <?php if(userdata('department')==1){ ?>  
                          <?php if($row['status']){ ?>
                          <a data-toggle="tooltip" data-title="<?= $this->lang->line('unsolved') ?>" href="<?= site_url('bugs/incomplete/'.$row['bug_id']) ?>" class="btn btn-warning btn-sm">
                              <i class="fa fa-star-o"></i>
                          </a>&nbsp;&nbsp;
                          <?php }else{ ?>
                          <a data-toggle="tooltip" data-title="<?= $this->lang->line('solved') ?>" href="<?= site_url('bugs/complete/'.$row['bug_id']) ?>" class="btn btn-success btn-sm">
                              <i class="fa fa-star"></i>                            
                          </a>&nbsp;&nbsp;
                          <?php } ?>
                          <?php } ?>

                          <a data-toggle="tooltip" data-title="<?= $this->lang->line('discussion') ?>" class="btn btn-danger btn-sm" href="<?= site_url('bugs/discussion/?bug_id='.$row['bug_id']) ?>">
                              <i class="fa fa-comments-o"></i>
                          </a>&nbsp;&nbsp;
                          
                          <?php if(!$row['status'] && !in_array($row['bug_id'], $my_votes)){ ?>
                          <a data-toggle="tooltip" data-title="<?= $this->lang->line('vote') ?>" class="btn btn-twitter btn-sm"  href="<?= site_url('bugs/vote/'.$row['bug_id']) ?>">
                              <i class="fa fa-thumbs-up"></i>
                          </a>&nbsp;&nbsp;
                          <?php } ?>

                          <?php if(userdata('department')==1){ ?>
                          <a data-toggle="tooltip" data-title="<?= $this->lang->line('edit') ?>" class="btn bg-orange btn-sm" href="<?= site_url('bugs/edit/'.$row['bug_id']) ?>">
                              <i class="fa fa-pencil"></i>
                          </a>&nbsp;&nbsp;
                          <a data-toggle="tooltip" data-title="<?= $this->lang->line('delete') ?>" class="btn btn-danger btn-sm" onclick="delete_bugs(<?= $row['bug_id'] ?>);">
                              <i class="fa fa-trash"></i>
                          </a>&nbsp;&nbsp;                          
                          <?php } ?>

                        </td>
                    </tr>                    
                    <?php } ?>
                </tbody>
            </table>
        </div>  
    </div><!-- END .box -->
</section>

<?= Modules::run('footer/footer/index') ?>

<script>

$(function(){
   $('.bug_list').addClass('active');
});

function delete_bugs(id){
    if(confirm('Are you sure?')){
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('bugs/delete' ); ?>/" + id,
            success: function(msg)
            {
               $('#item_' + id).fadeOut('normal');
            }
        });
    }
}

</script>
