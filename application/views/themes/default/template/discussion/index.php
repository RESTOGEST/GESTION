<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-comments"></i>
        <?= $this->lang->line('discussion') ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
 <div class="row">
        <div class="col-lg-5">
            <div class="box visible-lg box-primary" id="customer_detail_box">
                <div class="box-header">
                    <h3 class="box-title">
                        <?= $feature['title'] ?>
                    </h3>              
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <td><strong><?php echo $this->lang->line('product'); ?></strong></td>
                            <td>:</td> 
                            <td>
                                <?= $feature['product'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong><?php echo $this->lang->line('description'); ?></strong></td>
                            <td>:</td> 
                            <td>
                                <?= $feature['description'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong><?php echo $this->lang->line('votes'); ?></strong></td>
                            <td>:</td> 
                            <td><?php echo $feature['votes']; ?></td>
                        </tr>
                        <tr>
                            <td><strong><?php echo $this->lang->line('date'); ?></strong></td>
                            <td>:</td> 
                            <td><?php echo date('d F Y', strtotime($feature['date_added'])); ?></td>
                        </tr>
                        <tr>
                            <td><strong><?php echo $this->lang->line('status'); ?></strong></td>
                            <td>:</td> 
                            <td>
                                <?php if($feature['status']){ ?>
                                    <label class="label label-success">Complete</label>
                                <?php }else{ ?>
                                    <label class="label label-danger">Incomplete</label>
                                <?php } ?>    
                            </td>
                        </tr>
                    </table>
                </div>        
            </div><!-- END .box-body -->
        </div>
        <div class="col-lg-7">
            <div class="box  hidden-lg  box-primary" id="customer_detail_box">
                <div class="box-header">
                    <h3 class="box-title">
                        <?= $feature['title'] ?>
                    </h3>              
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <td><strong><?php echo $this->lang->line('product'); ?></strong></td>
                            <td>:</td> 
                            <td>
                                <?= $feature['product'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong><?php echo $this->lang->line('description'); ?></strong></td>
                            <td>:</td> 
                            <td>
                                <?= $feature['description'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong><?php echo $this->lang->line('votes'); ?></strong></td>
                            <td>:</td> 
                            <td><?php echo $feature['votes']; ?></td>
                        </tr>
                        <tr>
                            <td><strong><?php echo $this->lang->line('date'); ?></strong></td>
                            <td>:</td> 
                            <td><?php echo date('d F Y', strtotime($feature['date_added'])); ?></td>
                        </tr>
                        <tr>
                            <td><strong><?php echo $this->lang->line('status'); ?></strong></td>
                            <td>:</td> 
                            <td>
                                <?php if($feature['status']){ ?>
                                    <label class="label label-success">Complete</label>
                                <?php }else{ ?>
                                    <label class="label label-danger">Incomplete</label>
                                <?php } ?>    
                            </td>
                        </tr>
                    </table>
                </div>        
            </div><!-- END .box-body -->
            
            <div class="box">
                <div class="box-body table-responsive">
                    <ul class="timeline">

                        <?php foreach($rows as $row){ ?>
                            <!-- timeline item -->

                            <li id='item_<?= $row['fd_id'] ?>'>

                              <i class="fa fa-comments-o"></i>
                              <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> <?= date('d F Y g:i a',strtotime($row['datetime'])) ?></span>
                                <h3 class="timeline-header">
                                    <b><?= $row['author'] ?></b>
                                </h3>
                                <div class="timeline-body">
                                  <?= $row['comment'] ?>
                                </div>
                              </div>
                            </li>
                            <!-- END timeline item -->
                        <?php } ?>
                    </ul>

                    <?php if($links){ ?>
                    <div class="pagination">
                        <ul class="pagination">
                            <?php echo $links; ?>
                        </ul>                
                    </div>
                    <hr  />
                    <?php } ?>

                    <?php if($user_id > 0){ ?>
                    
                    <?php echo form_open(
                        '',
                        'class="" id="add-discussion-form"'
                      );
                    ?>

                        <div class="form-group">
                            <label>Comment</label>
                            <textarea class="form-control" name="comment"></textarea>
                            <input type="hidden" name="feature_id" value="<?= $feature['feature_id'] ?>" />
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" onclick="add_discussion();">Submit</button>
                        </div>
                    </form>
                    <?php } ?>
                </div>
            </div><!-- END .box -->
        </div>
 </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<style>
    .pagination{
        margin: 0 !important;
    }
</style>

<script>  
   
//get left box widh 
$width = $('#customer_detail_box').width();

$(document).scroll(function(){
    if($(this).scrollTop() >= 100){
       $('#customer_detail_box').css('position','fixed');
       $('#customer_detail_box').css('top','10px');
       $('#customer_detail_box').css('width', $width);        
    }else{
       $('#customer_detail_box').css('position','relative');
       $('#customer_detail_box').css('top','auto');        
    }
});

function add_discussion(){  
    if($('textarea[name="comment"]').val().length > 0){
        $('textarea[name="comment"]').css('border','1px solid #d2d6de');
        $.post('<?= site_url("features/addDiscussion") ?>', $('#add-discussion-form').serialize(), function(data){
            $('.timeline').append(data);
            $('textarea[name="comment"]').val('');
        });    
    }else{
       $('textarea[name="comment"]').css('border','1px solid red');
    }
}
    
$(function(){
   $('.feature_list').addClass('active');
});
</script>
