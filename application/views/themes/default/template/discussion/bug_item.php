<li id='item_<?= $row['fd_id'] ?>'>
    <i class="fa fa-comments-o"></i>
    <div class="timeline-item">
        <span class="time"><i class="fa fa-clock-o"></i> <?= format_time($row['datetime']) ?></span>
        <h3 class="timeline-header">
            <b><?= $row['author'] ?></b>
        </h3>
        <div class="timeline-body">
            <?= $row['comment'] ?>
        </div>
    </div>
</li>