<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $this->lang->line('view_feature') ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <?= $row['title'] ?>
            </h3>            
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">            
            <span class="time"><i class="fa fa-clock-o"></i> <?= date('M d,Y', strtotime($row['date_added'])) ?></span>
            <hr />
            <div class="timeline-body">
               <?= $row['description'] ?>
            </div>
        </div>
    </div><!-- END .box -->
</section>

<?= Modules::run('footer/footer/index') ?>

<script>

$(function(){
   $('.feature_list').addClass('active'); 
});    

</script>