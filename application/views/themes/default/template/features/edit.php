<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $this->lang->line('edit_feature') ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <?php if($msg){ ?>
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= $msg ?>
        </div>
    <?php } ?>
    
    <?php echo form_open(
        'features/edit/'.$row['feature_id'],
        'method="post"'
      );
    ?>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= $this->lang->line('edit_feature') ?></h3>            
            </div><!-- /.box-header -->
            <div class="box-body">       
                <div class="form-group">
                    <label><?= $this->lang->line('product') ?></label>
                    <input value="<?= $row['product'] ?>" class="form-control" name="product" />
                    <input value="<?= $row['product_id'] ?>" type="hidden" class="form-control" name="product_id" />
                </div>
                <div class="form-group">
                    <label><?= $this->lang->line('title') ?></label>
                    <textarea class="form-control" name="title"><?= set_value('title', $row['title']) ?></textarea>
                </div>
                <div class="form-group">
                    <label><?= $this->lang->line('description') ?></label>
                    <textarea class="ckeditor form-control" name="description"><?= set_value('description', $row['description']) ?></textarea>
                </div>
            </div>
            <div class="box-footer">
                <button type="sumit" class="btn btn-primary"><?= $this->lang->line('submit') ?></button>
            </div>
        </div>
    </form>
</section>

<?= Modules::run('footer/footer/index') ?>

<link href="application/views/themes/default/css/jquery-ui/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="application/views/themes/default/js/plugins/jQueryUI/jquery-ui-1.10.3.min.js"></script>

<script>
$("input[name='product']").autocomplete({
        source: function(request, response) {
                $.ajax({
                        url: '<?= site_url("features/productAutocomplete") ?>/' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {	
                                response($.map(json, function(item) {
                                        return {
                                                label: item.title,
                                                value: item.product_id,
                                        }
                                }));
                        }
                });
        }, 
        select: function(event, ui) {
                $('input[name=\'product\']').val(ui.item.label);
                $('input[name=\'product_id\']').val(ui.item.value);
                return false;
        },
        focus: function(event, ui) {
                return false;
        }
});
</script>

<script src="<?= base_url() ?>application/third_party/ckeditor/ckeditor.js"></script>

<script>
    $(function(){
       $('.feature_list').addClass('active'); 
    });
</script>