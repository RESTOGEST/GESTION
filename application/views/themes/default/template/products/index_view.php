<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-cube"></i>
        <?php echo $this->lang->line('products'); ?>
        
        <?php if(userdata('department') != 0){ ?>
        <div class="pull-right">
            <a href="<?php echo base_url('products/create'); ?>" class="btn btn-primary">
              <i class="fa fa-plus"></i>
                <?php echo $this->lang->line('create_a_new_product'); ?>
            </a>
        </div>
        <?php } ?>
                
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?php echo $this->session->flashdata('message'); ?>

    <?php if($message) echo $message; ?>
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <?= $this->lang->line('search_products') ?>
            </h3>
            <div class="pull-right box-tools">                
                <button class="btn bg-teal btn-smm" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>    
        <div class="box-body table-responsive">

            <?php echo form_open(
                'products/index',
                'method="get" class="form form-inline ticket_filter_form"'
              );
            ?>
                
                <input type="hidden" name="sort" value="<?= $sort ?>" />
                <input type="hidden" name="order" value="<?= $order ?>" />
                
                <div class="form-group">
                    <input placeholder="Enter Product Title" value="<?= $q ?>" name="q" class="form-control" />
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-search"></i>
                        <?= $this->lang->line('search')  ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
    
    <div class="box">
        <div class="box-body table-responsive">
            <table class="table table-products table-hover">
                <thead>
		<thead>
                    <tr>
                        <th width="10%"><a><?php echo $this->lang->line('image'); ?></a></th>
                        
                        <th class="sorting" width="50%" align="left">
                            <?php if ($sort == 'title') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_title ?>">
                                    <?php echo $this->lang->line('title'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_title ?>">
                                    <?php echo $this->lang->line('title'); ?>
                                </a>
                            <?php } ?>
                        </th>
                        
                        <th class="sorting" width="20%">
                            <?php if ($sort == 'price') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_price ?>">
                                    <?php echo $this->lang->line('price'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_price ?>">
                                    <?php echo $this->lang->line('price'); ?>
                                </a>
                            <?php } ?>
                        </th>
                        
                        <th style="text-align: right;" width="20%"><a><?php echo $this->lang->line('options'); ?></a></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($products)) { ?>
                        <?php foreach ($products as $product) { ?>
                            <tr id="product_id_<?php echo $product->product_id; ?>">
                                <td>
                                    <?php if($product->icon && file_exists(FCPATH.'images/products/'.$product->icon)){ ?>
                                        <img src="<?php echo site_url('images/products/'.$product->icon); ?>" width="50px" height="50px" />
                                    <?php }else{ ?>
                                        <img src="<?= site_url('images/no_image.jpg') ?>" width="50px" height="50px" />
                                    <?php } ?>
                                </td>
                                <td align="left">
                                    <?php echo $product->title; ?>
                                </td>
                                <td align="left">
                                    <?= $this->currency->format($product->price); ?>
                                </td>
                                <td align="right">
                                    
                                    <?php if($product->link){ ?>
                                    <a class="btn btn-twitter btn-sm" data-toggle="tooltip" data-title="<?= $this->lang->line('view') ?>"  target="_blank" href="<?php echo $product->link; ?>">
                                        <i class="fa fa-eye"></i> 
                                    </a>&nbsp;&nbsp;
                                    <?php } ?>
                                    
                                    <?php if(userdata('department') != 0){ ?>
                                    <a data-toggle="tooltip" data-title="<?= $this->lang->line('update') ?>"  class="btn bg-orange btn-sm" href="<?php echo base_url('products/update/' . $product->product_id); ?>">
                                        <i class="fa fa-pencil"></i>
                                    </a>&nbsp;&nbsp;
                                    
                                    <a data-toggle="tooltip" data-title="<?= $this->lang->line('delete') ?>"  class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="delete_product(<?php echo $product->product_id; ?>)">
                                        <i class="fa fa-trash"></i> 
                                    </a>
                                    <?php } ?>
                                    
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
            
            <div class="pagination_wrapper">
                <ul class="pagination pull-right">
                <?= $pagination ?>
                </ul>    
            </div>
            
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<style>
    .table-products td{
        line-height: 50px !important;
    }
</style>
<script>

$(function(){
    $('.products').addClass('active');
});

function delete_product(product_id){

    //return confirm('Are you sure?');

    $.ajax({
        type: "GET",
        url: "<?php echo base_url('products/delete' ); ?>/" + product_id,
        success: function(msg){
            var data = JSON.parse(msg);
            
	    if(data.msg == 'deleted' ){
                $('#product_id_' + product_id).fadeOut('normal');
            }
        }
    });
 }
 </script>
