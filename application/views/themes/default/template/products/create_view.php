<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('create_a_new_product'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <div id="ajax">            
        <?php echo $this->session->flashdata('message'); ?>
    </div>    
    
    <div class="box">
        <div class="box-body table-responsive">
            
            <?php echo form_open(
                '',
                'id="createform" enctype="multipart/form-data"'
              );
            ?>

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('title'); ?></label>
                    <input type="text" name="title" value=""  class="form-control" />
                </div>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('icon'); ?></label>
                    <input type="file" name="icon" value=""  class="form-control" />
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('description'); ?></label>
                    <textarea name="description" value=""  class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('price'); ?></label>
                    <input type="text" name="price" value=""  class="form-control" />
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('link'); ?></label>
                    <input type="text" name="link" value=""  class="form-control" />
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('sort_order'); ?></label>
                    <input type="text" name="sort_order" value=""  class="form-control" />
                </div>
                
                <div class="box-footer">
                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('create_a_new_product'); ?>" onclick="create_product()" class="btn btn-primary" />
                </div>	
                
            </form>
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<script>
    
$(function(){
    $('.products').addClass('active');
}); 

function create_product(){
    
    var form = document.getElementById("updateform");
    
    var formData = new FormData($('form')[0])
    
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('products/createProcess'); ?>",
        data: formData,//$("#createform").serialize(),
        processData: false,
        contentType: false,
        beforeSend : function(msg){ 
            $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />');
        },
        success: function(msg){
            
            var data = JSON.parse(msg);
            
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(data.msg); 
            $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('create_a_new_product'); ?>" onclick="create_product()" class="stbutton"/>');            
        }
    });
 }
 </script>

       