<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('update_service'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <div id="ajax">            
        <?php echo $this->session->flashdata('message'); ?>
    </div>    
    
    <div class="box">
        <div class="box-body">            

            <?php echo form_open(
                '',
                'id="updateform" enctype="multipart/form-data"'
              );
            ?>

                <input type="hidden" name="service_id" value="<?php echo $service->service_id; ?>" />
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('title'); ?></label>
                    <input type="text" name="title" value="<?php echo $service->title; ?>"  class="form-control" />
                </div>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('icon'); ?></label>
                    <input type="file" name="icon"  class="form-control" />
                    <div class="help-block">Select Icon to change</div>
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('description'); ?></label>
                    <textarea name="description" class="form-control"><?php echo $service->description; ?></textarea>
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('price'); ?></label>
                    <input type="text" name="price" value="<?php echo $service->price; ?>"  class="form-control" />
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('sort_order'); ?></label>
                    <input type="text" name="sort_order" value="<?php echo $service->sort_order; ?>"  class="form-control" />
                </div>
                
                <div class="box-footer">
                    <input type="button" name="button" id="button" value="<?php echo $this->lang->line('update_service'); ?>" onclick="update_service()" class="btn btn-primary" />
                </div>	
                
            </form>
            
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<script>
    
$(function(){
    $('.services').addClass('active');
});    

function update_service(){
    
    var formData = new FormData($("#updateform")[0]);//$('form')[0]

    $.ajax({
        type: "POST",
        url: "<?php echo base_url('services/updateProcess'); ?>",
        data: formData,//$("#updateform").serialize(),//formData,
        processData: false, // Don't process the files
        contentType: false,
        beforeSend : function(){ 
            $("#submitbutton").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />'); 
        },
        success: function(msg)
        {
            var data = JSON.parse(msg);
            
            $('body,html').animate({ scrollTop: 0 }, 200);
            $("#ajax").html(data.msg); 
            $("#submitbutton").html('<input type="button" name="button" id="button" value="<?php echo $this->lang->line('update_service'); ?>" onclick="update_service()" class="stbutton"/>');
        }
    });
}
</script>

