<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	    <i class="fa fa-shopping-cart"></i>
        <?php echo $this->lang->line('buy_service'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <div id="ajax">            
        <?php echo $this->session->flashdata('message'); ?>
    </div>    
    
    <div class="box">
        <div class="box-body">                        
            <div class='col-md-12'>
                
                <table class='table table-bordered'>
                    <tr>
                        <td><b><?= $this->lang->line('title') ?></b></td>
                        <td><?= $service->title ?></td>
                    </tr>
                    <tr>
                        <td><b><?= $this->lang->line('description') ?></b></td>
                        <td><?= $service->description ?></td>
                    </tr>
                </table>
                
                <div class='col-md-5'>    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <?= $this->lang->line('pay_with_paypal') ?>
                        </div>
                        <div class="panel-body">

                            <?php echo form_open(
                                $paypal_action,
                                'method="post" id="paypal_form"'
                              );
                            ?>
                                
                                <div class="form-group">
                                  <lable>Order Comment <small>(optional)</small></lable>
                                    <textarea placeholder="Note for us" name="comment" class="form-control"></textarea>
                                </div>
  
                                <center>
                                <?php if($service->price > 0){ ?>                                
                                    <button class="btn btn-success btn-lg">
                                        <?= $this->lang->line('pay_now') ?> 
                                        (<?= $this->currency->format($service->price) ?>)
                                    </button>
                                <?php }else{ ?>
                                
                                    <input type="text" class="form-control input-lg" name="price" placeholder="Enter Amount" />
                                
                                    <br />
                                    
                                    <button class="btn btn-success btn-lg">
                                        <?= $this->lang->line('pay_now') ?>
                                    </button>
                                <?php } ?>
                                </center>    
                                    
                            </form>    
                        </div>
                    </div>
                </div>
                <div class='col-md-7'>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <?= $this->lang->line('pay_with_ceredit_card') ?></b>
                        </div>
                        <div class="panel-body">

                            <?php echo form_open(
                                '',
                                'class="horizontal-form" id="auth-form"'
                              );
                            ?>
                                
                                <div class="msg_wrapper"></div>
                                
                                <div class="form-group">
                                  <lable>Order Comment <small>(optional)</small></lable>
                                    <textarea placeholder="Note for us" name="comment" class="form-control"></textarea>
                                </div>
                                
                                <div class="form-group">
                                    <lable><?= $this->lang->line('card_number') ?></lable>
                                    <input class="form-control" name="cardNumber" value="" />
                                </div>
                                <div class="form-group">
                                    <lable><?= $this->lang->line('expiration_date') ?></lable>
                                    <input data-inputmask='"mask": "99/99"' data-mask class="form-control" name="expirationDate" placeholder="DD/YY" value="" />
                                </div>
                                <div class="form-group">
                                    <lable><?= $this->lang->line('cvv_code') ?></lable>
                                    <input class="form-control" name="securityCode" placeholder="" value="" />
                                </div>
                                <?php if($service->price <= 0){ ?>   
                                <div class="form-group">
                                    <lable><?= $this->lang->line('amount') ?></lable>
                                    <input type="text" class="form-control" name="price" placeholder="<?= $this->lang->line('enter_amount') ?>" />
                                </div>
                                <?php } ?>                                
                                <div class="form-action">
                                    <input type="button" onclick="pay();" class="btn-auth-pay btn btn-warning" value="<?= $this->lang->line('pay_now') ?>" />
                                </div>                            
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix" />
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<script src='application/views/themes/default/js/plugins/input-mask/jquery.inputmask.js'></script>

<script>
        
$(function(){
    $('.services').addClass('active');
    $("input[name='expirationDate']").inputmask();
});    

$('#paypal_form').on('submit', function(){
    if($('#paypal_form input[type="text"]').length > 0){
       if($('#paypal_form input[type="text"]').val().length == 0){
          $('#paypal_form input[type="text"]').css('border','1px solid red'); 
          return false;
       } 
    }
});

//pay with authorize
function pay(){
    
    $is_valid = true;
    
    $('#auth-form input').each(function(){
       if($(this).val().length == 0){
           $(this).css('border', '1px solid red');
           $is_valid = false;
       }else{
           $(this).css('border', '1px solid #d2d6de');
       }
    });
    
    if($is_valid){
        $('.btn-auth-pay').val('sending data...').attr('disabled','disabled');

        $.post('<?= site_url("authorize/pay") ?>', $('#auth-form').serialize(), function(data){
            var data = JSON.parse(data);

            if(data.status){
                location = '<?= site_url("services") ?>';
            }else{
                $('#auth-form .msg_wrapper').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data.msg+'</div>');
                $('.btn-auth-pay').val('Pay Now').removeAttr('disabled');
            }
        });
    }
}
</script>

