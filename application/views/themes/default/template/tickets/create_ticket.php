<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('create_a_new_ticket'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('create_a_new_ticket'); ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">

            <div id="ajax"></div>

            <?php echo form_open(
                '',
                'id="createform"'
              );
            ?>
            
                <div class="form-group">
                  <label for="order_id"><?php echo $this->lang->line('order_id'); ?></label>
                  <input type="text" id="order_id" name="order_id" value="" maxlength="255"  class="form-control" />
                </div>

                <div>
                    <label style="padding-right: 10px;">
                        <input type="radio" name="type" value="product" checked="" />
                        <?php echo $this->lang->line('product'); ?>
                    </label>   
                    <label>
                        <input type="radio" name="type" value="service" />
                        <?php echo $this->lang->line('service'); ?>
                    </label>    
                </div>

                <div class="form-group" id="product">
                  <select name="product_id" class="form-control">
                      <option></option>
                      <?php foreach($products as $product){ ?>
                      <option value="<?= $product['product_id'] ?>"><?= $product['title'] ?></option>
                      <?php } ?>
                  </select>
                </div>
            
                <div class="form-group" id="service" style="display: none;">
                  <select name="service_id" class="form-control">
                      <option></option>
                      <?php foreach($services as $service){ ?>
                      <option value="<?= $service['service_id'] ?>"><?= $service['title'] ?></option>
                      <?php } ?>
                  </select>
                </div>
            
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $this->lang->line('title'); ?></label>
                  <input type="text" name="title" value="" maxlength="255"  class="form-control" />
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1"><?php echo $this->lang->line('department'); ?></label>
                  <select name="department" id="department" class="form-control">
                        <option selected="selected"><?php echo $this->lang->line('select_a_department'); ?></option>
                        <?php
                        if (!empty($departments)) {
                            foreach ($departments as $department) {
                                echo '<option value="' . $department->id . '">' . $department->department_name . '</option>';
                            }
                        }
                        ?>
                  </select>
                </div>
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('priority'); ?></label>
                    <select name="priority" id="priority" class="form-control">
                        <option value="1"><?php echo $this->lang->line('low'); ?></option>
                        <option value="2"><?php echo $this->lang->line('medium'); ?></option>
                        <option value="3"><?php echo $this->lang->line('high'); ?></option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('ftp_db_access'); ?></label>
                    <textarea name="ftp_db_access" id="ftp_db_access" class="ckeditor"></textarea>
                </div>	
            
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('message'); ?></label>
                    <textarea name="message" id="message" class="ckeditor"></textarea>
                </div>	                
                
                <div id="submitbutton">

                    <!-- file uploaded -->
                    <div id="file-uploaded">                                    
                        <input id="file_list" type="hidden" name="files" value="" />
                    </div>                    
                    <input multiple="" name="files[]" type="file" style="display: none;" />                    
                    <button type="button" id="btn-upload" class="pull-left btn btn-default"><i class="fa fa-paperclip"></i></button>

                    <div class="pull-left btn_wrapper" style="margin-left: 10px;">
                        <button class="btn btn-primary" onclick="create_ticket()" type="button"><?php echo $this->lang->line('submit_a_new_ticket'); ?></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<script src="<?= base_url() ?>application/third_party/ckeditor/ckeditor.js"></script>

<script>
 $(document).ready(function(){
    $('.create_a_new_ticket').addClass('active');
 });

 $('input[name="type"]').change(function(){
    $type = $(this).val();

    $('select[name="product_id"]').val('');
    $('select[name="service_id"]').val('');
       
    if($type === 'product') {
        $('#product').show();
        $('#service').hide();
    } else {
       $('#product').hide();
       $('#service').show();
    }
 });
 
 function create_ticket(){
    
    $('#ftp_db_access').val(CKEDITOR.instances['ftp_db_access'].getData());
    $('#message').val(CKEDITOR.instances['message'].getData());
     
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('tickets/createProcess'); ?>",
        data: $("#createform").serialize(),
        beforeSend : function(msg){ 
            $(".btn_wrapper").html('<img src="<?php echo base_url('images/loading.gif'); ?>" />');
        },
        success: function(msg){			
            
            $('body,html').animate({ scrollTop: 0 }, 200);
            
            var data = JSON.parse(msg);
            
            if(data.msg.substring(1,7) != 'script'){
                $("#ajax").html(data.msg);                 
            }else{ 
                $("#ajax").html(data.msg); 
            }

            $(".btn_wrapper").html('<button class="btn btn-primary" onclick="create_ticket()" type="button"><?php echo $this->lang->line('submit_a_new_ticket'); ?></button>');
        }
    });
 }

$(document).delegate('#btn-upload','click',function(){
   $('input[name="files[]"]').trigger('click'); 
});

$(document).delegate("input:file",'change', function(){
    upload_file();
});

function upload_file(){

    var formData = new FormData($("#createform")[0]);//$('form')[0]

    $.ajax({
        type: "POST",
        url: "<?php echo base_url('tickets/upload'); ?>",
        data: formData,//$("#updateform").serialize(),//formData,
        processData: false, // Don't process the files
        contentType: false,
        beforeSend : function(){ 
            $("#btn-upload").html('<i class="fa fa-spinner fa-spin-custom"></i>'); 
        },
        complete :function(){
            $("#btn-upload").html('<i class="fa fa-paperclip"></i>');
        },
        success: function(data){                                
            if(data.status){
                $("#file-uploaded").html(data.html);          
            }else{
                alert(data.msg);
            }
        }
    });    
}

$('.courses').addClass('active');

</script>




