<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-ticket"></i>
        <?php echo $this->lang->line('open_tickets'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <?php if(userdata('department') != 0){ ?>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <?php echo $this->lang->line('filter_tickets'); ?>
            </h3>
            <div class="pull-right">
                <button class="btn btn-primary" onclick="filter();">
                    <i class="fa fa-search"></i>
                    <?= $this->lang->line('filter') ?>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">

            <?php echo form_open(
                '',
                'class="ticket_filter_form"'
              );
            ?>

                <div class="col-md-6">
                    <div class="form-group">
                        <label><?= $this->lang->line('customer_name') ?></label>
                        <input class="form-control" name="filter_customer_name" value="<?= $filter_customer_name ?>" />
                    </div>
                    <?php if(userdata('department') == 1){ ?>
                    <div class="form-group">
                        <label><?= $this->lang->line('department') ?></label>
                        <select class="form-control" name="filter_department">
                            
                            <?php if(!$filter_department){ ?>
                                <option selected="" value=""><?= $this->lang->line('all') ?></option>
                            <?php }else{ ?>    
                                <option value=""><?= $this->lang->line('all') ?></option>
                            <?php } ?>
                                
                            <?php foreach($departments as $department){ ?>
                                <?php if($department['id'] == $filter_department){ ?>
                                    <option selected="" value="<?= $department['id'] ?>"><?= $department['department_name'] ?></option>
                                <?php }else{ ?>
                                    <option value="<?= $department['id'] ?>"><?= $department['department_name'] ?></option>
                                <?php } ?>
                            <?php } ?>    
                        </select>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label><?= $this->lang->line('customer_email') ?></label>
                        <input class="form-control" name="filter_customer_email" value="<?= $filter_customer_email ?>" />
                    </div>
                    <?php if(userdata('department') == 1){ ?>
                    <div class="form-group">
                        <label><?= $this->lang->line('agent') ?></label>
                        <input type="hidden" name="filter_agent" value="<?= $filter_agent ?>" />
                        <input class="form-control" name="filter_agent_name" value="<?= $filter_agent_name ?>" />
                    </div>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
    <?php } ?>
    
    <div class="box" id="open_tickets">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('open_tickets'); ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="10%"><?php echo $this->lang->line('date'); ?></th>
                        <th width="10%"><?php echo $this->lang->line('department'); ?></th>
                        <th width="25%"><?php echo $this->lang->line('title'); ?></th>
                        <th width="15%"><?php echo $this->lang->line('agent'); ?></th>
                        <th width="15%"><?php echo $this->lang->line('customer'); ?></th>
                        <th width="5%"><?php echo $this->lang->line('priority'); ?></th>
                        <th width="20%"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($open_tickets)) { ?>
                        <?php foreach ($open_tickets as $ticket) { ?>
                            <?php
                            switch ($ticket->ticket_status) {
                                case 1: $status_img = '<span class="open">' . $this->lang->line('open') . '</span>';
                                    break;
                                case 2: $status_img = '<span class="wait1">' . $this->lang->line('pending') . '</span>';
                                    break;
                                case 3: $status_img = '<span class="close">' . $this->lang->line('close') . '</span>';
                                    break;
                            }
                            ?>
                            <tr id="ticket_id_<?php echo $ticket->id; ?>">
                                <td><?php echo date('d M Y g:i a', $ticket->modified_time); ?></td>
                                <td><?php echo $ticket->department_name; ?></td>
                                <td><?php echo $ticket->title; ?></td>
                                <td><?php echo $ticket->name; ?></td>
                                <td><?php echo $ticket->customer; ?></td>
                                <td>
                                    <?php
                                    // Priority
                                    switch ($ticket->priority) {
                                        case 1: echo '<label class="label label-success">' . $this->lang->line('low') . '</label>' ;
                                            break;
                                        case 2: echo '<label class="label label-warning">' . $this->lang->line('medium') . '</label>';
                                            break;
                                        case 3: echo '<label class="label label-danger">' . $this->lang->line('high') . '</label>';
                                            break;
                                    }
                                    ?>
                                </td>
                                <td class="text-right">

                                    <?php if (userdata('department') == 1) { ?>
                                        <a class="assing_ticket_to_agent btn bg-green btn-sm" data-toggle="tooltip" data-title="<?= $this->lang->line('assign') ?>" data-id="<?php echo $ticket->id; ?>">
                                            <i class="fa fa-exchange"></i> 
                                        </a>&nbsp;&nbsp;
                                    <?php } ?>

                                    <a class="btn bg-orange btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('ticket_details'); ?>" href="<?php echo base_url('tickets/id/' . $ticket->id); ?>">
                                        <i class="fa fa-file-text-o"></i> 
                                    </a>&nbsp;&nbsp;
                                    
                                    <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('close'); ?>" href="javascript:void(0)" onclick="close_ticket(<?php echo $ticket->id; ?>)" id="closebutton_<?php echo $ticket->id; ?>">
                                        <i class="fa fa-check-square"></i> 
                                    </a>
                                </td>
                            </tr>
                    <?php } ?>
                <?php } else { ?> 
                            <tr><td colspan="7"><h2><?php echo $this->lang->line('any_ticket'); ?></h2></td></tr>
                 <?php } ?>
                </tbody>
            </table>
            <div class="bottombar"> </div>
        </div><!-- END .box-body -->
    </div><!-- END .box -->

    <div class="box" id="pending_tickets">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('pending_tickets'); ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="10%"><?php echo $this->lang->line('date'); ?></th>
                        <th width="10%"><?php echo $this->lang->line('department'); ?></th>
                        <th width="25%"><?php echo $this->lang->line('title'); ?></th>
                        <th width="15%"><?php echo $this->lang->line('agent'); ?></th>
                        <th width="15%"><?php echo $this->lang->line('customer'); ?></th>
                        <th width="5%"><?php echo $this->lang->line('priority'); ?></th>
                        <th width="20%"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($pending_tickets)) { ?>
                        <?php foreach ($pending_tickets as $ticket) { ?>
                            <?php
                            switch ($ticket->ticket_status) {
                                case 1: $status_img = '<span class="open">' . $this->lang->line('open') . '</span>';
                                    break;
                                case 2: $status_img = '<span class="wait1">' . $this->lang->line('pending') . '</span>';
                                    break;
                                case 3: $status_img = '<span class="close">' . $this->lang->line('close') . '</span>';
                                    break;
                            }
                            ?>
                            <tr id="ticket_id_<?php echo $ticket->id; ?>">
                                <td><?php echo date('d M Y g:i a', $ticket->modified_time); ?></td>
                                <td><?php echo $ticket->department_name; ?></td>
                                <td><?php echo $ticket->title; ?></td>
                                <td><?php echo $ticket->name; ?></td>
                                <td><?php echo $ticket->customer; ?></td>
                                <td>
                                    <?php
                                    // Priority
                                    switch ($ticket->priority) {
                                        case 1: echo '<label class="label label-success">' . $this->lang->line('low') . '</label>' ;
                                            break;
                                        case 2: echo '<label class="label label-warning">' . $this->lang->line('medium') . '</label>';
                                            break;
                                        case 3: echo '<label class="label label-danger">' . $this->lang->line('high') . '</label>';
                                            break;
                                    }
                                    ?>
                                </td>
                                <td class="text-right">

                                    <?php if (userdata('department') == 1) { ?>
                                        <a class="assing_ticket_to_agent btn bg-green btn-sm" data-toggle="tooltip" data-title="<?= $this->lang->line('assign') ?>" data-id="<?php echo $ticket->id; ?>">
                                            <i class="fa fa-exchange"></i> 
                                        </a>&nbsp;&nbsp;
                                    <?php } ?>

                                    <a class="btn bg-orange btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('ticket_details'); ?>" href="<?php echo base_url('tickets/id/' . $ticket->id); ?>">
                                        <i class="fa fa-file-text-o"></i> 
                                    </a>&nbsp;&nbsp;
                                    
                                    <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('close'); ?>" href="javascript:void(0)" onclick="close_ticket(<?php echo $ticket->id; ?>)" id="closebutton_<?php echo $ticket->id; ?>">
                                        <i class="fa fa-check-square"></i> 
                                    </a>
                                </td>
                            </tr>
                    <?php } ?>
                <?php } else { ?> 
                            <tr><td colspan="7"><h2><?php echo $this->lang->line('any_ticket'); ?></h2></td></tr>
                 <?php } ?>
                </tbody>
            </table>
            <div class="bottombar"> </div>
        </div><!-- END .box-body -->
    </div><!-- END .box -->
    
    <div class="box" id="close_tickets">
        <div class="box-header">
            <h3 class="box-title"><?php echo $this->lang->line('other_tickets'); ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table width="100%" class="table table-hover table-responsive ">
                <thead>
                <th width="10%"><?php echo $this->lang->line('date'); ?></th>
                <th width="10%"><?php echo $this->lang->line('department'); ?></th>
                <th width="25%"><?php echo $this->lang->line('title'); ?></th>
                <th width="15%"><?php echo $this->lang->line('agent'); ?></th>
                <th width="15%"><?php echo $this->lang->line('customer'); ?></th>
                <th width="5%"><?php echo $this->lang->line('priority'); ?></th>
                <th style="text-align: right;" width="20%"></th>
                </thead>
                <tbody id="head_tr">
                    <?php if (!empty($other_tickets)) { ?>
                        <?php foreach ($other_tickets as $ticket) { ?>

                            <tr id="ticket_id_<?php echo $ticket->id; ?>">
                                <td><?php echo date('d M Y g:i a', $ticket->modified_time); ?></td>
                                <td><?php echo $ticket->department_name; ?></td>
                                <td><?php echo $ticket->title; ?></td>
                                <td><?php echo $ticket->name; ?></td>
                                <td><?php echo $ticket->customer; ?></td>
                                <td>
                                    <?php
                                    // Priority
                                    switch ($ticket->priority) {
                                        case 1: echo '<label class="label label-success">' . $this->lang->line('low') . '</label>' ;
                                            break;
                                        case 2: echo '<label class="label label-warning">' . $this->lang->line('medium') . '</label>';
                                            break;
                                        case 3: echo '<label class="label label-danger">' . $this->lang->line('high') . '</label>';
                                            break;
                                    }
                                    ?>
                                </td>
                                <td class="text-right">

                                    <?php if (userdata('department') == 1) { ?>
                                        <a class="assing_ticket_to_agent btn bg-green btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('assign'); ?>" data-id="<?php echo $ticket->id; ?>">
                                            <i class="fa fa-exchange"></i>
                                        </a>&nbsp;&nbsp;
                                    <?php } ?>

                                    <a class="btn bg-orange btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('ticket_details'); ?>" href="<?php echo base_url('tickets/id/' . $ticket->id); ?>">
                                        <i class="fa fa-file-text-o"></i> 
                                    </a>&nbsp;&nbsp;
                                    
                                    <?php if (userdata('department') > 0) { ?>
                                        <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('delete'); ?>" href="javascript:void(0)" onclick="delete_ticket(<?php echo $ticket->id; ?>)">
                                            <i class="fa fa-check-square"></i> 
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                    <?php } ?>
                    <?php } else { ?>
                        <tr id="any_ticket_row">
                            <td colspan="7">
                                <h2><?php echo $this->lang->line('any_ticket'); ?></h2>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

            <div class="bottombar">
                <div class="actionbutton"></div>
                <div class="pagebutton">
                    <ul class="pagination">
                        <?php echo $links; ?>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div><!-- END .box-body -->
    </div><!-- End .box -->
</section><!-- End Ticket -->

<?= Modules::run('footer/footer/index') ?>

<link href="application/views/themes/default/css/jquery-ui/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="application/views/themes/default/js/plugins/jQueryUI/jquery-ui-1.10.3.min.js"></script>

<script>
    
function filter(){
    $('.ticket_filter_form').submit();
    
    $url = '<?= site_url("tickets") ?>?';
    
    if($('input[name="filter_customer_name"]').val().length > 0){
        $url += '&filter_customer_name='+$('input[name="filter_customer_name"]').val();
    }
     
    if($('select[name="filter_department"] option:selected').attr('value').length > 0){
        $url += '&filter_department='+$('select[name="filter_department"] option:selected').attr('value');
    } 
    
    if($('input[name="filter_customer_email"]').val().length > 0){
        $url += '&filter_customer_email='+$('input[name="filter_customer_email"]').val();
    }
     
    if($('input[name="filter_agent"]').val().length > 0){
        $url += '&filter_agent='+$('input[name="filter_agent"]').val();
    }
    
    location = $url;
}    

//autocomplete agent name 
$("input[name='filter_agent_name']").autocomplete({
        source: function(request, response) {
                $.ajax({
                        url: '<?= site_url("tickets/agentAutocomplete") ?>/' +  encodeURIComponent(request.term),
                        dataType: 'json',
                        success: function(json) {	
                                response($.map(json, function(item) {
                                        return {
                                                label: item.name,
                                                value: item.id,
                                        }
                                }));
                        }
                });
        }, 
        select: function(event, ui) {
                $('input[name=\'filter_agent_name\']').val(ui.item.label);
                $('input[name=\'filter_agent\']').val(ui.item.value);
                return false;
        },
        focus: function(event, ui) {
                return false;
        }
});
    
    $(function() {
        $('.all_tickets').addClass('active');
    });

<?php if (userdata('department') > 0) { ?>
        function delete_ticket(ticket_id) {
            //return confirm('Are you sure?');

            $.ajax({
                type: "GET",
                url: "<?php echo base_url('tickets/delete'); ?>/" + ticket_id,
                success: function(data)
                {
                    data = JSON.parse(data);
                    
                    if (data.status)
                    {
                        $('#ticket_id_' + ticket_id).fadeOut('normal');
                    }
                }
            });
        }
<?php } ?>

    //
    //get assign model html
    $(document).delegate('.assing_ticket_to_agent', 'click', function() {
        $.get('tickets/ticketAssign/' + $(this).attr('data-id'), function(data) {
            $('.modal').remove();
            $('<div class="modal fade">' + data + '</div>').modal();
        });
    });

//assignn
    $(document).delegate('.btn_assing_ticket_to_agent', 'click', function() {
        $.post('tickets/ticketAssign', $('#agent_form').serialize(), function(data) {
            var data = JSON.parse(data);
            if (data.status) {
                $('.modal .alert-success').removeClass('hide').html('<?= $this->lang->line('chat_assign_success') ?> ' + data.agent_name);
            } else {
                $('.modal .alert-success').removeClass('hide').html('<?= $this->lang->line('chat_error_msg') ?>');
            }
        });
    });

//filter agents
    $(document).delegate('#ticket_model select[name="department_id"]', 'change', function() {
        $.get('tickets/filterAgents/' + $(this).val(), function(data) {

            data = JSON.parse(data);

            if (data.status) {
                $('select[name="agent_id"]').html(data.html);
                $('.modal .alert').addClass('hide');
            } else {
                $('.modal .alert-success').removeClass('hide').html('<?= $this->lang->line('no_agent_found') ?>');
            }
        });
    });

    function close_ticket(ticket_id) {
        //return confirm('Are you sure?');

        $.ajax({
            type: "GET",
            url: "<?php echo base_url('tickets/close'); ?>/" + ticket_id,
            success: function(result) {
         
                $data = $.parseJSON(result);
                
                if ($data.status) {
                    
                    var ticket = $('#ticket_id_' + ticket_id).html();
                    ticket = ticket.replace('<span class="open"><?php echo $this->lang->line('open'); ?></span>', '<label class="label label-info"><?php echo $this->lang->line('close'); ?></label>');
                    ticket = ticket.replace('<span class="wait1"><?php echo $this->lang->line('pending'); ?></span>', '<label class="label label-info"><?php echo $this->lang->line('close'); ?></label>');
                    $('#ticket_id_' + ticket_id).fadeOut('normal');
                    $('#ticket_id_' + ticket_id).remove();
                    $('#head_tr').prepend('<tr id="ticket_id_' + ticket_id + '">' + ticket + '</tr>');
                    $('#closebutton_' + ticket_id).remove();
                    $('#any_ticket_row').remove();
                }
            }
        });
    }

    $(function() {
        $('.all_ticket').addClass('active');
    });
</script>
