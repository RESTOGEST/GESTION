<div class="modal-dialog">
<div class="modal-content" id="ticket_model">
    <div class="modal-header">
        <a data-dismiss="modal" class="close">×</a>
        <h4 class="modal-title"><?= $this->lang->line('assign_agent') ?></h4>
    </div>
    <div class="modal-body">
        
        <div class="alert alert-success hide"></div>

        <?php echo form_open(
            '',
            'id="agent_form"'
          );
        ?>
            
            <input type="hidden" name="ticket_id" value="<?= $ticket_id ?>" />
            
            <!-- All Departments -->
            <div class="control-group">
                <label class="control-label"><?= $this->lang->line('department') ?></label>
                <select name="department_id" class="form-control">
                    <option value=""><?= $this->lang->line('select_department') ?></option>
                    <?php foreach($departments as $department){ ?>
                    <option value="<?= $department['id'] ?>"><?= $department['department_name'] ?></option>
                    <?php } ?>
                </select>
                <p></p>
            </div>
                        
            <!-- User list -->
            <div class="control-group">
                <label class="control-label"><?= $this->lang->line('agent') ?></label>
                <select name="agent_id" class="form-control">
                </select>
            </div>
        </form>                    
    </div>
    <div class="modal-footer">
        <input type="button" class="btn btn_assing_ticket_to_agent btn-primary" value="Assign">
        <a class="btn-remove-model btn btn-default"><?= $this->lang->line('cancel') ?></a>
    </div>
</div>
</div>