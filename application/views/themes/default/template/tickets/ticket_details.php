<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        #<?php echo $ticket->id; ?> <?php echo $this->lang->line('ticket_info'); ?>
    </h1>    
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary" id="customer_detail_box">
                <div class="box-header">
                    <h3 class="box-title">
                        <?php echo $ticket->title; ?>                            
                    </h3>              
                </div><!-- /.box-header -->
                <div class="box-body">
                    <!-- Ticket info -->
                    <div id="ticketinfo" class="stbox ticketinfo">
                        <!--
                        <a href="#" class="ticketinfoclose" title="<?php echo $this->lang->line('hide'); ?>">
                        <?php echo $this->lang->line('close'); ?>
                        </a>
                        -->
                        <div class="row">
                            <div class="col-lg-12">

                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="active">
                                        <a href="#general" role="tab" data-toggle="tab">General</a>
                                    </li>
                                    <li>
                                        <a href="#detail" role="tab" data-toggle="tab">Details</a>
                                    </li>
                                    <li>
                                        <a href="#access" role="tab" data-toggle="tab">Access</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="general">
                                        <table class="table">
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('customer'); ?></strong></td>
                                                <td>:</td> 
                                                <td>
                                                    <?php echo $ticket->name; ?>                                    
                                                </td>
                                            </tr>
                                            
                                            <?php if($ticket->order_id) { ?>
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('order'); ?></strong></td>
                                                <td>:</td> 
                                                <td>
                                                    <?php echo $ticket->order_id; ?>                                    
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            
                                            <?php if($product) { ?>
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('product'); ?></strong></td>
                                                <td>:</td> 
                                                <td>
                                                    <?php echo $product; ?>                                    
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            
                                            <?php if($service) { ?>
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('service'); ?></strong></td>
                                                <td>:</td> 
                                                <td>
                                                    <?php echo $service; ?>                                    
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('department'); ?></strong></td>
                                                <td>:</td> 
                                                <td><?php echo $department; ?></td>
                                            </tr>
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('priority'); ?></strong></td>
                                                <td>:</td> 
                                                <td><?php
                                                    // Priority
                                                    switch ($ticket->priority) {
                                                        case 1: echo $this->lang->line('low');
                                                            break;
                                                        case 2: echo $this->lang->line('medium');
                                                            break;
                                                        case 3: echo $this->lang->line('high');
                                                            break;
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('email'); ?></strong></td>
                                                <td>:</td> 
                                                <td><?php echo $ticket->email; ?></td>
                                            </tr>
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('status'); ?></strong></td>
                                                <td>:</td> 
                                                <td>
                                                    <?php
                                                    switch ($ticket->ticket_status) {
                                                        case 1: echo '<label class="open">' . $this->lang->line('open') . '</span>';
                                                            break;
                                                        case 2: echo '<label class="wait1">' . $this->lang->line('pending') . '</span>';
                                                            break;
                                                        case 3: echo '<label class="">' . $this->lang->line('close') . '</span>';
                                                            break;
                                                    }
                                                    ?>
                                                </td>
                                            </tr>     
                                            <tr>
                                                <td><strong><?php echo $this->lang->line('date'); ?></strong></td>
                                                <td>:</td> 
                                                <td><?php echo date('d F Y g:i a', $ticket->modified_time); ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="detail">
                                        
                                        <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#modal_details">
                                            <i class="fa fa-pencil"></i>
                                            <?php if($ticket->details) { ?>
                                            Edit
                                            <?php }else{ ?>
                                            Add
                                            <?php } ?>
                                        </button>
                                        
                                        <div id="details_content">
                                            <?php echo $ticket->details; ?>                                         
                                        </div>
                                        
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="access">
                                        
                                        <button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#modal_access">
                                            <i class="fa fa-pencil"></i>
                                            <?php if($ticket->ftp_db_access) { ?>
                                            Edit
                                            <?php }else{ ?>
                                            Add
                                            <?php } ?>
                                        </button>
                                        
                                        <div id="access_content">
                                            <?php echo $ticket->ftp_db_access; ?>                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="clear"></div>
                    </div>
                    <!-- End Ticket info -->

                </div>        
            </div><!-- END .box-body -->
        </div>
        <div class="col-md-6">
            
            <div class="box ticketinfo  box-primary">
                <div class="box-body">
                    <div class="direct-chat-messages">
                        <?php
                        if (!empty($messages)) {
                            foreach ($messages as $message) {
                                if ($message->department == 0) {
                                    ?>

                                    <!-- Message. Default to the left -->
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left"><?php echo $this->lang->line('customer'); ?></span>
                                            <span class="direct-chat-timestamp pull-right"><?php echo date('d F Y g:i a', $message->created_time); ?></span>
                                        </div><!-- /.direct-chat-info -->
                                        <img alt="message user image" src="<?php echo base_url(); ?>images/customer-user.png" class="direct-chat-img"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            <?php echo $message->message; ?>

                                            <?php
                                            if ($message->files) {

                                                $temp = explode(',', $message->files);
                                                ?>
                                                <div class="attachments">
                                                    <h4><?php echo $this->lang->line('attach'); ?> : </h4>
                                                    <ul>
                                                        <?php
                                                        foreach ($temp as $file) {
                                                            echo '<li><a href="' . base_url('attachments/' . $file) . '" target="_blank">' . $file . '</a></li>';
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                            <?php } ?>
                                        </div><!-- /.direct-chat-text -->
                                    </div><!-- /.direct-chat-msg -->

                                <?php } else { ?>	

                                    <!-- Message to the right -->
                                    <div class="direct-chat-msg right">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-right"><?php echo $this->lang->line('support'); ?></span>
                                            <span class="direct-chat-timestamp pull-left"><?php echo date('d F Y g:i a', $message->created_time); ?></span>
                                        </div><!-- /.direct-chat-info -->
                                        <img alt="message user image" src="<?php echo base_url(); ?>images/support-user.png" class="direct-chat-img"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            <?php echo $message->message; ?>
                                            <?php
                                            if ($message->files) {

                                                $temp = explode(',', $message->files);
                                                ?>
                                                <div class="attachments">
                                                    <h4><?php echo $this->lang->line('attach'); ?> : </h4>
                                                    <ul>
                                                        <?php
                                                        foreach ($temp as $file) {
                                                            echo '<li><a href="' . base_url('attachments/' . $file) . '" target="_blank">' . $file . '</a></li>';
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                            <?php } ?>
                                        </div><!-- /.direct-chat-text -->
                                    </div><!-- /.direct-chat-msg -->        
                                    <?php
                                }
                            }
                        }
                        ?>	
                    </div><!-- END .direct-chat-messages -->

                    <div id="ajax"></div>

                    <!-- start reply -->
                    <div class="reply" id="reply">

                        <div id="upload_status" style="margin-bottom:15px;"></div>

                        <?php
                        echo form_open(
                                '', 'id="replyform"'
                        );
                        ?>

                        <div class="clearfix"></div>

                        <textarea name="message" id="message" class="ckeditor"></textarea>
                        <input type="hidden" name="ticket_id" value="<?php echo $this->uri->segment(3); ?>" />

                        <div id="loading_wrapper" style="display: none;text-align: right;margin-top:10px;">
                            <img src="<?php echo base_url('images/loading.gif'); ?>" />
                        </div>
                            
                        <div id="submitbutton" style="text-align: right;margin-top:10px;">

                            <!-- file upload -->
                            <div id="file-uploaded">                                    
                                <input id="file_list" type="hidden" name="files" value="" />
                            </div>&nbsp;&nbsp;&nbsp;

                            <input type="button" value="<?php echo $this->lang->line('close_ticket'); ?>" onclick="close_ticket(<?php echo $this->uri->segment(3); ?>);" class="btn btn-primary" style="float: left; width:150px;"/>

                            <input multiple="" name="files[]" type="file" style="display: none;" />
                            <button type="button" id="btn-upload" class="btn btn-default"><i class="fa fa-paperclip"></i></button>

                            &nbsp;

                            <button type="button" name="button" id="button" onclick="reply_process();" class="btn btn-success">
                                <?php echo $this->lang->line('send'); ?> &nbsp;<i class="fa fa-mail-forward"></i>
                            </button>
                        </div>

                        <div class="clear"></div>
                        
                        </form>
                    </div>
                    <!-- end reply -->
                </div>
            </div>

            <br />
            <br />
            
        </div>
</section>

<div id="modal_access" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">FTP/ Database/Site Access</h4>
      </div>
      <div class="modal-body">
          <textarea id="ftp_db_access" name="ftp_db_access" class="ckeditor"><?= $ticket->ftp_db_access ?></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-update-access">Submit</button>
      </div>
    </div>
  </div>
</div><!-- END #modal_details -->

<div id="modal_details" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Customer Detail</h4>
      </div>
      <div class="modal-body">
          <textarea id="details" name="details" class="ckeditor"><?= $ticket->details ?></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-update-details">Submit</button>
      </div>
    </div>
  </div>
</div><!-- END #modal_details -->

<?= Modules::run('footer/footer/index') ?>

<script src="<?= base_url() ?>application/third_party/ckeditor/ckeditor.js"></script>

<script type="text/javascript">
    
    $('#btn-update-access').click(function(){        
        $.post('<?php echo base_url('tickets/updateAccess'); ?>',
        {
            'csrf_a1support' : $('input[name="csrf_a1support"]').val(),
            'id' : '<?= $ticket->id ?>', 
            'ftp_db_access' : CKEDITOR.instances['ftp_db_access'].getData()
        }, 
        function(){
            $('#access_content').html(CKEDITOR.instances['ftp_db_access'].getData());
            $('#modal_access').modal('hide');
        });        
    });
    
    $('#btn-update-details').click(function(){
        
        $.post('<?php echo base_url('tickets/updateDetails'); ?>',
        {
            'csrf_a1support' : $('input[name="csrf_a1support"]').val(),
            'user_id' : '<?= $ticket->user_id ?>', 
            'details' : CKEDITOR.instances['details'].getData()
        }, 
        function(){
            $('#details_content').html(CKEDITOR.instances['details'].getData());
            $('#modal_details').modal('hide');
        });        
    });
    
    //get left box widh 
    $width = $('#customer_detail_box').width();
    
    $(document).scroll(function(){
        if($(window).width() > 768 && $(this).scrollTop() >= 100){
           $('#customer_detail_box').css('position','fixed');
           $('#customer_detail_box').css('top','10px');
           $('#customer_detail_box').css('width', $width);        
        }else{
           $('#customer_detail_box').css('position','relative');
           $('#customer_detail_box').css('top','auto');        
        }
    });

    $(document).delegate('#btn-upload','click',function(){
       $('input[name="files[]"]').trigger('click'); 
    });

    $(document).delegate("input:file",'change', function(){
        upload_file();
    });
   
    function upload_file(){

        var formData = new FormData($("#replyform")[0]);//$('form')[0]

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('tickets/upload'); ?>",
            data: formData,//$("#updateform").serialize(),//formData,
            processData: false, // Don't process the files
            contentType: false,
            beforeSend : function(){ 
                $("#btn-upload").html('<i class="fa fa-spinner fa-spin-custom"></i>'); 
            },
            complete :function(){
                $("#btn-upload").html('<i class="fa fa-paperclip"></i>');
            },
            success: function(data){                                
                if(data.status){
                    $("#file-uploaded").html(data.html);          
                }else{
                    alert(data.msg);
                }
            }
        });    
    }
    
    function reply_process(){     
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('tickets/replyProcess'); ?>",
            data: { csrf_a1support: $('input[name="csrf_a1support"]').val(), files : $('#file_list').val(), message : CKEDITOR.instances['message'].getData(), ticket_id : $('input[name="ticket_id"]').val() },
            beforeSend : function(){
                $("#loading_wrapper").show();
                $('#submitbutton').hide();
            },
            success: function(data){
                
                data = JSON.parse(data);
                
                if(data.html.substring(0,4) === '<div') {
                    
                    $('.direct-chat-messages').append(data.html);
                    
                    $("#ajax").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo $this->lang->line('reply_succes'); ?></div>').hide().toggle('slow'); 
                    
                } else {
                    
                    $("#ajax").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul>' + data.html + '</ul></div>'); 
                    
                }
                
                $('#loading_wrapper').hide();
                $('#submitbutton').show();
            }
        });
    }

    function close_ticket(ticket_id){
    
        $("#loading_wrapper").show();
        $('#submitbutton').hide();

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>tickets/close/" + ticket_id,
            success: function(data){      
                
                data = JSON.parse(data);

                if (data.status){
                    location.href = '<?php echo base_url('tickets/index'); ?>';
                }else{
                    alert('<?php echo $this->lang->line('technical_problem'); ?>');
                    $("#loading_wrapper").hide();
                    $('#submitbutton').show();                
                }
            }
        });
    }
    
$(function(){ 
    $('.all_tickets').addClass('active');
});

$('.courses').addClass('active');

</script>



