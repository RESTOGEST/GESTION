<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-rocket"></i>
        <?php echo $this->lang->line('paypal_error_title'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?php echo $this->session->flashdata('message'); ?>

    <div role="alert" class="alert alert-danger alert-dismissible">
        <button aria-label="Close" data-dismiss="alert" class="close" type="button">
            <span aria-hidden="true">×</span>
        </button>
        <?= $msg ?>
    </div>

    <?php if($xml){ ?>
        <pre>
            <?php print_r($xml); ?>
        </pre>
    <?php } ?>

</section>

<?= Modules::run('footer/footer/index') ?>
