<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $this->lang->line('edit_faq') ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <?php if($msg){ ?>
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= $msg ?>
        </div>
    <?php } ?>

    <?php echo form_open(
        'faq/edit/'.$row['faq_id'],
        'method="post"'
      );
    ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= $this->lang->line('edit_faq') ?></h3>            
            </div><!-- /.box-header -->
            <div class="box-body">               
                <div class="form-group">
                    <label><?= $this->lang->line('question') ?></label>
                    <textarea class="form-control" name="question"><?= set_value('question', $row['question']) ?></textarea>
                </div>
                <div class="form-group">
                    <label><?= $this->lang->line('answer') ?></label>
                    <textarea class="ckeditor form-control" name="answer"><?= set_value('answer', $row['answer']) ?></textarea>
                </div>
                <div class="form-group">
                    <label><?= $this->lang->line('sort_order') ?></label>
                    <input type="text" class="form-control" name="sort_order" value="<?= set_value('sort_order', $row['sort_order']) ?>" />
                </div>
            </div>
            <div class="box-footer">
                <button type="sumit" class="btn btn-primary"><?= $this->lang->line('submit') ?></button>
            </div>
        </div>
    </form>
</section>


<?= Modules::run('footer/footer/index') ?>

<script src="<?= base_url() ?>application/third_party/ckeditor/ckeditor.js"></script>

<script>
    $(function(){
       $('.faq_list').addClass('active'); 
    });
</script>