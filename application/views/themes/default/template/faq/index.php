<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      <i class="fa fa-question-circle"></i>
        <?= $this->lang->line('faqs') ?>
        <?php if($department == 1){ ?>
        <div class="box-tools pull-right">
            <a class="btn btn-primary" href="<?= site_url('faq/insert') ?>">
                <i class="fa fa-plus"></i> <?= $this->lang->line('add_new') ?>
            </a>
        </div>
        <?php } ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?= $msg ?>

    <br />

    <?php echo form_open(
        'faq',
        'id="search_faq"'
      );
    ?>
    
        <input placeholder="<?= $this->lang->line('search....') ?>" type="text" class="form-control pull-left" name="q" value="<?= $this->input->get('q') ?>" />
        <button class="btn btn-primary" type="submit">
            <i class="fa fa-search"></i>
        </button>
    </form>
    
    <br />

    <div class="row" id="faq_wrapper">  
        <?php if(!empty($rows[0])){ ?>
        <div class="col-lg-6">
            <?php foreach ($rows[0] as $row) { ?>
            <div id='item_<?= $row['faq_id'] ?>' class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title"><?= $row['question'] ?></h4>                        
                    <i class="fa fa-plus"></i>
                </div>
                <div class="box-body">
                    <?= word_limiter($row['answer'], 100) ?>

                    <div class="clearfix"></div>

                    <div class="pull-right">
                        <a href="<?= site_url('faq/view/' . $row['faq_id']) ?>" class="btn btn-primary btn-xs">
                            <?= $this->lang->line('read_more') ?>
                        </a>&nbsp;&nbsp;
                        <?php if ($department == 1) { ?>
                            <a href="<?= site_url('faq/edit/' . $row['faq_id']) ?>" class="btn bg-orange btn-xs"><i class="fa fa-pencil"></i>
                                <?= $this->lang->line('edit') ?>
                            </a>&nbsp;&nbsp;
                            <a onclick="delete_faq(<?= $row['faq_id'] ?>);" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>
                                <?= $this->lang->line('delete') ?>
                            </a>&nbsp;&nbsp;
                        <?php } ?>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>        
            <?php } ?>
        </div>    
        <?php } ?>
        
        <?php if(!empty($rows[1])){ ?>
        <div class="col-lg-6">
            <?php foreach ($rows[1] as $row) { ?>
                <div id='item_<?= $row['faq_id'] ?>' class="box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title"><?= $row['question'] ?></h4>
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="box-body">
                            <?= word_limiter($row['answer'], 100) ?>

                            <div class="clearfix"></div>

                            <div class="pull-right">
                                <a href="<?= site_url('faq/view/' . $row['faq_id']) ?>" class="btn btn-primary btn-xs">
                                    <?= $this->lang->line('read_more') ?>
                                </a>&nbsp;&nbsp;
                                <?php if ($department == 1) { ?>
                                    <a href="<?= site_url('faq/edit/' . $row['faq_id']) ?>" class="btn bg-orange btn-xs"><i class="fa fa-pencil"></i>
                                        <?= $this->lang->line('edit') ?>
                                    </a>&nbsp;&nbsp;
                                    <a onclick="delete_faq(<?= $row['faq_id'] ?>);" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>
                                        <?= $this->lang->line('delete') ?>
                                    </a>&nbsp;&nbsp;
                                <?php } ?>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                    </div>        
            <?php } ?>
        </div>        
        <?php } ?>
    </div>

    <div class="pagination">
        <ul class="pagination">
            <?php echo $links; ?>
        </ul>
    </div>

</section>

<?= Modules::run('footer/footer/index') ?>

<script>

$(function(){
   $('.faq_list').addClass('active');
   
   $('#faq_wrapper .box-header').click(function(){
        if($(this).hasClass('expanded')){
            $(this).parent().find('.box-body').slideUp();//'slow'
            $(this).removeClass('expanded');            
            $(this).find('.fa').addClass('fa-plus').removeClass('fa-minus');
        }else{
            $(this).parent().find('.box-body').slideDown();//'slow'
            $(this).addClass('expanded');
            $(this).find('.fa').removeClass('fa-plus').addClass('fa-minus');
        }
   });   
});

function delete_faq(id){
    if(confirm('Are you sure?')){
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('faq/delete' ); ?>/" + id,
            success: function(msg)
            {
               $('#item_' + id).fadeOut('normal');
            }
        });
    }
}

</script>
