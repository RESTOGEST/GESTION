<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      <i class="fa fa-home"></i>
        <?php echo $this->lang->line('statistics'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?php echo $this->session->flashdata('message'); ?>

    <!--
    //total users
        //total tickets
        //total open tickets
        //total close tickets
        //total customers
        //total departments

        //total pending tickets
        //total banned users
        //total messages
    -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-custome-d">
                <div class="inner">
                    <h3><?= $total_pending_tickets  ?></h3>
                    <p><?= $this->lang->line('total_pending_tickets') ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-tags"></i>
                </div>
                <a class="small-box-footer" href="tickets/index#pending_tickets">
                    <?= $this->lang->line('more_info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-teal-active">
                <div class="inner">
                    <h3><?= $total_open_tickets  ?></h3>
                    <p><?= $this->lang->line('total_open_tickets') ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-tags"></i>
                </div>
                <a class="small-box-footer" href="tickets/index#open_tickets">
                    <?= $this->lang->line('more_info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-light-blue">
                <div class="inner">
                    <h3><?= $total_tickets ?></h3>
                    <p><?= $this->lang->line('total_tickets') ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-rocket"></i>
                </div>
                <a class="small-box-footer" href="tickets/index">
                    <?= $this->lang->line('more_info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3><?= $total_close_tickets ?></h3>
                    <p><?= $this->lang->line('total_close_tickets') ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-check-square"></i>
                </div>
                <a class="small-box-footer" href="tickets/index#close_tickets">
                    <?= $this->lang->line('more_info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div>

     <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $total_users-1 ?></h3>
                    <p><?= $this->lang->line('total_users') ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a class="small-box-footer" href="user/userList">
                    <?= $this->lang->line('more_info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= $total_customers ?></h3>
                    <p><?= $this->lang->line('total_customers') ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                </div>
                <a class="small-box-footer" href="customer/customerList">
                    <?= $this->lang->line('more_info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-olive">
                <div class="inner">
                    <h3><?= $total_departments ?></h3>
                    <p><?= $this->lang->line('total_departments') ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-building"></i>
                </div>
                <a class="small-box-footer" href="departments/index">
                    <?= $this->lang->line('more_info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $total_banned_customers ?></h3>
                    <p><?= $this->lang->line('total_banned_customers') ?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-ban"></i>
                </div>
                <a class="small-box-footer" href="customer/customerList?filter_banned=1">
                    <?= $this->lang->line('more_info') ?>
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div style="box-shadow: 0 0 6px 0 rgba(34, 45, 50, 0.7);" class="box box-solid bg-yellow-gradient">
                <div class="box-header">
                    <h3 class="box-title"><?= $this->lang->line('tickets_graph') ?></h3>
                    <div class="pull-right box-tools">
                        <button title="" data-toggle="tooltip" class="btn btn-grey btn-sm daterange pull-right" data-original-title="Date Range">
                            <i style="color:black;" class="fa fa-calendar"></i>
                        </button>
                        <div class="ranges hide">
                            <ul>
                                <li data-value="today"><?= $this->lang->line('today') ?></li>
                                <li data-value="week"><?= $this->lang->line('week') ?></li>
                                <li data-value="month"><?= $this->lang->line('month') ?></li>
                                <li data-value="year" class="active"><?= $this->lang->line('year') ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    <div style="height: 250px;" id="line-chart" class="chart"></div>
                </div>
            </div>
        </div>
    </div><!-- END graph -->
</section>

<?= Modules::run('footer/footer/index') ?>

<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="application/views/themes/default/js/plugins/morris/morris.min.js"></script>
<link rel="stylesheet" type="text/css" href="application/views/themes/default/js/plugins/morris/morris.css" />

<script>

    var line = null;

    $(function(){
       $('.dashboard').addClass('active');

       //initialize graph
       line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            redraw: true,
         //   data: [{'time':o, ],
            xkey: 'time',
            ykeys: ['total','closed'],
            labels: ['Total','Closed'],
            parseTime: false,
            lineColors: ['#DD4B39', '#3c8dbc'],//['#efefef','#efefef'],
            lineWidth: 2,
            hideHover: 'auto',
            gridTextColor: "#fff",
            gridStrokeWidth: 0.4,
            pointSize: 4,
            pointStrokeColors: ["#efefef"],
            gridLineColor: "#efefef",
            gridTextFamily: "Open Sans",
            gridTextSize: 10,
        });

        get_graph('year');

        $('.sidebar-toggle').click(function(){
           $(window).delay('slow').trigger('resize');
        });
    });

    $('.daterange').click(function(){
        $('.ranges').toggleClass('hide');
    });

    $('.ranges li').click(function(){
        $('.ranges .active').removeClass('active');
        $(this).addClass('active');
        $('.ranges').addClass('hide');
        get_graph($(this).attr('data-value'));
    });

    function get_graph($range){

        $.get('graph/getGraphData/'+$range,function(data){
            line.setData(JSON.parse(data));
        });
    }
</script>
