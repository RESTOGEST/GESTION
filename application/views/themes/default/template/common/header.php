<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo config('site_name'); ?></title>
        <base href="<?= base_url() ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <!-- Bootstrap 3.3.2 -->
        <link href="application/views/themes/default/css/bootstrap.min.css?v=1" rel="stylesheet" type="text/css" />

        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css?v=1" rel="stylesheet" type="text/css" />
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css?v=1" rel="stylesheet" type="text/css" />
        
        <!-- Theme style -->
        <link href="application/views/themes/default/css/A1support.min.css?v=1" rel="stylesheet" type="text/css" />
        
        <!-- A1support Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link href="application/views/themes/default/css/skins/_all-skins.min.css?v=1" rel="stylesheet" type="text/css" />
        
        <!-- Custome -->
        <link href="application/views/themes/default/css/custome.css?v=1" rel="stylesheet" type="text/css" />
        
        <!-- Chat -->
        <?php if(userdata('department') > 1) { ?>
        <link href="application/views/themes/default/css/chat_agent.css?v=1" rel="stylesheet" type="text/css" />
        <?php } else { ?>
        <link href="application/views/themes/default/css/chat_customer.css?v=1" rel="stylesheet" type="text/css" />
        <?php } ?>
        
        <link rel="icon" type="image/png" href="images/favicon.png" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style>
            .color-palette {
                height: 50px;
                line-height: 50px;
                text-align: center;

            }
            .color-palette-set {
                margin-bottom: 15px;
            }
            .color-palette span {
                display: none;
            }
            .color-palette:hover span {
                display: block;
            }
        </style>
    </head>
    <body class="skin-blue">
        <div class="wrapper">

            <header class="main-header">
                <a href="<?= base_url() ?>" class="logo">
                    <img src="<?php echo base_url(); ?>images/logo-white.png" alt="Logo" title="A1 Support Desk" width="40px"/> Support Desk
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <?php if (isLogin()) { ?>
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                
                                <?php if(userdata('department') == 0){ ?>
                                <li class="dropdown start_chat_wrapper <?php if(!userdata('is_chat_banned')) echo 'hide'; ?>">
                                    <a id="start_chat" href="#">
                                        <i class="fa fa-comments-o"></i> Start chat
                                    </a>
                                </li>
                                <?php } ?>
                                
                                <!-- hide from admin -->
                                <?php if(userdata('department') != 1){ ?>
                                <li>
                                    <a id="toogle_sound">
                                        <?php if($play_sound){ ?>
                                            <i class="fa fa-bell-o"></i>
                                        <?php } else { ?>
                                            <i class="fa fa-bell-slash-o"></i>
                                        <?php } ?>
                                    </a>
                                </li>
                                <li class="dropdown messages-menu chat-notification show-on-hover">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                        <i class="fa fa-comment"></i> <span class="chat-count badge up badge-success">0</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">
                                            <i class="fa fa-comment"></i>
                                            <span class="chat-count">0</span> <?= $this->lang->line('new_messages') ?>
                                        </li>
                                        <li>
                                            <ul class="menu list-unstyled" id="chat-notification-list"></ul>
                                        </li>
                                    </ul>
                                </li>
                                <?php } ?>
                                
                                <!-- User Account: style can be found in dropdown.less -->
                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <?php if (userdata('department') == 0) { ?>
                                            <img src="images/customer-user.png" class="user-image" alt="User Image"/>
                                        <?php } else { ?>
                                            <img src="images/support-user.png" class="user-image" alt="User Image"/>
                                        <?php } ?>
                                        <span class="hidden-xs username">
                                            <?= $name ?>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <!-- User image -->
                                        <li class="user-header">
                                            <?php if (userdata('department') == 0) { ?>
                                                <img src="images/customer-user.png" class="img-circle" alt="User Image" />
                                            <?php } else { ?>
                                                <img src="images/support-user.png" class="img-circle" alt="User Image"/>
                                            <?php } ?>
                                            <p class="username">
                                                <?= $name ?>
                                            </p>
                                        </li>
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="<?= site_url('user/settings') ?>" class="btn btn-default btn-flat"><?= $this->lang->line('settings') ?></a>
                                            </div>
                                            <div class="pull-right">
                                                <a href="<?php echo base_url('user/logout'); ?>" class="btn btn-default btn-flat"><?php echo $this->lang->line('logout'); ?></a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->

                    <?php if (isLogin()) { ?>
                        <div class="user-panel">
                            <div class="pull-left image">
                                <?php if (userdata('department') == 0) { ?>
                                    <img src="images/customer-user.png" class="img-circle" alt="User Image" />
                                <?php } else { ?>
                                    <img src="images/support-user.png" class="user-image" alt="User Image"/>
                                <?php } ?>
                            </div>
                            <div class="pull-left info">
                                <p class="username"><?= $name ?></p>
                                <a href="#"><i class="fa fa-circle text-success"></i> <?= $this->lang->line('online') ?></a>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">

                        <?php if (isLogin()) { ?>
                        
                        <?php if (userdata('department') == 1) { ?>
                        <li class="dashboard">
                            <a href="<?php echo base_url('index'); ?>">
                                <i class="fa fa-home"></i>
                                <?php echo $this->lang->line('dashboard'); ?>
                            </a>
                        </li>
                        <?php } ?>
                        
                        <li class="products">
                            <a href="<?php echo site_url('products'); ?>">
                                <i class="fa fa-cube"></i>
                                <?php echo $this->lang->line('products'); ?>
                            </a>
                        </li> 
                        
                        <li class="services">
                            <a href="<?php echo site_url('services'); ?>">
                                <i class="fa fa-rocket"></i>
                                <?php echo $this->lang->line('services'); ?>
                            </a>
                        </li> 

                        <?php if(userdata('department') != 0){ ?>
                        <li class="orders">
                            <a href="<?php echo site_url('services/orders'); ?>">
                                <i class="fa fa-dropbox"></i>
                                <?php echo $this->lang->line('orders'); ?>
                            </a>
                        </li> 
                        <?php } ?>
                        
                            <?php if (userdata('department') == 0) { ?>
                                <li class="faq_list">
                                    <a href="<?php echo base_url('faq'); ?>">
                                        <i class="fa fa-question-circle"></i>
                                        <?= $this->lang->line('faq') ?>
                                    </a>
                                </li>
                                <li class="feature_list">
                                    <a href="<?php echo base_url('features'); ?>">
                                        <i class="fa fa-plus-circle"></i>
                                        <?= $this->lang->line('features') ?>
                                    </a>
                                </li>
                                <li class="bug_list">
                                    <a href="<?php echo base_url('bugs'); ?>">
                                        <i class="fa fa-bug"></i>
                                        <?= $this->lang->line('report_bug') ?>
                                    </a>
                                </li>
                                <li class="create_a_new_ticket">
                                    <a href="<?php echo base_url('tickets/create'); ?>">
                                        <i class="fa fa-ticket"></i>
                                        <?php echo $this->lang->line('create_a_new_ticket'); ?>
                                    </a>
                                </li>
                                <li class="all_ticket">
                                    <a href="<?php echo base_url('tickets/index'); ?>">
                                        <i class="fa fa-ticket"></i>
                                        <?php echo $this->lang->line('all_tickets'); ?>
                                    </a>
                                </li>
                                <li class="settings">
                                    <a  href="<?php echo base_url('user/settings'); ?>">
                                        <i class="fa fa-cog"></i>
                                        <?php echo $this->lang->line('settings'); ?>
                                    </a>
                                </li>
                            <?php } elseif (userdata('department') == 1) { ?>

                                <li class="notice_list">
                                    <a href="<?php echo base_url('notice'); ?>">
                                        <i class="fa fa-exclamation-triangle"></i>
                                        <?= $this->lang->line('notice') ?>
                                    </a>
                                </li>
                                <li class="faq_list">
                                    <a href="<?php echo base_url('faq'); ?>">
                                        <i class="fa fa-question-circle"></i>
                                        <?= $this->lang->line('faq') ?>
                                    </a>
                                </li>
                                <li class="feature_list">
                                    <a href="<?php echo base_url('features'); ?>">
                                        <i class="fa fa-plus-circle"></i>
                                        <?= $this->lang->line('features') ?>
                                    </a>
                                </li>
                                <li class="bug_list">
                                    <a href="<?php echo base_url('bugs'); ?>">
                                        <i class="fa fa-bug"></i>
                                        <?= $this->lang->line('bugs') ?>
                                    </a>
                                </li>
                                <li class="customer_list">
                                    <a href="<?php echo base_url('customer/customerList'); ?>"><i class="fa fa-users"></i><?php echo $this->lang->line('customer_management'); ?></a>
                                </li>
                                <li class="user_list">
                                    <a href="<?php echo base_url('user/userList'); ?>"><i class="fa fa-user"></i><?php echo $this->lang->line('user_management'); ?></a>
                                </li>
                                
                                <li class="departments">
                                    <a href="<?php echo base_url('departments/index'); ?>"><i class="fa fa-building"></i><?php echo $this->lang->line('departments'); ?></a>
                                </li>
                                <li class="all_tickets">
                                    <a href="<?php echo base_url('tickets/index'); ?>"><i class="fa fa-ticket"></i><?php echo $this->lang->line('all_tickets'); ?></a>
                                </li>
                                <li class="all_chats">
                                  <a href="#">
                                    <i class="fa fa-comments-o"></i> <span>Chat</span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                  </a>
                                  <ul class="treeview-menu">
                                    <li class="chat_history">
                                        <a href="<?php echo base_url('chat/index'); ?>">
                                            <i class="fa fa-circle-o"></i>
                                            <?php echo $this->lang->line('chat_history'); ?>
                                        </a>
                                    </li>
                                    <li class="visitor">
                                        <a href="<?php echo base_url('chat/visitor'); ?>">
                                            <i class="fa fa-circle-o"></i>
                                            <?= $this->lang->line('visitors') ?>
                                        </a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="envato">
                                    <a href="<?php echo base_url('envato'); ?>">
                                        <i class="fa fa-leaf"></i>
                                        <?php echo $this->lang->line('envato'); ?>
                                    </a>
                                </li>
                                
                                <li class="settings">
                                  <a href="#">
                                    <i class="fa fa-cog"></i> <span><?php echo $this->lang->line('settings'); ?></span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                  </a>
                                  <ul class="treeview-menu">
                                    <li class="settings">
                                        <a href="<?php echo base_url('user/settings'); ?>">
                                            <i class="fa fa-circle-o"></i>
                                            <?php echo $this->lang->line('settings'); ?>
                                        </a>
                                    </li>
                                    <li class="mail_template">
                                        <a href="<?php echo base_url('user/mailTemplate'); ?>">
                                            <i class="fa fa-circle-o"></i>
                                            <?= $this->lang->line('mail_templates') ?>
                                        </a>
                                    </li>
                                  </ul>
                                </li>
                                
                                
                                    
                                </li>
                            <?php } else { ?>
                                <li class="faq_list">
                                    <a href="<?php echo base_url('faq'); ?>">
                                        <i class="fa fa-question-circle"></i>
                                        <?= $this->lang->line('faq') ?>
                                    </a>
                                </li>
                                <li class="customer_list">
                                    <a href="<?php echo base_url('customer/customerList'); ?>"><i class="fa fa-users"></i><?php echo $this->lang->line('customer_management'); ?></a>
                                </li>
                                <li class="feature_list">
                                    <a href="<?php echo base_url('features'); ?>">
                                        <i class="fa fa-plus-circle"></i>
                                        <?= $this->lang->line('features') ?>
                                    </a>
                                </li>
                                <li class="bug_list">
                                    <a href="<?php echo base_url('bugs'); ?>">
                                        <i class="fa fa-bug"></i>
                                        <?= $this->lang->line('bugs') ?>
                                    </a>
                                </li>
                                <li class="all_chats">
                                  <a href="#">
                                    <i class="fa fa-comments-o"></i> <span>Chat</span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                  </a>
                                  <ul class="treeview-menu">
                                    <li class="chat_history">
                                        <a href="<?php echo base_url('chat/index'); ?>">
                                            <i class="fa fa-circle-o"></i>
                                            <?php echo $this->lang->line('chat_history'); ?>
                                        </a>
                                    </li>
                                    <li class="visitor">
                                        <a href="<?php echo base_url('chat/visitor'); ?>">
                                            <i class="fa fa-circle-o"></i>
                                            <?= $this->lang->line('visitors') ?>
                                        </a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="all_tickets">
                                    <a href="<?php echo base_url('tickets/index'); ?>">
                                        <i class="fa fa-ticket"></i>
                                        <?php echo $this->lang->line('all_tickets'); ?>
                                    </a>
                                </li>
                                <li class="envato">
                                    <a href="<?php echo base_url('envato'); ?>">
                                        <i class="fa fa-leaf"></i>
                                        <?php echo $this->lang->line('envato'); ?>
                                    </a>
                                </li>
                                <li class="settings">
                                    <a href="<?php echo base_url('user/settings'); ?>">
                                        <i class="fa fa-cog"></i>
                                        <?php echo $this->lang->line('settings'); ?>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } else { ?>
                            <li class="login">
                                <a href="<?php echo site_url('faq'); ?>">
                                    <i class="fa fa-question-circle"></i>
                                    <?php echo $this->lang->line('faq'); ?>
                                </a>
                            </li>
                            <li class="login">
                                <a href="<?php echo site_url('user/login'); ?>">   
                                    <i class="fa fa-home"></i>
                                    <?php echo $this->lang->line('login'); ?>
                                </a>
                            </li>
                            <li class="register">
                                <a href="<?php echo base_url('user/create'); ?>">
                                    <i class="fa fa-user"></i>
                                    <?= $this->lang->line('create_account') ?>
                                </a>
                            </li>
                        <?php } ?>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">

                <?php if ($notices) { ?>
                    <div class="alert alert-warning" style="border-radius: 0;">
                        <button onclick="hide_notice()" type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php foreach ($notices as $notice) { ?>
                            <p><?= $notice['content'] ?></p>
                        <?php } ?>
                    </div>
                <?php } ?>

                <script>
                    function hide_notice(){
                        $.get('user/hideNotice');
                    }    
                </script>
                    