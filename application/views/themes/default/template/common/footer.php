
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b><?= $this->lang->line('version') ?></b> <?= VERSION ?>
        </div>
        <strong><p><?= $this->lang->line('copyright') ?> - <b><?php echo config('site_name'); ?></b></p></strong>
      </footer>

      <?php if(userdata('department') == 0) { ?>
      <div id="live-chat-ui-wrapper">
          <div style="display: none;" id="customer_start_chat">
              <!--<img src="images/start_chat.jpg" />-->
          </div>
      </div>
      <?php } ?>
      
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script src="application/views/themes/default/js/plugins/jQuery/jQuery-2.1.3.min.js?v=1"></script>
    
    <!-- Bootstrap 3.3.2 JS -->
    <script src="application/views/themes/default/js/bootstrap.min.js?v=1" type="text/javascript"></script>
    
    <script src="application/views/themes/default/js/moment.min.js?v=1" type="text/javascript"></script>
    <script src="application/views/themes/default/js/livestamp.js?v=1" type="text/javascript"></script>    
    
    <!-- FastClick -->
    <script src='application/views/themes/default/js/plugins/fastclick/fastclick.min.js?v=1'></script>
    
    <!-- A1support App -->
    <script src="application/views/themes/default/js/app.min.js?v=1" type="text/javascript"></script>
    
    <!-- A1support for demo purposes -->
    <script src="application/views/themes/default/js/demo.js?v=1" type="text/javascript"></script>

    <!-- Chat -->
    <script src="application/views/themes/default/js/plugins/slimscroll/jquery.slimscroll.min.js?v=1"></script>
    <script src="application/views/themes/default/js/plugins/slimscroll/jquery.slimscroll.init.js?v=1"></script>
            
    <?php if(!$timezone) { ?>   
    <script>    
    $(document).ready(function() {
        var time = new Date();
        $.ajax({
            type: "GET",
            url: "chat/timezone",
            data: 'value='+ parseInt(time.getTimezoneOffset()),
            success: function(){
                location.reload();
            }
        });
    });        
    </script>
    <?php } ?>
    
    <!-- language variables -->
    <script>
        $play_sound = "<?= userdata('play_sound') ?>";
        $chat_error_msg = "<?= $this->lang->line('chat_error_msg') ?>";
        $no_agent_found = "<?= $this->lang->line('no_agent_found') ?>";
        $message_check_time = <?php echo config('message_check_time'); ?>;
        $online_update_time = <?php echo config('online_update_time'); ?>;
        $notification_check_time = <?php echo config('notification_check_time'); ?>;
    </script>
    
    <script src="application/views/themes/default/js/chat.js?v=1"></script>
    
    <?php if(userdata('department') == 0){ ?>
        <script src="application/views/themes/default/js/customer.js?v=1"></script>
        
        <div id="modal_agent_offline_alert" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <a data-dismiss="modal" class="close">×</a>
                        <h4 class="modal-title"><?= $this->lang->line('agent_offline_title') ?></h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <?= $this->lang->line('agent_offline_body') ?>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn_agent_offline_alert_yes btn-primary" value="Yes">
                        <a onclick="$('#modal_agent_offline_alert').remove();" class="btn-remove-model btn btn-default"><?= $this->lang->line('cancel') ?></a>
                    </div>
                </div>
            </div>
        </div>
        
    <?php } else if (userdata('id')) { ?>
        <script src="application/views/themes/default/js/agent.js?v=1"></script>
    <?php } ?>
    
        <script>
            
            setInterval(function(){
                $.get('chat/iAmOnline/'+$('.live-chat-ui').length, function(data){
                    var data = JSON.parse(data);

                    if(data.redirect){
                        location  = data.redirect;
                    }
                });
            }, $online_update_time);

            //start all chats added in user_chatroom 
            <?php foreach($chats as $chat) {
                  if($chat['receiver_id'] != $user_id){
            ?>                        
                    if($('#live-chat-ui-<?= $chat["receiver_id"] ?>').length == 0){    
                        start_chat(<?= $chat["receiver_id"] ?>);
                    }                                        
            <?php } else { ?>
                    if($('#live-chat-ui-<?= $chat["user_id"] ?>').length == 0){    
                        start_chat(<?= $chat["user_id"] ?>);
                    }
            <?php }
            }
            ?>

        </script>            
    </body>
</html>
