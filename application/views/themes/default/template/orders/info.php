<div class="modal-dialog">
<div class="modal-content" id="ticket_model">
    <div class="modal-header">
        <a data-dismiss="modal" class="close">×</a>
        <h4 class="modal-title"><?= $this->lang->line('order_detail') ?></h4>
    </div>
    <div class="modal-body">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td>Order ID</td>
                    <td>#<?= $order->id ?></td>
                </tr>
                <tr>
                    <td>Customer</td>
                    <td><?= $order->customer ?></td>
                </tr>
                <tr>
                    <td>Service</td>
                    <td><?= $order->service ?></td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td><?= $this->currency->format($order->amount) ?></td>
                </tr>
                <tr>
                    <td>Comment</td>
                    <td><?= $order->comment ?></td>
                </tr>
                <tr>
                    <td>Work Status</td>
                    <td>
                        <?php if($order->status){ ?>
                            <label class="label label-success">Complete</label>
                        <?php }else{ ?>
                            <label class="label label-danger">Incomplete</label>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>Date added</td>
                    <td><?= date('d F, Y', strtotime($order->date_added)) ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>