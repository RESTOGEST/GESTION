<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-dropbox"></i>
        <?php echo $this->lang->line('orders'); ?>
        
        <a href="<?= site_url('services/orders/add') ?>" class="btn btn-primary pull-right">
            <i class="fa fa-plus"></i>
            &nbsp;
            <?= $this->lang->line('add_new_order') ?>
        </a>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <?php echo $this->session->flashdata('message'); ?>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <?= $this->lang->line('search_orders') ?>
            </h3>
            <div class="pull-right box-tools">                
                <button data-original-title="<?= $this->lang->line('collapse') ?>" style="margin-right: 5px;" title="" data-toggle="tooltip" data-widget="collapse" class="btn bg-teal btn-smm"><i class="fa fa-minus"></i></button>
            </div>
        </div>    
        <div class="box-body table-responsive">
            
            <?php echo form_open(
                $action,
                'class="form form-inline ticket_filter_form"'
              );
            ?>
                
                <input type="hidden" value="<?= $sort ?>" name="sort" />
                <input type="hidden" value="<?= $order ?>" name="order" />
                
                <div class="form-group">
                    <label><?= $this->lang->line('order_id') ?></label><br>
                    <input class="form-control" name="filter_order" value="<?= $filter_order ?>" />
                </div>
                
                <div class="form-group">
                    <label><?= $this->lang->line('customer') ?></label><br>
                    <input class="form-control" name="filter_customer" value="<?= $filter_customer ?>" />
                </div>
                
                <div class="form-group">
                    <label>Status</label><br>
                    <select class="form-control" name="filter_status">
                        <option value="">All</option>
                        
                        <?php if($filter_status == 1){ ?>
                            <option selected value="1">Complete</option>
                        <?php }else{ ?>
                            <option value="1">Complete</option>
                        <?php } ?>
                         
                        <?php if($filter_status == 0 && $filter_status != ""){ ?>
                            <option selected="" value="0">Incomplete</option>
                        <?php }else{ ?>    
                            <option value="0">Incomplete</option>
                        <?php } ?>        
                    </select>
                </div>
                
                <div class="form-group">
                    <label>&nbsp;</label><br>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-search"></i>
                        <?= $this->lang->line('search') ?>   
                    </button>
                </div>
            </form>
        </div>
    </div>
    
    <div class="box">
        <div class="box-body table-responsive">
            <table class="table table-orders table-hover">
                <thead>
		<thead>
                    <tr>
                        <th class="sorting" width="10%" align="left">
                            <?php if ($sort == 'u2s.id') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_id ?>">
                                    <?php echo $this->lang->line('order_id'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_id ?>">
                                    <?php echo $this->lang->line('order_id'); ?>
                                </a>
                            <?php } ?>                                
                        </th>
                        <th class="sorting" width="20%" align="left">
                            <?php if ($sort == 'u.name') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_customer ?>">
                                    <?php echo $this->lang->line('customer'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_customer ?>">
                                    <?php echo $this->lang->line('customer'); ?>
                                </a>
                            <?php } ?>                                
                        </th>
                        <th class="sorting" width="20%">
                            <?php if ($sort == 's.title') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_service ?>">
                                    <?php echo $this->lang->line('service'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_service ?>">
                                    <?php echo $this->lang->line('service'); ?>
                                </a>
                            <?php } ?>                                     
                        </th>
                        <th class="sorting" width="10%">
                            <?php if ($sort == 'u2s.amount') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_amount ?>">
                                    <?php echo $this->lang->line('amount'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_amount ?>">
                                    <?php echo $this->lang->line('amount'); ?>
                                </a>
                            <?php } ?>                                     
                        </th>
                        <th class="sorting" width="10%">
                            <?php if ($sort == 'u2s.date_added') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_date_added ?>">
                                    <?php echo $this->lang->line('date_added'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_date_added ?>">
                                    <?php echo $this->lang->line('date_added'); ?>
                                </a>
                            <?php } ?>                                     
                        </th>
                        <th class="sorting" width="10%">
                            <?php if ($sort == 'u2s.status') { ?>
                                <a class="<?php echo strtolower($order); ?>" href="<?= $sort_status ?>">
                                    <?php echo $this->lang->line('work_status'); ?>
                                </a>
                            <?php }else{ ?>
                                <a href="<?= $sort_status ?>">
                                    <?php echo $this->lang->line('work_status'); ?>
                                </a>
                            <?php } ?>                                                                     
                        </th>
                        <th style="text-align: right;" width="10%"><?php echo $this->lang->line('options'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($orders)) { ?>
                        <?php foreach ($orders as $order) { ?>
                    <tr id="id_<?php echo $order->id; ?>">
                                <td>
                                    <?php echo $order->id; ?>
                                </td>
                                <td>
                                    <?php echo $order->customer; ?>
                                </td>
                                <td align="left">
                                    <?php echo $order->service; ?>
                                </td>
                                <td align="left">
                                    <?php echo $this->currency->format($order->amount); ?>
                                </td>
                                <td align="left">
                                    <?php echo date('d F, Y',strtotime($order->date_added)); ?>
                                </td>
                                <td align="left">
                                    <?php if($order->status){
                                              echo '<label class="label label-success">' . $this->lang->line('complete') . '</label>';
                                          } else{ 
                                              echo '<label class="label label-danger">' . $this->lang->line('uncomplete') . '</label>';
                                          } ?>
                                </td>
                                <td align="right">
                                    
                                    <a class="btn btn-info order-info btn-sm" data-toggle="tooltip" title="<?php echo $this->lang->line('detail'); ?>" data-id="<?php echo $order->id; ?>">
                                        <i class="fa fa-eye"></i>
                                    </a>&nbsp;&nbsp;
                                    
                                    <?php if($order->status){ ?>
                                    
                                    <a class="btn bg-red btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('uncomplete'); ?>" href="<?php echo base_url('services/orders/uncomplete/' . $order->id); ?>">
                                        <i class="fa fa-star-o"></i> 
                                    </a>&nbsp;&nbsp;
                                    
                                    <?php }else{ ?>
                                    
                                    <a class="btn bg-orange btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('complete'); ?>"  href="<?php echo base_url('services/orders/complete/' . $order->id); ?>">
                                        <i class="fa fa-star"></i>
                                    </a>&nbsp;&nbsp;
                                    
                                    <?php } ?>
                                    
                                    <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-title="<?php echo $this->lang->line('delete'); ?>" href="javascript:void(0)" onclick="delete_order(<?php echo $order->id; ?>)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>            
            
            <div class="pagination_wrapper">
                <ul class="pagination pull-right">
                <?= $pagination ?>
                </ul>    
            </div>
            
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<style>
    .table-orders td{
        line-height: 50px !important;
    }
</style>
<script>
    
//get assign model html
$(document).delegate('.order-info','click', function(){
   $.get('services/orders/info/'+$(this).attr('data-id'),function(data){
       $('.modal').remove();
       $('<div class="modal fade">' + data + '</div>').modal();
   });
   return false;
});

$(function(){
    $('.orders').addClass('active');
});

function delete_order(id){

    //return confirm('Are you sure?');

    $.ajax({
        type: "GET",
        url: "<?php echo base_url('services/orders/delete' ); ?>/" + id,
        success: function(msg)
        {
            var data = JSON.parse(msg);
            
	    if( data.msg == 'deleted' )
            {
                $('#id_' + id).fadeOut('normal');
            }
        }
    });
 }
 </script>
