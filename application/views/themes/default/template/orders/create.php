<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('add_new_order'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <?php if($msg){ ?>
    <div class="alert alert-warning">            
        <?= $msg ?>
        <button style='line-height: 1 !important;' type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>    
    <?php } ?>
    
    <div class="box">
        <div class="box-body table-responsive">

            <?php echo form_open(
                $action,
                'id="createform" method="post" enctype="multipart/form-data"'
              );
            ?>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('customer'); ?></label>
                    <input type="text" name="user" value="<?= set_value('user') ?>"  class="form-control" />
                    <input type="hidden" name="user_id" value="<?= set_value('user_id') ?>"  class="form-control" />
                </div>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('service'); ?></label>
                    <input type="text" name="service" value="<?= set_value('service') ?>"  class="form-control" />
                    <input type="hidden" name="service_id" value="<?= set_value('service_id') ?>"  class="form-control" />
                </div>
                
                <div class="form-group">
                    <label class="stlabel"><?php echo $this->lang->line('amount'); ?></label>
                    <input type="text" name="amount" value="<?= set_value('amount') ?>"  class="form-control" />
                </div>
                
                <div class="box-footer">
                    <input type="submit" name="button" id="button" value="<?php echo $this->lang->line('submit'); ?>" class="btn btn-primary" />
                </div>	
                
            </form>
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>


<link rel="stylesheet" type="text/css" href="application/views/themes/default/css/jquery-ui/smoothness/jquery-ui.css" />
<script src="application/views/themes/default/js/plugins/jQueryUI/jquery-ui-1.10.3.min.js"></script>

<script>

$('input[name="service"]').autocomplete({
    source: function(request, response) {
        $.ajax({
            url: 'services/autocomplete/' +  encodeURIComponent(request.term),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.title,
                        value: item.service_id,
                        price: item.price
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        $('input[name="service"]').val(ui.item.label);
        $('input[name="service_id"]').val(ui.item.value);
        $('input[name="amount"]').val(ui.item.price);
        return false;
    },
    focus: function(event, ui) {
        return false;
    }
});

$('input[name="user"]').autocomplete({
    source: function(request, response) {
        $.ajax({
            url: 'chat/customerAutocomplete/' +  encodeURIComponent(request.term),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.name,
                        value: item.id,
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        $('input[name="user"]').val(ui.item.label);
        $('input[name="user_id"]').val(ui.item.value);
        return false;
    },
    focus: function(event, ui) {
        return false;
    }
});
</script>