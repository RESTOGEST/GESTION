<?= Modules::run('header/header/index') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $this->lang->line('view_service'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    
    <div id="ajax">            
        <?php echo $this->session->flashdata('message'); ?>
    </div>    
    
    <div class="box">
        <div class="box-body">                        
            <div class='col-md-12'>
                <div class='col-md-2'>    
                    <img src='<?= site_url("images/services/".$service->icon) ?>' />
                </div>
                <div class='col-md-10'>
                    <table class='table table-bordered'>
                        <tr>
                            <td><b><?php echo $this->lang->line('title') ?></b></td>
                            <td><?php echo $service->title; ?></td>
                        </tr>
                        <tr>
                            <td><b><?php echo $this->lang->line('description') ?></b></td>
                            <td><?php echo $service->description; ?></td>
                        </tr>
                        <tr>
                            <td><b><?php echo $this->lang->line('price') ?></b></td>
                            <td><?php echo $service->price; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div class="clearfix" />
        </div>
    </div>
</section>

<?= Modules::run('footer/footer/index') ?>

<script>
    
$(function(){
    $('.services').addClass('active');
});    

</script>

