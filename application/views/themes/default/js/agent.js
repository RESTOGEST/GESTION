function start_chat_for_notification($receiver_id){
    if ($('#live-chat-ui-wrapper').length == 0) {

        //add entry to chatroom 
        $.get('chat/enterChatroom/'+$receiver_id);

        //redirect to visitor page 
        location = 'chat/visitor';    

    } else {

        start_chat($receiver_id);
        
    }
}

function check_notification(){
    $.get('chat/checkNotification',function(data){
        var data = JSON.parse(data);

        $old_count = $('.chat-notification .chat-count').html();

        if($play_sound && data.count > 0 && $old_count != data.count){ //alert only first time
            $('#chatAudio')[0].play();
        }

        $('#chat-notification-list').html(data.html);
        $('.chat-notification .chat-count').html(data.count);

        if(data.count > 0){
            $('.chat-notification a').addClass('noti_active');
        }else{
            $('.chat-notification a').removeClass('noti_active');
        }

        //init_scroll();
        //popup_notification(data.rows);
    });
}

//agent close chat
$(document).delegate('.chat-close','click', function(e) {
    e.preventDefault();

    $box = $(this).parent().attr('href');

    $.get('chat/leaveChatroom/' + $(''+$box+' form input[name="receiver_id"]').val());//leave chatroom
    $.post('chat/setNotified', $(''+$box+' form').serialize());

    $(''+$box).remove();//remove tab-content 
    $(this).parents('li').remove();//remove tab 

    $('#live-chat-ui-wrapper ul li a').first().trigger('click');

    return false;
});

//agent initialized chat 
function start_chat($receiver_id) {          
    $.get('chat/startChat/'+$receiver_id, function(data) {
        var data = JSON.parse(data);
        if ($('#live-chat-ui-' + $receiver_id).length == 0) {

            $html  = '<li>';
            $html += '    <a data-toggle="tab" href="#live-chat-ui-' + $receiver_id+'">';

            if(data.total_new_msg == 0){
                $html += '      <span class="chat-message-counter" style="display: none;">'; 
                $html +=            data.total_new_msg;
                $html += '      </span>'; 
            }else{
                $html += '      <span class="chat-message-counter" style="display: inline;">'; 
                $html +=            data.total_new_msg;
                $html += '      </span>'; 
            }

            $html +=        data.receiver.name;                    
            $html += '      <button class="chat-close btn btn-xs">';
            $html += '       <i class="fa fa-times"></i>';
            $html += '      </button>';
            $html += '    </a>';
            $html += '</li>';

            $('#live-chat-ui-wrapper .nav-tabs').append($html);
            $('#live-chat-ui-wrapper .tab-content').append(data.html);
            $('a[href="#live-chat-ui-' + $receiver_id+'"]').trigger('click');

            init_scroll();                    
            reposition_chat();

        } else {
            $('#live-chat-ui-' + $receiver_id + ' input').trigger('focus');
        }

        //tooltip
        $("[data-toggle='tooltip']").tooltip();
        $('#customer_start_chat').hide();
    });
}

//get assign model html
$(document).delegate('.chat-exchange','click', function(){
   $.get('chat/chatAssign/'+$(this).attr('data-id'),function(data){
       $('.modal').remove();
       $('<div class="modal fade">' + data + '</div>').modal();
   });
   return false;
});

//assignn
$(document).delegate('.btn-assign-agent','click', function(){
   $.post('chat/chatAssign',$('#agent_form').serialize(), function(data){
       var data = JSON.parse(data);
       if(data.status){
            //$('#agent_model .alert-success').removeClass('hide').html('<?= $this->lang->line('chat_assign_success') ?> '+data.agent_name);
            $('.modal').modal('hide');
       }else{
           $('#agent_model .alert-success').removeClass('hide').html($chat_error_msg);
       }
   });
   return false;
});

//filter agents
$(document).delegate('#agent_model select[name="type"],#agent_model select[name="department_id"]','change', function(){
   $.post('chat/filterAgents',$('#agent_form').serialize(),function(data){
       if(data){
           $('select[name="agent_id"]').html(data);
           $('#agent_model .alert').addClass('hide');
       }else{
           $('select[name="agent_id"]').html('');
           $('#agent_model .alert-success').removeClass('hide').html($no_agent_found);
       }
   });
});
