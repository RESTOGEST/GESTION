function check_notification(){
    $.get('chat/checkNotification',function(data){
        
        var data = JSON.parse(data);

        if(data.status==1){
            $old_count = $('.chat-notification .chat-count').html();

            if($play_sound && data.count > 0 && $old_count != data.count){ //alert only first time
                $('#chatAudio')[0].play();
            }

            $('#chat-notification-list').html(data.html);
            $('.chat-notification .chat-count').html(data.count);

            if(data.count > 0){
                $('.chat-notification a').addClass('noti_active');
            }else{
                $('.chat-notification a').removeClass('noti_active');
            }

            //init_scroll();
            popup_notification(data.rows);
        }
    });
}

//mini,ize chat window 
$(document).delegate('.live-chat-ui header','click', function(){                
    $id = $(this).attr('data-id');
    $.get('chat/chatMinimize/' + $id);
    $('#live-chat-ui-' + $id + ' .fa-angle-down').toggleClass('fa-rotate-180');
    $('#live-chat-ui-' + $id + ' .chat').toggleClass('hide');
    $('#live-chat-ui-' + $id).toggleClass('minimized');
    reposition_chat();
    return false; 
});    

/* 
 *  Chat initiated by ccustomer 
 */        
$(document).delegate('#customer_start_chat', 'click', function(){
    $.get('chat/getAgentId',function(id){
        start_chat(id);
    });
});

function start_chat_for_notification($receiver_id){                
    start_chat($receiver_id);
}

//agent close chat
$(document).delegate('.chat-close','click', function(e) {
    e.preventDefault();

    $(this).parent().parent().fadeOut(300).remove();
    reposition_chat();

    $receiver_id = $(this).parent().parent().find('input[name="receiver_id"]').val();

    $.get('chat/leaveChatroom/'+$receiver_id);//leave chatroom
    $.post('chat/setNotified', $(this).parent().parent().find('form').serialize());

    if($('.live-chat-ui').length == 0){
        $('#customer_start_chat').show();
    }

    return false;
});

function start_chat($receiver_id) {
    $.get('chat/startChat/'+$receiver_id, function(data) {
        var data = JSON.parse(data);
        if ($('#live-chat-ui-' + $receiver_id).length == 0) {
            $('#live-chat-ui-wrapper').append(data.html);
            init_scroll();
            reposition_chat();
        } else {
            $('#live-chat-ui-' + $receiver_id + ' input').trigger('focus');
        }

        //tooltip
        $("[data-toggle='tooltip']").tooltip();
        $('#customer_start_chat').hide();
    });
}

//chat response
$(document).delegate('.like_unlike_wrapper div','click', function(){
    $this = $(this);
    $response = $(this).attr('data-response');
    $agent = $(this).attr('data-agent'); 
    $.get('chat/response/'+$response+'/'+$agent,function(){
        //$('.like_unlike_wrapper div').removeClass('active');
        $this.parent().find('.active').removeClass('active');
        $this.addClass('active');
    });

    //send like message 
    $form = $this.parents('form.send_message');            
    $input = $form.find('input[name="msg"]');

    $input.val($this.html());

    //send i liked the chat 
    $.post('chat/sendMessage', $form.serialize(), function(data) {

        var data = JSON.parse(data);
        $this.parents('.live-chat-ui').find('.chat-history').append(data.html);

        //scroll to bottom        
        $this.parents('.chat').find('.chat-history').slimScroll({scrollTo: 10000000});
    });

    $input.val('');

});