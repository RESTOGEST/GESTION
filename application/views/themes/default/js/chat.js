
$('#toogle_sound').click(function(){
   if ($(this).find('i').hasClass('fa-bell-o')) {//play_sound is 1 make it 0
       $.get('chat/playSound/0');
       $(this).find('i').attr('class','fa fa-bell-slash-o');
       $play_sound = 0;
   } else {//play_sound is 1 make it 0
       $.get('chat/playSound/1');
       $(this).find('i').attr('class','fa fa-bell-o');
       $play_sound = 1;
   }
});

setInterval(function(){
    check_message();
}, $message_check_time);

setInterval(function(){
    check_notification();
}, $notification_check_time);

$(function(){
    $('<audio id="chatAudio"><source src="images/notify.mp3" type="audio/ogg"></audio>').appendTo('body');
    
    $('#start_chat').click(function(){
        $.get('chat/chat_start/',function(){
           location = location;
       });
    });
    
    $(document).delegate('.chat-notification a.start_chat', 'click',function(){

        $receiver_id = $(this).attr('data-user-id');

        start_chat_for_notification($receiver_id);  
        
        check_notification();
    });

    setInterval(function(){
        if($('.live-chat-ui').length == 0){ 
            $.get('chat/getChatIcon',function(html){
                $('#customer_start_chat').html(html).show();                            
            }); 
        } else {
            $('#customer_start_chat').hide();
        }
    },1000);
});

$(document).delegate('.load_more_wrapper a','click', function(e){
    
    $this = $(this);    
    $page = parseInt($this.attr('data-page'));
    $receiver_id = $this.attr('data-receiver-id');
    
    $last_msg = $this.parents('.chat-history').find('.chat-message').first();
    
    if($last_msg.length > 0){
        $last_msg_id = $last_msg.attr('data-id');
    }else{
        $last_msg_id = 0;
    }
    
    $.post('chat/myChats', 
        {
            'csrf_a1support' : $("input[name='csrf_a1support']").val(),
            'last_msg_id' : $last_msg_id,
            'receiver_id' : $receiver_id
        },
        function(data){
     
            var data = JSON.parse(data);
            
            if(data.html){
                if($('#live-chat-ui-'+$receiver_id+' .chat-message').length > 0){
                    $('#live-chat-ui-'+$receiver_id+' .chat-message').first().before(data.html);
                }else{
                    $('#live-chat-ui-'+$receiver_id+' .chat-history').append(data.html);
                }
            }else{
                $this.parent().html('No more messages.');
            }
        }
    );
    
    e.preventDefault();
});

//edit customer data 
$(document).delegate('.chat-info', 'click', function() {
    $.get('chat/chatInfo/' + $(this).attr('data-id'), function(data) {
        $('.modal').remove();
        $('<div class="modal fade">' + data + '</div>').modal();
    });
    return false;
});

$(document).delegate('.chat_transcript','click', function(){
   $.get('chat/chatTranscript/'+$(this).attr('data-id'),function(data){
       $('.modal').remove();
       $('<div class="modal fade">' + data + '</div>').modal();
   });
   return false;
});

//mail transcript
$(document).delegate('.btn-chat-transcript', 'click', function() {
    $.post('chat/chatTranscript', $('#chat_transcript_form').serialize(), function(data) {
        var data = JSON.parse(data);

        //$('#chat_model .close').trigger('click');

        $button = '<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>';

        if (data.status) {
             $('#chat_model .alert-success').removeClass('hide').html($button + data.msg);
        } else {
             $('#chat_model .alert-success').removeClass('hide').html($button + 'Error Processing Your Request!');
        }
    });
    return false;
});

$(document).delegate('.btn-remove-model', 'click', function() {
    $(this).parents('.modal').find('.close').trigger('click');
});
            
/*
 * Open Chat for new messages if not opened 
 * 
 */
function popup_notification(rows){
    
    var called = new Array();
    
    for($i=0; $i< rows.length; $i++){
        if($('#live-chat-ui-'+rows[$i].user_id).length==0 && typeof called[rows[$i].user_id] === 'undefined'){
            called[rows[$i].user_id] = 1;
            start_chat_for_notification(rows[$i].user_id); //call from notification 
        }
    }
}

//send msg
$(document).delegate('input[name="msg"]', 'keypress', function(e) {
    send_feedback(e, $(this));
});

//slimScroll Function for Live chat
//--------------------------------
function init_scroll() {
    $('.chat-history,.in-chat').slimScroll({
        height: '290px',
        alwaysVisible: false,
        disableFadeOut: true,
        touchScrollStep: 50,
        start: 'bottom' //set to bottom
    });
    
    $('.chat-history,.in-chat').slimScroll().bind('slimscroll', function(e, pos){
        if(pos === 'bottom'){
            $(this).parent().parent().find('.chat-alert').hide();
        }else{
            $(this).parent().parent().find('input[name="msg"]').blur();
        }
    });
}

function send_feedback(e, $input) {

    $form = $input.parents('form.send_message');

    //send msg on enter
    if (e.which == 13 && $input.val().length > 0) {

        $.post('chat/sendMessage', $form.serialize(), function(data) {
            var data = JSON.parse(data);
            $form.parents('.live-chat-ui').find('.chat-history').append(data.html);

            $('.chat-history > .alert, .chat-history > br').remove();

            //scroll to bottom        
            $input.parents('.chat').find('.chat-history').slimScroll({scrollTo: 10000000});
        });

        $input.val('');

        if ($('.chat-message').length > 5) {
            //   init_scroll();
        }

        e.preventDefault();

        //send typing status    
    } else {
        $.post('chat/updateTypingMomment', $form.serialize(), function(data){
            var data = JSON.parse(data);
            
            if(data.redirect){
               location = data.redirect; 
            }
        });
    }
}

$(document).delegate('.chat-message', 'mouseenter', function(){
    
    $receiver_id = $(this).parents('.chat').find("input[name='receiver_id']").val();
    $csrf_a1support = $(this).parents('.chat').find("input[name='csrf_a1support']").val();
    
    $.post('chat/setMsgNotified/', 
        { 
            'csrf_a1support' : $csrf_a1support,
            'msg_id' : $(this).attr('data-id'),
            'receiver_id' : $receiver_id  
        } ,
        function(data){

            var data = JSON.parse(data);

            if (data.total_new_msg) {
                $('#live-chat-ui-' + $receiver_id + ' .chat-message-counter').html(data.total_new_msg);
                $('#live-chat-ui-' + $receiver_id + ' .chat-message-counter').show();
                
                $('a[href="#live-chat-ui-' + $receiver_id + '"]  .chat-message-counter').html(data.total_new_msg);
                $('a[href="#live-chat-ui-' + $receiver_id + '"]  .chat-message-counter').show();
                
            } else {
                $('#live-chat-ui-' + $receiver_id + ' .chat-message-counter').hide();
                
                $('a[href="#live-chat-ui-' + $receiver_id + '"]  .chat-message-counter').hide();
            }
        }
    );
});

$(document).delegate('.chat input[name="msg"]', 'focus', function() {
    $.post('chat/setNotified', $(this).parents('form').serialize());
});
 
function reposition_chat() {
    $max_height = 0;

    if ($('.live-chat-ui').length > 1) {
        $('.live-chat-ui').each(function() {
            if ($(this).height() > $max_height) {
                $max_height = $(this).height();
            }
        });

        //remove header height 
        $max_height -= 43;
    }

    $('.live-chat-ui').css('top', 0);
    $('.live-chat-ui.minimized').css('top', $max_height + 'px');
}

$(document).delegate('.chat-alert','click',function(){    
  $(this).parent().find('.chat-history').slimScroll({scrollTo: 10000000});  
  $(this).hide();
});
           
function check_message() {
    $('form.send_message').each(function(index) {
        $.post('chat/checkMessage', $(this).serialize(), function(data) {
            var data = JSON.parse(data);

            if(data.refresh){
                location = location;
            }
            
            $('#live-chat-ui-' + data.receiver_id + ' .chat-history').append(data.html);
            
            if (data.total_new_msg) {
                $('#live-chat-ui-' + data.receiver_id + ' .chat-message-counter').html(data.total_new_msg);
                $('#live-chat-ui-' + data.receiver_id + ' .chat-message-counter').show();
                
                $('a[href="#live-chat-ui-' + data.receiver_id + '"]  .chat-message-counter').html(data.total_new_msg);
                $('a[href="#live-chat-ui-' + data.receiver_id + '"]  .chat-message-counter').show();
                
            } else {
                $('#live-chat-ui-' + data.receiver_id + ' .chat-message-counter').hide();
                
                $('a[href="#live-chat-ui-' + data.receiver_id + '"] .chat-message-counter').hide();
            }
            
            //last message 
            if (data.last_msg_id) {
                $('#live-chat-ui-' + data.receiver_id + ' input[name="last_msg_id"]').val(data.last_msg_id);
            }

            //algo 
            //--------------
            // scrol height + top position >= wrapper height - allowed height -> scroll is down 
            //          scroll to bottom 
            // else if scrollbar
            //          display alert 
            
            if($('#live-chat-ui-' + data.receiver_id + ' .slimScrollBar').length === 1){
                $allowed_height = 10;//allow new message 
                $wrapper_height = $('#live-chat-ui-' + data.receiver_id + ' .slimScrollDiv').height();
                $total_height = $wrapper_height - $allowed_height;            
                $height = $('#live-chat-ui-' + data.receiver_id + ' .slimScrollBar').height();            
                $top = parseInt($('#live-chat-ui-' + data.receiver_id + ' .slimScrollBar').css('top').replace('px', '')); 

                if( ($height + $top >= $total_height) && data.html){
                    $('#live-chat-ui-' + data.receiver_id + ' .chat-history').slimScroll({scrollTo: 10000000});  
                    $('#live-chat-ui-' + data.receiver_id + ' .chat-alert').hide();
                }else if(($wrapper_height!==$height) && data.html){
                    $('#live-chat-ui-' + data.receiver_id + ' .chat-alert').show();
                }
            }

            if (data.is_online) {
                $('#live-chat-ui-' + data.receiver_id).addClass('online').removeClass('offline');
                $('#live-chat-ui-' + data.receiver_id + ' h4 .badge').html('online');
                $('#live-chat-ui-' + data.receiver_id + ' .alert').hide();
            } else {
                                
                $('#live-chat-ui-' + data.receiver_id).addClass('offline').removeClass('online');
                $('#live-chat-ui-' + data.receiver_id + ' h4 .badge').html('offline');
                
                //if opponent offline + i am customer + agent online 
                //Your support officer got offline for now, would you like to chat with other? 
                //currently not have any online agent chat 
                if(data.show_agent_offline_alert == 1 && $('.live-chat-ui.online').length == 0){
                    agent_offline_alert();
                }
                
            }

            //if new agent assigned 
            if (data.new_agent_id != 0 && $('#live-chat-ui-' + data.new_agent_id).length == 0) {
                //start_chat(data.new_agent_id);
                $('#live-chat-ui-' + data.receiver_id + ' input[name="receiver_id"]').val(data.new_agent_id);

                $html = '<div class="chat-message clearfix">';
                $html += '<img src="images/customer-user.png" alt="" style="width: 37px;height:37px;">';
                $html += '<div class="chat-message-content clearfix">';
                $html += ' <span class="chat-time" data-livestamp="'+data.time+'"></span>';
                $html += '<h5>' + data.new_agent_name + '</h5>';
                $html += '<p>Hello! How Can I Help You, Sir?</p>';
                $html += '</div> <!-- end chat-message-content -->';
                $html += '</div>';

                $('#live-chat-ui-' + data.receiver_id + ' input[name="chat_room_id"]').val(data.chat_room_id);
                $('#live-chat-ui-' + data.receiver_id + ' input[name="receivername"]').val(data.new_agent_name);
                
                $('#live-chat-ui-' + data.receiver_id).attr('id', 'live-chat-ui-' + data.new_agent_id);
                $('#live-chat-ui-' + data.new_agent_id + ' .chat-history').append($html);
                $('#live-chat-ui-' + data.new_agent_id + ' h4').html(data.new_agent_name + '&nbsp;<span class="badge badge-success">online</span>');

            }            

            //if typing 
             if(data.is_typing){                 
                $('#live-chat-ui-' + data.receiver_id + ' .chat-feedback').css('display','block');  
             }else{
                $('#live-chat-ui-' + data.receiver_id + ' .chat-feedback').css('display','none');      
             }
             
             //if like                  
             $('#live-chat-ui-' + data.receiver_id + ' .active').removeClass('active');
                 
             if(data.chat_response == 1){//like
                $('#live-chat-ui-' + data.receiver_id + ' .like').addClass('active');
             }else if(data.chat_response == 2){//unlike    
                $('#live-chat-ui-' + data.receiver_id + ' .unlike').addClass('active'); 
             }             
        });
    });
}

function agent_offline_alert() {
    if(!$('#modal_agent_offline_alert').hasClass('in')){
        $('#modal_agent_offline_alert').modal('show');
    }
}

$(document).delegate('.btn_agent_offline_alert_yes', 'click', function(){
   $.get('chat/refreshChat', function(){
       location = location;
   });
});

    