<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>A1 Support Desk - Installation</title>
<base href="<?= $base_url ?>" />
<link rel="stylesheet" type="text/css" href="application/views/install/stylesheet/stylesheet.css" />
<link rel="icon" type="image/png" href="images/logo.png">
<script type="text/javascript" src="application/views/install/javascript/cufon/cufon-yui.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Verdana_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Trebuchet_MS_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Trebuchet_MS_italic_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Aller_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/jquery.min.js"></script>
<script type="text/javascript" src="application/views/install/javascript/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript"><!--
Cufon.replace('h1', {fontFamily: 'Aller', textShadow: '2px 2px 0 rgba(0,0,0,0.2)'});
Cufon.replace('h2, .blog-list .date, .blog-list .comment, .blog-info .date, .blog-info .comment', {fontFamily: 'Aller', hover: true});
//--></script>
</head>
    <body>
        <div id="container">
            <div id="header">
  <div id="logo">
    <a href="#">
        <img src="images/logo.png" alt="" width="60px" />
    </a>
  </div>
  <div id="logo">
    Support Desk Installation
  </div>
</div>
            <h1>Step1 - Pre-Installation</h1>
            <div id="column-right">
                <ul>
                    <li><b>Pre-Installation</b></li>
                    <li>License</li>
                    <li>Configuration</li>
                    <li>Finished</li>
                </ul>
            </div>
<div id="content">

    <?php echo form_open(
            $action,
            'class="form-horizontal" method="post" style="margin-top:30px;"'
          );
    ?>

        <?php if ($msg): ?>
            <div class="warning" style="font-size:11px;">
            <?= $msg ?>
        </div>
    
    <?php endif; ?>

    <p>1. Please configure your PHP settings to match requirements listed below.</p>
    <fieldset>
      <table>
        <tr>
          <th width="35%" align="left"><b>PHP Settings</b></th>
          <th width="25%" align="left"><b>Current Settings</b></th>
          <th width="25%" align="left"><b>Required Settings</b></th>
          <th width="15%" align="center"><b>Status</b></th>
        </tr>
        <tr>
          <td>PHP Version:</td>
          <td><?php echo phpversion(); ?></td>
          <td>5.3+</td>
          <td align="center"><?php echo (phpversion() >= '5.3') ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>Register Globals:</td>
          <td><?php echo (ini_get('register_globals')) ? 'On' : 'Off'; ?></td>
          <td>Off</td>
          <td align="center"><?php echo (!ini_get('register_globals')) ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>Magic Quotes GPC:</td>
          <td><?php echo (ini_get('magic_quotes_gpc')) ? 'On' : 'Off'; ?></td>
          <td>Off</td>
          <td align="center"><?php echo (!ini_get('magic_quotes_gpc')) ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>File Uploads:</td>
          <td><?php echo (ini_get('file_uploads')) ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo (ini_get('file_uploads')) ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>Session Auto Start:</td>
          <td><?php echo (ini_get('session_auto_start')) ? 'On' : 'Off'; ?></td>
          <td>Off</td>
          <td align="center"><?php echo (!ini_get('session_auto_start')) ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>Short Open Tag:</td>
          <td><?php echo (ini_get('short_open_tag')) ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo (ini_get('short_open_tag')) ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
         <tr>
          <td>Output Buffering:</td>
          <td><?php echo (ini_get('output_buffering')) ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo (ini_get('output_buffering')) ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        
      </table>
    </fieldset>
    <p>2. Please make sure the PHP extensions listed below are installed.</p>
    <fieldset>
      <table>
        <tr>
          <th width="35%" align="left"><b>Extension</b></th>
          <th width="25%" align="left"><b>Current Settings</b></th>
          <th width="25%" align="left"><b>Required Settings</b></th>
          <th width="15%" align="center"><b>Status</b></th>
        </tr>
        <tr>
          <td>MySQLi:</td>
          <td><?php echo extension_loaded('mysqli') ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo extension_loaded('mysqli') ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>GD:</td>
          <td><?php echo extension_loaded('gd') ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo extension_loaded('gd') ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>cURL:</td>
          <td><?php echo extension_loaded('curl') ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo extension_loaded('curl') ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>mCrypt:</td>
          <td><?php echo function_exists('mcrypt_encrypt') ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo function_exists('mcrypt_encrypt') ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <tr>
          <td>ZIP:</td>
          <td><?php echo extension_loaded('zlib') ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo extension_loaded('zlib') ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <?php if (!function_exists('iconv')) { ?>
        <tr>
          <td>mbstring:</td>
          <td><?php echo extension_loaded('mbstring') ? 'On' : 'Off'; ?></td>
          <td>On</td>
          <td align="center"><?php echo extension_loaded('mbstring') ? '<img src="application/views/install/image/good.png" alt="Good" />' : '<img src="application/views/install/image/bad.png" alt="Bad" />'; ?></td>
        </tr>
        <?php } ?>
      </table>
    </fieldset>
    <p>3. Please make sure you have set the correct permissions on the files list below.</p>
    <fieldset>
      <table>
        <tr>
          <th align="left"><b>Files</b></th>
          <th align="left"><b>Status</b></th>
        </tr>
        <?php foreach($files as $file){ ?>
        <tr>
          <td><?php echo $file; ?></td>
          <td><?php if (!file_exists(FCPATH.$file)) { ?>
                <span class="bad">Missing</span>
              <?php } elseif (!is_writable(FCPATH.$file)) { ?>
                <span class="bad">Unwritable</span>
              <?php } else { ?>
                <span class="good">Writable</span>
              <?php } ?>
          </td>
        </tr>
        <?php } ?>
      </table>
    </fieldset>
    <p>4. Please make sure you have set the correct permissions on the directories list below.</p>
    <fieldset>
      <table>
        <tr>
          <th align="left"><b>Directories</b></th>
          <th align="left"><b>Status</b></th>
        </tr>
        <?php foreach($directories as $directory){ ?>
        <tr>
          <td><?php echo $directory . '/'; ?></td>
          <td><?php echo is_writable(FCPATH.$directory) ? '<span class="good">Writable</span>' : '<span class="bad">Unwritable</span>'; ?></td>
        </tr>
        <?php } ?>
      </table>
    </fieldset>
    <div class="buttons">
      <div class="left"><a href="" class="button">Back</a></div>
      <div class="right">
        <input type="submit" class="button" name="step_2" value="Continue"/>
      </div>
    </div>
  </form>
</div>
<div id="footer">
    A1 Support Desk v<?= VERSION ?> | Developed by
    <a href="http://www.wowtech.co/?utm_source=A1Support&amp;utm_medium=footer&amp;utm_campaign=Installer" target="_blank">
        <img src="application/views/install/image/footer-logo.png" height="40px">
    </a>
</div>
        </div>
    </div>
</body>
</html>
