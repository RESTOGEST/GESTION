<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>A1 Support Desk - Installation</title>
<base href="<?= base_url() ?>" />
<link rel="stylesheet" type="text/css" href="application/views/install/stylesheet/stylesheet.css" />
<link rel="icon" type="image/png" href="images/logo.png">
<script type="text/javascript" src="application/views/install/javascript/cufon/cufon-yui.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Verdana_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Trebuchet_MS_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Trebuchet_MS_italic_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Aller_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/jquery.min.js"></script>
<script type="text/javascript" src="application/views/install/javascript/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript"><!--
Cufon.replace('h1', {fontFamily: 'Aller', textShadow: '2px 2px 0 rgba(0,0,0,0.2)'});
Cufon.replace('h2, .blog-list .date, .blog-list .comment, .blog-info .date, .blog-info .comment', {fontFamily: 'Aller', hover: true});
//--></script>
</head>
    <body>
        <div id="container">
            <div id="header">
  <div id="logo">
    <a href="#">
        <img src="images/logo.png" alt="" width="60px" />
    </a>
  </div>
  <div id="logo">
    Support Desk Installation
  </div>
</div>
            <h1>Step 3 - Configuration</h1>
            <div id="column-right">
                <ul>
                    <li>License</li>
                    <li>Pre-Installation</li>
                    <li><b>Configuration</b></li>
                    <li>Finished</li>
                </ul>
            </div>
            <div id="content">

                <?php echo form_open(
                        'install/step3',
                        'class="form-horizontal" method="post" style="margin-top:30px;"'
                      );
                ?>

                    <?php if ($msg): ?>
                        <div class="warning" style="font-size:11px;">
                            <?= $msg ?>
                        </div>
                    <?php endif; ?>

                    <p>1. Please enter your database connection details.</p>

                    <fieldset>

                        <table class="form">
                            <tr>
                                <td>Database host</td>
                                <td><input type="text" name="inputDBhost" value="<?= set_value('inputDBhost') ?>" class="form-control" required /></td>
                            </tr>
                            <tr>
                                <td>Database name</td>
                                <td><input type="text" name="inputDBname" value="<?= set_value('inputDBname') ?>" class="form-control" required /></td>
                            </tr>
                            <tr>
                                <td>Database username</td>
                                <td><input type="text" name="inputDBusername" value="<?= set_value('inputDBusername') ?>" class="form-control" required /></td>
                            </tr>
                            <tr>
                                <td>Database password</td>
                                <td><input type="text" name="inputDBpassword" value="<?= set_value('inputDBpassword') ?>" class="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Install Sample Data</td>
                                <td>
                                    <label>
                                        <input checked="" type="radio" name="installSampleDB" value="1" class="form-control" />
                                        Yes
                                    </label>
                                    <label>
                                        <input type="radio" name="installSampleDB" value="0" class="form-control" />
                                        No
                                    </label>
                                </td>
                            </tr>
                            <!--
                            <tr>
                                <td>Base URL</td>
                                <td><input type="text" name="inputBaseUrl" class="form-control" /></td>
                            </tr>
                            -->
                        </table>
                    </fieldset>

                    <p>2. Please enter a username and password for the administration.</p>

                     <fieldset>
                        <table>
                            <tr>
                                <td>Username</td>
                                <td>
                                    <input type="text" name="name" value="<?= set_value('name') ?>" class="form-control" required />
                                </td>
                            </tr>
                            <tr>
                                <td>Login Password</td>
                                <td>
                                    <input type="password" name="password" value="<?= set_value('password') ?>" class="form-control" required />
                                </td>
                            </tr>
                            <tr>
                                <td>Login Email Address</td>
                                <td>
                                    <input type="email" name="email" value="<?= set_value('email') ?>" class="form-control" required />
                                </td>
                            </tr>
                        </table>
                    </fieldset>

                   
                    
                    <div class="buttons">
                        <div class="left"><input type="reset" class="button" name="btn" value="Reset"/></div>
                        <div class="right">
                            <input type="submit" class="button" name="step_3" value="Install"/>
                        </div>
                    </div>


                </form>
            </div>
<div id="footer">
    A1 Support Desk v<?= VERSION ?> | Developed by
    <a href="http://www.wowtech.co/?utm_source=A1Support&amp;utm_medium=footer&amp;utm_campaign=Installer" target="_blank">
        <img src="application/views/install/image/footer-logo.png" height="40px">
    </a>
</div>
        </div>
    </div>
</body>
</html>
