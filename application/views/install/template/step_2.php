<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>A1 Support Desk - Installation</title>
<base href="<?= $base_url ?>" />
<link rel="stylesheet" type="text/css" href="application/views/install/stylesheet/stylesheet.css" />
<link rel="icon" type="image/png" href="images/logo.png">
<script type="text/javascript" src="application/views/install/javascript/cufon/cufon-yui.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Verdana_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Trebuchet_MS_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Trebuchet_MS_italic_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Aller_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/jquery.min.js"></script>
<script type="text/javascript" src="application/views/install/javascript/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript"><!--
Cufon.replace('h1', {fontFamily: 'Aller', textShadow: '2px 2px 0 rgba(0,0,0,0.2)'});
Cufon.replace('h2, .blog-list .date, .blog-list .comment, .blog-info .date, .blog-info .comment', {fontFamily: 'Aller', hover: true});
//--></script>
</head>
<body>
<div id="container">
<div id="header">
  <div id="logo">
    <a href="#">
        <img src="images/logo.png" alt="" width="60px" />
    </a>
  </div>
  <div id="logo">
    Support Desk Installation
  </div>
</div>
<h1>Step 2 - License</h1>
<div id="column-right">
  <ul>
    <li>Pre-Installation</li>
    <li><b>License</b></li>
    <li>Configuration</li>
    <li>Finished</li>
  </ul>
</div>
<div id="content">

  <?php if($msg){ ?>
    <div id="notification">
        <div style="" class="warning">
            <?= $msg ?>
        </div>
    </div>
  <?php } ?>

  <?php echo form_open($action, 'method="post" enctype="multipart/form-data"'); ?>

      <fieldset>
            <table class="form licence_table">
                <tr>
                    <td>Buyer</td>
                    <td>
                        <input name='buyer' type="text" value="" class="form-control" required="" />
                    </td>
                </tr>
            	<tr>
                    <td>Purchase Code</td>
                    <td><input type="text" name="code" class="form-control" required /></td>
            	</tr>
            </table>
      </fieldset>

    <div class="terms">
      <h3>License</h3>

<p>This script or plugin is comprised of two parts.</p>

<p>(1) The PHP code and integrated HTML are licensed under the General Public
License (GPL). You will find a copy of the GPL in the same directory as this
text file.</p>

<p>(2) All other parts, but not limited to the CSS code, images, and design are
licensed according to the license purchased from Envato.</p>

<p>Read more about licensing here: http://codecanyon.net/licenses</p>

      <p>Copyright &copy; 2015 WowTech. &lt;http://www.wowtech.co/&gt;</p>
    </div>
    <div class="buttons">
      <div class="right">
        <input name='step_1' type="submit" value="I Agree" class="button" />
      </div>
    </div>
  </form>
</div>
<div id="footer">
    A1 Support Desk v<?= VERSION ?> | Developed by
    <a href="http://www.wowtech.co/?utm_source=A1Support&amp;utm_medium=footer&amp;utm_campaign=Installer" target="_blank">
        <img src="application/views/install/image/footer-logo.png" height="40px">
    </a>
    
</div>
</div>
</body></html>
