<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>A1 Support Desk - Installation</title>
<base href="<?= base_url() ?>" />
<link rel="stylesheet" type="text/css" href="application/views/install/stylesheet/stylesheet.css" />
<link rel="icon" type="image/png" href="images/logo.png">
<script type="text/javascript" src="application/views/install/javascript/cufon/cufon-yui.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Verdana_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Trebuchet_MS_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Trebuchet_MS_italic_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/cufon/Aller_400.font.js"></script>
<script type="text/javascript" src="application/views/install/javascript/jquery.min.js"></script>
<script type="text/javascript" src="application/views/install/javascript/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript"><!--
Cufon.replace('h1', {fontFamily: 'Aller', textShadow: '2px 2px 0 rgba(0,0,0,0.2)'});
Cufon.replace('h2, .blog-list .date, .blog-list .comment, .blog-info .date, .blog-info .comment', {fontFamily: 'Aller', hover: true});
//--></script>
</head>
<body>
<div id="container">
<div id="header">
  <div id="logo">
    <a href="#">
        <img src="images/logo.png" alt="" width="60px" />
    </a>
  </div>
  <div id="logo">
    Support Desk Installation
  </div>
</div>
<h1>Step 4 - Finished!</h1>
<div id="column-right">
  <ul>
    <li>License</li>
    <li>Pre-Installation</li>
    <li>Configuration</li>
    <li><b>Finished</b></li>
  </ul>
</div>
<div id="content">
  <p>Congratulations! You have successfully installed A1 Support Desk v<?= VERSION ?></p>
  <div class="success">
      <center>
          <a href="user/login">
              <img src="application/views/install/image/screenshot.png" alt="" width="630px" />
          </a><br />
          <a class="button" href="user/login">
             Login to Dashboard
          </a>
      </center>
  </div>
</div>

<div id="footer">
    A1 Support Desk v<?= VERSION ?> | Developed by
    <a href="http://www.wowtech.co/?utm_source=A1Support&amp;utm_medium=footer&amp;utm_campaign=Installer" target="_blank">
        <img src="application/views/install/image/footer-logo.png" height="40px">
    </a>
</div>
</div>
</body>
