<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Install extends CI_Controller
{
    //1 licence
    //2 Database 
    //3 Configuration 
    //4 Success

    public function __construct()
    {
        parent::__construct();
    }
    
    //check file permissions
    public function index()
    {
        $data['files'] = array(
            'images/products',
            'images/products/thumbs/',
            'images/services/',
            'images/products/thumbs/',
            'images/users/',
            'images/users/thumbs',
            'attachments/',
        );

        $data['directories'] = array(
            'application/config/database.php',
            'application/config/config.php',
            'application/config/autoload.php',
            'application/config/routes.php',
        );

        //$this->load->library('form_validation');

        $data['msg'] = '';

        if ($this->validatePermission() && 
            $this->input->server('REQUEST_METHOD', true) == 'POST'
        ) {
            redirect($this->getBaseURL().'install/step2');
        }

        $data['action'] = $this->getBaseURL();
        
        $data['base_url'] = $this->getBaseURL(); 
                
        $this->load->view('install/template/index', $data);
    }
    
    public function step2()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('buyer', 'Buyer', 'required');
        $this->form_validation->set_rules('code', 'Purchase_code', 'required');

        if ($this->form_validation->run()) {
            if ($this->verifyPurchase()) {    
                redirect('install/step3');
            } else {
                $data['msg'] = 'Error: Purchase Code Or Buyer Not Valid!';
            }
        } else {
            $data['msg'] = validation_errors();
        }
        
        //set base url                
        $file_config = FCPATH.'application/config/config.php';

        $search_config = array(
            "\$config['base_url'] = ''",
        );

        $replace_config = array(
            "\$config['base_url'] = '".$this->getBaseURL()."'",
        );

        $content_config = str_replace(
            $search_config,
            $replace_config,
            file_get_contents($file_config)
        );

        file_put_contents($file_config, $content_config);

        $data['action'] = $this->getBaseURL().'install/step2';
        
        $data['base_url'] = $this->getBaseURL(); 
        
        $this->load->view('install/template/step_2.php', $data);
    }


    public function validatePermission()
    {
        /*  Files
          -----------------------------
          images/products
          images/products/thumbs/
          images/services/
          images/products/thumbs/
          images/users/
          images/users/thumbs
          attachments/

          Directory
          -----------------------------
          FCPATH."application/config/database.php"
          FCPATH."application/config/config.php"
          FCPATH."application/config/autoload.php"
          FCPATH."application/config/routes.php"
         */

        if (phpversion() < '5.3') {
            return false;
        }

        if (ini_get('register_globals')) {
            return false;
        }

        if (ini_get('magic_quotes_gpc')) {
            return false;
        }

        if (!ini_get('file_uploads')) {
            return false;
        }

        if (ini_get('session_auto_start')) {
            return false;
        }

        if (!ini_get('short_open_tag')) {
            return false;
        }

        if (!ini_get('output_buffering')) {
            return false;
        }

        $extensions = array(
            'mysqli',
            'gd',
            'curl',
            //    'mcrypt_encrypt',
            'zlib',
        );

        if (!function_exists('mcrypt_encrypt')) {
            return false;
        }

        if (!function_exists('iconv')) {
            $extensions[] = 'mbstring';
        }

        $files = array(
            FCPATH.'images/products',
            FCPATH.'images/products/thumbs/',
            FCPATH.'images/services/',
            FCPATH.'images/products/thumbs/',
            FCPATH.'images/users/',
            FCPATH.'images/users/thumbs',
            FCPATH.'attachments/',
        );

        $directories = array(
            FCPATH.'application/config/database.php',
            FCPATH.'application/config/config.php',
            FCPATH.'application/config/autoload.php',
            FCPATH.'application/config/routes.php',
        );

        foreach ($extensions as $ext) {
            if (!extension_loaded($ext)) {
                return false;
            }
        }
        
        foreach ($files as $file) {
            if (!is_writable($file)) {
                return false;
            }
        }

        foreach ($directories as $directory) {
            if (!is_writable($directory)) {
                return false;
            }
        }

        return true;
    }

    //import sql tables
    public function importData($file)
    {
        $this->load->database();

        //install database tables 
        $this->load->helper('file');
        $backup = read_file(FCPATH.$file.'.sql');

        foreach (explode(";\n", $backup) as $sql) {
            $sql = trim($sql);
            if ($sql) {
                $this->db->query($sql);
            }
        }

        redirect('install/success');
    }

    //user configuration 
    public function step3()
    {
        $this->load->library('form_validation');

        //$this->form_validation->set_rules('inputBaseUrl','Base URL','required');
        
        $this->form_validation->set_rules(
            'inputDBhost',
            'Database Host',
            'required'
        );
        
        $this->form_validation->set_rules(
            'inputDBusername',
            'Database Username',
            'required'
        );

        $this->form_validation->set_rules(
            'inputDBname',
            'Database Name',
            'required'
        );

        $this->form_validation->set_rules(
            'inputDBpassword',
            'Database Password',
            ''
        );
        
        $this->form_validation->set_rules('name', 'Name', 'required');
        
        $this->form_validation->set_rules(
                'password', 
                'Password', 
                'required|min_length[6]'
        );
        
        $this->form_validation->set_rules(
            'email',
            'Email address',
            'required|valid_email'
        );

        if ($this->form_validation->run()) {
            
            if ($this->checkDbConnection()) {

                // setting database

                $host = $this->input->post('inputDBhost', true);
                $username = $this->input->post('inputDBusername', true);
                $password = $this->input->post('inputDBpassword', true);
                $database = $this->input->post('inputDBname', true);

                $file_db = FCPATH.'application/config/database.php';

                $search_db = array(
                    "'hostname' => ''",
                    "'username' => ''",
                    "'password' => ''",
                    "'database' => ''"
                );

                $replace_db = array(
                    "'hostname' => '".$host."'",
                    "'username' => '".$username."'",
                    "'password' => '".$password."'",
                    "'database' => '".$database."'"
                );

                $content_db = str_replace(
                    $search_db,
                    $replace_db,
                    file_get_contents($file_db)
                );
                
                file_put_contents($file_db, $content_db);

                $data = array(
                    'name' => $this->input->post('name', true),
                    'email' => $this->input->post('email', true),
                    'password' => md5($this->input->post('password', true)),
                    'register_time' => strtotime(date('d F Y g:i a')),
                    'ip_address' => $this->input->server('REMOTE_ADDR'),
                    'department' => 1,
                    'status' => 1,
                );

                $this->session->set_userdata('temp', $data);

                if (empty($_POST['installSampleDB'])) {
                    redirect('install/importData/install'); // no sample data
                } else {
                    redirect('install/importData/sample'); // yes sample data
                }
            } else {
                $data['msg'] = 'Error: Database Connection problem!';
            }
        } else {
            $data['msg'] = validation_errors('<b>', '</b>');
        }
        $this->load->view('install/template/step_3', $data);
    }

    public function success()
    {        
        //insret user
        $user_data = $this->session->userdata('temp'); 
        
        if ($user_data) {
            $this->load->database();
            $this->db->insert('users', $user_data);        
            $this->session->unset_userdata('temp');
        }
                
        //rename install.php
        $old_name = FCPATH.'application/controllers/Install.php';
        $new_name = FCPATH.'application/controllers/Install_.php';
        rename($old_name, $new_name);

        //change config.php 
        $file_index = FCPATH.'application/config/config.php';
        $content_index = str_replace(
            "\$config['enable_hooks'] = false;",
            "\$config['enable_hooks'] = true;",
            file_get_contents($file_index)
        );
        file_put_contents($file_index, $content_index);

        //change autoload.php
        $file_autoload = FCPATH.'application/config/autoload.php';

        $content_autoload = str_replace(
            "array('session')",
            "array('session', 'database', 'form_validation', 'currency'"
            . ", 'theme')",
            file_get_contents($file_autoload)
        );
        file_put_contents($file_autoload, $content_autoload);

        //set installed = true
        $file_routes = FCPATH.'application/config/routes.php';
        $content_routes = str_replace(
            'install',
            'index',
            file_get_contents($file_routes)
        );
        file_put_contents($file_routes, $content_routes);

        $this->load->view('install/template/success');
    }

    // Database validation check from user input settings
    private function checkDbConnection()
    {
        $link = @mysql_connect(
            $this->input->post('inputDBhost', true),
            $this->input->post('inputDBusername', true),
            $this->input->post('inputDBpassword', true)
        );

        if (!$link) {
            @mysql_close($link);

            return false;
        }

        $db_selected = mysql_select_db(
            $this->input->post('inputDBname', true),
            $link
        );

        if (!$db_selected) {
            @mysql_close($link);

            return false;
        }

        @mysql_close($link);

        return true;
    }

    // Item purchase verification by envato api
    public function verifyPurchase()
    {   
        $buyer = $this->input->post('buyer', true);
        $purchase_code = $this->input->post('code', true);

        $path = 'http://marketplace.envato.com/api/edge/wowtech/';
        $path .= '7eroxktrw1n57wszxhnxzrvyp6e6kfgi/verify-purchase:';
        $path .= $purchase_code.'.xml';

        $curl = curl_init($path);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_USERAGENT,
            'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');

        $purchase_data = curl_exec($curl);
        
        curl_close($curl);

        $result = (array) simplexml_load_string($purchase_data);

        $purchase_data = json_decode(json_encode($result), 1);

        if (isset($purchase_data['verify-purchase']['buyer'])) {
            $buyer_info = $purchase_data['verify-purchase']['buyer'];
        } else {
            $buyer_info = array();
        }

        if (isset($buyer_info) && $buyer_info == $buyer) {
            return true;
        } else {
            return false;
        }
    }
    
    private function getBaseURL()
    {
        $pageURL = 'http';
        
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";            
        }
        
        $pageURL .= "://";
        
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .=
            $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"]
                    .$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        
        $search = array('install/', 'step2', 'index.php');
        $replace = array('', '', '');
                
        $return = str_replace($search, $replace, $pageURL);
        
        return $return;
    }
}
