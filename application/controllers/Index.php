<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Index extends MX_Controller
{
    public function __construct()
    {                
        parent::__construct();
        if (userdata('department') == 0) {
            redirect('tickets');
        }
        $this->load->model('statModel');
    }

    public function index()
    {
        if (!isLogin() == 1) {
            redirect('user/login');
        }

        //total users
        //total tickets 
        //total open tickets 
        //total close tickets 
        //total customers
        //total departments
        //total pending tickets
        //total banned users 
        //total messages

        $data['total_pending_tickets'] = $this->statModel
            ->totalPendingTickets();

        $data['total_tickets'] = $this->statModel->totalTickets();

        $data['total_open_tickets'] = $this->statModel
            ->totalOpenTickets();

        $data['total_close_tickets'] = $this->statModel
            ->totalCloseTickets();

        $data['total_banned_customers'] = $this->statModel
            ->totalBannedCustomers();

        // $data['total_messages'] = $this->statModel->total_messages();
        $data['total_users'] = $this->statModel->totalUsers();
        
        $data['total_customers'] = $this->statModel->totalCustomers();

        $data['total_departments'] = $this->statModel
            ->totalDepartments();

        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/common/home';

        if (file_exists($path)) {
            $this->load->view('themes/'.$theme.'/template/common/home', $data);
        } else {
            $this->load->view('themes/default/template/common/home', $data);
        }
    }
}
