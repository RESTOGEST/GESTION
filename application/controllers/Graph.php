<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Graph extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (userdata('department') == 0) {
            redirect('tickets');
        }

        $this->load->model('statModel');
    }

    public function getGraphData($range = 'year')
    {
        echo json_encode($this->statModel->getGraph($range));
    }
}
