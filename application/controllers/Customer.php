<?php
/**
 * @copyright	Copyright 2015. All rights reserved.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Customer extends MX_Controller
{
    /**
     * Constructor.
     */
    public function customer()
    {
        parent::__construct();
        $this->load->model('customerModel');
        $this->load->library('form_validation');
    }

    /*
     *  Reset password as user
     */
    public function resetPwd($customerId)
    {
        $department = $this->session->userdata('department');

        if ($department == 0) {
            show_404();
        }

        $data['user_id'] = $customerId;

        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/customer/reset_pwd';

        if (file_exists($path)) {
            $this->load->view(
                'themes/'.$theme.'/template/customer/reset_pwd',
                $data
            );
        } else {
            $this->load->view(
                'themes/default/template/customer/reset_pwd',
                $data
            );
        }
    }

    /*
     *  Displays form for new user
     */
    public function create()
    {
        //if department login
        if (isLogin() == 1 && userdata('department') > 0) {

            //new customer 
            $theme = $this->session->userdata('theme');

            $data['departments'] = $this->customerModel->departmentList();

            $template = '/template/customer/create_customer';

            if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
                $this->load->view('themes/'.$theme.$template, $data);
            } else {
                $this->load->view('themes/default'.$template, $data);
            }

            //if customer login
        } elseif (isLogin() == 1) {
            redirect(base_url('tickets/index'), 'refresh');

            //new customer
        } else {
            $theme = $this->session->userdata('theme');

            $template = '/template/customer/register_customer';

            if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
                $this->load->view('themes/'.$theme.$template);
            } else {
                $this->load->view('themes/default'.$template);
            }
        }
    }

    public function registerSuccess()
    {
        $username = $this->session->userdata('username');
        $userdata = $this->customerModel->userData($username);

        $data['name'] = $userdata->name;

        $theme = $this->session->userdata('theme');

        $template = '/template/customer/register_succes';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    public function customerList()
    {
        checkLogin();

        if (userdata('department') == 0) {
            die('Access Forbidden');
        }

        $url = site_url('customer/customerList?');

        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $sort = $this->input->get('sort', true);

        if ($sort) {
            $data['sort'] = $sort;
            $url .= '&sort='.$sort;
        } else {
            $data['sort'] = 'title';
        }

        $order = $this->input->get('order', true);

        if ($order) {
            $data['order'] = $order;
            $url .= '&order='.$order;
        } else {
            $data['order'] = 'ASC';
        }

        $data['q'] = $this->input->get('q', true);

        if ($data['q']) {
            $url .= '&q='.$data['q'];
        }

        $data['filter_banned'] = $this->input->get('filter_banned', true);

        if ($data['filter_banned']) {
            $url .= '&filter_banned='.$data['filter_banned'];
        }

        $limit = config('tickets_per_page');
        $offset = ($page - 1) * $limit;
        $this->load->library('pagination');

        $config['base_url'] = $url;
        
        $config['total_rows'] = $this->customerModel->totalUsers(
            $data['filter_banned'],
            $data['q']
        );
        
        $config['per_page'] = config('tickets_per_page');
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;
        $config['page_query_string'] = true;
        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();
        
        $data['users'] = $this->customerModel->userList(
            $limit,
            $offset,
            $data['filter_banned'],
            $data['sort'],
            $data['order'],
            $data['q']
        );

        /* sort
         * ----------
         * name 
         * email 
         * register_time 
         * status 
         */

        $url = '';

        if ($data['order'] == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if ($data['filter_banned']) {
            $url .= '&filter_banned='.$data['filter_banned'];
        }

        if ($data['q']) {
            $url .= '&q='.$data['q'];
        }

        $base = 'customer/customerList?sort=';

        $data['sort_name'] = site_url($base.'name'.$url);
        $data['sort_email'] = site_url($base.'email'.$url);
        $data['sort_register_time'] = site_url($base.'register_time'.$url);
        $data['sort_status'] = site_url($base.'status'.$url);

        $theme = $this->session->userdata('theme');

        $template = '/template/customer/customer_list';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    /**
     *  Displays user update form.
     *
     *  @return update form 
     */
    public function update($user_id)
    {
        checkLogin();

        if (userdata('department') == 0) {
            die('Access Forbidden');
        }

        $data['user'] = $this->customerModel->getUser($user_id);
        
        $data['departments'] = $this->customerModel->departmentList();

        $theme = $this->session->userdata('theme');

        $template = '/template/customer/customer_update';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    /**
     *  update processing.
     */
    public function updateProcess()
    {
        checkLogin();

        if (userdata('department') == 0) {
            die('Access Forbidden');
        }

        $json = array();

        $this->form_validation->set_rules(
            'name',
            $this->lang->line('name_lastname'),
            'required|trim|htmlspecialchars|min_length[3]|max_length[50]'
        );

        $this->form_validation->set_rules(
            'email',
            $this->lang->line('email'),
            'required|trim|htmlspecialchars|max_length[50]|valid_email'
        );

        $this->form_validation->set_rules(
            'banned',
            $this->lang->line('banned'),
            'trim|htmlspecialchars'
        );

        $this->form_validation->set_rules('user_id',
            $this->lang->line('user_id'),
            'required|trim|htmlspecialchars|numeric'
        );

        $this->form_validation->set_rules(
            'telephone',
            $this->lang->line('telephone'),
            'numeric'
        );
        
        if ($this->form_validation->run() == false) {
            $json['html'] = '<div class="alert alert-danger"><button '
                .'type="button" class="close" data-dismiss="alert" aria-label='
                .'"Close"><span aria-hidden="true">&times;</span></button>'
                .'<ul>'.validation_errors('<li>',
                    '</li>').'</ul></div>';
            $json['status'] = 0;
        } else {
            
            if ($this->customerModel->updateUser()) {
                $json['html'] = '<div class="alert alert-success"><button '
                    .'type="button" class="close" data-dismiss="alert" '
                    .'aria-label="Close"><span aria-hidden="true">&times;'
                    .'</span></button>'.$this->lang->line('update_succesful')
                    .'</div>';
                $json['status'] = 0;
            } else {
                $json['status'] = 0;
                $json['html'] = $this->lang->line('technical_problem');
            }
        }

        echo json_encode($json);
    }

    /**
     * private function - Checks user ID.
     * 
     * @param user_id integer
     *
     * @return bool
     */
    public function checkUserId($user_id)
    {
        return 1;
        
        $result = $this->customerModel->checkUserId($user_id);

        if ($result == 0) {
            
            $this->form_validation->set_message(
                'checkUserId',
                'The user id hidden field is invalid.'
            );

            return false;
        } else {
            return true;
        }
    }

    /*
     * deletes user
     * @param  a user id integer
     * @return string for ajax
     */
    public function delete($user_id)
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        if ($this->checkUserId($user_id)) {
            if ($this->customerModel->delete($user_id)) {
                echo 'deleted';
            }
        }
    }
}
