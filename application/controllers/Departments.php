<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Departments extends MX_Controller
{
    public function departments()
    {
        parent::__construct();

        $this->load->model('departmentsModel'); // Load departments model
        $this->load->library('form_validation'); // Load form validation library

        checkLogin(); // session helper function

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }
    }

    // Listing the Departments, displays new department adding form, makes
    // controls for adding new department,
    public function index()
    {
        $data['departments'] = $this->departmentsModel->departmentList();

        //load view
        $theme = $this->session->userdata('theme');

        $template = '/template/departments/index_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    // Displays department create form
    public function create()
    {
        $theme = $this->session->userdata('theme');

        $template = '/template/departments/create_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template);
        } else {
            $this->load->view('themes/default'.$template);
        }
    }

    public function createProcess()
    {
        $json = array();

        $name = $this->input->post('name');

        $this->form_validation->set_rules(
            'name',
            $this->lang->line('department_name'),
            'required|trim|htmlspecialchars|max_length[50]'
        );

        if ($this->form_validation->run() == false) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } elseif ($this->departmentsModel->existDepName($name) == 1) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('already_exists_dep')
            );
        } else {
            if ($this->departmentsModel->create()) {
                
                $msg1 = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('dept_create_succesful')
                );

                $this->session->set_flashdata('message', $msg1);

                $msg2 = '<script>location.href="'.base_url('departments/index');
                $msg2 .= '";</script>';

                $json['msg'] = $msg2;
            } else {
                $json['msg'] = $this->lang->line('technical_problem');
            }
        }

        echo json_encode($json);
    }

    // Displays department update form
    public function update($department_id)
    {
        $data['department'] = $this->departmentsModel->getDepartment(
            $department_id
        );

        $theme = $this->session->userdata('theme');

        $template = '/template/departments/update_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    public function updateProcess()
    {
        $json = array();

        $name = $this->input->post('name');

        $this->form_validation->set_rules(
            'name',
            $this->lang->line('department_name'),
            'required|trim|htmlspecialchars|max_length[50]'
        );

        $this->form_validation->set_rules(
            'department_id',
            $this->lang->line('department_id'),
            'required|trim|htmlspecialchars|numeric'
        );

        if ($this->form_validation->run() == false) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } elseif ($this->departmentsModel->existDepName($name) == 1) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('technical_problem')
            );
        } else {
            if ($this->departmentsModel->update()) {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $json['msg'] = $this->lang->line('technical_problem');
            }
        }
        echo json_encode($json);
    }

    /* Checks the Departman ID.
     * @param  a department id, integer
     * @return bool
     */
    private function checkDepartmentId($department_id)
    {
        if ($this->departmentsModel->checkDepartmentId($department_id) == 0) {
            $this->form_validation->set_message('checkDepartmentId',
                'The department id hidden field is invalid.');

            return false;
        } else {
            return true;
        }
    }
    /*
     * Deletes the Department and related items.
     * @param  a department id, integer
     * @return string for ajax
     */

    public function delete($department_id)
    {
        if ($this->checkDepartmentId($department_id)) {
            if ($this->departmentsModel->delete($department_id)) {
                echo json_encode(array('msg' => 'deleted'));
            }
        }
    }
}
