<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends MX_Controller
{
    //constuctor
    public function user()
    {
        parent::__construct();

        $this->lang->load('tickets', $this->session->userdata('language'));

        $this->load->model('userModel');
        $this->load->library('form_validation');
    }

    //disable notice 
    public function hideNotice()
    {
        $this->session->set_userdata('hide_notice', 1);
    }

    public function index()
    {
        redirect('user/login');
    }

    //as user 
    public function resetPwd($user_id)
    {
        $department = $this->session->userdata('department');

        if ($department != 1) {
            show_404();
        }

        $data['user_id'] = $user_id;

        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/user/reset_pwd';

        if (file_exists($path)) {
            $this->load->view(
                'themes/'.$theme.'/template/user/reset_pwd',
                $data
            );
        } else {
            $this->load->view('themes/default/template/user/reset_pwd', $data);
        }
    }

    public function resetPwdProcess()
    {
        $this->lang->load('tickets');

        $json = array();

        $this->form_validation->set_rules(
            'password',
            'Password',
            'required|max_length[20]|min_length[6]'
        );

        $this->form_validation->set_rules(
            'confirm_password',
            'Confirm Passoword',
            'required|max_length[20]|min_length[6]|matches[password]'
        );

        if ($this->form_validation->run() == false) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            if ($this->userModel->resetPwd()) {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('reset_pwd_success')
                );
            } else {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }
        
        echo json_encode($json);
    }

    // Login as user
    public function loginAs($user_id)
    {
        $department = $this->session->userdata('department');

        if ($department != 1) {
            show_404();
        }

        $userdata = $this->userModel->getUser($user_id);

        $hash = $this->config->item('password_hash');

        $session_data = array(
            'username' => $userdata->email,
            'name' => $userdata->name,
            'user_id' => $userdata->id,
            'photo' => $userdata->photo,
            'is_chat_banned' => $userdata->is_chat_banned,
            'userhash' => md5($userdata->password.$hash),
            'department' => $userdata->department,
        );

        $this->session->set_userdata($session_data);

        //refresh chat room
        $this->db->query('delete from user_chatroom WHERE (user_id = '.
            $userdata->id.' OR receiver_id='.$userdata->id.')');

        if ($userdata->department) {
            redirect('index'); //admin
        } else {
            redirect(site_url('tickets/index')); //user
        }
    }

    // Displays form for new user
    public function create()
    {
        //if department login
        if (isLogin() == 1 && userdata('department') > 0) {

            //new user 
            $theme = $this->session->userdata('theme');

            $data['departments'] = $this->userModel->departmentList();

            $path = APPPATH.'views/themes/'.$theme.'/template/user/create_user';

            if (file_exists($path)) {
                $this->load->view('themes/'.$theme.'/template/user/create_User',
                    $data);
            } else {
                $this->load->view('themes/default/template/user/create_user',
                    $data);
            }

            //if user login
        } elseif (isLogin() == 1) {
            redirect('tickets/index');

            //new user
        } else {
            
            $data['site_name'] = config('site_name');
            
            $theme = $this->session->userdata('theme');

            $path = APPPATH.'views/themes/'.$theme
                .'/template/user/register_user';

            if (file_exists($path)) {
                $this->load->view(
                    'themes/'.$theme.'/template/user/register_user',
                    $data
                );
            } else {
                $this->load->view('themes/default/template/user/register_user', $data);
            }
        }
    }

    // Makes controls for new user to add new users from admin
    public function addProcess()
    {
        $json = array();

        $this->form_validation->set_rules(
            'name',
            $this->lang->line('name_lastname'),
            'required|trim|htmlspecialchars|min_length[3]|max_length[50]'
        );

        $this->form_validation->set_rules(
            'email',
            $this->lang->line('email'),
            'required|trim|htmlspecialchars|max_length[50]|valid_email'
        );

        $this->form_validation->set_rules(
            'pass1',
            $this->lang->line('password'),
            'required|trim|htmlspecialchars|min_length[6]|max_length[20]'
        );

        $this->form_validation->set_rules(
            'telephone',
            $this->lang->line('telephone'),
            'numeric'
        );

        $email = $this->input->post('email', true);

        if ($this->form_validation->run() == false) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } elseif ($this->userModel->existsEmail($email) > 0) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('already_account')
            );
        } else {
            if ($this->userModel->createUser()) {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('msg_new_user_added')
                );
            } else {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }
        echo json_encode($json);
    }

    public function createProcess()
    {
        $json = array();

        $this->form_validation->set_rules(
            'name',
            $this->lang->line('name_lastname'),
            'required|trim|htmlspecialchars|min_length[3]|max_length[50]'
        );

        $this->form_validation->set_rules(
            'email',
            $this->lang->line('email'),
            'required|trim|htmlspecialchars|max_length[50]|valid_email'
        );

        $this->form_validation->set_rules(
            'pass1',
            $this->lang->line('password'),
            'required|trim|htmlspecialchars|min_length[6]|max_length[20]'
        );

        $email = $this->input->post('email', true);

        if ($this->form_validation->run() == false) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } elseif ($this->userModel->existsEmail($email) > 0) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('already_account')
            );
        } else {
            if ($this->userModel->createUser()) {

                // login process
                $session_data = array(
                    'username' => $this->input->post('email', true),
                    'name' => $this->input->post('name', true),
                    'department' => 0,
                    'photo' => null,
                    'userhash' => md5(md5($this->input->post('pass1', true))
                        .$this->config->item('password_hash')),
                );

                $this->session->set_userdata('user_id', $this->db->insert_id());
                $this->session->set_userdata($session_data);

                $json['msg'] = '<script>location.href="'
                    .base_url('user/registerSuccess').'";</script>';
            } else {
                $json['msg'] = $this->lang->line('technical_problem');
            }
        }
        echo json_encode($json);
    }

    public function registerSuccess()
    {
        $userdata = $this->userModel->userData(
            $this->session->userdata('username')
        );

        $data['name'] = $userdata->name;

        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/user/register_succes';

        if (file_exists($path)) {
            $this->load->view(
                'themes/'.$theme.'/template/user/register_succes',
                $data
            );
        } else {
            $this->load->view(
                'themes/default/template/user/register_succes',
                $data
            );
        }
    }

    public function lostpwSuccess()
    {
        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/user/lostpw_succes';

        if (file_exists($path)) {
            $this->load->view('themes/'.$theme.'/template/user/lostpw_succes');
        } else {
            $this->load->view('themes/default/template/user/lostpw_succes');
        }
    }

    // Displays the login form
    public function login()
    {
//        if (isLogin() == 1) {
//
//            $department = $this->session->userdata('department');
//
//            if ($department) {
//                redirect(base_url('tickets/index'));
//            } else {
//                redirect(base_url('index'));
//            }
//        }

        $data['site_name'] = config('site_name');
        
        $user_id = $this->session->userdata('user_id');

        $data['chats'] = array();

        if ($user_id) {
            $data['chats'] = $this->db->query('select * from user_chatroom '
                .'WHERE (user_id = '.$user_id.' OR receiver_id='
                .$user_id.')')->result_array();
        }

        $data['user_id'] = $user_id;

        $data['timezone'] = $this->session->userdata('timezone');
        
        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/user/login_user';

        if (file_exists($path)) {
            $this->load->view(
                'themes/'.$theme.'/template/user/login_user',
                $data
            );
        } else {
            $this->load->view('themes/default/template/user/login_user', $data);
        }
    }

    public function loginProcess()
    {
        $json = array();

        $this->form_validation->set_rules(
            'password',
            $this->lang->line('password'),
            'required|trim|htmlspecialchars|min_length[6]|max_length[20]'
        );

        $this->form_validation->set_rules(
            'email',
            $this->lang->line('email'),
            'required|trim|htmlspecialchars|max_length[50]|valid_email'
        );

        $email = $this->input->post('email', true);

        if ($this->form_validation->run() == false) {

            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );

        } elseif ($this->userModel->existsEmail($email) == 0) {

            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('you_must_create_account')
            );
            
        } elseif ($this->userModel->checkUserDetail() == false) {

            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('invalid_email_or_pass')
            );

        } else {
            $email = $this->input->post('email', true);

            $userdata = $this->userModel->userData($email);

            $session_data = array(
                'username' => $userdata->email,
                'name' => $userdata->name,
                'user_id' => $userdata->id,
                'photo' => $userdata->photo,
                'is_chat_banned' => $userdata->is_chat_banned,
                'userhash' => md5($userdata->password
                    .$this->config->item('password_hash')),
                'isLogin' => true,
                'department' => $userdata->department,
            );

            $this->session->set_userdata($session_data);

            //refresh chat room
            $this->db->query('delete from user_chatroom WHERE (user_id = '
                .$userdata->id.' OR receiver_id='.$userdata->id.')');

            if ($userdata->department) {
                $json['msg'] = '<script>location.href="'
                    .base_url('index').'";</script>'; //admin
            } else {
                $json['msg'] = '<script>location.href="'
                    .base_url('tickets/index').'";</script>'; //user
            }
        }
        echo json_encode($json);
    }

    // Logout function
    public function logout()
    {
        $this->session->sess_destroy();        
        redirect('user/login');
    }

    // Displays the Password reset form
    public function lostpassword()
    {
        if (isLogin() == 1) {
            redirect(base_url('tickets/index'), 'refresh');
        }

        $data['site_name'] = config('site_name');
        
        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/user/lostpassword';

        if (file_exists($path)) {
            $this->load->view('themes/'.$theme.'/template/user/lostpassword', $data);
        } else {
            $this->load->view('themes/default/template/user/lostpassword', $data);
        }
    }

    // Sends request for password reset
    public function lostpasswordProcess()
    {
        $json = array();

        $this->form_validation->set_rules(
            'email',
            $this->lang->line('email'),
            'required|trim|htmlspecialchars|valid_email'
        );

        $email = $this->input->post('email', true);

        if ($this->form_validation->run() == false) {
            
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );

        } elseif ($this->userModel->existsEmail($email) == 0) {
            
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('you_must_create_account')
            );
            
        } else {
            
            $lostpw_code = $this->userModel->createLostpwCode();

            $subject = 'Reset password request';

            $message = 'Hello, <br> To reset your password please follow '
                .'the link below: <br> <a href="'
                .base_url('user/checkCode/'.$this->input->post('email', true).'/'
                .$lostpw_code).'">'.base_url('user/checkCode/'
                .$this->input->post('email', true).'/'.$lostpw_code).'</a>';

            sendNotice($this->input->post('email', true), $subject, $message);

            $json['msg'] = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('lostpw_message1')
            );
        }

        echo json_encode($json);
    }

    // Checks the reset password code
    public function checkCode($email, $code)
    {
        $status = $this->userModel->checkCode($email, $code);

        if ($status == 0) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('invalid_reset_code')
            );

            $this->session->set_flashdata('message', $msg);

            redirect('user/login');
        } else {
            $new_password = $this->userModel->createNewPassword($email);

            $subject = 'New Password';

            $message = 'Hello, <br><br> New password is <b>'.$new_password
                .'</b>. Please <a href="'.base_url('user/login')
                .'">click here</a> for login';

            sendNotice($email, $subject, $message);

            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('new_pass_sent')
            );

            $this->session->set_flashdata('message', $msg);

            redirect('user/login');
        }
    }

    public function mailTemplate()
    {
        checkLogin();

        if (userdata('department') == 1) {
            $data['user'] = $this->userModel->userData(userdata('email'));
            $data['language'] = config('language');

            $theme = $this->session->userdata('theme');

            $path = APPPATH.'views/themes/'.$theme
                .'/template/user/mail_template_settings';

            if (file_exists($path)) {
                $this->load->view(
                    'themes/'.$theme.'/template/user/mail_template_settings',
                    $data
                );
            } else {
                $this->load->view(
                    'themes/default/template/user/mail_template_settings',
                    $data
                );
            }
        }
    }

    public function updateMailTemplate()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $this->form_validation->set_rules(
            'new_ticket_template',
            $this->lang->line('new_ticket_template'),
            'required'
        );

        $this->form_validation->set_rules(
            'new_ticket_subject',
            $this->lang->line('new_ticket_subject'),
            'required'
        );

        if ($this->form_validation->run() == false) {
            echo sprintf(
                    $this->lang->line('msg_danger'),
                    '<ul>'.validation_errors('<li>', '</li>').'</ul>'
                );
        } else {
            if ($this->userModel->updateMailTemplate()) {
                echo sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                echo sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }
    }

    // Displays the settings
    public function settings()
    {
        checkLogin();

        if (userdata('department') == 1) {

            $this->load->model('ticketModel');
            $this->load->helper('directory');

            $data['user'] = $this->userModel->userData(userdata('email'));
            $data['language'] = config('language');
            
            $data['languages'] = directory_map(APPPATH.'/language', 1);
            
            $theme = $this->session->userdata('theme');

            $path = APPPATH.'views/themes/'.$theme
                .'/template/user/admin_settings';

            if (file_exists($path)) {
                $this->load->view(
                    'themes/'.$theme.'/template/user/admin_settings',
                    $data
                );
            } else {
                $this->load->view(
                    'themes/default/template/user/admin_settings',
                    $data
                );
            }
        } else {
            $data['user'] = $this->userModel->userData(userdata('email'));

            $theme = $this->session->userdata('theme');

            $path = APPPATH.'views/themes/'.$theme
                .'/template/user/user_settings';

            if (file_exists($path)) {
                $this->load->view(
                    'themes/'.$theme.'/template/user/user_settings',
                    $data
                );
            } else {
                $this->load->view(
                    'themes/default/template/user/user_settings',
                    $data
                );
            }
        }
    }

    public function changePassword()
    {
        checkLogin();

        $json = array();

        $this->form_validation->set_rules(
            'currentpass',
            $this->lang->line('current_password'),
            'required|trim|htmlspecialchars|min_length[6]|max_length[20]'
        );

        $this->form_validation->set_rules(
            'pass1',
            $this->lang->line('new_password'),
            'required|trim|htmlspecialchars|min_length[6]|max_length[20]'
            .'|matches[pass2]'
        );

        $this->form_validation->set_rules(
            'pass2',
            $this->lang->line('new_password_again'),
            'required|trim|htmlspecialchars|min_length[6]|max_length[20]'
        );

        if ($this->form_validation->run() == false) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } elseif ($this->userModel->checkPassword() == 0) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                $this->lang->line('invalid_pass')
            );
        } else {
            if ($this->userModel->passwordUpdate()) {
                $session_data = array(
                    'username' => userdata('email'),
                    'userhash' => md5(userdata('password')
                        .$this->config->item('password_hash')),
                    'department' => userdata('department'),
                );

                $this->session->set_userdata($session_data);

                $json['msg'] = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }
        echo json_encode($json);
    }

    public function changeEmail()
    {
        checkLogin();

        $this->form_validation->set_rules(
            'email', $this->lang->line('email'),
            'required|trim|htmlspecialchars|max_length[20]|valid_email'
        );

        if ($this->form_validation->run() == false) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            $session_data = array(
                'username' => $this->input->post('email', true),
                'userhash' => md5(userdata('password')
                    .$this->config->item('password_hash')),
                'department' => userdata('department'),
            );

            if ($this->userModel->changeEmail()) {
                $this->session->set_userdata($session_data);
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $msg = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }
        echo $msg;
    }

    public function updateDetails()
    {
        checkLogin();

        $this->db->set('details', $this->input->post('details', true));
        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('users');

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('detail_update_succesful')
        );

        echo json_encode(array('msg' => $msg));
    }

    public function uploadSettings()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $this->form_validation->set_rules(
            'allowed_extensions',
            $this->lang->line('allowed_files'),
            'required|trim|htmlspecialchars|max_length[50]'
        );

        $this->form_validation->set_rules(
            'max_upload_files',
            $this->lang->line('max_upload_files'),
            'required|trim|htmlspecialchars|max_length[2]|numeric'
        );

        $this->form_validation->set_rules(
            'max_upload_file_size',
            $this->lang->line('max_upload_file_size'),
            'required|trim|htmlspecialchars|max_length[5]|numeric'
        );

        if ($this->form_validation->run() == false) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            if ($this->userModel->uploadSettings()) {
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $msg = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }

        echo $msg;
    }

    public function authSettings()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $this->form_validation->set_rules(
            'auth_trans_key',
            $this->lang->line('auth_trans_key'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'auth_login_id',
            $this->lang->line('auth_login_id'),
            'required|trim'
        );

        if ($this->form_validation->run() == false) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            if ($this->userModel->authSettings()) {
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $msg = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }

        echo $msg;
    }

    public function paypalSettings()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $this->form_validation->set_rules(
            'paypal_signature',
            $this->lang->line('signature'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'paypal_username',
            $this->lang->line('username'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'paypal_password',
            $this->lang->line('password'),
            'required|trim'
        );

        if ($this->form_validation->run() == false) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            if ($this->userModel->paypalSettings()) {
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $msg = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }

        echo $msg;
    }

    public function chatSettings()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $this->form_validation->set_rules(
            'online_check_time',
            $this->lang->line('online_check_time'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'online_update_time',
            $this->lang->line('online_update_time'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'message_check_time',
            $this->lang->line('message_check_time'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'notification_check_time',
            $this->lang->line('notification_check_time'),
            'required|trim'
        );

        if ($this->form_validation->run() == false) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            if ($this->userModel->chatSettings()) {
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $msg = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }

        echo $msg;
    }

    public function envatoSettings()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $this->form_validation->set_rules(
            'envato_app_id',
            $this->lang->line('envato_app_id'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'envato_username',
            $this->lang->line('envato_username'),
            'required|trim'
        );

        if ($this->form_validation->run() == false) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            if ($this->userModel->envatoSettings()) {
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $msg = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }

        echo $msg;
    }

    public function smtpSettings()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $this->form_validation->set_rules(
            'smtp_protocol',
            $this->lang->line('smtp_protocol'),
            'required|trim'
        );

        if ($this->form_validation->run() == false) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            if ($this->userModel->smtpSettings()) {
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $msg = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }

        echo $msg;
    }

    public function generalSettings()
    {
        checkLogin();
        
        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $this->form_validation->set_rules(
            'site_name',
            $this->lang->line('site_name'),
            'required|trim|htmlspecialchars|max_length[255]'
        );

        $this->form_validation->set_rules(
            'site_email',
            $this->lang->line('site_email'),
            'required|trim|htmlspecialchars|max_length[60]|valid_email'
        );

        $this->form_validation->set_rules(
            'tickets_per_page',
            $this->lang->line('tickets_per_page'),
            'required|trim|htmlspecialchars|max_length[3]|numeric'
        );

        if ($this->form_validation->run() == false) {
            $msg = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {
            if ($this->userModel->generalSettings()) {
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
            } else {
                $msg = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }

        echo $msg;
    }

    public function userList()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $url = site_url('user/userList?');

        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $sort = $this->input->get('sort', true);

        if ($sort) {
            $data['sort'] = $sort;
            $url .= '&sort='.$sort;
        } else {
            $data['sort'] = 'title';
        }

        $order = $this->input->get('order', true);

        if ($order) {
            $data['order'] = $order;
            $url .= '&order='.$order;
        } else {
            $data['order'] = 'ASC';
        }

        $data['q'] = $this->input->get('q', true);

        if ($data['q']) {
            $url .= '&q='.$data['q'];
        }

        $limit = config('tickets_per_page');
        $offset = ($page - 1) * $limit;
        $this->load->library('pagination');

        $config['base_url'] = $url;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->userModel->totalUsers($data['q']);
        $config['per_page'] = config('tickets_per_page');
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        $data['users'] = $this->userModel->userList($limit,
            $offset, $data['sort'], $data['order'], $data['q']);

        $url = '';

        if ($data['order'] == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if ($data['q']) {
            $url .= '&q='.$data['q'];
        }

        $base = 'user/userList?sort=';

        $data['sort_name'] = site_url($base.'users.name'.$url);
        $data['sort_department'] = site_url($base.'departments.department_name'.$url);
        $data['sort_email'] = site_url($base.'users.email'.$url);
        $data['sort_register_time'] = site_url($base.'users.register_time'.$url);
        $data['sort_status'] = site_url($base.'users.status'.$url);

        $theme = $this->session->userdata('theme');

        if (file_exists(APPPATH.'views/themes/'.$theme.'/template/user/user_list')) {
            $this->load->view('themes/'.$theme.'/template/user/user_list', $data);
        } else {
            $this->load->view('themes/default/template/user/user_list', $data);
        }
    }

    // Displays user update form
    public function update($user_id)
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $data['user'] = $this->userModel->getUser($user_id);
        $data['departments'] = $this->userModel->departmentList();

        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/user/user_update';

        if (file_exists($path)) {
            $this->load->view(
                'themes/'.$theme.'/template/user/user_update',
                $data
            );
        } else {
            $this->load->view(
                'themes/default/template/user/user_update',
                $data
            );
        }
    }

    // update processing
    public function updateProcess()
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        $json = array();

        $this->form_validation->set_rules(
            'name',
            $this->lang->line('name_lastname'),
            'required|trim|htmlspecialchars|min_length[3]|max_length[50]'
        );

        $this->form_validation->set_rules(
            'email',
            $this->lang->line('email'),
            'required|trim|htmlspecialchars|max_length[50]|valid_email'
        );

        $this->form_validation->set_rules(
            'department',
            $this->lang->line('user_dep'),
            'required|trim|htmlspecialchars|numeric'
        );

        $this->form_validation->set_rules(
            'banned',
            $this->lang->line('banned'),
            'trim|htmlspecialchars'
        );

        $this->form_validation->set_rules('user_id',
            $this->lang->line('user_id'),
            'required|trim|htmlspecialchars|numeric'
        );

        $this->form_validation->set_rules(
            'telephone',
            $this->lang->line('telephone'),
            'numeric'
        );

        if ($this->form_validation->run() == false) {
            $json['html'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
            $json['status'] = 0;
        } else {
            if ($this->userModel->updateUser()) {
                $json['html'] = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('update_succesful')
                );
                $json['status'] = 0;
            } else {
                $json['status'] = 0;
                $json['html'] = $this->lang->line('technical_problem');
            }
        }

        echo json_encode($json);
    }

    /* private function - Checks user ID
     * @param  a user id integer
     * @return bool
     */
    private function checkUserId($user_id)
    {
        if ($this->userModel->checkUserId($user_id) == 0) {
            $this->form_validation->set_message('checkUserId',
                'The user id hidden field is invalid.');

            return false;
        } else {
            return true;
        }
    }

    /* deletes user
     * @param  a user id integer
     * @return string for ajax
     */
    public function delete($user_id)
    {
        checkLogin();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        if ($this->checkUserId($user_id)) {
            if ($this->userModel->delete($user_id)) {
                echo 'deleted';
            }
        }
    }
}
