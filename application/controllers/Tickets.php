<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tickets extends MX_Controller
{
    public function Tickets()
    {
        parent::__construct();
        $this->load->model('userModel'); // load user database model
        $this->load->model('ticketModel'); // load ticket database model
        $this->load->library('form_validation'); // load form validation library
    }

    // Listing and paging tickets.
    public function index()
    {
        checkLogin();

        if (is_numeric($this->uri->segment(3))) {
            $start = $this->uri->segment(3);
        } else {
            $start = 0;
        }

        $url = '';

        $filter_data['filter_department'] =
            $this->input->get('filter_department', true);

        if ($filter_data['filter_department']) {
            $url .= '&filter_department='.$filter_data['filter_department'];
        }

        $filter_data['filter_agent'] = $this->input->get('filter_agent', true);

        if ($filter_data['filter_agent']) {
            $url .= '&filter_agent='.$filter_data['filter_agent'];
        }

        $filter_data['filter_customer_name'] =
            $this->input->get('filter_customer_name', true);

        if ($filter_data['filter_customer_name']) {
            $url .= '&filter_customer_name='
                .$filter_data['filter_customer_name'];
        }

        $filter_data['filter_customer_email'] =
            $this->input->get('filter_customer_email', true);

        if ($filter_data['filter_customer_email']) {
            $url .= '&filter_customer_email='.
                $filter_data['filter_customer_email'];
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url('tickets/index');
        $config['total_rows'] = $this->ticketModel->totalTickets($filter_data);
        $config['per_page'] = config('tickets_per_page');
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;

        $this->pagination->initialize($config);

        $data = $filter_data;

        $data['filter_agent_name']
            = $this->ticketModel->getAgentName($filter_data['filter_agent']);
        
        $data['links'] = $this->pagination->create_links();

        $data['open_tickets'] = $this->ticketModel->openTickets($filter_data);

        $data['pending_tickets']
            = $this->ticketModel->pendingTickets($filter_data);
        
        $data['other_tickets'] = $this->ticketModel->otherTickets(
            $start,
            $config['per_page'],
            $filter_data
        );

        $data['departments'] = $this->db->get('departments')->result_array();

        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/tickets/ticket_list';

        if (file_exists($path)) {
            
            $this->load->view(
                'themes/'.$theme.'/template/tickets/ticket_list',
                $data
            );
            
        } else {
            
            $this->load->view(
                'themes/default/template/tickets/ticket_list',
                $data
            );
        }
    }

    // Shows new ticket form
    public function create()
    {
        checkLogin();

        $data['departments'] = $this->ticketModel->departments();
        $data['products'] = $this->ticketModel->products();
        $data['services'] = $this->ticketModel->services();
        
        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/tickets/create_ticket';

        if (file_exists($path)) {
            
            $this->load->view(
                'themes/'.$theme.'/template/tickets/create_ticket',
                $data
            );

        } else {

            $this->load->view(
                'themes/default/template/tickets/create_ticket',
                $data
            );
        }
    }

    // new ticket processing
    public function createProcess()
    {
        checkLogin();

        $json = array();

        $this->form_validation->set_rules(
            'title',
            $this->lang->line('title'),
            'required|trim|htmlspecialchars|min_length[3]|max_length[255]'
        );
        
        $this->form_validation->set_rules(
            'ftp_db_access',
            $this->lang->line('ftp_db_access'),
            'trim|max_length[2000]'
        );

        $this->form_validation->set_rules(
            'department',
            $this->lang->line('department'),
            'required|trim|htmlspecialchars'
        );

        $this->form_validation->set_rules(
            'priority',
            $this->lang->line('priority'),
            'required|trim|htmlspecialchars'
        );

        $this->form_validation->set_rules(
            'message',
            $this->lang->line('message'),
            'required|trim|min_length[6]|max_length[2000]'
        );

        if ($this->form_validation->run() == false) {

            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );

        } else {

            $ticket_id = $this->ticketModel->createTicket();

            if ($ticket_id > 0) {

                // send e-mail
                $email = config('site_email');
                
                $subject = sprintf(
                    $this->lang->line('new_ticket_from_customer_subject'),
                    $ticket_id,
                    date('d F Y g:i a')
                );

                $message = $this->lang->line('new_ticket_from_customer');

                sendNotice($email, $subject, $message);

                $json['msg'] = '<script>location.href="'.
                    base_url('tickets/id/'.$ticket_id).'";</script>';
                
            } else {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_danger'),
                    $this->lang->line('technical_problem')
                );
            }
        }
        echo json_encode($json);
    }

    // Checks whether the ticket is available in the system
    private function existsTicket($ticket_id)
    {
        if ($this->ticketModel->existsTicket($ticket_id) == 0) {
            
            $this->form_validation->set_message(
                'existsTicket',
                'The ticket id hidden field is invalid.'
            );

            return false;
        } else {
            return true;
        }
    }
    
    /*
     * Private Function - Checks whether the Department ID is available in the
     *  system
     * @param  a department id integer
     * @return bool
     */
    private function checkDepartmentId($id)
    {
        if ($this->ticketModel->check_department_id($id) == 0) {
            
            $this->form_validation->set_message(
                'checkDepartmentId',
                'The Department field is required.'
            );

            return false;
            
        } else {
            return true;
        }
    }
    
    /*
     * Private Function - check priority id
     * @param  a priority id integer
     * @return bool
     */
    private function checkPriorityId($id)
    {
        $priority = array(1, 2, 3);

        return in_array($id, $priority) ? true : false;
    }
    
    /* Mail link to login + redirect to view
     * @param  a ticket id integer
     */
    public function mailLink($ticket_id, $secret)
    {
        $ticket = $this->db->get_where('tickets', array('id' => $ticket_id))
            ->row_array();

        if ($ticket['secret'] == $secret) {
            $this->load->model('userModel');

            $userdata = $this->userModel->getUser($ticket['user_id']);

            $session_data = array(
                'username' => $userdata->email,
                'name' => $userdata->name,
                'user_id' => $userdata->id,
                'photo' => $userdata->photo,
                'is_chat_banned' => $userdata->is_chat_banned,
                'userhash' => md5($userdata->password
                    .$this->config->item('password_hash')),
                'department' => $userdata->department,
            );

            $this->session->set_userdata($session_data);

            redirect('tickets/id/'.$ticket_id);
            
        } else {
            die('Accesss denied!');
        }
    }
    
    /*
     * Displays details of the ticket according to incoming ticket ID.
     * @param  a ticket id integer
     */
    public function id($ticket_id = 0)
    {
        checkLogin();

        if (is_numeric($ticket_id) && 
            $this->ticketModel->existsTicket($ticket_id) == 1) {
            
            $data['ticket'] = $this->ticketModel->ticketDetails($ticket_id);
            $data['messages'] = $this->ticketModel->ticketMessages($ticket_id);

            if (is_object($data['ticket'])) {
                
                $data['department'] = $this->ticketModel->getDepartmentName(
                    $data['ticket']->department_id
                );

                //get product 
                $data['product'] = $this->ticketModel->getProductName(
                    $data['ticket']->product_id
                );
                                
                //get product 
                $data['service'] = $this->ticketModel->getServiceName(
                    $data['ticket']->service_id
                );
                
                $theme = $this->session->userdata('theme');

                $path = APPPATH.'views/themes/'.$theme
                    .'/template/tickets/ticketDetails';

                if (file_exists($path)) {
                    
                    $this->load->view(
                        'themes/'.$theme.'/template/tickets/ticket_details',
                        $data
                    );
                    
                } else {

                    $this->load->view(
                        'themes/default/template/tickets/ticket_details',
                        $data
                    );
                }
            } else {
                echo 'Access Denied';
            }
        } else {
            echo 'Invalid ticket id';
        }
    }

    //update user ftp database details 
    public function updateAccess() 
    {
        $this->ticketModel->updateAccess();
    }
    
    //update customer details 
    public function updateDetails()
    {
        $this->ticketModel->updateDetails();
    }
    
    // reply message processing
    public function replyProcess()
    {        
        checkLogin();

        $json = array();

        $this->form_validation->set_rules(
            'message',
            $this->lang->line('message'),
            'required|trim|min_length[2]|max_length[2000]'
        );

        $this->form_validation->set_rules(
            'ticket_id',
            $this->lang->line('ticket_id'),
            'required|trim|htmlspecialchars|numeric'
        );

        if ($this->form_validation->run() == false) {
            $json['status'] = 0;
            $json['html'] = '<ul>'.validation_errors('<li>', '</li>').'</ul>';
        } else {
            $insert_id = $this->ticketModel->newMessage();

            $messageDetails = $this->ticketModel->messageDetails($insert_id);

            if (userdata('department') == 0) {
                $email = config('site_email');

                $subject = sprintf(
                    $this->lang->line('new_message_from_customer_subject'),
                    $this->input->post('ticket_id', true),
                    date('d F Y g:i a')
                );
                
                $message = $this->lang->line('new_message_from_customer');

                sendNotice($email, $subject, $message);

                $html = '<div class="direct-chat-msg">'
                    . '<div class="direct-chat-info clearfix">'

                    . '<span class="direct-chat-name pull-left">'.
                         $this->lang->line('customer')
                    . '</span>'

                    . '<span class="direct-chat-timestamp pull-right">'
                    .    date('d F Y g:i a', $messageDetails->created_time)
                    . '</span>'

                    . '</div>'
                    
                    . '<img alt="message user image" src="'.base_url()
                    . 'images/customer-user.png" class="direct-chat-img" />'
                    
                    . '<div class="direct-chat-text">';
                        $html .= $messageDetails->message;

                    if ($messageDetails->files) {
                        $temp = explode(',', $messageDetails->files);

                        $html .= '<div class="attachments"><h4>'.
                            $this->lang->line('attach').' : </h4><ul>';

                        foreach ($temp as $file) {
                            $html .= '<li><a href="'.base_url('attachments/'.$file)
                                .'" target="_blank">'.$file.'</a></li>';
                        }

                        $html .= '</ul></div>';
                    }

                $html .= '</div></div>';
                
            } else {

                $ticket_id = $this->input->post('ticket_id', true);

                $email = config('site_email');

                $subject = sprintf(
                    $this->lang->line('new_message_from_support_subject'),
                    $this->input->post('ticket_id', true),
                    date('d F Y g:i a')
                );
                
                $message = $this->lang->line('new_message_from_support');

                $ticket_detail = $this->ticketModel->ticketDetails($ticket_id);

                sendNotice($ticket_detail->email, $subject, $message);

                $html = '<div class="direct-chat-msg right">';
                $html .= '    <div class="direct-chat-info clearfix">';
                $html .= '        <span class="direct-chat-name pull-right">';
                $html .= $this->lang->line('support').'</span>';
                $html .= '        <span class="direct-chat-timestamp pull-left">';
                $html .= date('d F Y g:i a', $messageDetails->created_time);
                $html .= '        </span>';
                $html .= '    </div><!-- /.direct-chat-info -->';
                $html .= '    <img alt="message user image" src="'.base_url();
                $html .= 'images/support-user.png" class="direct-chat-img">';
                $html .= '    <div class="direct-chat-text">';
                $html .= $messageDetails->message;

                if ($messageDetails->files) {
                    $html .= '<div class="attachments">
                                <h4>'.$this->lang->line('attach').' : </h4>
                        <ul>';

                    $temp = explode(',', $messageDetails->files);

                    foreach ($temp as $file) {
                        $html .= '<li><a href="'.base_url('attachments/'.$file)
                            .'" target="_blank">'.$file.'</a></li>';
                    }

                    $html .= '</ul></div>';
                }

                $html .= '</div><!-- /.direct-chat-text -->'
                    . '</div><!-- /.direct-chat-msg -->';
            }

            $json['status'] = 1;
            $json['html'] = $html;
        }

        echo json_encode($json);
    }
    
    /*
     * delete ticket and ticket's messages
     * @param  a ticket id integer
     * @return string for ajax
     */
    public function delete($ticket_id)
    {
        checkLogin();

        if (is_numeric($ticket_id) &&
            $this->ticketModel->existsTicket($ticket_id) == 1 &&
            userdata('department') > 0) {

            if ($this->ticketModel->deleteTicket($ticket_id)) {
                echo json_encode(array('status' => 1)); // dont change !! This is key
            }
            
        } else {
            echo json_encode(array('status' => 0)); // dont change !! This is key
        }
    }
    
    /*
     * Close the ticket
     * @param  a ticket id integer
     * @return string for ajax
     */
    public function close($ticket_id)
    {
        checkLogin();

        $exists = $this->ticketModel->existsTicket($ticket_id);

        if (is_numeric($ticket_id) && $exists == 1) {
            
            if ($this->ticketModel->closeTicket($ticket_id)) {
                
                echo json_encode(array('status' => 1));
            }
        }
    }

    public function upload()
    {
        checkLogin();

        $json = array();

        $config['upload_path'] = FCPATH.'attachments/';
        $config['allowed_types'] = config('allowed_extensions');
        $config['max_size'] = config('max_upload_file_size');
        $config['encrypt_name'] = true;

        $this->load->library('upload');

        $this->upload->initialize($config);

        //upload_path

        if (!$this->upload->do_upload('files')) {
            
            $json['status'] = 0;
            $json['msg'] = $this->upload->display_errors();

        } else {

            $rows = $this->upload->data();

            //if single file was uploaded
            if (isset($rows['file_name'])) {
                $rows = array(0 => $rows);
            }

            $implode1 = $implode2 = array();

            $i = 1;
            foreach ($rows as $row) {
                $implode1[] = '('.$i.') '.$row['orig_name'];
                $implode2[] = $row['file_name'];
                ++$i;
            }

            $json['html'] = '<strong>Files uploaded: </strong> '.implode(', ',
                    $implode1).'<input type="hidden" id="file_list"'
                .' name="files" value="'.implode(',',
                    $implode2).'" />';

            $json['status'] = 1;

            header('Content-type: application/json');
            
            echo json_encode($json);
        }
    }

    public function uploadedFiles($ticket_id = 0)
    {
        checkLogin();

        if ($ticket_id != 0 && !$this->existsTicket($ticket_id)) {
            die('Invalid Ticket ID');
        }

        $files = $this->ticketModel->getAttachmentsTmp($ticket_id);

        if (!empty($files)) {
            foreach ($files as $file) {
                $temp = '<a href="javascript:void(0)" onclick="deleteFile(\''
                    .$file->file_name.'\');"><img src="'
                    .base_url('images/delete.gif').'" />'
                    .'</a><a href="'.base_url('attachments/'.$file->file_name)
                    .'" target="_blank">'.$file->file_name.'</a><br />';

                echo $temp;
            }
        }
    }

    public function deleteFile($file_name)
    {
        checkLogin();

        if ($this->ticketModel->existsFile($file_name) == 0) {
            die('Invalid file name');
        }

        if ($this->ticketModel->deleteFile($file_name)) {
            unlink('./attachments/'.$file_name);
            echo json_encode(array('status' => 1));
        }
    }

    public function ticketAssign($ticket_id = 0)
    {
        checkLogin();

        $this->form_validation->set_rules(
            'agent_id',
            $this->lang->line('agent'), 
            'required|trim'
        );

        if ($this->form_validation->run() == false) {
            
            $data['ticket_id'] = $ticket_id;

            //departments
            $data['departments'] = $this->db->get('departments')
                ->result_array();

            $theme = $this->session->userdata('theme');

            $tempate = '/template/tickets/agent_assign';

            if (file_exists(APPPATH.'views/themes/'.$theme.$tempate)) {
                $this->load->view('themes/'.$theme.$tempate, $data);
            } else {
                $this->load->view('themes/default'.$tempate, $data);
            }
        } else {
            $agent_name = $this->ticketModel->ticketAssign();

            $response = array(
                'agent_name' => $agent_name,
                'status' => 1,
                'msg' => 'Success: Agent Assigned Successfully!',
            );

            echo json_encode($response);
        }
    }

    public function filterAgents($department_id = 0)
    {
        checkLogin();

        if ($department_id) {
            $this->db->where('department', $department_id);
        } else {
            $this->db->where('department!=0', null, false);
        }
        
        $rows = $this->db->get('users')->result_array();

        $html = '';
        foreach ($rows as $row) {
            $html .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }
        echo json_encode(array('html' => $html, 'status' => 1));
    }

    public function agentAutocomplete($q)
    {
        checkLogin();

        $this->db->select('id, name');
        $this->db->like('name', $q);
        $this->db->where('department != 0');
        echo json_encode($this->db->get_where('users')->result_array());
    }
}
