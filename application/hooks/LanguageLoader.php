<?php
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
       
        $language = $ci->db->query('select value from settings WHERE name="language"')->row()->value;
        
        $ci->lang->load('tickets',$language);
        $ci->session->set_userdata('language', $language);
    }
}