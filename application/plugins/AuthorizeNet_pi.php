<?php
/**
 * The AuthorizeNet PHP SDK. Include this file in your project.
 *
 * @package AuthorizeNet
 */
$root = FCPATH;

require $root.'system/plugins/anet_php_sdk/lib/shared/AuthorizeNetRequest.php';
require $root.'system/plugins/anet_php_sdk/lib/shared/AuthorizeNetTypes.php';

require $root.
    '/system/plugins/anet_php_sdk/lib/shared/AuthorizeNetXMLResponse.php';

require $root.
    '/system/plugins/anet_php_sdk/lib/shared/AuthorizeNetResponse.php';

require $root.'system/plugins/anet_php_sdk/lib/AuthorizeNetAIM.php';
require $root.'system/plugins/anet_php_sdk/lib/AuthorizeNetARB.php';
require $root.'system/plugins/anet_php_sdk/lib/AuthorizeNetCIM.php';
require $root.'system/plugins/anet_php_sdk/lib/AuthorizeNetSIM.php';
require $root.'system/plugins/anet_php_sdk/lib/AuthorizeNetDPM.php';
require $root.'system/plugins/anet_php_sdk/lib/AuthorizeNetTD.php';
require $root.'system/plugins/anet_php_sdk/lib/AuthorizeNetCP.php';

if (class_exists("SoapClient")) {
    require $root.'system/plugins/anet_php_sdk/lib/AuthorizeNetSOAP.php';
}

/**
 * Exception class for AuthorizeNet PHP SDK.
 *
 * @package AuthorizeNet
 */
class AuthorizeNetException extends Exception
{

}
