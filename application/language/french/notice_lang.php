<?php 

$lang['notice'] = 'Notice';
$lang['edit_notice'] = 'Edit Notice';
$lang['add_notice'] = 'Add Notice';
$lang['user'] = 'User';
$lang['content'] = 'Content';
$lang['customer'] = 'Customer';
$lang['request_feature'] = 'Request Feature';

$lang['no_user_found'] = '(No user found!)';