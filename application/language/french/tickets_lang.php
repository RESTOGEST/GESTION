<?php

$lang['order'] = 'Ordre';
$lang['ftp_db_access'] = 'FTP/ Database/ Site Access';

$lang['agent_offline_title'] = 'Redémarrer Chat';
$lang['agent_offline_body'] = 'Votre agent de soutien a déconnecté pour linstant, voulez-vous discuter avec d autres?';

$lang['msg_success'] = '<div class="alert alert-success alert-dismissible" '
    . 'role="alert"><button type="button" class="close" data-dismiss="alert"'
    . ' aria-label="Close"><span aria-hidden="true">&times;</span></button>'
    . '%s</div>';

$lang['msg_danger'] = '<div class="alert alert-danger alert-dismissible" '
    . 'role="alert"><button type="button" class="close" data-dismiss="alert"'
    . ' aria-label="Close"><span aria-hidden="true">&times;</span></button>'
    . '%s</div>';

$lang['load_history'] = 'Load History';
$lang['search_orders'] = 'Search Orders';
$lang['order_detail'] = 'Order Detail';
$lang['detail'] = 'Detail';
$lang['order_id'] = 'Order ID/ Purchase Code';
$lang['search_user'] = 'Search user';
$lang['ticket_status_change_template'] = 'Ticket Status Change Template';
$lang['ticket_status_change_subject'] = 'Ticket Status Change Subject';
$lang['ticket_status_change_mail'] = 'Ticket Status Change Email';
$lang['available_variables'] = 'Available Variables';
$lang['new_ticket_mail_for_customer'] = 'New Ticket Email For Customer';
$lang['new_ticket_subject'] = 'New Ticket Subject';
$lang['new_ticket_template'] = 'New Ticket Mail Template';
$lang['mail_templates'] = 'Email Templates';
$lang['paypal'] = 'Paypal';
$lang['smtp'] = 'Mail';
$lang['upload'] = 'Upload';
$lang['enter_user_name'] = 'Enter User Name';
$lang['enter_customer_name'] = 'Enter Customer Name';
$lang['current_password'] = 'New Password';
$lang['new_password'] = 'Retype New Password';
$lang['add_new_order'] = 'Add New Order';
$lang['order_create_success'] = 'Success: Order Created Successfully!';
$lang['general'] = 'General';
$lang["currency"] = 'Currency';
$lang['currency_sl'] = 'Currency Left Symbol';
$lang['currency_sr'] = 'Currency Right Symbol';

$lang['mail_transcript'] = 'Mail Transcript';
$lang['transcript_mailed_succesfull'] = 'Chat Transcript Mailed Successfully!';
$lang['new_message'] = 'New message';
$lang['hide_permanent'] = 'Hide Permanent';
$lang['transfer_chat'] = 'Transfer Chat';
$lang['pending_tickets'] = 'Pending Tickets';
$lang['date_added'] = 'Date added';
$lang['search_features'] = 'Fonctions de Recherche';
$lang['search_services'] = 'Search Services';
$lang['enter_service_title'] = 'Enter Service Title';
$lang['search_products'] = 'Search Products';
$lang['search....'] = 'Search....';
$lang['search'] = 'Search';
$lang['online_agents'] = 'Online Agents';
$lang['no_online_visitors_to_list'] = 'No Online Visitors To List!';
$lang['no_online_agents_to_list'] = 'No Online Agents To List!';
$lang['response'] = 'Response';
$lang['telephone'] = 'Telephone';
$lang['total_active_chats'] = 'Total Active Chats';
$lang['visitor'] = 'Visitors';
$lang['online_visitors'] = 'Online Visitors';
$lang['collapse'] = 'Collapse';

$lang['information'] = 'Information';
$lang['reset_pwd'] = 'Reset Password';
$lang['confirm_password'] = 'Confirm Password';
$lang['update_succesfull'] = 'Update Successfull!';

$lang['reset_pwd_success'] = 'Success: Password Changed Successfully!';
$lang['total_banned_customers'] = 'Total Banned Customers';
$lang['any_customer'] = 'No customer found!';
$lang['add_new_customer'] = 'Add New Customer';
$lang['customer_details'] = 'Customer Details';
$lang['customers'] = 'Customers';
$lang['create_customer'] = 'Create Customer';
$lang['update_customer'] = 'Update Customer';
$lang['customer_management'] = 'Gestion de la Clientèle';

$lang['chat_history'] = 'Histoire Chat';
$lang['dept_create_succesful'] = 'Success: New Department Added Successfully!';
$lang['login_as'] = 'Login as';
$lang['bugs'] = 'Bogues';
$lang['orders'] = 'Ordres';
$lang['report_bug'] = 'Report Bug';
$lang['filter'] = 'Filter';
$lang['filter_tickets'] = 'Filter Tickets';
$lang['customer_name'] = 'Customer Name';
$lang['department'] = 'Department';
$lang['features'] = 'Fonctionnalités';
$lang['all'] = 'All';
$lang['customer_email'] = 'Customer Email';
$lang['agent'] = 'Agent';
$lang['login_to_support_desk']= 'Login to Support Desk';
$lang['browse_faq']= 'FAQ';
$lang['start_chat'] = 'Chat';

//envato
$lang['envato'] = 'Vérifiez Envato Achat';
$lang['varify_purchse_code'] = 'Verify Purchase Code';
$lang['buyer'] = 'Buyer';
$lang['purchase_code'] = 'Purchase Code';
$lang['envato_success_msg'] = 'Success: Purchase Code Verified Successfully!';
$lang['envato_error_msg'] = 'Error: Purchase Code or Buyer Not Valid!';

$lang['discussion'] = 'Discussion';
$lang['product'] = 'Product';
$lang['attachment'] = 'Attachment';
$lang['select_file'] = 'Select File';
$lang['start_upload'] = 'Start Upload';

//validation
$lang['banned'] = 'Banned';
$lang['user_id'] = 'User ID';

//footer
$lang['copyright'] = '&copy; Copyright 2015';

//envato 
$lang['envato_username'] = 'Envato Username';
$lang['envato_settings'] = 'Envato Settings';
$lang['envato_app_id'] = 'Envato API Key';
$lang['update_succesful'] = 'Update Successful!';

//chat
$lang['online'] = 'online';
$lang['offline'] = 'offline';
$lang['chat_help_text'] = 'Hello! How Can I Help You, Sir?';
$lang['0_seconds_ago'] = 'a few seconds ago';
$lang['partner_typing_text'] = ' is typing…';
$lang['chat_like_text'] = 'I liked the chat!';
$lang['chat_unlike_text'] = 'I didn\'t like the chat!';
$lang['type_message'] = 'Type your message…';
$lang['new_messages'] = 'New Messages';
$lang['no_agent_found'] = 'Warning: No Agent Found!';
$lang['chat_assign_success'] = 'Success: Chat Assigned to:';
$lang['chat_error_msg'] = 'Error: Something Going Wrong!';
$lang['assign_agent_help_text'] = 'Note: If assigned agent is offline, chat will initialize with online agent(If any).If assigned agent logs in during chat new chat window will popup.';
$lang['select_department'] = '-- Select Department --';
$lang['faqs'] = 'FAQ';
$lang['assign_agent'] = 'Assign Agent ';
$lang['agent'] = 'Agent ';
$lang['all'] = 'All ';
$lang['cancel'] = 'Cancel ';
$lang['assign'] = 'Assign ';
$lang['chat'] = 'Chat ';
$lang['user'] = 'User ';
$lang['version'] = 'Version ';
$lang['department'] = 'Department ';
$lang['all'] = 'All ';
$lang['type'] = 'Type ';
$lang['agent_assign_success'] = 'Success: Agent Assigned Successfully!';

//faq
$lang['faq'] = 'Parcourir FAQ ';
$lang['add_new'] = 'Add New';
$lang['submit'] = 'Submit ';
$lang['question'] = 'Question ';
$lang['answer'] = 'Answer ';
$lang['insert_faq'] = 'Insert FAQ ';
$lang['edit_faq'] = 'Edit FAQ ';
$lang['add_new_faq'] = 'Add New FAQ ';
$lang['edit'] = 'Edit ';
$lang['view_faq'] = 'View FAQ';
$lang['read_more'] = 'Read More ';
$lang['sort_order'] = 'Sort Order ';
$lang['faq_insert_msg'] = 'Success: FAQ Entry Inserted Successfully!';
$lang['faq_update_msg'] = 'Success: FAQ Updated Successfully!';

//user
$lang['add_new_user'] = 'Add New User';
$lang['total_pending_tickets'] = 'Total Pending Tickets ';
$lang['forgott_password'] = 'Forgot Password ';
$lang['recover_password'] = 'Recover Password ';
$lang['back_to_login'] = 'Login ';
$lang['log_in'] = 'A1 Support Desk Login';
$lang['sign_up'] = 'Sign Up';
$lang['create_account_to_start'] = 'Create Account to Start';
$lang['i_forgot_my_password'] = 'Forgot My Password ';
$lang['create_account'] = 'Create Account ';
$lang['already_registered'] = 'Already Registered?';

//settings
$lang['language'] = 'Language';
$lang['online_check_time'] = 'Online Check Interval Time (sec)';
$lang['online_update_time'] = 'Online Update Interval Time (ms)';
$lang['message_check_time'] = 'Message Check Interval Time (ms)';
$lang['notification_check_time'] = 'Notification Check Interval Time (ms)';

// Header
$lang['add_new_user'] = 'Add New User';
$lang['msg_new_user_added'] = 'Success: New User Added Successfully!';
$lang['notice'] = 'Avis';
$lang['search...'] = 'Search...';
$lang['agent'] = 'Agent';
$lang['all_chats'] = 'All Chats';
$lang['statistics'] = 'Statistics';
$lang['create_a_new_ticket'] = 'Create A New Ticket';
$lang['all_tickets'] = 'Tous les Billets';
$lang['settings'] = 'Paramètres';
$lang['user_management'] = 'Gestion des Utilisateurs';
$lang['departments'] = 'Departments';
$lang['logout'] = 'Logout';
$lang['login'] = 'Login';
$lang['request_feature'] = 'Request New Feature';
$lang['create_a_new_account'] = 'Create a New Account';
$lang['forgot_my_password'] = 'Forgot My Password';
$lang['smtp_protocol'] = 'Mail Protocol';
$lang['smtp_host'] = 'SMTP Host'; 
$lang['smtp_user'] = 'SMTP User';
$lang['smtp_pass'] = 'SMTP Password';
$lang['smtp_port'] = 'SMTP Port';
$lang['smtp_timeout'] = 'SMTP Timeout';
$lang['smtp_settings'] = 'SMTP Settings';

// Services 
$lang['create_user'] = 'Create User';
$lang['details'] = 'Details';
$lang['is_in_test_mode'] = 'Is in Test mode?';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['transaction_key'] = 'Transaction Key';
$lang['login_id'] = 'Login ID';
$lang['auth_trans_key'] = 'Transaction Key';
$lang['auth_login_id'] = 'Login ID';
$lang['signature'] = 'API Signature';
$lang['username']= 'Username';
$lang['password'] = 'Password';
$lang['paypal_settings'] = 'Paypal Settings';
$lang['auth_settings'] = 'Authorize.Net Settings';
$lang['complete'] = 'Complete';
$lang['uncomplete'] = 'Incomplete';
$lang['pay_with_paypal'] = 'Pay with  <b>Paypal</b>';
$lang['pay'] = 'Pay';
$lang['pay_now'] = 'Pay Now';
$lang['pay_with_ceredit_card'] = 'Pay with  <b>Credit/Debit Card</b>';
$lang['card_number'] = 'Card Number';
$lang['expiration_date'] = 'Expiration Date';
$lang['cvv_code'] = 'CVV Code';
$lang['amount'] = 'Amount';
$lang['customer'] = 'Customer';
$lang['enter_amount'] = 'Enter Amount';
$lang['complete_success'] = 'Success: You have modified orders!';
$lang['uncomplete_success'] = 'Success: You have modified orders!';
$lang['paypal_error_title'] = 'Erorr on Paypal Transaction!';
$lang['payment_success'] = 'Success: Payment Saved To Database Successfully!';
$lang['payment_error'] = 'Error: Transaction not Completed Successfully!';
$lang['services']= 'Services';
$lang['buy_service'] = 'Buy Service';
$lang['view_service'] = 'View Service';
$lang['update_service'] = 'Update Service';
$lang['service_create_succesful'] = 'Services Created Successfully!';
$lang['service_update_succesful'] = 'Services Updated Successfully!';
$lang['update_details'] = 'Update Details';
$lang['detail_update_succesful'] = 'Detail Updated Successfully!';
$lang['name_lastname'] = 'Name';
$lang['retype_password'] = 'Retype Password';
$lang['title'] = 'Titte';
$lang['sort_order']= 'Sort Order';
$lang['description']= 'Description';
$lang['icon'] = 'Icon';
$lang['create_a_new_service']= 'Create a New Service';
$lang['buy'] = 'Buy';
$lang['service'] = 'Service';


//Products
$lang['products']= 'Produits';
$lang['create_a_new_product'] = 'Create a New Product';
$lang['view_product'] = 'View Product';
$lang['update_product'] = 'Update Product';
$lang['product_create_succesful'] = 'Products Created Successfully!';
$lang['product_update_succesful'] = 'Products Updated Successfully!';
$lang['link'] = 'Link';
$lang['price'] = 'Price';
$lang['view'] = 'View';
$lang['image'] = 'Image';

//Orders
$lang['work_status'] = 'Work Status';

//Features
$lang['search_features'] = 'Search Features';
$lang['date_added'] = 'Date Added';
$lang['votes'] = 'Votes';

//Bugs
$lang['search_bugs'] = 'Search Bugs';
$lang['solved'] = 'Solved';
$lang['unsolved'] = 'Unsolved';

//Chats
$lang['visitors'] = 'Visiteurs';

// Departments
$lang['department'] = 'Département';
$lang['department_id'] = 'Department ID';
$lang['create_a_new_department'] = 'Add Department';
$lang['department_name'] = 'Department Name';
$lang['departments'] = 'Départements';
$lang['options'] = 'Options';
$lang['update'] = 'Update';
$lang['delete'] = 'Delete';
$lang['update_department'] = 'Update Department';

// Tickets
$lang['submit_a_new_ticket'] = 'Submit a New Ticket';
$lang['title'] = 'Title';
$lang['department'] = 'Department';
$lang['select_a_department'] = 'Select a Department';
$lang['priority'] = 'Priority';
$lang['low'] = 'Low';
$lang['medium'] = 'Normal';
$lang['high'] = 'High';
$lang['message'] = 'Message';
$lang['reply_succes'] = 'Your message has been sent';
$lang['send'] = 'Send';
$lang['ticket_info'] = 'Ticket Info';
$lang['hide'] = 'Hide';
$lang['close'] = 'Close';
$lang['customer'] = 'Customer';
$lang['date'] = 'Date';
$lang['email'] = 'E-mail';
$lang['status'] = 'Status';
$lang['customer'] = 'Customer';
$lang['support'] = 'Support';
$lang['attach'] = 'Attachment File';
$lang['open'] = 'Open';
$lang['pending'] = 'Pending';
$lang['close'] = 'Close';
$lang['open_tickets'] = 'Open Tickets';
$lang['ticket_details'] = 'Ticket Details';
$lang['any_ticket'] = 'You don\'t have any ticket';
$lang['other_tickets'] = 'Other Tickets';
$lang['close_ticket'] = 'Close the Ticket';
$lang['close_ticket'] = 'Close the Ticket';

// Admin Settings
$lang['change_email'] = 'Change Registered E-mail';
$lang['change_password'] = 'Change Password';
$lang['save'] = 'Save';
$lang['admin_settings'] = 'Paramètres';
$lang['general_settings'] = 'General Settings';
$lang['chat_settings'] = 'Chat Settings';
$lang['upload_settings'] = 'Upload Settings';
$lang['site_name'] = 'Site Name';
$lang['site_email'] = 'Site E-mail';
$lang['tickets_per_page'] = 'Tickets Per Page';
$lang['current_password'] = 'Current Password';
$lang['new_password'] = 'New Password';
$lang['new_password_again'] = 'New Password Again';
$lang['retype_new_password'] = 'Retype New Password';
$lang['allowed_files'] = 'Allowed File Extensions';
$lang['max_upload_files'] = 'Maximum Upload Files';
$lang['max_upload_file_size'] = 'Maximum Upload File Size';
$lang['site_name_desc'] = 'Site Title, Footer Copyright Text and Automatic E-mail Title';
$lang['site_email_desc'] = 'New Tickets and Messages will be sent to this this E-mail Address.';

// User
$lang['create_a_new_account_button'] = 'Create A New Account';
$lang['name_lastname'] = 'Name';
$lang['password'] = 'Password';
$lang['lostpw_message1'] = 'Your password reset link has been sent to your e-mail address !.. ';
$lang['lostpw_message2'] = 'You can reset your password by entering the link that is sent to your e-mail.';
$lang['register_succes_msg1'] = 'Your account has been created!';
$lang['register_succes_msg2'] = 'Hey <b>%1$s</b>, Your account has been created..';
$lang['update_user'] = 'Update User';
$lang['yes'] = 'Yes';
$lang['banned'] = 'Banned';
$lang['user_dep'] = 'User Type / Department';
$lang['users'] = 'Users';
$lang['register_time'] = 'Registration Time';
$lang['user_details'] = 'User Details';
$lang['any_user'] = 'You don\'t have any user or customer';
$lang['name'] = 'Name';
$lang['team_leader'] = 'Team Leader/Manager/Supervisor';
$lang['all_login_users'] = 'All Login Users';

// Controller Messages
$lang['already_exists_dep'] = 'Department Name Already Exists.';
$lang['create_Successful'] = 'Create Successful';
$lang['technical_problem'] = 'Technical Problem';
$lang['update_Successful'] = 'Update Successful';
$lang['already_account'] = 'You already have an account. <a href="'.base_url('user/login').'">Login Here</a>';
$lang['invalid_pass'] = 'Invalid Password';
$lang['new_pass_sent'] = 'New password will be sent to your e-mail address';
$lang['invalid_reset_code'] = 'Invalid Reset Code';
$lang['you_must_create_account'] = 'You must create an account. <a href="'.base_url('user/create').'">Create an Account</a>';
$lang['invalid_email_or_pass'] = 'Invalid E-mail or Password';
$lang['next'] = 'Next';
$lang['previous'] = 'Previous';
$lang['file_upload_limit_error'] = 'File upload limit : %1$s file(s)';


// E-mail
$lang['new_ticket_from_customer'] = 'Hello, <br> The new ticket is created. Please <a href="'.base_url().'">click here</a> for ticket management panel ';
$lang['new_ticket_from_customer_subject'] = 'New Ticket # %1$s - %2$s';
$lang['new_message_from_customer'] = 'Hello, <br> The new message is wrote. Please <a href="'.base_url().'">click here</a> for ticket management panel ';
$lang['new_message_from_customer_subject'] = 'New Message # %1$s - %2$s';
$lang['new_message_from_support'] = 'Hello, <br> The new message is wrote. Please <a href="'.base_url().'">click here</a> for ticket panel ';
$lang['new_message_from_support_subject'] = 'New Message # %1$s - %2$s';

//dashboard
$lang['dashboard'] = 'Tableau de bord';
$lang['more_info'] = 'More Info ';
$lang['total_open_tickets'] = 'Total Open Tickets ';
$lang['total_tickets'] = 'Total Tickets ';
$lang['total_close_tickets'] = 'Total Closed Tickets ';
$lang['total_users'] = 'Total Users ';
$lang['total_customers'] = 'Total Customers ';
$lang['total_departments'] = 'Total Departments ';
$lang['total_banned_users'] = 'Total Banned Users ';
$lang['tickets_graph'] = 'Tickets Graph ';
$lang['today'] = 'Today ';
$lang['week'] = 'Week ';
$lang['month'] = 'Month';
$lang['year'] = 'Year';

?>
