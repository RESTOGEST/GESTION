<?php 

$lang['request_bug'] = 'Report Bug';
$lang['vote'] = 'Vote';
$lang['view_bug'] = 'View Bug';
$lang['add_new_bug'] = 'Add New Bug';
$lang['bugs'] = 'Bugs';
$lang['add_new'] = 'Add New';
$lang['edit_bug'] = 'Edit Bug';
$lang['title'] = 'Title';
$lang['description'] = 'Description';
$lang['complete'] = 'Complete';
$lang['incomplete'] = 'Incomplete';

$lang['bug_vote_msg'] = 'Success: Vote Added Successfully!';
$lang['bug_insert_msg'] = 'Success: New Bug Added Successfully!';
$lang['bug_update_msg'] = 'Success: Bug Updated Successfully!';
$lang['bug_complete_msg'] = 'Success: Bug State Changed To Complete Successfully!';
$lang['bug_incomplete_msg'] = 'Success: Bug State Changed To Incomplete Successfully!';