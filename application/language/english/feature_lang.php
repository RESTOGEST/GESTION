<?php 

$lang['request_feature'] = 'Request Feature';
$lang['vote'] = 'Vote';
$lang['view_feature'] = 'View Feature';
$lang['add_new_feature'] = 'Add New Feature';
$lang['features'] = 'Features';
$lang['add_new'] = 'Add New';
$lang['edit_feature'] = 'Edit Feature';
$lang['title'] = 'Title';
$lang['description'] = 'Description';
$lang['complete'] = 'Complete';
$lang['incomplete'] = 'Incomplete';

$lang['feature_vote_msg'] = 'Success: Vote Added Successfully!';
$lang['feature_insert_msg'] = 'Success: New Feature Added Successfully!';
$lang['feature_update_msg'] = 'Success: Feature Updated Successfully!';
$lang['feature_complete_msg'] = 'Success: Feature State Changed To Complete Successfully!';
$lang['feature_incomplete_msg'] = 'Success: Feature State Changed To Incomplete Successfully!';