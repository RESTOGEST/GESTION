<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Bugs extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        checkLogin(); // session helper function
        
        $this->lang->load('bug', $this->session->userdata('language'));
        $this->load->helper('time_format');
        $this->load->model('bugModel');
    }

    public function index()
    {
        $url = site_url('bugs/index?');

        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $data['sort'] = $this->input->get('sort', true);
        $data['order'] = $this->input->get('order', true);
        $data['title'] = $this->input->get('title', true);
        $data['product'] = $this->input->get('product', true);
        $data['status'] = $this->input->get('status', true);

        if (!$data['sort']) {
            $data['sort'] = 'title';
            $url .= '&sort=title';
        } else {
            $url .= '&sort='.$data['sort'];
        }

        if (!$data['order']) {
            $data['order'] = 'ASC';
            $url .= '&order=ASC';
        } else {
            $url .= '&order='.$data['order'];
        }

        if ($data['title']) {
            $url .= '&title='.$data['title'];
        }

        if ($data['product']) {
            $url .= '&product='.$data['product'];
        }

        if ($data['status']) {
            $url .= '&status='.$data['status'];
        }

        $this->load->helper('text');

        $filter_data = array(
            'sort' => $data['sort'],
            'order' => $data['order'],
            'title' => $this->input->get('title', true),
            'product' => $this->input->get('product', true),
            'status' => $this->input->get('status', true),
        );

        $this->load->helper('text');

        //pagination
        $limit = 5;
        $offset = ($page - 1) * $limit;
        $this->load->library('pagination');
        $config['base_url'] = $url;
        $config['total_rows'] = $this->bugModel->getTotal($filter_data);
        $config['per_page'] = $limit;
        $config['page_query_string'] = true;
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;

        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();
        $data['msg'] = $this->session->flashdata('msg');
        $data['my_votes'] = $this->bugModel->getMyVoted();

        $data['rows'] = $this->bugModel->get(
            false,
            $limit,
            $offset,
            $filter_data
        );

        $url = '';

        if ($data['order'] == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if ($data['title']) {
            $url .= '&title='.$data['title'];
        }

        if ($data['product']) {
            $url .= '&product='.$data['product'];
        }

        $status = $data['status'];

        if ($status || ($status != '' && $status == 0)) {
            $url .= '&status='.$data['status'];
        }

        $data['sort_title'] = site_url('bugs/index?sort=b.title'.$url);
        $data['sort_product'] = site_url('bugs/index?sort=p.title'.$url);
        $data['sort_status'] = site_url('bugs/index?sort=b.status'.$url);
        $data['sort_votes'] = site_url('bugs/index?sort=b.votes'.$url);
        $data['sort_date_added'] =
            site_url('bugs/index?sort=b.date_added'.$url);

        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/bugs/index';

        if (file_exists($path)) {
            $this->load->view('themes/'.$theme.'/template/bugs/index', $data);
        } else {
            $this->load->view('themes/default/template/bugs/index', $data);
        }
    }

    public function addDiscussion()
    {
        $id = $this->bugModel->addDiscussion();

        $data['row'] = $this->bugModel->getDiscussion($id);

        $theme = $this->session->userdata('theme');

        $path = APPPATH.'views/themes/'.$theme.'/template/discussion/bug_item';

        if (file_exists($path)) {
            $this->load->view(
                'themes/'.$theme.'/template/discussion/bug_item',
                $data
            );
        } else {
            $this->load->view(
                'themes/default/template/discussion/bug_item',
                $data
            );
        }
    }

    public function discussion()
    {
        $bug_id = $this->input->get('bug_id', true);
        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $data['bug'] = $this->bugModel->getBug($bug_id);

        if (empty($data['bug'])) {
            show_404();
        }

        //pagination
        $limit = 10;
        $offset = ($page - 1) * $limit;

        $this->load->library('pagination');

        $config['base_url'] = base_url('bugs/discussion/?bug_id='.$bug_id);
        $config['total_rows'] = $this->bugModel->getTotalDiscussions($bug_id);
        $config['per_page'] = $limit;
        $config['page_query_string'] = true;
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;

        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();

        $data['rows'] = $this->bugModel->getDiscussions(
            $bug_id,
            $offset,
            $limit
        );

        $theme = $this->session->userdata('theme');

        if (file_exists(APPPATH.'views/themes/'.$theme
            .'/template/bugs/discussion/bug_index')) {
            $this->load->view(
                'themes/'.$theme.'/template/discussion/bug_index',
                $data
            );
        } else {
            $this->load->view(
                'themes/default/template/discussion/bug_index',
                $data
            );
        }
    }

    public function insert()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules(
            'product',
            $this->lang->line('product'),
            ''
        );

        $this->form_validation->set_rules(
            'product_id',
            $this->lang->line('product'),
            'required'
        );

        $this->form_validation->set_rules(
            'title',
            $this->lang->line('title'),
            'required'
        );

        $this->form_validation->set_rules(
            'description',
            $this->lang->line('description'),
            'required'
        );

        if ($this->form_validation->run() == false) {
            $data['msg'] = validation_errors();
            $theme = $this->session->userdata('theme');

            $path = APPPATH.'views/themes/'.$theme.'/template/bugs/insert';

            if (file_exists($path)) {
                $this->load->view(
                    'themes/'.$theme.'/template/bugs/insert',
                    $data
                );
            } else {
                $this->load->view(
                    'themes/default/template/bugs/insert',
                    $data
                );
            }
        } else {
            $this->bugModel->insert();

            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('bug_insert_msg')
            );

            $this->session->set_flashdata('msg', $msg);

            redirect('bugs');
        }
    }

    public function view($id)
    {
        $data['row'] = $this->bugModel->get($id);

        $theme = $this->session->userdata('theme');

        if (file_exists(APPPATH.'views/themes/'.$theme.'/template/bugs/view')) {
            $this->load->view('themes/'.$theme.'/template/bugs/view', $data);
        } else {
            $this->load->view('themes/default/template/bugs/view', $data);
        }
    }

    public function productAutocomplete($q)
    {
        $this->db->like('title', $q);
        $result = $this->db->get_where('products')->result_array();
        echo json_encode($result);
    }

    public function edit($id)
    {
        $department = $this->session->userdata('department');

        if (!$department) {
            show_404();
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules(
            'product',
            $this->lang->line('product'),
            ''
        );

        $this->form_validation->set_rules(
            'product_id',
            $this->lang->line('product'),
            'required'
        );

        $this->form_validation->set_rules(
            'title',
            $this->lang->line('title'),
            'required'
        );

        $this->form_validation->set_rules(
            'description',
            $this->lang->line('description'),
            'required'
        );

        if ($this->form_validation->run() == false) {

            $data['msg'] = validation_errors();
            
            $data['row'] = $this->bugModel->get($id);

            $data['row']['product'] = $this->bugModel->getProductName(
                $data['row']['product_id']
            );

            $theme = $this->session->userdata('theme');

            $path = APPPATH.'views/themes/'.$theme.'/template/bugs/edit';

            if (file_exists($path)) {
                $this->load->view('themes/'.$theme.'/template/bugs/edit', $data);
            } else {
                $this->load->view('themes/default/template/bugs/edit', $data);
            }
            
        } else {

            $this->bugModel->update($id);

            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('bug_update_msg')
            );

            $this->session->set_flashdata('msg', $msg);

            redirect('bugs');
        }
    }

    public function vote($id)
    {
        $this->bugModel->vote($id);

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('bug_vote_msg')
        );

        $this->session->set_flashdata('msg', $msg);

        redirect('bugs');
    }

    public function complete($id)
    {
        $this->bugModel->complete($id);

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('bug_complete_msg')
        );

        $this->session->set_flashdata('msg', $msg);

        redirect('bugs');
    }

    public function incomplete($id)
    {
        $this->bugModel->incomplete($id);

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('bug_incomplete_msg')
        );

        $this->session->set_flashdata('msg', $msg);

        redirect('bugs');
    }

    public function delete($id)
    {
        $department = $this->session->userdata('department');

        if (!$department) {
            show_404();
        }

        $this->bugModel->delete($id);
    }
}
