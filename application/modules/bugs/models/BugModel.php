<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class BugModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTotalDiscussions($bug_id)
    {
        $this->db->join('users u', 'u.id = d.user_id');
        $this->db->where(array('bug_id' => $bug_id));

        return $this->db->count_all_results('bug_discussion d');
    }

    public function addDiscussion()
    {
        $data = array(
            'user_id' => $this->session->userdata('user_id'),
            'comment' => $this->input->post('comment', true),
            'bug_id' => $this->input->post('bug_id', true),
        );
        $this->db->insert('bug_discussion', $data);

        return $this->db->insert_id();
    }

    public function getDiscussion($discussion_id)
    {
        $this->db->select('u.name as author, d.*');
        $this->db->join('users u', 'u.id = d.user_id');

        return $this->db->get_where('bug_discussion d',
                array('fd_id' => $discussion_id))->row_array();
    }

    public function getDiscussions($bug_id, $offset, $limit)
    {
        $this->db->select('u.name as author, d.*');
        $this->db->offset($offset);
        $this->db->order_by('fd_id ASC');
        $this->db->limit($limit);
        $this->db->join('users u', 'u.id = d.user_id');

        return $this->db->get_where('bug_discussion d',
                array('bug_id' => $bug_id))->result_array();
    }

    public function getProductName($product_id)
    {
        $row = $this->db->get_where('products',
                array('product_id' => $product_id))->row_array();
        if ($row) {
            return $row['title'];
        }
    }

    public function complete($id)
    {
        $this->db->where('bug_id', $id);
        $this->db->set('status', 1);
        $this->db->update('bugs');
    }

    public function incomplete($id)
    {
        $this->db->where('bug_id', $id);
        $this->db->set('status', 0);
        $this->db->update('bugs');
    }

    public function getTotal($filter_data)
    {
        if (!empty($filter_data['title'])) {
            $this->db->like('b.title', $filter_data['title']);
        }

        if (!empty($filter_data['product'])) {
            $this->db->like('b.title', $filter_data['product']);
        }

        if (!empty($filter_data['status'])) {
            $this->db->where('b.status', $filter_data['status']);
        }

        $this->db->join('products p', 'p.product_id = b.product_id');

        return $this->db->count_all_results('bugs b');
    }

    public function getMyVoted()
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $rows = $this->db->get('bug_vote_history')->result_array();
        $ids = array();
        foreach ($rows as $row) {
            $ids[] = $row['bug_id'];
        }

        return $ids;
    }

    public function vote($id)
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('bug_id', $id);
        $is_exist = $this->db->count_all_results('bug_vote_history');

        if (!$is_exist) {
            $data = array(
                'user_id' => $this->session->userdata('user_id'),
                'bug_id' => $id,
                'date_added' => date('Y-m-d'),
            );
            $this->db->insert('bug_vote_history', $data);
            $this->db->query('update bugs set votes = votes + 1 where '
                .'bug_id = '.$id);
        }
    }

    public function getBug($id)
    {
        $this->db->select('p.title as product, b.*');
        $this->db->join('products p', 'p.product_id = b.product_id');

        return $this->db->get_where('bugs b', array('bug_id' => $id))
            ->row_array();
    }

    public function get($id = 0, $limit = 10, $offset = 0,
                        $filter_data = array())
    {
        if ($id) {
            return $this->db->get_where('bugs', array('bug_id' => $id))
                ->row_array();
        } else {
            $this->db->select('b.*, p.title as product');

            if (!empty($filter_data['title'])) {
                $this->db->like('b.title', $filter_data['title']);
            }

            if (!empty($filter_data['product'])) {
                $this->db->like('p.title', $filter_data['product']);
            }

            $status = $filter_data['status'];

            if ($status || ($status != '' && $status == 0)) {
                $this->db->where('b.status', $filter_data['status']);
            }

            $arr_order = array(
                'DESC',
                'ASC',
            );

            $arr_sort = array(
                'b.title',
                'p.title',
                'b.status',
                'b.votes',
                'b.date_added',
            );

            if (in_array($filter_data['order'], $arr_order) &&
                in_array($filter_data['sort'], $arr_sort)
            ) {
                $this->db->order_by($filter_data['sort'].' '.
                    $filter_data['order']);
            }

            $this->db->join('products p', 'p.product_id = b.product_id');

            return $this->db->get('bugs b', $limit, $offset)->result_array();
        }
    }

    public function insert()
    {
        $data = array(
            'user_id' => $this->session->userdata('user_id'),
            'product_id' => $this->input->post('product_id', true),
            'title' => $this->input->post('title', true),
            'description' => $this->input->post('description', true),
            'date_added' => date('Y-m-d'),
        );
        $this->db->insert('bugs', $data);
    }

    public function update($id)
    {
        $data = array(
            'title' => $this->input->post('title', true),
            'product_id' => $this->input->post('product_id', true),
            'description' => $this->input->post('description', true),
        );
        $this->db->where('bug_id', $id);
        $this->db->update('bugs', $data);
    }

    public function delete($id)
    {
        $this->db->delete('bugs', array('bug_id' => $id));
    }
}
