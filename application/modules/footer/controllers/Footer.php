<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Footer extends MX_Controller
{
    public function index()
    {
        $data['chats'] = array();

        $user_id = $this->session->userdata('user_id');

        $banned_row = $this->db->get_where(
                           'users',
                            array('id' => $user_id)
                      )->row_array();

        //if not chat banned
        if ($user_id && $banned_row && !$banned_row['is_chat_banned']) {
            $sql = 'select * from user_chatroom WHERE (user_id = '.$user_id.''
                .' OR receiver_id='.$user_id.')';

            $data['chats'] = $this->db->query($sql)->result_array();
        }

        $data['user_id'] = $user_id;
                
        $data['timezone'] = $this->session->userdata('timezone');
        
        //load view
        $theme = $this->session->userdata('catalog_theme');

        $template = '/template/common/footer';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }
}
