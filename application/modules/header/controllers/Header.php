<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class header extends MX_Controller
{
    public function index()
    {
        $department_id = $this->session->userdata('department');
        $user_id = $this->session->userdata('user_id');

        $hide_notice = $this->session->userdata('hide_notice');

        if (!$hide_notice && $user_id) {
            $sql = 'select * from notice n where (n.notice_id NOT IN (select '
                .'DISTINCT notice_id from notice_to_user) OR n.notice_id IN '
                .'(select DISTINCT notice_id from notice_to_user WHERE '
                .'user_id="'.$this->session->userdata('user_id').'")) AND '
                .'n.department_id = "'.$department_id.'"';

            $data['notices'] = $this->db->query($sql)->result_array();
        } else {
            $data['notices'] = array();
        }

        $data['name'] = $this->session->userdata('name');
        $data['photo'] = $this->session->userdata('photo');

        $data['play_sound'] = userdata('play_sound');
                
        //load view
        $theme = $this->session->userdata('catalog_theme');

        $template = '/template/common/header';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }
}
