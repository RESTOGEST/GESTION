<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class FaqModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTotal($q = null)
    {
        if ($q) {
            if ($q) {
                $this->db->like('question', $q);
                $this->db->or_like('answer', $q);
            }

            return $this->db->count_all_results('faq');
        } else {
            return $this->db->count_all('faq');
        }
    }

    public function get($id = 0, $limit = 10, $offset = 0, $q = null)
    {
        if ($id) {
            return $this->db->get_where('faq', array('faq_id' => $id))
                ->row_array();
        } else {
            if ($q) {
                $this->db->like('question', $q);
                $this->db->or_like('answer', $q);
            }
            $this->db->order_by('sort_order', 'DESC');

            return $this->db->get('faq', $limit, $offset)->result_array();
        }
    }

    public function insert()
    {
        $data = array(
            'question' => $this->input->post('question', true),
            'answer' => $this->input->post('answer', true),
            'datetime' => date('Y-m-d H:i:s'),
            'sort_order' => $this->input->post('sort_order', true),
        );
        $this->db->insert('faq', $data);
    }

    public function update($id)
    {
        $data = array(
            'question' => $this->input->post('question', true),
            'answer' => $this->input->post('answer', true),
            'datetime' => date('Y-m-d H:i:s'),
            'sort_order' => $this->input->post('sort_order', true),
        );
        $this->db->where('faq_id', $id);
        $this->db->update('faq', $data);
    }

    public function delete($id)
    {
        $this->db->delete('faq', array('faq_id' => $id));
    }
}
