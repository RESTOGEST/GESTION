<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faq extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('time_format');
        $this->load->model('faqModel');
    }

    public function index()
    {
        if ($this->input->get('page', true)) {
            $page = $this->input->get('page', true);
        } else {
            $page = 1;
        }
        
        $q = $this->input->get('q', true);

        $this->load->helper('text');

        //pagination
        $limit = 6;
        $offset = ($page - 1) * $limit;

        $this->load->library('pagination');

        $config['base_url'] = base_url('faq/index?q='.$q);
        $config['total_rows'] = $this->faqModel->getTotal($q);
        $config['per_page'] = $limit;
        $config['page_query_string'] = true;
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;

        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();
        $rows = $this->faqModel->get(false, $limit, $offset, $q);

        if ($rows) {
            $data['rows'] = array_chunk($rows, ceil(count($rows) / 2));
        } else {
            $data['rows'] = array();
        }

        $data['department'] = $this->session->userdata('department');

        $data['msg'] = $this->session->flashdata('msg');

        $theme = $this->session->userdata('theme');

        if (file_exists(APPPATH.'views/themes/'.$theme.'/template/faq/index')) {
            $this->load->view('themes/'.$theme.'/template/faq/index', $data);
        } else {
            $this->load->view('themes/default/template/faq/index', $data);
        }
    }

    public function insert()
    {
        $department = $this->session->userdata('department');

        if ($department != 1) {
            show_404();
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules(
            'question',
            $this->lang->line('question'),
            'required'
        );

        $this->form_validation->set_rules(
            'answer',
            $this->lang->line('answer'),
            'required'
        );

        $this->form_validation->set_rules('sort_order', '', '');

        if ($this->form_validation->run() == false) {

            $data['msg'] = validation_errors();
            
            $theme = $this->session->userdata('theme');

            if (file_exists(
                    APPPATH.'views/themes/'.$theme.'/template/faq/insert')
                ) {
                $this->load->view(
                    'themes/'.$theme.'/template/faq/insert',
                    $data
                );
            } else {
                $this->load->view('themes/default/template/faq/insert', $data);
            }
        } else {
            $this->faqModel->insert();

            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('faq_insert_msg')
            );

            $this->session->set_flashdata('msg', $msg);
            redirect('faq');
        }
    }

    public function view($id)
    {
        $data['row'] = $this->faqModel->get($id);

        $theme = $this->session->userdata('theme');

        if (file_exists(APPPATH.'views/themes/'.$theme.'/template/faq/view')) {
            $this->load->view('themes/'.$theme.'/template/faq/view', $data);
        } else {
            $this->load->view('themes/default/template/faq/view', $data);
        }
    }

    public function edit($id)
    {
        $department = $this->session->userdata('department');

        if ($department != 1) {
            show_404();
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules(
            'question',
            $this->lang->line('question'),
            'required'
        );

        $this->form_validation->set_rules(
            'answer',
            $this->lang->line('answer'),
            'required'
        );

        $this->form_validation->set_rules('sort_order', '', '');

        if ($this->form_validation->run() == false) {

            $data['msg'] = validation_errors();

            $data['row'] = $this->faqModel->get($id);
            
            $theme = $this->session->userdata('theme');

            if (file_exists(
                   APPPATH.'views/themes/'.$theme.'/template/faq/edit')
                ) {
                $this->load->view('themes/'.$theme.'/template/faq/edit', $data);
            } else {
                $this->load->view('themes/default/template/faq/edit', $data);
            }
        } else {
            $this->faqModel->update($id);

            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('faq_update_msg')
            );

            $this->session->set_flashdata('msg', $msg);

            redirect('faq');
        }
    }

    public function delete($id)
    {
        $department = $this->session->userdata('department');

        if ($department != 1) {
            show_404();
        }

        $this->faqModel->delete($id);
    }
}
