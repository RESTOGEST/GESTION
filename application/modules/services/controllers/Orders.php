<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Orders extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('orderModel'); // Load orders model
        $this->load->library('form_validation'); // Load form validation library

        checkLogin(); // session helper function
    }

    // Listing the Departments, displays new order adding form,
    //  makes controls for adding new order,
    public function index($page = 1)
    {
        $sort = $this->input->get('sort', true);
        $order = $this->input->get('order', true);
        $filter_order = $this->input->get('filter_order', true);
        $filter_status = $this->input->get('filter_status', true);
        $filter_customer = $this->input->get('filter_customer', true);

        $url = site_url('services/index?');

        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        if ($sort) {
            $url .= '&sort='.$sort;
        }

        if ($order) {
            $url .= '&order='.$order;
        }

        if ($filter_order) {
            $url .= '&filter_order='.$filter_order;
        }

        if ($filter_status) {
            $url .= '&filter_status='.$filter_status;
        }

        if ($filter_customer) {
            $url .= '&filter_customer='.$filter_customer;
        }

        //pagination
        $this->load->library('pagination');
        $limit = 10;
        $start = ($page - 1) * $limit;

        $filter_data = array(
            'sort' => $this->input->get('sort', true),
            'order' => $this->input->get('order', true),
            'start' => $start,
            'limit' => $limit,
            'filter_order' => $this->input->get('filter_order', true),
            'filter_status' => $this->input->get('filter_status', true),
            'filter_customer' => $this->input->get('filter_customer', true),
        );

        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['base_url'] = $url;
        $config['total_rows'] = $this->orderModel->countAll($filter_data);
        $config['per_page'] = $limit;
        $config['page_query_string'] = true;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['orders'] = $this->orderModel->orderList($filter_data);

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if ($filter_order) {
            $url .= '&filter_order='.$filter_order;
        }

        if ($filter_status) {
            $url .= '&filter_status='.$filter_status;
        }

        if ($filter_customer) {
            $url .= '&filter_customer='.$filter_customer;
        }

        $sort_base = 'services/orders/index?sort=';

        $data['sort_customer'] = site_url($sort_base.'u.name'.$url);
        $data['sort_service'] = site_url($sort_base.'s.title'.$url);
        $data['sort_amount'] = site_url($sort_base.'u2s.amount'.$url);
        $data['sort_status'] = site_url($sort_base.'u2s.status'.$url);
        $data['sort_id'] = site_url($sort_base.'u2s.id'.$url);
        $data['sort_date_added'] = site_url($sort_base.'u2s.date_added'.$url);

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['filter_order'] = $filter_order;
        $data['filter_status'] = $filter_status;
        $data['filter_customer'] = $filter_customer;

        $data['action'] = site_url('services/orders/index');

        //load view
        $theme = $this->session->userdata('theme');

        $template = '/template/orders/index_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    //  View order info in modal
    public function info($order_id)
    {
        checkLogin();

        $data['order'] = $this->orderModel->getOrder($order_id);

        $theme = $this->session->userdata('theme');

        $template = '/template/orders/info';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    public function add()
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $this->form_validation->set_rules(
            'user_id', $this->lang->line('customer'), 'required'
        );

        $this->form_validation->set_rules(
            'service_id', $this->lang->line('service'), 'required'
        );

        $this->form_validation->set_rules('user', '');

        $this->form_validation->set_rules('service', '');

        $this->form_validation->set_rules(
            'amount', $this->lang->line('amount'), 'required'
        );

        if (!$this->form_validation->run()) {
            $data['msg'] = validation_errors();

            $data['action'] = site_url('services/orders/add');

            //load view
            $theme = $this->session->userdata('theme');

            $template = '/template/orders/create';

            if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
                $this->load->view('themes/'.$theme.$template, $data);
            } else {
                $this->load->view('themes/default'.$template, $data);
            }
        } else {
            $this->orderModel->add();

            $message = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('order_create_success')
            );

            $this->session->set_flashdata('message', $message);
            redirect('services/orders');
        }
    }

    public function uncomplete($order_id)
    {
        $this->orderModel->uncomplete($order_id);

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('uncomplete_success')
        );

        $this->session->set_flashdata('message', $msg);
        redirect('services/orders');
    }

    public function complete($order_id)
    {
        $this->orderModel->complete($order_id);

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('complete_success')
        );

        $this->session->set_flashdata('message', $msg);
        redirect('services/orders');
    }

    public function delete($order_id)
    {
        $json = array();

        if ($this->checkOrderId($order_id)) {
            if ($this->orderModel->delete($order_id)) {
                $json['msg'] = 'deleted';
            }
        }

        echo json_encode($json);
    }

    /*
     * Checks the Departman ID.
     * @param  a order id, integer
     * @return bool
     */
    private function checkOrderId($order_id)
    {
        if ($this->orderModel->checkOrderId($order_id) == 0) {
            
            $this->form_validation->set_message(
                'checkOrderId',
                'The order id hidden field is invalid.'
            );

            return false;
        } else {
            return true;
        }
    }
}
