<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Services extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('serviceModel'); // Load services model
        $this->load->library('form_validation'); // Load form validation library

        checkLogin(); // session helper function
    }

    // Listing the Services
    public function index()
    {
        $url = site_url('services/index?');

        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $sort = $this->input->get('sort', true);

        if ($sort) {
            $data['sort'] = $sort;
            $url .= '&sort='.$sort;
        } else {
            $data['sort'] = 'title';
        }

        $order = $this->input->get('order', true);

        if ($order) {
            $data['order'] = $order;
            $url .= '&order='.$order;
        } else {
            $data['order'] = 'ASC';
        }

        $data['q'] = $this->input->get('q', true);

        $q = $data['q'];

        if ($data['q']) {
            $url .= '&q='.$data['q'];
        }

        //display paypal result
        $data['message'] = $this->session->userdata('paypal_msg');

        //remove paypal session data
        if ($data['message']) {
            $this->session->unset_userdata('price');
            $this->session->unset_userdata('service_id');
            $this->session->unset_userdata('paypal');
            $this->session->unset_userdata('paypal_msg');
        }

        //pagination
        $this->load->library('pagination');
        $per_page = 10;
        $offset = ($page - 1) * $per_page;
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->serviceModel->countAll($q);
        $config['per_page'] = $per_page;
        $config['base_url'] = $url;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['services'] = $this->serviceModel->serviceList(
            $offset,
            $per_page,
            $data['sort'],
            $data['order'],
            $data['q']
        );

        $url = '';

        if ($data['order'] == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if ($data['q']) {
            $url .= '&q='.$data['q'];
        }

        $data['sort_title'] = site_url('services/index?sort=title'.$url);
        $data['sort_price'] = site_url('services/index?sort=price'.$url);

        //load view
        $theme = $this->session->userdata('theme');

        $template = '/template/services/index_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    // Displays service create form
    public function create()
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $theme = $this->session->userdata('theme');

        $template = '/template/services/create_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template);
        } else {
            $this->load->view('themes/default'.$template);
        }
    }

    public function createProcess()
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $json = array();

        $this->form_validation->set_rules(
            'title',
            $this->lang->line('title'),
            'required|trim'
        );
        
        $this->form_validation->set_rules(
            'description',
            $this->lang->line('description'),
            'required|trim'
        );

        if ($this->form_validation->run() == false) {

            $json['msg'] = sprintf(
                    $this->lang->line('msg_danger'),
                    '<ul>'.validation_errors('<li>', '</li>').'</ul>'
                );
            
        } else {

            //upload icon
            $base_path = $this->config->item('upload_path');
            $config['upload_path'] = $base_path.'/services/';
            $config['allowed_types'] = config('allowed_extensions');
            $config['max_size'] = config('max_upload_file_size');
            $config['encrypt_name'] = true;
            $file_name = '';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('icon')) {
                //  echo '<div class="alert alert-danger">' .
                //   $this->upload->display_errors().'</div>';
                //  return;
            } else {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
            }

            if ($this->serviceModel->create($file_name)) {
                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('service_create_succesful')
                );

                $this->session->set_flashdata('message', $msg);

                $json['msg'] = '<script>';
                $json['msg'] .= 'location.href="'.base_url('services/index').'";';
                $json['msg'] .= '</script>';
            } else {
                $json['msg'] = $this->lang->line('technical_problem');
            }
        }
        echo json_encode($json);
    }

    // Displays service info
    public function view($service_id)
    {
        $data['service'] = $this->serviceModel->getService($service_id);

        $theme = $this->session->userdata('theme');

        $template = '/template/services/view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    // Displays service update form     
    public function update($service_id)
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $data['service'] = $this->serviceModel->getService($service_id);

        $theme = $this->session->userdata('theme');

        $template = '/template/services/update_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    public function updateProcess()
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $json = array();

        $this->form_validation->set_rules(
            'title',
            $this->lang->line('title'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'description',
            $this->lang->line('description'), 
            'required|trim'
        );

        if ($this->form_validation->run() == false) {

            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
            
        } else {

            //upload icon
            $base_path = $this->config->item('upload_path');
            $config['upload_path'] = $base_path.'/services/';
            $config['allowed_types'] = config('allowed_extensions');
            $config['max_size'] = config('max_upload_file_size');
            $config['encrypt_name'] = true;
            $file_name = '';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('icon')) {
                //  echo '<div class="alert alert-danger">' .
                //   $this->upload->display_errors().'</div>';
                //  return;
            } else {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
            }

            if ($this->serviceModel->update($file_name)) {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('service_update_succesful')
                );
            } else {
                $json['msg'] = $this->lang->line('technical_problem');
            }
        }
        echo json_encode($json);
    }

    /*
     * Checks the Departman ID.
     * @param  a service id, integer
     * @return bool
     */
    private function checkServiceId($service_id)
    {
        if ($this->serviceModel->checkServiceId($service_id) == 0) {
            $this->form_validation->set_message(
                'checkServiceId',
                'The service id hidden field is invalid.'
            );

            return false;
        } else {
            return true;
        }
    }

    /*
     * Deletes the Department and related items.
     * @param  a service id, integer
     * @return string for ajax
     */
    public function delete($service_id)
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        if ($this->checkServiceId($service_id)) {
            if ($this->serviceModel->delete($service_id)) {
                echo 'deleted';
            }
        }
    }

    public function buy($service_id)
    {
        $data['service'] = $this->serviceModel->getService($service_id);

        if (!$data['service']) {
            show_404();
        }

        $this->session->set_userdata('service_id', $service_id);

        $data['paypal_action'] = site_url('paypal');

        $theme = $this->session->userdata('theme');

        $template = '/template/services/buy';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    public function autocomplete($q)
    {
        $this->db->select('title, price, service_id');
        $this->db->like('title', $q);
        $rows = $this->db->get('services')->result_array();

        echo json_encode($rows);
    }
}
