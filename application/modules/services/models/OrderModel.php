<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class OrderModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add()
    {
        if ($this->input->post('status', true)) {
            $status = $this->input->post('status', true);
        } else {
            $status = 0;
        }

        $data = array(
            'user_id' => $this->input->post('user_id', true),
            'service_id' => $this->input->post('service_id', true),
            'amount' => $this->input->post('amount', true),
            'status' => $status,
        );
        $this->db->insert('user_to_service', $data);
    }

    public function countAll($data)
    {
        if ($data['filter_order']) {
            $this->db->where('u2s.id', $data['filter_order']);
        }

        if ($data['filter_status'] != '') {
            $this->db->where('u2s.status', $data['filter_status']);
        }

        if ($data['filter_customer']) {
            $this->db->like('u.name', $data['filter_customer']);
        }

        $this->db->join('services s', 's.service_id = u2s.service_id');
        $this->db->join('users u', 'u.id = u2s.user_id');

        return $this->db->count_all('user_to_service u2s');
    }

    public function orderList($data)
    {
        $arr_sort = array(
            'u2s.date_added',
            'u2s.id',
            'u.name',
            's.title',
            'u2s.amount',
            'u2s.status',
        );

        if (in_array($data['sort'], $arr_sort)) {
            $this->db->order_by($data['sort'].' '.$data['order']);
        }

        if ($data['filter_order']) {
            $this->db->where('u2s.id', $data['filter_order']);
        }

        if ($data['filter_status'] != '') {
            $this->db->where('u2s.status', $data['filter_status']);
        }

        if ($data['filter_customer']) {
            $this->db->like('u.name', $data['filter_customer']);
        }

        $this->db->select('u.name as customer, u2s.*, s.title as service');
        $this->db->join('services s', 's.service_id = u2s.service_id');
        $this->db->join('users u', 'u.id = u2s.user_id');
        $this->db->offset($data['start']);
        $this->db->limit($data['limit']);

        return $this->db->get('user_to_service u2s')->result();
    }

    public function complete($order_id)
    {
        $this->db->set('status', 1);
        $this->db->where('id', $order_id);
        $this->db->update('user_to_service');
    }

    public function uncomplete($order_id)
    {
        $this->db->set('status', 0);
        $this->db->where('id', $order_id);
        $this->db->update('user_to_service');
    }

    public function getOrder($order_id)
    {
        $this->db->select('u.name as customer, u2s.*, s.title as service');
        $this->db->join('services s', 's.service_id = u2s.service_id');
        $this->db->join('users u', 'u.id = u2s.user_id');
        $this->db->where('u2s.id', $order_id);

        return $this->db->get('user_to_service u2s')->row();
    }

    public function checkOrderId($order_id)
    {
        return $this->db->get_where(
                    'user_to_service',
                    array(
                        'id' => $order_id,
                    )
                )->num_rows();
    }

    public function delete($order_id)
    {
        $this->db->delete('user_to_service', array('id' => $order_id));

        return true;
    }
}
