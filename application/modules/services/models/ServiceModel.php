<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ServiceModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function countAll($q = false)
    {
        if ($q) {
            $this->db->like('title', $q);
        }

        return $this->db->count_all_results('services');
    }

    public function create($icon = '')
    {
        $data = array(
            'title' => $this->input->post('title', true),
            'icon' => $icon,
            'description' => $this->input->post('description'),
            'price' => $this->input->post('price', true),
            'sort_order' => $this->input->post('sort_order', true),
        );

        return $this->db->insert('services', $data);
    }

    public function update($icon = '')
    {
        $data = array(
            'title' => $this->input->post('title', true),
            'description' => $this->input->post('description'),
            'price' => $this->input->post('price', true),
            'sort_order' => $this->input->post('sort_order', true),
        );

        if ($icon) {
            $data['icon'] = $icon;
        }

        return $this->db->update(
            'services',
            $data,
            array('service_id' => $this->input->post('service_id', true))
        );
    }

    public function serviceList($offset, $limit, $sort = 'title', $order = 'ASC',
                          $q = false)
    {
        if ($q) {
            $this->db->like('title', $q);
        }

        $this->db->order_by($sort.' '.$order);
        $this->db->offset($offset);
        $this->db->limit($limit);

        return $this->db->get('services')->result();
    }

    public function getService($service_id)
    {
        return $this->db->get_where('services',
                array('service_id' => $service_id))->row();
    }

    public function checkServiceId($service_id)
    {
        return $this->db->get_where('services',
                array('service_id' => $service_id))->num_rows();
    }

    public function delete($service_id)
    {
        $this->db->delete('services', array('service_id' => $service_id));

        return true;
    }
}
