<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Products extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('productModel'); // Load products model
        $this->load->library('form_validation'); // Load form validation library

        checkLogin(); // session helper function
    }

    // Listing the Departments, displays new product adding form, makes controls
    // for adding new product,
    public function index()
    {
        $url = site_url('products/index?');

        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $sort = $this->input->get('sort', true);

        if ($sort) {
            $data['sort'] = $sort;
            $url .= '&sort='.$sort;
        } else {
            $data['sort'] = 'title';
        }

        $order = $this->input->get('order', true);

        if ($order) {
            $data['order'] = $order;
            $url .= '&order='.$order;
        } else {
            $data['order'] = 'ASC';
        }

        $data['q'] = $this->input->get('q', true);

        if ($data['q']) {
            $url .= '&q='.$data['q'];
        }

        //display paypal result
        $data['message'] = $this->session->userdata('paypal_msg');

        //remove paypal session data
        if ($data['message']) {
            $this->session->unset_userdata('price');
            $this->session->unset_userdata('product_id');
            $this->session->unset_userdata('paypal');
            $this->session->unset_userdata('paypal_msg');
        }

        //pagination
        $this->load->library('pagination');
        $per_page = 10;
        $offset = ($page - 1) * $per_page;
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['base_url'] = $url;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->productModel->countAll($data['q']);
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['products'] = $this->productModel->productList(
            $offset,
            $per_page,
            $data['sort'],
            $data['order'],
            $data['q']
        );

        $url = '';

        if ($data['order'] == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if ($data['q']) {
            $url .= '&q='.$data['q'];
        }

        $data['sort_title'] = site_url('products/index?sort=title'.$url);
        $data['sort_price'] = site_url('products/index?sort=price'.$url);

        //load view
        $theme = $this->session->userdata('theme');

        $template = '/template/products/index_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    // Displays product create form
    public function create()
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $theme = $this->session->userdata('theme');

        $template = '/template/products/create_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template);
        } else {
            $this->load->view('themes/default'.$template);
        }
    }

    public function createProcess()
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $json = array();

        $this->form_validation->set_rules(
            'title', 
            $this->lang->line('title'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'description',
            $this->lang->line('description'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'link',
            $this->lang->line('link'),
            'valid_url|trim'
        );

        if ($this->form_validation->run() == false) {

            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
            
        } else {

            //upload icon
            $base_path = $this->config->item('upload_path');

            $config['upload_path'] = $base_path.'/products/';
            $config['allowed_types'] = config('allowed_extensions');
            $config['max_size'] = config('max_upload_file_size');
            $config['encrypt_name'] = true;
            $file_name = '';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('icon')) {
                //  echo '<div class="alert alert-danger">'
                //  . $this->upload->display_errors().'</div>';
                //  return;
            } else {
                
                $data = $this->upload->data();
                $file_name = $data['file_name'];                
            }

            if ($this->productModel->create($file_name)) {

                $msg = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('product_create_succesful')
                );

                $this->session->set_flashdata('message', $msg);

                $json['msg'] = '<script>';
                $json['msg'] .= 'window.location.replace("'.base_url('products/index').'");';
                $json['msg'] .= '</script>';
                
            } else {
                $json['msg'] = $this->lang->line('technical_problem');
            }
        }

        echo json_encode($json);
    }

    // Displays product info
    public function view($product_id)
    {
        $data['product'] = $this->productModel->getProduct($product_id);

        $theme = $this->session->userdata('theme');

        $template = '/template/products/view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    // Displays product update form
    public function update($product_id)
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $data['product'] = $this->productModel->getProduct($product_id);

        $theme = $this->session->userdata('theme');

        $template = '/template/products/update_view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    public function updateProcess()
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        $json = array();

        $this->form_validation->set_rules(
            'title',
            $this->lang->line('title'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'description',
            $this->lang->line('description'),
            'required|trim'
        );

        $this->form_validation->set_rules(
            'link',
            $this->lang->line('link'),
            'valid_url|trim'
        );

        if ($this->form_validation->run() == false) {
            $json['msg'] = sprintf(
                $this->lang->line('msg_danger'),
                '<ul>'.validation_errors('<li>', '</li>').'</ul>'
            );
        } else {

            //upload icon
            $base_path = $this->config->item('upload_path');

            $config['upload_path'] = $base_path.'/products/';
            $config['allowed_types'] = config('allowed_extensions');
            $config['max_size'] = config('max_upload_file_size');
            $config['encrypt_name'] = true;
            $file_name = '';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('icon')) {
                //  echo '<div class="alert alert-danger">' .
                //  $this->upload->display_errors().'</div>';
                //  return;
            } else {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
            }

            if ($this->productModel->update($file_name)) {
                $json['msg'] = sprintf(
                    $this->lang->line('msg_success'),
                    $this->lang->line('product_update_succesful')
                );
            } else {
                $json['msg'] = $this->lang->line('technical_problem');
            }
        }
        echo json_encode($json);
    }
    
    /*
     * Checks the Departman ID.
     * @param  a product id, integer
     * @return bool
     */
    private function checkProductId($product_id)
    {
        if ($this->productModel->checkProductId($product_id) == 0) {
            
            $this->form_validation->set_message(
                'checkProductId',
                'The product id hidden field is invalid.'
            );

            return false;
        } else {
            return true;
        }
    }
    
    /*
     * Deletes the Department and related items.
     * @param  a product id, integer
     * @return string for ajax
     */
    public function delete($product_id)
    {
        if (userdata('department') == 0) {
            die('Access forbiden!');
        }

        if ($this->checkProductId($product_id)) {
            if ($this->productModel->delete($product_id)) {
                echo json_encode(array('msg' => 'deleted'));
            }
        }
    }

    public function buy($product_id)
    {
        $data['product'] = $this->productModel->getProduct($product_id);

        if (!$data['product']) {
            show_404();
        }

        $this->session->set_userdata('product_id', $product_id);

        $data['paypal_action'] = site_url('paypal');

        $theme = $this->session->userdata('theme');

        $template = '/template/products/buy';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }
}
