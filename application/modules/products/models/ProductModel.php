<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ProductModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function countAll($q)
    {
        $sql = 'select count(*) as total from products';

        if ($q) {
            $sql .= ' WHERE title LIKE "%'.$q.'%"';
        }

        return $this->db->query($sql)->row()->total;
    }

    public function productList($offset, $limit, $sort, $order, $q)
    {
        $arr_sort = array(
            'title',
            'price',
        );

        $arr_order = array(
            'DESC',
            'ASC',
        );

        if (in_array($order, $arr_order) && in_array($sort, $arr_sort)) {
            $this->db->order_by($sort.' '.$order);
        }

        if ($q) {
            $this->db->where('title LIKE "%'.$q.'%"', '', false);
        }

        $this->db->offset($offset);
        $this->db->limit($limit);

        return $this->db->get('products')->result();
    }

    public function create($icon = '')
    {
        $data = array(
            'title' => $this->input->post('title', true),
            'icon' => $icon,
            'description' => $this->input->post('description', true),
            'price' => $this->input->post('price', true),
            'link' => $this->input->post('link', true),
            'sort_order' => $this->input->post('sort_order', true),
        );

        return $this->db->insert('products', $data);
    }

    public function update($icon = '')
    {
        $data = array(
            'title' => $this->input->post('title', true),
            'description' => $this->input->post('description', true),
            'link' => $this->input->post('link', true),
            'price' => $this->input->post('price', true),
            'sort_order' => $this->input->post('sort_order', true),
        );

        if ($icon) {
            $data['icon'] = $icon;
        }

        $this->db->update('products', $data,
            array('product_id' => $this->input->post('product_id', true)));

        return true;
    }

    public function getProduct($product_id)
    {
        return $this->db->get_where('products',
                array('product_id' => $product_id))->row();
    }

    public function checkProductId($product_id)
    {
        return $this->db->get_where('products',
                array('product_id' => $product_id))->num_rows();
    }

    public function delete($product_id)
    {
        $this->db->delete('products', array('product_id' => $product_id));

        return true;
    }
}
