<?php

class Paypal extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text');
        $this->load->model('paypalModel');

         checkLogin(); // session helper function
    }

    public function index()
    {
        $service_id = $this->session->userdata('service_id');

        //get user
        $service = $this->db->get_where(
            'services',
            array('service_id' => $service_id)
        )->row_array();

        //price
        $price = $this->db->get_where(
            'services',
            array('service_id' => $service_id)
        )->row()->price;

        $this->session->set_userdata('comment', $this->input->post('comment', true));

        if ($price <= 0) {
            $price = $this->input->post('price', true);
            $this->session->set_userdata('price', $price);
        }

        $is_sandbox = config('paypal_test');

        $data = array(
            'METHOD' => 'SetExpressCheckout',
            'RETURNURL' => site_url('paypal/expressComplete'),
            'CANCELURL' => site_url('services'),
            'REQCONFIRMSHIPPING' => 0,
            'NOSHIPPING' => 1,
            'LOCALECODE' => 'EN',
            'LANDINGPAGE' => 'Login',
            'ALLOWNOTE' => 1,
            'PAYMENTREQUEST_0_SHIPPINGAMT' => '',
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
            'PAYMENTREQUEST_0_ITEMAMT' => number_format($price, 2, '.', ''),
            'PAYMENTREQUEST_0_AMT' => number_format($price, 2, '.', ''),
        );

        //course 
        $data['L_PAYMENTREQUEST_0_DESC0'] = word_limiter(
            $service['description'],
            3
        );

        $data['L_PAYMENTREQUEST_0_NAME0'] = word_limiter(
            $service['title'],
            3
        );

        $data['L_PAYMENTREQUEST_0_AMT0'] = number_format($price, 2, '.', '');
        $data['L_PAYMENTREQUEST_0_QTY0'] = 1;
        $data['L_PAYMENTREQUEST_0_ITEMURL0'] = site_url('services');

        $result = $this->paypalModel->call($data);

        /*
         * If a failed PayPal setup happens, handle it.
         */
        if (!isset($result['TOKEN'])) {

            $data['msg'] = 'Error: Invalid Token!';
            $data['xml'] = $result;
            
            //load view
            $theme = $this->session->userdata('theme');

            $template = '/template/paypal/error.tpl';

            if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
                $this->load->view('themes/'.$theme.$template, $data);
            } else {
                $this->load->view('themes/default'.$template, $data);
            }
        }else{
            $paypal['TOKEN'] = $result['TOKEN'];

            $this->session->set_userdata('paypal', $paypal);

            if ($is_sandbox) {
                $url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?'
                    .'cmd=_express-checkout&token='.$result['TOKEN'];

                redirect($url);
            } else {
                $url = 'https://www.paypal.com/cgi-bin/webscr?'
                    .'cmd=_express-checkout&token='.$result['TOKEN'];

                redirect($url);
            }
        }
    }

    public function expressComplete()
    {
        $is_sandbox = config('paypal_test');

        $paypal = $this->session->userdata('paypal');
        $this->session->unset_userdata('paypal');

        $data = array(
            'METHOD' => 'GetExpressCheckoutDetails',
            'TOKEN' => $paypal['TOKEN'],
        );

        $result = $this->paypalModel->call($data);

        $paypal['payerid'] = $result['PAYERID'];
        $paypal['result'] = $result;

        $paypal_data = array(
            'TOKEN' => $paypal['TOKEN'],
            'PAYERID' => $paypal['payerid'],
            'METHOD' => 'DoExpressCheckoutPayment',
            'PAYMENTREQUEST_0_NOTIFYURL' => '', //ipn url
            'RETURNFMFDETAILS' => 1,
        );

        $paypal_data = array_merge(
            $paypal_data,
            $this->paypalModel->paymentRequestInfo()
        );

        $result = $this->paypalModel->call($paypal_data);

        if ($result['ACK'] == 'Success') {

            //success action
            $data = array(
                'user_id' => $this->session->userdata('user_id'),
                'service_id' => $this->session->userdata('service_id'),
                'amount' => $result['PAYMENTINFO_0_AMT'],
                'date_added' => date('Y-m-d'),
                'comment' => $this->session->userdata('comment'),
            );

            $this->db->insert('user_to_service', $data);

            //unset data
            $this->session->unset_userdata('comment');

            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('payment_success')
            );

            $this->session->set_userdata('paypal_msg', $msg);

            redirect('services');

            /*
              //add transaction to paypal transaction table
              $paypal_transaction_data = array(
              'customer_id' => $this->session->userdata('customer_id'),
              'course_id' =>  $this->session->userdata('course_id'),
              'transaction_id' => $result['PAYMENTINFO_0_TRANSACTIONID'],
              'parent_transaction_id' => '',
              'note' => '',
              'msgsubid' => '',
              'receipt_id' => (isset($result['PAYMENTINFO_0_RECEIPTID']) ?
             *  $result['PAYMENTINFO_0_RECEIPTID'] : ''),
              'payment_type' => $result['PAYMENTINFO_0_PAYMENTTYPE'],
              'payment_status' => $result['PAYMENTINFO_0_PAYMENTSTATUS'],
              'pending_reason' => $result['PAYMENTINFO_0_PENDINGREASON'],
              'transaction_entity' => 'Sale',
              'amount' => $result['PAYMENTINFO_0_AMT'],
              'debug_data' => json_encode($result),
              );
              $this->db->insert('paypal_transactions', $paypal_transaction_data);
             */
        } else {
            if ($result['L_ERRORCODE0'] == '10486') {
                if (isset($this->session->userdata['paypal_redirect_count'])) {
                    if ($this->session->userdata['paypal_redirect_count'] == 2) {
                        $this->session->set_userdata('paypal_redirect_count', 0);
                        $this->session->set_flashdata('msg',
                            'Error: too many failure');

                        redirect('paypal/error');
                    } else {
                        ++$this->session->userdata['paypal_redirect_count'];
                    }
                } else {
                    $this->session->userdata['paypal_redirect_count'] = 1;
                }

                if ($is_sandbox) {
                    $url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?';
                    $url .= 'cmd=_express-checkout&token=';
                    $url .= $this->session->userdata['paypal']['token'];

                    $this->redirect($url);
                } else {
                    $url = 'https://www.paypal.com/cgi-bin/webscr?';
                    $url .= 'cmd=_express-checkout&token=';
                    $url .= $this->session->userdata['paypal']['token'];

                    $this->redirect($url);
                }
            }

            $msg = sprintf(
                $this->lang->line('msg_danger'),
                $result['L_LONGMESSAGE0']
            );

            $this->session->set_userdata('paypal_msg', $msg);

            redirect('services');
        }
    }

    public function error()
    {
        $data['msg'] = $this->session->flashdata('msg');
        $data['xml'] = $this->session->flashdata('xml');

        //load view
        $theme = $this->session->userdata('theme');

        $template = '/template/paypal/error.tpl';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }
}
