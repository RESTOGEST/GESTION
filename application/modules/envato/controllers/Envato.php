<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Envato extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        checkLogin(); // session helper function
    }

    public function index()
    {
        $data['msg'] = $this->session->flashdata('msg');

        $theme = $this->session->userdata('theme');

        if (file_exists(
                APPPATH.'views/themes/'.$theme.'/template/envato/index')
            ) {
            $this->load->view('themes/'.$theme.'/template/envato/index', $data);
        } else {
            $this->load->view('themes/default/template/envato/index', $data);
        }
    }

    public function varifyPost()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules(
            'buyer',
            $this->lang->line('buyer'),
            'required'
        );

        $this->form_validation->set_rules(
            'code',
            $this->lang->line('purchase_code'), 
            'required'
        );

        if ($this->form_validation->run() && $this->verifyPurchase()) {

            $response = array(
                'msg' => $this->lang->line('envato_success_msg'),
                'status' => 1,
            );
            
        } else {
            $response = array(
                'msg' => $this->lang->line('envato_error_msg'),
                'status' => 0,
            );
        }
        echo json_encode($response);
    }

    //Item purchase verification by envato api
    public function verifyPurchase()
    {
        $buyer = $this->input->post('buyer', true);
        $purchase_code = $this->input->post('code', true);
        
        $app_id = config('envato_app_id');
        //t3593ia0r3xbxcx85b4zve2j53y7zzrr

        $curl = curl_init('http://marketplace.envato.com/api/edge/'.
            config('envato_username').'/'.$app_id.'/verify-purchase:'.
            $purchase_code.'.xml');

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_USERAGENT,
            'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');

        $purchase_data = curl_exec($curl);
        curl_close($curl);

        $purchase_data = json_decode(
            json_encode((array) simplexml_load_string($purchase_data)),
            1
        );

        if (isset($purchase_data['verify-purchase']['buyer'])) {
            $purchase_buyer = $purchase_data['verify-purchase']['buyer'];
        } else {
            $purchase_buyer = '';
        }

        if ($purchase_buyer == $buyer) {
            return true;
        } else {
            return false;
        }
    }
}
