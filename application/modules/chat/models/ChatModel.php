<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ChatModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onlineAgents()
    {
        $online_check_time = config('online_check_time');

        $where = 'department > 1 '
            .'AND TIMESTAMPDIFF(SECOND, last_activity, NOW()) <= '
            .$online_check_time;

        $this->db->where($where, null, false);

        return $this->db->get('users')->result_array();
    }

    public function onlineVisitors()
    {
        $online_check_time = config('online_check_time');

        $where = 'department = 0  '
            .'AND TIMESTAMPDIFF(SECOND, last_activity, NOW()) <= '
            .$online_check_time;

        $this->db->where($where, null, false);

        return $this->db->get('users')->result_array();
    }

    public function userAutocomplete($q)
    {
        $this->db->like('name', $q);

        return $this->db->get('users', 10)->result_array();
    }

    public function customerAutocomplete($q)
    {
        $this->db->like('name', $q);
        $this->db->where('department = 0');

        return $this->db->get('users', 10)->result_array();
    }

    public function agentAutocomplete($q)
    {
        $this->db->like('name', $q);
        $this->db->where('department != 0');

        return $this->db->get('users', 10)->result_array();
    }

    public function getUserName($user_id)
    {
        $where = array('id' => $user_id);

        $row = $this->db->get_where('users', $where)->row_array();

        if ($row) {
            return $row['name'];
        } else {
            return false;
        }
    }

    public function myTotalChats($to_id = 0)
    {
        $user_id = $this->session->userdata('user_id');

        if ($to_id) {
            $where = '(from_id = '.$user_id.' AND to_id = '.$to_id.') OR'
                .' (from_id = '.$to_id.' AND to_id = '.$user_id.')';

            $this->db->where($where);
        } else {
            $where = '(from_id = '.$user_id.' OR to_id = '.$user_id.')';

            $this->db->where($where);
        }

        $this->db->join('users', 'users.id = user_messages.from_id');

        return $this->db->count_all_results('user_messages');
    }

    public function mailTranscript()
    {
        $user_id = $this->session->userdata('user_id');
        $to_id = $this->input->post('receiver_id', true);
   
        $timezone = $this->session->userdata('timezone');
        
        $this->db->select('CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'") as timestamp,'
                . ' um.msg_id, um.chat_room_id, um.from_id, um.to_id, um.msg, um.is_notified', false);
                
        if ($to_id) {
            $where = '(from_id = '.$user_id.' AND to_id = '.$to_id.') OR'
                .' (from_id = '.$to_id.' AND to_id = '.$user_id.')';

            $this->db->where($where);
        } else {
            $where = '(from_id = '.$user_id.' OR to_id = '.$user_id.')';

            $this->db->where($where);
        }

        $data['messages'] = $this->db->get('user_messages um')->result_array();
        
        $data['user_id'] = $user_id;

        $this->db->select('department, name, photo, id as user_id');
        $this->db->where(array('id' => $user_id));
        $data['user'] = $this->db->get('users')->row_array();

        $this->db->select('department, name, photo, id as user_id');
        $this->db->where(array('id' => $to_id));
        $data['receiver'] = $this->db->get('users')->row_array();

        //send mail with messages
        $message = $this->load->view('chat_tran_mail.tpl', $data, true);

        $subject = 'Chat Transcript With : '.$data['receiver']['name'];
        sendNotice($this->input->post('email', true), $subject, $message);
    }

    //  Online chat load message history
    public function loadMsg($to_id = 0, $limit = 10, $last_msg_id = 0)
    {
        $user_id = $this->session->userdata('user_id');

        $timezone = $this->session->userdata('timezone');
        
        $this->db->select('CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'") as timestamp, um.*, users.*', false);
        
        if ($to_id) {
            $where = '(from_id = '.$user_id.' AND to_id = '.$to_id.') OR'
                .' (from_id = '.$to_id.' AND to_id = '.$user_id.')';
        } else {
            $where = '(from_id = '.$user_id.' OR to_id = '.$user_id.')';
        }

        if ($last_msg_id) {
            $where = '('.$where.') AND um.msg_id < '.$last_msg_id;
            //only old messages needed
        }

        $this->db->where($where, null, false);

        $this->db->join('users', 'users.id = um.from_id');

        return $this->db->get('user_messages um', $limit)->result_array();
    }

    public function myChats($to_id = 0, $limit = 10, $offset = 0)
    {
        $user_id = $this->session->userdata('user_id');

        $timezone = $this->session->userdata('timezone');
        
        $this->db->select('CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'") as timestamp, um.*, users.*', false);
        
        if ($to_id) {
            $where = '(from_id = '.$user_id.' AND to_id = '.$to_id.') OR'
                .' (from_id = '.$to_id.' AND to_id = '.$user_id.')';

            $this->db->where($where);
        } else {
            $where = '(from_id = '.$user_id.' OR to_id = '.$user_id.')';

            $this->db->where($where);
        }

        $this->db->join('users', 'users.id = user_messages.from_id');

        return $this->db->get('user_messages um', $limit, $offset)->result_array();
    }

    public function loadHistory($chat_id, $limit = 10, $offset = 0)
    {        
        $timezone = $this->session->userdata('timezone');
        
        $this->db->select('CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'") as timestamp, um.*', false);
        
        return $this->db->get_where(
                   'user_messages um',
                   array('chat_id' => $chat_id),
                   $limit,
                   $offset
               )->result_array();
    }

    public function updateChatInfo($customer_id)
    {
        $data = array(
            'name' => $this->input->post('name', true),
            'email' => $this->input->post('email', true),
            'telephone' => $this->input->post('telephone', true),
            'details' => $this->input->post('details', true),
        );
        $this->db->where('id', $customer_id);
        $this->db->update('users', $data);
    }

    //get agent option list to assign
    public function filterAgents()
    {
        $department = $this->input->post('department_id', true);
        $type = $this->input->post('type', true);
        $online_check_time = config('online_check_time');

        if ($department) {
            $this->db->where('department', $department);
        }

        if ($type == 'online') {
            
//            $agent_max_chat = config('agent_max_chat');
//            total_active_chat <= '.$agent_max_chat.' AND 
//            
            $where = ' TIMESTAMPDIFF(SECOND, last_activity, NOW()) <= '
                .$online_check_time;

            $this->db->where($where, null, false);
            
        } elseif ($type == 'offline') {
            $where = 'TIMESTAMPDIFF(SECOND, last_activity, NOW()) > '
                .$online_check_time;

            $this->db->where($where, null, false);
        }

        $this->db->where('status = 1 AND department > 1', null, false);

        $rows = $this->db->get('users')->result_array();

        $html = '';
        
        foreach ($rows as $row) {
            $html .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }

        return $html;
    }

    //  Chat icon for customer
    public function getChatIcon()
    {
        $online_assigned_agent = $this->getOnlineAssignedAgent();        
        $online_agent = $this->getOnlineAgent();
        
        //agent assigned
        if ($online_assigned_agent || $online_agent) {
            return '<img src="images/start_chat_online.png" />';
        } else {
            return '<img src="images/start_chat_offline.png" />';
        }
    }

    //get online agents 
    public function getOnlineAgent() {
        
        $online_check_time = config('online_check_time');

        //if any agent online with lowest active chat
        //hide admin by dept. > 1
        
//        $agent_max_chat = config('agent_max_chat');
//        total_active_chat <= '.$agent_max_chat.' AND
//        
        $where = ' department > 1 AND status = 1 '
                . ' AND TIMESTAMPDIFF(SECOND, last_activity, NOW())'
                .' <= '.$online_check_time;

        $this->db->order_by('total_active_chat', 'ASC');
        
        $this->db->where($where, null, false);
                
        return $this->db->get('users')->row_array();
    }
    
    // get assigned agent if online 
    public function getOnlineAssignedAgent() {
        
        $user_id = $this->session->userdata('user_id');

        $online_check_time = config('online_check_time');

        //get agent id if agent assigned & agent exists & online
        $this->db->select('client.agent_id');
        $this->db->join('users as agent', 'agent.id = client.agent_id');
//
//        $agent_max_chat = config('agent_max_chat');
//        agent.total_active_chat <= '.$agent_max_chat.' AND
//                
        $where = ' client.id='.$user_id.' '
            .'AND agent.department > 1 AND agent.status = 1 AND '
            .'TIMESTAMPDIFF(SECOND, agent.last_activity, NOW()) <= '
            .$online_check_time;

        $this->db->where($where, null, false);

        $result = $this->db->get('users as client')->row_array();
        
        return $result;
    }
    
    //find agent id
    public function findAgent()
    {
        $online_agent = $this->getOnlineAgent();

        //agent found online
        if ($online_agent) {
            
            return $online_agent['id'];

        //no agent online get offline agent
        } else {
            
            $this->db->order_by('total_active_chat', 'ASC');
            $this->db->where('department > 1', null, false);
            $offline_user = $this->db->get('users')->row_array();

            if ($offline_user) {
                return $offline_user['id'];
            } else {
                return '{Error: "No Agent Found!"}';
            }
        }
    }

    public function getAssignedAgent()
    {        
        $user_id = $this->session->userdata('user_id');

        //get agent id if agent assigned & agent exists & online
        $this->db->select('client.agent_id');
        $this->db->join('users as agent', 'agent.id = client.agent_id');
        
        //
        //        $agent_max_chat = config('agent_max_chat');
        //        agent.total_active_chat <= '.$agent_max_chat.' AND
        //                
        
        $where = ' client.id='.$user_id.' AND agent.department > 1 AND agent.status = 1';

        $this->db->where($where, null, false);

        $result = $this->db->get('users as client')->row_array();
        
        return $result;
    }
    
    //get agent id for login customer
    public function getAgentId()
    {
        $row = $this->getAssignedAgent();

        //agent assigned
        if ($row) {
            return $row['agent_id'];
        } else {
            return $this->findAgent();
        }
    }

    public function chatAssign()
    {
        $this->db->set('agent_id', $this->input->post('agent_id', true));
        $this->db->where('id', $this->input->post('customer_id', true));
        $this->db->update('users');

        return $this->db->get_where(
            'users',
            array('id' => $this->input->post('agent_id', true))
        )->row()->name;
    }

    public function sendMessage()
    {
        $user = $this->session->all_userdata();

        $data = array(
            'from_id' => $user['user_id'],
            'to_id' => $this->input->post('receiver_id', true),
            'chat_room_id' => $this->input->post('chat_room_id', true),
            'msg' => $this->input->post('msg', true),
        );
        $this->db->insert('user_messages', $data);

        $html = '<div class="chat-message clearfix">';
        $html .= '<img src="'.base_url().'images/customer-user.png"'
            .' alt="" style="width: 37px;height:37px;">';
        $html .= '<div class="chat-message-content clearfix">';
        $html .= '<span class="chat-time" data-livestamp="'.time().'">';
        $html .=    $this->lang->line('0_seconds_ago');
        $html .= '</span>';
        $html .= '<h5>'.$user['name'].'</h5>';
        $html .= '<p>'.$data['msg'].'</p>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    public function updateTypingMomment()
    {
        $user_id = $this->session->userdata('user_id');
        $this->db->where('id', $user_id);
        $this->db->set('typing_momment', 'NOW()', false);
        $this->db->update('users');
    }

    public function setMsgNotified($msg_id)
    {
        $this->db->update('user_messages', array('is_notified' => 1), array('msg_id' => $msg_id));
    }
    
    public function setNotified()
    {
        $user = $this->session->all_userdata();
        $receiver_id = $this->input->post('receiver_id', true);

        $filter = array(
            'from_id' => $receiver_id,
            'to_id' => $user['user_id'],
            'is_notified' => 0,
        );
                
        $this->db->where($filter);
        
        $this->db->update('user_messages', array('is_notified' => 1));
    }

    public function checkMessage()
    {
        $user = $this->session->all_userdata();
        $receiver_id = $this->input->post('receiver_id', true);
        $last_msg_id = $this->input->post('last_msg_id', true);

        $timezone = $this->session->userdata('timezone');
        
        $this->db->select('CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'") as timestamp,'
                . ' um.msg_id, um.chat_room_id, um.from_id, um.to_id, um.msg, um.is_notified', false);
        
        //message from receiver to sender (call by sender)
        $filter = array(
            'from_id' => $receiver_id,
            'to_id' => $user['user_id'],
            'is_notified' => 0,
        );
        $this->db->where($filter);

        if ($last_msg_id) {
            $this->db->where('msg_id > '.$last_msg_id, null, false);
        }

        $this->db->order_by('timestamp ASC');
        
        $rows = $this->db->get('user_messages um')->result_array();
        
        //total new messages
        $total_new_message = $this->db->where($filter)
                                ->count_all_results('user_messages');

        //set notified to true
        //$this->db->where($filter);
        //$this->db->update('user_messages', array('is_notified'=>1));
        //check if opponent typing?
        if (!$rows) {
            $where = 'TIMESTAMPDIFF(SECOND, typing_momment,NOW()) <= 2 '
                .'AND id = '.$receiver_id;

            $is_typing = $this->db->where($where, null, false)
                ->count_all_results('users');
        } else {
            $is_typing = 0;
        }

        //if opponent online
        $online_check_time = config('online_check_time');

        $where = 'TIMESTAMPDIFF(SECOND, last_activity, NOW()) <= '.
            $online_check_time.' AND id = '.$receiver_id;

        $this->db->where($where, null, false);

        $is_online = $this->db->count_all_results('users');

        $receiver = $this->db->select('department, name, photo, id as user_id')
            ->get_where('users', array('id' => $receiver_id))
            ->row_array();

        $show_agent_offline_alert = 0;
        
        //if opponent offline + i am customer + agent online 
        //Your support officer got offline for now, would you like to chat with other? 
        
        $online_agent = $this->getOnlineAgent();
        
        if(userdata('department')==0 && !$is_online && $online_agent){ 
            $show_agent_offline_alert = 1;
        }
        
        $response = array(
            'new_agent_id' => $this->getNewAgentId(),//check if chat transferred
            'time' => time(),
            'show_agent_offline_alert' => $show_agent_offline_alert,
            'receiver_id' => $receiver_id,
            'is_typing' => $is_typing,
            'is_online' => $is_online,
            'total_new_msg' => $total_new_message,
            'receiver' => $receiver,
            'messages' => $rows,
        );

        //get last message id to avoid duplicate message
        if ($rows) {
            $temp = end($rows);
            $response['last_msg_id'] = $temp['msg_id'];
        }

        //get agent name if chat transfered

        $new_agent_id = $response['new_agent_id'];

        if ($new_agent_id && $new_agent_id != $receiver_id) {
            $this->db->where(array('id' => $response['new_agent_id']));

            $response['new_agent_name'] = $this->db->get('users')->row()->name;

            //start new chatroom
            $this->db->delete('user_chatroom',
                array(
                'user_id' => $user['user_id'],
                'receiver_id' => $receiver_id,
                )
            );

            $this->db->insert('user_chatroom',
                array(
                'user_id' => $user['user_id'],
                'receiver_id' => $response['new_agent_id'],
                )
            );

            $response['chat_room_id'] = $this->db->insert_id();

            //if not chat transfered check if name changes?
        } else {
            //check if my name change
            $username_old = $this->session->userdata('name');
            $user_id = $this->session->userdata('user_id');

            $username_new = $this->db->get_where(
                'users',
                array('id' => $user_id)
            )->row()->name;

            if ($username_old != $username_new) {
                $this->session->set_userdata('name', $username_new);
                $response['refresh'] = 1;
            }

            $receivername_old = $this->input->post('receivername', true);

            $receivername_new = $this->db->get_where(
                'users',
                array('id' => $receiver_id)
            )->row()->name;

            if ($receivername_new != $receivername_old) {
                $response['refresh'] = 1;
            }
        }

        //get chat response if like/unlike
        if ($response['receiver']['department'] == 0) {
            $temp = $this->db->get_where(
                'user_rating',
                array(
                    'customer_id' => $receiver_id,
                    'agent_id' => $user['user_id'],
                )
            )->row();
        } else {
            $temp = $this->db->get_where(
                'user_rating',
                array(
                    'customer_id' => $user['user_id'],
                    'agent_id' => $receiver_id,
                )
            )->row();
        }

        if ($temp) {
            $response['chat_response'] = $temp->rating;
        } else {
            $response['chat_response'] = 0;
        }
        
        //

        return $response;
    }

    //check if chat assign to defferent agent get new agent id
    public function getNewAgentId()
    {
        if (userdata('department') == 0) {
            return $this->getAgentId();
        } else {
            return 0;
        }
    }

    public function checkNotification()
    {
        $user = $this->session->all_userdata();
   
        $timezone = $this->session->userdata('timezone');
                
        $this->db->select('count(*) as count, CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'")'
                . ' as timestamp,name, photo, id as user_id, GROUP_CONCAT(um.msg ORDER BY `um`.`msg_id` DESC SEPARATOR "<br />") as msg, um.msg_id,'
                . ' um.chat_room_id, um.from_id, um.to_id, um.is_notified', false)
            ->join('users', 'users.id = um.from_id');

        $this->db->order_by('um.msg_id DESC');
        
        $this->db->group_by('from_id');
                
        $data['rows'] = $this->db->get_where(
                'user_messages um',
                array(
                    'is_notified' => 0,
                    'to_id' => $user['user_id'],
                )
            )->result_array();
       
        //unread mesage count         
        $this->db->join('users', 'users.id = um.from_id');
        $this->db->where(array('um.is_notified' => 0, 'um.to_id' => $user['user_id']));        
        $data['count'] = $this->db->count_all_results('user_messages um');
        $data['status'] = 1;
        return $data;
    }

    public function startOfflineChat2($receiver_id)
    {
        $user_id = $this->session->userdata('user_id');
        $name = $this->session->userdata('name');

        //assign agent
        $this->db->set('agent_id', $receiver_id);
        $this->db->where('id', $user_id);
        $this->db->update('users');

        /*
          total messages
          ---------------------------------------------
          $total_msg = $this->db->query('select count(*) as total from
           user_messages'
          . ' WHERE (from_id = "'.$receiver_id.'" AND to_id = "'.$user_id.'")'
          . ' OR (from_id = "'.$user_id.'" AND to_id = "'.$receiver_id.'")')
         * ->row()->total;
         */

        //list message
        $timezone = $this->session->userdata('timezone');
        
        $sql = 'select CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'") as timestamp,'
            . ' um.msg, um.msg_id, um.chat_room_id, um.from_id, um.to_id, um.is_notified'
            . ' from user_messages um  WHERE is_notified = 0 AND '
            . '((from_id = "'.$receiver_id.'" AND to_id = "'.$user_id.'") OR '
            . '(from_id = "'.$user_id.'" AND to_id = "'.$receiver_id.'"))'
            . ' ORDER BY timestamp ASC LIMIT 5';

        $new_msg = $this->db->query($sql)->result_array();

        //total new messages
        $this->db->where(array(
            'from_id' => $receiver_id,
            'to_id' => $user_id,
            'is_notified' => 0,
        ));
        $total_new_message = $this->db->count_all_results('user_messages');

        //set notified to true
        $this->db->where(array(
            'from_id' => $receiver_id,
            'to_id' => $user_id,
            'is_notified' => 0,
        ));
        $this->db->update('user_messages', array('is_notified' => 1));

        //add entry to chatroom not already exists
        $sql = 'select * from user_chatroom WHERE '
            .'(user_id="'.$user_id.'" OR user_id="'.$receiver_id.'") AND '
            .'(receiver_id="'.$user_id.'" OR receiver_id = '.$receiver_id.')';
        $temp = $this->db->query($sql)->row_array();

        if (!$temp) {
            $data = array(
                'user_id' => $user_id,
                'receiver_id' => $receiver_id,
            );

            $this->db->insert('user_chatroom', $data);

            $chat_room_id = $this->db->insert_id();
        } else {
            $chat_room_id = $temp['chat_room_id'];
        }

        //chat response
        $department = $this->session->userdata('department');

        if ($department == 0) {
            // i am customer

            $chat_response_row = $this->db->get_where(
                'user_rating',
                array('agent_id' => $receiver_id, 'customer_id' => $user_id)
            )->row_array();
        } else {
            // i am agent

            $chat_response_row = $this->db->get_where(
                'user_rating',
                 array('agent_id' => $user_id, 'customer_id' => $receiver_id)
            )->row_array();
        }

        if ($chat_response_row) {
            $chat_response = $chat_response_row['rating'];
        } else {
            $chat_response = 0;
        }

        ///check opponent is online?
        $online_check_time = config('online_check_time');

        $where = 'TIMESTAMPDIFF(SECOND, last_activity, NOW()) <= '
            .$online_check_time.' AND id = '.$receiver_id;

        $is_online = $this->db->where($where, null, false)
            ->count_all_results('users');

        //get receiver 
        $this->db->select('name, photo, last_activity, id as user_id');
        $receiver = $this->db->get_where('users', array('id' => $receiver_id))
            ->row_array();

        //data to start conversation
        return array(
            'chat_room_id' => $chat_room_id,
            //'total_msg' => $total_msg,
            'is_online' => $is_online,
            'chat_response' => $chat_response,
            'total_new_msg' => $total_new_message,
            'sender' => array($name, false, $user_id),
            'receiver' => $receiver,
            'messages' => $new_msg,
        );
    }

    public function startOfflineChat()
    {
        //check if user is exist
        $userdata = array(
            'name' => $this->input->post('name', true),
            'email' => $this->input->post('email', true),
            'register_time' => strtotime(date('d F Y g:i a')),
            'ip_address' => $this->input->server('REMOTE_ADDR'),
            'status' => 1,
        );
        
        $user_from_db = $this->db->get_where(
            'users',
            array('email' => $userdata['email'])
        )->row_array();

        //create user if not exist
        if (!$user_from_db) {
            $this->db->insert('users', $userdata);
            $user_id = $this->db->insert_id();
        } else {
            $user_id = $user_from_db['id'];
        }

        //set session data
        $this->session->set_userdata('name', $userdata['name']);
        $this->session->set_userdata('user_id', $user_id);
        $this->session->set_userdata('isLogin', 0);
        $this->session->set_userdata('department', 0);

        //if receiver id not found in user list get the user with latest
        //last_activity
        $receiver_id = $this->getAgentId();

        //assign agent
        $this->db->set('agent_id', $receiver_id);
        $this->db->where('id', $user_id);
        $this->db->update('users');

        /*
          check have more messages
          ----------------------------------------
          $total_msg = $this->db->query('select count(*) as total from
         * user_messages'
          . ' WHERE (from_id = "'.$receiver_id.'" AND to_id = "'.$user_id.'")'
          . ' OR (from_id = "'.$user_id.'" AND to_id = "'.$receiver_id.'")'
          . ' ORDER BY is_notified DESC')->row()->total;
         */

        //get messages
        
        $timezone = $this->session->userdata('timezone');
        
        $sql = 'select CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'") as timestamp,'
            . ' um.msg, um.msg_id, um.chat_room_id, um.from_id, um.to_id, um.is_notified from user_messages um WHERE is_notified=0 AND '
            .'((from_id = "'.$receiver_id.'" AND to_id = "'.$user_id.'")'
            .' OR (from_id = "'.$user_id.'" AND to_id = "'.$receiver_id.'"))'
            .' ORDER BY timestamp ASC'
            .' LIMIT 5';

        $new_msg = $this->db->query($sql)->result_array();

        //total new messages
        $this->db->where(array(
            'from_id' => $receiver_id,
            'to_id' => $user_id,
            'is_notified' => 0,
        ));
        $total_new_message = $this->db->count_all_results('user_messages');

        //set notified to true
        $this->db->where(array(
            'from_id' => $receiver_id,
            'to_id' => $user_id,
            'is_notified' => 0,
        ));
        $this->db->update('user_messages', array('is_notified' => 1));

        //add entry to chatroom not already exists
        $sql = 'select * from user_chatroom WHERE'
            .' (user_id="'.$user_id.'" OR user_id="'.$receiver_id.'") AND'
            .' (receiver_id="'.$user_id.'" OR receiver_id = '.$receiver_id.')';

        $temp = $this->db->query($sql)->row_array();

        if (!$temp) {
            $data = array(
                'user_id' => $user_id,
                'receiver_id' => $receiver_id,
            );
            $this->db->insert('user_chatroom', $data);
            $chat_room_id = $this->db->insert_id();
        } else {
            $chat_room_id = $temp['chat_room_id'];
        }

        //chat response
        $department = $this->session->userdata('department');

        if ($department == 0) {
            // i am customer

            $chat_response_row = $this->db->get_where(
                'user_rating',
                array('agent_id' => $receiver_id, 'customer_id' => $user_id)
            )->row_array();
        } else {
            // i am agent

            $chat_response_row = $this->db->get_where(
                'user_rating',
                array('agent_id' => $user_id, 'customer_id' => $receiver_id)
            )->row_array();
        }

        if ($chat_response_row) {
            $chat_response = $chat_response_row['rating'];
        } else {
            $chat_response = 0;
        }

        ///check opponent is online?
        $online_check_time = config('online_check_time');

        $where = 'TIMESTAMPDIFF(SECOND, last_activity, NOW()) <= '.
            $online_check_time.' AND id = '.$receiver_id;

        $is_online = $this->db->where($where, null, false)
            ->count_all_results('users');

        $this->db->select('name, photo, last_activity, id as user_id');
        $receiver = $this->db->get_where('users', array('id' => $receiver_id))
            ->row_array();

        //data to start conversation
        return array(
            'chat_room_id' => $chat_room_id,
            // 'total_msg' => $total_msg,
            'is_online' => $is_online,
            'chat_response' => $chat_response,
            'total_new_msg' => $total_new_message,
            'sender' => array($userdata['name'], false, $user_id),
            'receiver' => $receiver,
            'messages' => $new_msg,
        );
    }

    public function leaveChatroom($receiver_id)
    {
        $user = $this->session->all_userdata();

        $sql = "delete from user_chatroom where (user_id='".$user['user_id']."' "
            ."AND  receiver_id = '".$receiver_id."') "
            ."OR (user_id='".$receiver_id."' "
            ."AND receiver_id = '".$user['user_id']."')";

        $this->db->query($sql);
    }

    public function enterChatroom($receiver_id)
    {
        $user = $this->session->all_userdata();
        
        //add entry to chatroom not already exists
        $sql = 'select * from user_chatroom WHERE (user_id="'.$user['user_id']
            . '" AND receiver_id="'.$receiver_id.'") OR (user_id="'.$receiver_id.'"'
            . ' AND receiver_id="'.$user['user_id'].'")';
        
        $temp = $this->db->query($sql)->row_array();

        if (!$temp) {
            $data = array(
                'user_id' => $user['user_id'],
                'receiver_id' => $receiver_id,
            );
            $this->db->insert('user_chatroom', $data);

            $chat_room_id = $this->db->insert_id();
        } else {
            $chat_room_id = $temp['chat_room_id'];
        }
        
        return $chat_room_id;
    }
    
    public function startChat($receiver_id)
    {
        $user = $this->session->all_userdata();
         
        //if receiver id not found in user list get the user with
        // latest last_activity
        if (!$receiver_id) {
            $receiver_id = $this->getAgentId();
        }

        //add entry to chatroom not already exists
        $sql = 'select * from user_chatroom WHERE (user_id="'.$user['user_id']
            . '" AND receiver_id="'.$receiver_id.'") OR (user_id="'.$receiver_id.'"'
            . ' AND receiver_id="'.$user['user_id'].'")';
        
        $temp = $this->db->query($sql)->row_array();

        if (!$temp) {
            $data = array(
                'user_id' => $user['user_id'],
                'receiver_id' => $receiver_id,
            );
            $this->db->insert('user_chatroom', $data);

            $chat_room_id = $this->db->insert_id();
        } else {
            $chat_room_id = $temp['chat_room_id'];
        }

        //get messages        
        $timezone = $this->session->userdata('timezone');
        
        $new_msg = $this->db->query('select CONVERT_TZ(um.timestamp, @@session.time_zone, "'.$timezone.'") '
            . 'as timestamp, um.msg, um.msg_id, um.chat_room_id, um.from_id, um.to_id, um.is_notified '
            . 'from user_messages um'
            .' WHERE is_notified=0 AND '
            .' ((from_id = "'.$receiver_id.'" AND '
            .'to_id = "'.$user['user_id'].'") OR '
            .' (from_id = "'.$user['user_id'].'" AND '
            .'to_id = "'.$receiver_id.'")) '
            .' ORDER BY timestamp ASC')->result_array();

        /*
          check have more messages
          ---------------------------------------
          $sql  = 'select count(*) as total from user_messages';
          $sql .= ' WHERE (from_id = "'.$receiver_id.'" AND
         * to_id = "'.$user['user_id'].'")';
          $sql .= ' OR (from_id = "'.$user['user_id'].'" AND
         *  to_id = "'.$receiver_id.'")';
          $total_msg = $this->db->query($sql)->row()->total;
         */

        //total new message
        $this->db->where(array(
            'from_id' => $receiver_id,
            'to_id' => $user['user_id'],
            'is_notified' => 0,
        ));
        $total_new_message = $this->db->count_all_results('user_messages');

        //set notified to true
        //$this->db->where($filter);
        //$this->db->update('user_messages', array('is_notified'=>1));
        //assign agent
        $this->db->set('agent_id', $receiver_id);
        $this->db->where('id', $user['user_id']);
        $this->db->update('users');

        //chat response
        $department = $this->session->userdata('department');

        if ($department == 0) {
            // i am customer

            $chat_response_row = $this->db->get_where(
                'user_rating',
                array(
                    'agent_id' => $receiver_id,
                    'customer_id' => $user['user_id'],
                )
            )->row_array();
        } else {
            // i am agent

            $chat_response_row = $this->db->get_where(
                'user_rating',
                array(
                    'agent_id' => $user['user_id'],
                    'customer_id' => $receiver_id,
                )
            )->row_array();
        }

        if ($chat_response_row) {
            $chat_response = $chat_response_row['rating'];
        } else {
            $chat_response = 0;
        }

        ///check opponent is online?
        $online_check_time = config('online_check_time');

        $where = 'TIMESTAMPDIFF(SECOND, last_activity, NOW()) <= '
            .$online_check_time.' AND id = '.$receiver_id;

        $this->db->where($where, null, false);

        $is_online = $this->db->count_all_results('users');

        //get receiver 
        $this->db->select('name, photo, last_activity, id as user_id');
        $this->db->where(array('id' => $receiver_id));
        $receiver = $this->db->get_where('users')->row_array();

        //data to start conversation
        return array(
            'chat_room_id' => $chat_room_id,
            'is_online' => $is_online,
            //'total_msg' => $total_msg,
            'chat_response' => $chat_response,
            'total_new_msg' => $total_new_message,
            'sender' => array($user['name'], '', $user['user_id']),
            'receiver' => $receiver,
            'messages' => $new_msg,
        );
    }

    public function countChatDetail($chat_room_id)
    {
        $this->db->join('users fu', 'fu.id = user_messages.from_id');
        $this->db->join('users tu', 'tu.id = user_messages.to_id');
        $this->db->where('user_messages.chat_room_id', $chat_room_id);

        return $this->db->count_all_results('user_messages');
    }

    public function chatDetail($chat_room_id, $limit, $offset)
    {
        $timezone = $this->session->userdata('timezone');
        
        $select = 'CONVERT_TZ(user_messages.timestamp, @@session.time_zone, "'.$timezone.'") as timestamp,'
            .' fu.name as from_name, tu.name as to_name, fu.department as '
            .' from_department, tu.department as to_department,'
            .' user_messages.msg, user_messages.chat_room_id';

        $this->db->select($select, false);
        $this->db->join('users fu', 'fu.id = user_messages.from_id');
        $this->db->join('users tu', 'tu.id = user_messages.to_id');
        $this->db->where('user_messages.chat_room_id', $chat_room_id);
        $this->db->order_by('user_messages.timestamp ASC');

        return $this->db->get('user_messages', $limit, $offset)->result_array();
    }

    public function chatHistory($agent_id, $customer_id, $limit, $offset)
    {
        if ($agent_id && $customer_id) {
            $where = '(from_id = '.$agent_id.' AND to_id = '.$customer_id.') OR'
                .' (from_id = '.$customer_id.' AND to_id = '.$agent_id.')';

            $this->db->where($where, null, false);
        } elseif ($agent_id) {
            $where = '(from_id = '.$agent_id.' OR to_id = '.$agent_id.')';

            $this->db->where($where);
        } elseif ($customer_id) {
            $where = '(from_id = '.$customer_id.' OR to_id = '.$customer_id.')';

            $this->db->where($where);
        }

        $timezone = $this->session->userdata('timezone');
        
        $select = 'CONVERT_TZ(timestamp, @@session.time_zone, "'.$timezone.'") as timestamp, '
            . 'fu.id as from_id, tu.id as to_id, fu.name as from_name,'
            .' tu.name as to_name, fu.department as from_department,'
            .' tu.department as to_department, user_messages.msg,'
            .' user_messages.chat_room_id';

        $this->db->select($select, false);
        $this->db->group_by('user_messages.chat_room_id');
        $this->db->order_by('user_messages.timestamp ASC');
        $this->db->join('users fu', 'fu.id = user_messages.from_id');
        $this->db->join('users tu', 'tu.id = user_messages.to_id');

        return $this->db->get('user_messages', $limit, $offset)->result_array();
    }

    public function countChatHistory($agent_id = false, $customer_id = false)
    {
        if ($agent_id && $customer_id) {
            $where = '(from_id = '.$agent_id.' AND to_id = '.$customer_id.') OR'
                .' (from_id = '.$customer_id.' AND to_id = '.$agent_id.')';

            $this->db->where($where, null, false);
        } elseif ($agent_id) {
            $where = '(from_id = '.$agent_id.' OR to_id = '.$agent_id.')';

            $this->db->where($where);
        } elseif ($customer_id) {
            $where = '(from_id = '.$customer_id.' OR to_id = '.$customer_id.')';

            $this->db->where($where);
        }
        
        $this->db->join('users fu', 'fu.id = user_messages.from_id');
        $this->db->join('users tu', 'tu.id = user_messages.to_id');

        $select = 'COUNT(DISTINCT  `user_messages`.`chat_room_id` ) AS'
            .' `numrows`';

        $this->db->select($select, false);

        $ret = $this->db->get('user_messages')->row_array();

        if ($ret) {
            return $ret['numrows'];
        } else {
            return 0;
        }
    }

    public function getRating($customer_id, $agent_id)
    {
        $row = $this->db->get_where(
            'user_rating',
            array('customer_id' => $customer_id, 'agent_id' => $agent_id)
        )->row_array();

        if ($row) {
            return $row['rating'];
        }
    }
    
    //remove from chatroom for customer 
    public function refreshChat(){
        
        $user_id = $this->session->userdata('user_id');
        
        //remove from chatroom 
        $this->db->where('user_id = '.$user_id.' OR receiver_id = '.$user_id);
        $this->db->delete('user_chatroom');
        
        //remove assigned agent 
        $this->db->update('users', array('agent_id' => 0), array('id' => $user_id));        
    }
}
