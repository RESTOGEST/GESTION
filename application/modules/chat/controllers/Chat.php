<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Chat extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('time_format');
        $this->load->model('chatModel');
    }

    public function index() {
        $user_id = $this->session->userdata('user_id');

        if (!$user_id) {
            redirect('/');
        }

        $agent_id = $this->input->get('agent_id', true);

        if (userdata('department') > 1) {
            $agent_id = $this->session->userdata('user_id');
        }

        $customer_id = $this->input->get('customer_id', true);

        $page = $this->input->get('page', true) ? $this->input->get('page', true) : 1;

        $base_url = base_url('chat/index?agent_id=' . $agent_id . '&customer_id='
                . $customer_id);

        //pagination
        $limit = config('tickets_per_page');
        $offset = ($page - 1) * $limit;
        $this->load->library('pagination');
        $config['base_url'] = $base_url;
        $config['total_rows'] = $this->chatModel->countChatHistory(
                $agent_id, $customer_id
        );
        $config['per_page'] = config('tickets_per_page');
        $config['next_link'] = $this->lang->line('next') . ' &rarr;';
        $config['page_query_string'] = true;
        $config['prev_link'] = '&larr; ' . $this->lang->line('previous');
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();

        $data['rows'] = array();

        $rows = $this->chatModel->chatHistory(
                $agent_id, $customer_id, $limit, $offset
        );

        foreach ($rows as $row) {
            if ($row['from_department'] == 0) {

                $department_row = $this->db->get_where(
                                'departments', array('id' => $row['to_department'])
                        )->row_array();

                $chat_response_row = $this->db->get_where(
                                'user_rating', array(
                            'agent_id' => $row['to_id'],
                            'customer_id' => $row['from_id'],
                                )
                        )->row_array();
            } else {

                $department_row = $this->db->get_where(
                                'departments', array(
                            'id' => $row['from_department'],
                                )
                        )->row_array();

                $chat_response_row = $this->db->get_where(
                                'user_rating', array(
                            'agent_id' => $row['to_id'],
                            'customer_id' => $row['from_id'],
                                )
                        )->row_array();
            }

            if ($department_row) {
                $row['department'] = $department_row['department_name'];
            } else {
                $row['department'] = '';
            }

            if ($chat_response_row) {
                $row['chat_response'] = $chat_response_row['rating'];
            } else {
                $row['chat_response'] = 0;
            }

            $row['action'] = site_url('chat/detail?chat_room_id='
                    . $row['chat_room_id']);

            $data['rows'][] = $row;
        }

        $data['agent_id'] = $agent_id;
        $data['agent'] = $this->chatModel->getUserName($agent_id);

        $data['customer_id'] = $customer_id;
        $data['customer'] = $this->chatModel->getUserName($customer_id);

        //get rating
        $data['rating'] = $this->chatModel->getRating(
                $customer_id, $agent_id
        );

        $theme = $this->session->userdata('theme');

        if (file_exists(
                        APPPATH . 'views/themes/' . $theme . '/template/chat/index')
        ) {
            $this->load->view('themes/' . $theme . '/template/chat/index', $data);
        } else {
            $this->load->view('themes/default/template/chat/index', $data);
        }
    }

    // load more chats
    public function myChats() {
        $receiver_id = $this->input->post('receiver_id', true);
        $last_msg_id = $this->input->post('last_msg_id', true);

        $limit = 5;

        $data['messages'] = $this->chatModel->loadMsg(
                $receiver_id, $limit, $last_msg_id
        );

        $this->db->select('department, name, photo, id as user_id');
        $this->db->where(array('id' => $receiver_id));
        $data['receiver'] = $this->db->get('users')->row_array();

        $json['html'] = $this->load->view('chat_messages.tpl', $data, true);

        echo json_encode($json);
    }

    public function visitorAjax() {
        $json = array();

        $agents = $this->chatModel->onlineAgents();
        $visitors = $this->chatModel->onlineVisitors();

        $json['visitors'] = '';

        foreach ($visitors as $row) {
            $json['visitors'] .= '<li>';
            $json['visitors'] .= '<a onclick="start_chat(' . $row['id'] . ');" rel="tooltip" data-toggle="tooltip"';
            $json['visitors'] .= '       data-html="true" data-title="';
            $json['visitors'] .= '<ul class=list-unstyled>';
            $json['visitors'] .= '<li>';
            $json['visitors'] .= ' <b>Email:</b>' . $row['email'];
            $json['visitors'] .= '</li>';
            $json['visitors'] .= '<li>';
            $json['visitors'] .= ' <b>Telephone: </b>' . $row['telephone'];
            $json['visitors'] .= '</li>';

            if ($row['chat_response'] == 2) {

                $json['visitors'] .= '        <li>';
                $json['visitors'] .= '            <b>Response</b>';
                $json['visitors'] .= "            <i class='fa fa-thumbs-down'></i>";
                $json['visitors'] .= '        </li>';
            } else if ($row['chat_response'] == 1) {

                $json['visitors'] .= '        <li>';
                $json['visitors'] .= '            <b>Response</b>';
                $json['visitors'] .= "            <i class='fa fa-thumbs-up'></i>";
                $json['visitors'] .= '        </li>';
            }

            $json['visitors'] .= '        <li>';
            $json['visitors'] .= '            <b>Active chat:</b>' . $row['total_active_chat'];
            $json['visitors'] .= '        </li>';
            $json['visitors'] .= '        </ul>">';
            $json['visitors'] .= $row['name'];
            $json['visitors'] .= '    </a>';
            $json['visitors'] .= '</li>';
        }

        $json['agents'] = '';

        foreach ($agents as $row) {
            $json['agents'] .= '<li>';
            $json['agents'] .= '<a onclick="start_chat(' . $row['id'] . ');" rel="tooltip" data-toggle="tooltip"';
            $json['agents'] .= '       data-html="true" data-title="';
            $json['agents'] .= '<ul class=list-unstyled>';
            $json['agents'] .= '<li>';
            $json['agents'] .= ' <b>Email:</b>' . $row['email'];
            $json['agents'] .= '</li>';
            $json['agents'] .= '<li>';
            $json['agents'] .= ' <b>Telephone: </b>' . $row['telephone'];
            $json['agents'] .= '</li>';

            if ($row['chat_response'] == 2) {

                $json['agents'] .= '        <li>';
                $json['agents'] .= '            <b>Response</b>';
                $json['agents'] .= "            <i class='fa fa-thumbs-down'></i>";
                $json['agents'] .= '        </li>';
            } else if ($row['chat_response'] == 1) {

                $json['agents'] .= '        <li>';
                $json['agents'] .= '            <b>Response</b>';
                $json['agents'] .= "            <i class='fa fa-thumbs-up'></i>";
                $json['agents'] .= '        </li>';
            }

            $json['agents'] .= '        <li>';
            $json['agents'] .= '            <b>Active chat:</b>' . $row['total_active_chat'];
            $json['agents'] .= '        </li>';
            $json['agents'] .= '        </ul>">';
            $json['agents'] .= $row['name'];
            $json['agents'] .= '    </a>';
            $json['agents'] .= '</li>';
        }

        echo json_encode($json);
    }

    public function visitorAdminAjax() {
        $json = array();

        $agents = $this->chatModel->onlineAgents();
        $visitors = $this->chatModel->onlineVisitors();

        $json['visitors'] = '';

        foreach ($visitors as $row) {
            $json['visitors'] .= '<tr>'
                    . '<td> '
                    . $row['name']
                    . '</td> '
                    . '<td>' . $row['email'] . '</td>'
                    . '<td>' . $row['telephone'] . '</td>'
                    . '<td>';

            if ($row['chat_response'] == 2) {
                $json['visitors'] .= ' <i class="fa fa-thumbs-down"></i>';
            } elseif ($row['chat_response'] == 1) {
                $json['visitors'] .= '  <i class="fa fa-thumbs-up"></i>';
            } else {
                $json['visitors'] .= '  -';
            }

            $json['visitors'] .= '</td>'
                    . ' <td>' . $row['total_active_chat'] . '</td>'
                    . '</tr>';
        }

        $json['agents'] = '';

        foreach ($agents as $row) {
            $json['agents'] .= '<tr> '
                    . '<td>' . $row['name'] . '</td>'
                    . '<td>' . $row['email'] . '</td>'
                    . '<td>' . $row['telephone'] . '</td>'
                    . '<td>';

            if ($row['chat_response'] == 2) {
                $json['agents'] .= '<i class="fa fa-thumbs-down"></i>';
            } elseif ($row['chat_response'] == 1) {
                $json['agents'] .= '<i class="fa fa-thumbs-up"></i>';
            } else {
                $json['agents'] .= '-';
            }

            $json['agents'] .= '</td>'
                    . '<td>' . $row['total_active_chat'] . '</td>'
                    . '</tr>';
        }

        echo json_encode($json);
    }

    public function visitor() {
        $data['agents'] = $this->chatModel->onlineAgents();
        $data['visitors'] = $this->chatModel->onlineVisitors();

        $theme = $this->session->userdata('theme');

        if (userdata('department') == 1) {
            $template = 'visitor_admin';
        } else {
            $template = 'visitor';
        }

        if (file_exists(
                        APPPATH . 'views/themes/' . $theme . '/template/chat/' . $template)
        ) {
            $this->load->view('themes/' . $theme . '/template/chat/' . $template, $data);
        } else {
            $this->load->view('themes/default/template/chat/' . $template, $data);
        }
    }

    public function leaveChatroom($receiver_id) {
        $this->chatModel->leaveChatroom($receiver_id);
    }

    //  Chat details
    public function detail() {
        $user_id = $this->session->userdata('user_id');

        if (!$user_id) {
            redirect('/');
        }

        $chat_room_id = $this->input->get('chat_room_id', true);

        $page = $this->input->get('page', true) ? $this->input->get('page', true) : 1;

        $base_url = base_url('chat/detail?chat_room_id=' . $chat_room_id);

        //pagination
        $limit = config('tickets_per_page');
        $offset = ($page - 1) * $limit;
        $this->load->library('pagination');
        $config['base_url'] = $base_url;
        $config['total_rows'] = $this->chatModel->countChatDetail(
                $chat_room_id
        );
        $config['per_page'] = config('tickets_per_page');
        $config['next_link'] = $this->lang->line('next') . ' &rarr;';
        $config['page_query_string'] = true;
        $config['prev_link'] = '&larr; ' . $this->lang->line('previous');
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();

        $data['rows'] = $this->chatModel->chatDetail(
                $chat_room_id, $limit, $offset
        );

        $theme = $this->session->userdata('theme');

        if (file_exists(
                        APPPATH . 'views/themes/' . $theme . '/template/chat/detail')
        ) {
            $this->load->view('themes/' . $theme . '/template/chat/detail', $data);
        } else {
            $this->load->view('themes/default/template/chat/detail', $data);
        }
    }

    public function userAutocomplete($q) {
        echo json_encode($this->chatModel->userAutocomplete($q));
    }

    public function customerAutocomplete($q) {
        echo json_encode($this->chatModel->customerAutocomplete($q));
    }

    public function agentAutocomplete($q) {
        echo json_encode($this->chatModel->agentAutocomplete($q));
    }

    public function loadHistory() {
        $this->chatModel->loadHistoty();
    }

    public function checkNotification() {
        $user_id = $this->session->userdata('user_id');

        if ($user_id) {
            $data = $this->chatModel->checkNotification();
            $data['html'] = $this->load->view('chat_notification.tpl', $data, true);
            echo json_encode($data);
        }else{
            echo json_encode(array('status' => 0));
        }
    }

    public function updateTypingMomment() {
        $user_id = $this->session->userdata('user_id');

        if (!$user_id) {
            $json = array(
                'status' => 0,
                'redirect' => site_url('user/login'),
            );
        } else {
            $json = array(
                'status' => 1,
            );
            $this->chatModel->updateTypingMomment();
        }

        echo json_encode($json);
    }

    public function setMsgNotified() {
        $this->chatModel->setMsgNotified($this->input->post('msg_id'));

        $filter = array(
            'from_id' => $this->input->post('receiver_id'),
            'to_id' => $this->session->userdata('user_id'),
            'is_notified' => 0,
        );

        //total new messages
        $total_new_message = $this->db->where($filter)
                ->count_all_results('user_messages');

        echo json_encode(array('total_new_msg' => $total_new_message));
    }

    public function setNotified() {
        $this->chatModel->setNotified();
    }

    public function sendMessage() {
        $data['html'] = $this->chatModel->sendMessage();
        echo json_encode($data);
    }

    public function checkMessage() {
        $data = $this->chatModel->checkMessage();
        $data['html'] = $this->load->view('chat_messages.tpl', $data, true);
        echo json_encode($data);
    }

    //save chat feedback
    public function response($response, $agent_id) {
        $user_id = $this->session->userdata('user_id');

        //remove old data
        $this->db->delete(
                'user_rating', array('customer_id' => $user_id, 'agent_id' => $agent_id)
        );

        $this->db->insert(
                'user_rating', array(
            'customer_id' => $user_id,
            'agent_id' => $agent_id,
            'rating' => $response,
                )
        );
    }

    public function chatMinimize($id) {
        $data = array(
            'receiver_id' => $id,
            'sender_id' => $this->session->userdata('user_id'),
        );

        //check if exist then remove else add
        $this->db->where($data);
        $is_found = $this->db->get('chat_minimize')->row_array();

        if (!$is_found) {
            $this->db->insert('chat_minimize', $data);
        } else {
            $this->db->where($data);
            $this->db->delete('chat_minimize');
        }
    }

    public function startOfflineChat2($receiver_id) {
        $data = $this->chatModel->startOfflineChat();

        //remove minimized
        $where = array(
            'receiver_id' => $receiver_id,
            'sender_id' => $this->session->userdata('user_id'),
        );
        $this->db->where($where);
        $this->db->delete('chat_minimize');
        $data['is_minimize'] = 0;
        $data['status'] = 1;
        $data['html'] = $this->load->view('chat.tpl', $data, true);

        echo json_encode($data);
    }

    public function startOfflineChat() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules(
                'name', $this->lang->line('name'), 'required'
        );

        $this->form_validation->set_rules(
                'email', $this->lang->line('email'), 'required|valid_email'
        );

        if ($this->form_validation->run() == false) {
            $data['status'] = 0;

            $data['msg'] = sprintf(
                    $this->lang->line('msg_danger'), validation_errors()
            );
        } else {
            $data = $this->chatModel->startOfflineChat();

            //remove minimized
            $where = array(
                'receiver_id' => $data['receiver']['user_id'],
                'sender_id' => $this->session->userdata('user_id'),
            );
            $this->db->where($where);
            $this->db->delete('chat_minimize');
            $data['is_minimize'] = 0;
            $data['status'] = 1;
            $data['html'] = $this->load->view('chat.tpl', $data, true);
        }
        echo json_encode($data);
    }

    // add entry to chatroom if not already
    public function enterChatroom($receiver_id) {
        $this->chatModel->enterChatroom($receiver_id);
    }

    public function startChat($receiver_id) {
        $data = $this->chatModel->startChat($receiver_id);

        //check if exist then remove else add
        $where = array(
            'receiver_id' => $data['receiver']['user_id'],
            'sender_id' => $this->session->userdata('user_id'),
        );
        $this->db->where($where);

        $data['is_minimize'] = $this->db->get('chat_minimize')->num_rows();

        if (userdata('department') == 0) {
            $data['html'] = $this->load->view('chat.tpl', $data, true);
        } else {
            $data['html'] = $this->load->view('chat_agent.tpl', $data, true);
        }

        echo json_encode($data);
    }

    public function iAmOnline($total_active_chat) {
        $user_id = $this->session->userdata('user_id');

        if (!$user_id) {
            $json = array(
                'status' => 0,
                'redirect' => site_url('user/login'),
            );
        } else {
            $this->db->where('id', $user_id);
            $this->db->set('last_activity', 'NOW()', false);
            $this->db->set('total_active_chat', $total_active_chat);
            $this->db->update('users');

            $json = array(
                'status' => 1,
            );
        }

        echo json_encode($json);
    }

    public function getAgentId() {
        $agent_id = $this->chatModel->getAgentId();

        //assign agent to customer 
        $this->db->update(
                'users', array('agent_id' => $agent_id), array('id' => $this->session->userdata('user_id'))
        );

        echo $agent_id;
    }

    public function getChatIcon() {
        $user_id = $this->session->userdata('user_id');

        if ($user_id) {
            echo $this->chatModel->getChatIcon();
        }
    }

    public function chatTranscript($receiver_id = 0) {
        $this->form_validation->set_rules(
                'email', $this->lang->line('email'), 'required'
        );

        $this->form_validation->set_rules(
                'receiver_id', $this->lang->line('receiver'), 'required'
        );

        /* receiver_id
         * email
         */
        if ($this->form_validation->run() == false) {
            $where = array(
                'department' => 0,
                'id' => $this->session->userdata('user_id'),
            );

            $this->db->where($where);

            $data['customer'] = $this->db->get('users')->row_array();
            $data['receiver_id'] = $receiver_id;

            $this->load->view('chat_transcript.tpl', $data);
        } else {

            $this->chatModel->mailTranscript();

            $response = array(
                'status' => 1,
                'msg' => $this->lang->line('transcript_mailed_succesfull'),
            );

            echo json_encode($response);
        }
    }

    public function chatInfo($customer_id = 0) {
        $this->form_validation->set_rules(
                'id', $this->lang->line('id'), 'required'
        );

        $this->form_validation->set_rules(
                'name', $this->lang->line('name'), 'required'
        );

        $this->form_validation->set_rules(
                'email', $this->lang->line('email'), 'valid_email|required'
        );

        $this->form_validation->set_rules(
                'telephone', $this->lang->line('telephone'), 'numeric'
        );

        /* name
         * telephone 
         * details 
         * 
         */
        if ($this->form_validation->run('chatInfo') == false) {
            $where = array(
                'department' => 0,
                'id' => $customer_id,
            );
            $this->db->where($where);

            $data['customer'] = $this->db->get('users')->row_array();

            $this->load->view('chat_info.tpl', $data);
        } else {
            $department = $this->session->userdata('department');

            //if i am customer
            //override my customer id for security 
            if ($department == 0) {
                $customer_id = $this->session->userdata('user_id');
            } else {
                $customer_id = $this->input->post('id', true);
            }

            $this->chatModel->updateChatInfo($customer_id);

            $response = array(
                'status' => 1,
                'msg' => $this->lang->line('update_succesfull'),
            );

            if ($department == 0) {
                $name = $this->input->post('name', true);

                $this->session->set_userdata('name', $name); //update user name
                $response['name'] = $name;
            }

            echo json_encode($response);
        }
    }

    public function processChatInfo() {
        $this->form_validation->set_rules(
                'id', $this->lang->line('id'), 'required'
        );

        $this->form_validation->set_rules('name', $this->lang->line('name'), 'required'
        );

        $this->form_validation->set_rules(
                'email', $this->lang->line('email'), 'valid_email|required'
        );

        $this->form_validation->set_rules(
                'telephone', $this->lang->line('telephone'), 'numeric'
        );

        if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 0,
                'msg' => validation_errors(),
            );
        } else {
            $department = $this->session->userdata('department');

            //if i am customer
            //override my customer id for security 
            if ($department == 0) {
                $customer_id = $this->session->userdata('user_id');

                //refresh if email change
                $new_email = $this->input->post('email', true);

                //old email 
                $this->db->where(array('id' => $customer_id));
                $old_email = $this->db->get('users')->row()->email;
            } else {
                $new_email = '';
                $old_email = '';
                $customer_id = $this->input->post('id', true);
            }

            $this->chatModel->updateChatInfo($customer_id);

            $response = array(
                'status' => 1,
                'msg' => $this->lang->line('update_succesfull'),
            );

            if ($new_email != $old_email) {
                $response['redirect'] = site_url('user/login');
                $this->session->unset_userdata('isLogin');
                $this->session->unset_userdata('user_id');
            }

            if ($department == 0) {
                $name = $this->input->post('name', true);

                $this->session->set_userdata('name', $name); //update user name
                $response['name'] = $name;
            }
        }

        echo json_encode($response);
    }

    public function chatAssign($customer_id = 0) {
        $this->load->library('form_validation');

        $this->form_validation->set_rules(
                'agent_id', $this->lang->line('agent'), 'required|trim'
        );

        if ($this->form_validation->run() == false) {

            $data['customer_id'] = $customer_id;

            //departments
            $data['departments'] = $this->db->get('departments')
                    ->result_array();

            //agents 
            $this->db->where('department!=0', null, false);
            $data['agents'] = $this->db->get('users')->result_array();

            $this->load->view('chat_assign.tpl', $data);
        } else {

            $agent_name = $this->chatModel->chatAssign();

            $response = array(
                'agent_name' => $agent_name,
                'status' => 1,
                'msg' => $this->lang->line('agent_assign_success'),
            );

            echo json_encode($response);
        }
    }

    //remove chat ban
    public function chatStart() {
        $this->db->set('is_chat_banned', 0);
        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('users');
        $this->session->set_userdata('is_chat_banned', 0);
    }

    public function chatBan() {
        $this->db->set('is_chat_banned', 1);
        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('users');
        $this->session->set_userdata('is_chat_banned', 1);
    }

    public function filterAgents() {
        echo $this->chatModel->filterAgents();
    }

    /*
     *  Set timezone offset to display message time in client side timezone 
     */

    public function timezone() {
        $offset = $this->input->get('value');

        // convert +360 minutes to +5:30
        if ($offset) {

            $hours = (int) (abs($offset) / 60);

            $minutes = abs($offset) - $hours * 60;

            //convert to +5:30
            if (substr($offset, 0, 1) == '+') {
                $value = '-' . $hours . ':' . $minutes;
            } else {
                $value = '+' . $hours . ':' . $minutes;
            }

            $this->session->set_userdata('timezone', $value);
        }
    }

    public function playSound($flag) {
        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('users', array('play_sound' => $flag));
    }

    public function agentOfflineAlert() {

        $theme = $this->session->userdata('theme');

        if (file_exists(
                        APPPATH . 'views/themes/' . $theme . '/template/chat/agent_offline_alert')
        ) {
            $this->load->view('themes/' . $theme . '/template/chat/agent_offline_alert');
        } else {
            $this->load->view('themes/default/template/chat/agent_offline_alert');
        }
    }

    public function refreshChat() {
        $this->chatModel->refreshChat();
    }

}
