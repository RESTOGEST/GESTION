<?php 
    
    if($is_online){ 
        $class = 'online';
    }else{
        $class = 'offline';
    }
    
    $last_msg_id = 0;
    
?>

<div id="live-chat-ui-<?= $receiver['user_id'] ?>" class="live-chat-ui <?= $class ?> <?php if($is_minimize) echo 'minimized'; ?>">
    <header class="clearfix" data-id="<?= $receiver['user_id'] ?>">

            <a href="#" class="chat-close btn btn-xs btn-danger" data-toggle="tooltip" data-title="<?= $this->lang->line('close') ?>">
                <i class="fa fa-times"></i>
            </a>

            <?php 
            
            $isLogin = $this->session->userdata('isLogin');
            $user_id = $this->session->userdata('user_id'); 
            $name = $this->session->userdata('name'); 
            
            if($isLogin){ ?>
            
            <?php if(userdata('department') != 0){ ?>

                <!--
                 login user is admin 
                 receiver is customer 
                -->     
                <a href="#" data-id="<?= $receiver['user_id'] ?>" class="chat-info btn btn-xs bg-green" data-toggle="tooltip" data-title="<?= $this->lang->line('details') ?>">
                    <i class="fa fa-user"></i>
                </a>
            
                <a class="chat-exchange btn btn-xs bg-yellow" data-toggle="tooltip" data-title="<?= $this->lang->line('transfer_chat') ?>" data-id="<?= $receiver['user_id'] ?>">
                   <i class="fa fa-exchange"></i>
                </a>            
            <?php } else { ?>            
            
                <!--
                 receiver is admin 
                 login user is customer 
                -->     
                <a href="#" data-id="<?= $this->session->userdata('user_id') ?>" class="chat-info btn btn-xs bg-green" data-toggle="tooltip" data-title="<?= $this->lang->line('details') ?>">
                    <i class="fa fa-user"></i>
                </a>
            
                <a class="chat_transcript btn btn-xs bg-yellow" data-toggle="tooltip" data-title="<?= $this->lang->line('mail_transcript') ?>" data-id="<?= $receiver['user_id'] ?>">
                   <i class="fa fa-envelope"></i>
                </a>
            <?php } ?>
            
            <?php } ?>
            
            <h4><?= $receiver['name']; ?>&nbsp;
            
                <?php if($is_online){ ?>
                    <span class="badge badge-success"><?= $this->lang->line('online') ?></span>
                <?php }else{ ?>
                    <span class="badge badge-warning"><?= $this->lang->line('offline') ?></span>
                <?php } ?>
            </h4> 
            
            <span class="chat-message-counter" style="display: <?= $total_new_msg?'inline':'none' ?>;">
                <?= $total_new_msg ?>
            </span>
            
    </header>
    <div class="chat <?php if($is_minimize) echo 'hide'; ?>">
        
            <p style="display: none;" class="chat-alert">
                <?= $this->lang->line('new_message') ?>
                <i class="fa fa-angle-down"></i>
            </p>
        
            <div class="chat-history">     
                
                <?php if($messages) { ?>
                <div class="load_more_wrapper">
                    <a class="label label-info" data-receiver-id="<?= $receiver['user_id'] ?>">
                        <?php echo $this->lang->line('load_history') ?>
                    </a>
                </div>
                <?php } ?>
                
                <?php if(!$messages && $is_online && userdata('department') == 0){ ?>
                <div class="chat-message clearfix">                    
                    <?php if($receiver['photo']){ ?>
                        <img src="<?= base_url().'images/users/thumbs/'.$receiver['photo'] ?>" alt="" style="width: 37px;height:37px;">
                    <?php }else{ ?>
                        <img src="images/customer-user.png" alt="" style="width: 37px;height:37px;">
                    <?php } ?>
                    <div class="chat-message-content clearfix">
                        <!--<span><?= $this->lang->line('0_seconds_ago') ?></span>-->
                        <span class="chat-time" data-livestamp="<?= time() ?>"></span> 
                        <h5><?= $receiver['name'] ?></h5>
                        <p><?= $this->lang->line('chat_help_text') ?></p>                        
                    </div> <!-- end chat-message-content -->
                </div> <!-- end chat-message -->
                <!-- <hr class="separator" /> -->
                <?php } else if(!$messages && !$is_online && userdata('department') == 0){ ?>
                    <br />
                    <p class='alert alert-info' style="text-align: center;">
                        No agent(s) online,<br />
                        you can leave message,<br /> 
                        we will contact you ASAP
                        <div class="clearfix"></div>
                    </p>
                <?php } ?>

                <?php foreach($messages as $msg){
                
                      $last_msg_id = $msg['msg_id'];
                      
                ?>
                <div class="chat-message clearfix" data-id="<?= $msg['msg_id'] ?>">                    
                    <img src="images/customer-user.png" alt="" style="width: 37px;height:37px;">                    
                    <div class="chat-message-content clearfix">
                        <span class="chat-time" data-livestamp="<?= $msg['timestamp'] ?>"></span>
                        <?php if($msg['from_id'] == $user_id){ // i am sender ?>
                        <h5><?= $name ?></h5>
                        <?php }else{ ?>
                        <h5><?= $receiver['name'] ?></h5>
                        <?php } ?> 
                        <p><?= $msg['msg'] ?></p>
                    </div> <!-- end chat-message-content -->
                </div> <!-- end chat-message -->
                <!--  <hr class="separator" /> -->
                <?php } ?>
            </div> <!-- end chat-history -->
            
            
            <p style="display: none;" class="chat-feedback"><?= $receiver['name'] ?> <?= $this->lang->line('partner_typing_text') ?></p>
            
            <?php echo form_open('', 'class="send_message" method="post" onsubmit="return false;"'); ?>
            
                <div class="like_unlike_wrapper btn-group">
                    <div data-agent="<?= $receiver['user_id'] ?>" data-response='1' class="like btn btn-sm <?php if($chat_response == 1) echo 'active'; ?>">
                        <i class="fa fa-thumbs-up"></i>
                        <?= $this->lang->line('chat_like_text') ?>
                    </div>
                    <div data-agent="<?= $receiver['user_id'] ?>" data-response='2' class="unlike btn btn-sm pull-right <?php if($chat_response == 2) echo 'active'; ?>">
                        <i class="fa fa-thumbs-down"></i>
                        <?= $this->lang->line('chat_unlike_text') ?>
                    </div>
                </div>
                
                <div class="input-icon right">	
                    <span class="fa fa-share"></span>
                    <input name="msg" type="text" class="form-control" placeholder="<?= $this->lang->line('type_message') ?>" autocomplete="off">
                    <input type="hidden" name="chat_room_id" value="<?= $chat_room_id ?>" />
                    <input type="hidden" name="receivername" value="<?= $receiver['name'] ?>" />
                    <input type="hidden" name="receiver_id" value="<?= $receiver['user_id'] ?>" />
                    <input type="hidden" value="<?= $last_msg_id ?>" name="last_msg_id" />
                </div>
            </form>
    </div> <!-- end chat -->
</div> <!-- end live-chat -->	
