<?php foreach($rows as $msg){ ?>
  <li><!-- start message -->
    <a class="start_chat" data-id="<?= $msg['msg_id'] ?>" data-user-id="<?= $msg['user_id'] ?>">
      <div class="pull-left">
        <?php if($msg['photo']){ ?>
            <img alt="" src="<?= base_url().'images/users/thumbs/'.$msg['photo'] ?>" class="img-circle" />
        <?php }else{ ?>
            <img alt="" src="<?= base_url().'images/customer-user.png' ?>" class="img-circle" />   
        <?php } ?>
      </div>
      <h4>
          
        <?= $msg['name'] ?>
        
        <span style="color: #30BBBB;">(<?= $msg['count'] ?>)</span>
        
        <small><i class="fa fa-clock-o"></i>
            <span><?= date('d M, Y h:m a', strtotime($msg['timestamp'])) ?></span> <!-- data-livestamp=" -->
        </small>
      </h4>
      <p><?= $msg['msg'] ?></p>
    </a>
  </li><!-- end message -->
<?php } ?>