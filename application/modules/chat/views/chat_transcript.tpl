<div class="modal-dialog">
<div class="modal-content" id="chat_model">
    <div class="modal-header">
        <a data-dismiss="modal" class="close">×</a>
        <h4 class="modal-title"><?= $this->lang->line('mail_transcript') ?></h4>
    </div>
    <div class="modal-body">
        
        <div class="alert alert-success hide"></div>
        
        <?php echo form_open('', 'id="chat_transcript_form"'); ?>
            
            <input type="hidden" name="receiver_id" value="<?= $receiver_id ?>" />
            
            <!-- Telephone -->
            <div class="form-group">
                <label class="control-label"><?= $this->lang->line('email') ?></label>
                <input name="email" class="form-control" value="<?= $customer['email'] ?>" />
            </div>
            
        </form>                    
    </div>
    <div class="modal-footer">
        <input type="button" class="btn btn-chat-transcript btn-primary" value="<?= $this->lang->line('submit') ?>" />
        <a class="btn-remove-model btn btn-default"><?= $this->lang->line('cancel') ?></a>
    </div>
</div>
</div>