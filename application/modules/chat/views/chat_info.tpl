<div class="modal-dialog">
<div class="modal-content" id="chat_model">
    <div class="modal-header">
        <a data-dismiss="modal" class="close">×</a>
        <h4 class="modal-title"><?= $this->lang->line('information') ?></h4>
    </div>
    <div class="modal-body">
        
        <div class="alert alert-success hide"></div>
        
        <?php echo form_open('', 'id="chat_info_form"'); ?>
            
            <input type="hidden" name="id" value="<?= $customer['id'] ?>" />
            
            <!-- Telephone -->
            <div class="form-group">
                <label class="control-label"><?= $this->lang->line('email') ?></label>
                <input name="email" class="form-control" value="<?= $customer['email'] ?>" />
            </div>
            
            <!-- Telephone -->
            <div class="form-group">
                <label class="control-label"><?= $this->lang->line('name') ?></label>
                <input name="name" class="form-control" value="<?= $customer['name'] ?>" />
            </div>
            
            <!-- Telephone -->
            <div class="form-group">
                <label class="control-label"><?= $this->lang->line('telephone') ?></label>
                <input name="telephone" class="form-control" value="<?= $customer['telephone'] ?>" />
            </div>
            
            <div class="form-group">
                <label class="control-label"><?= $this->lang->line('details') ?></label>
                <textarea name="details" id="details"><?= $customer['details'] ?></textarea>
            </div>
            
        </form>                    
    </div>
    <div class="modal-footer">
        <input type="button" class="btn btn-chat-info btn-primary" value="<?= $this->lang->line('submit') ?>" />
        <a class="btn-remove-model btn btn-default"><?= $this->lang->line('cancel') ?></a>
    </div>
</div>
</div>

<script src="<?= site_url('application/third_party/ckeditor/ckeditor.js') ?>"></script>
<script src="<?= site_url('application/third_party/ckeditor/adapters/jquery.js') ?>"></script>

<script>
      
//class="ckeditor"
      
$(function(){    
    $('#details').ckeditor();
});    
    
</script>
    

<script>
  
//update chat info 
$(document).delegate('.btn-chat-info', 'click', function() {
    $.post('chat/processChatInfo',
    {
        'details':CKEDITOR.instances['details'].getData(), //$('#chat_info_form').serialize()
        'id' : $('input[name="id"]').val(),
        'csrf_a1support' : $('input[name="csrf_a1support"]').val(),
        'email' : $('input[name="email"]').val(),
        'name' : $('input[name="name"]').val(),
        'telephone' : $('input[name="telephone"]').val(),         
    }, function(data) {
        var data = JSON.parse(data);

        if(data.redirect){
            location = data.redirect;
        }
        
        //update name
        if(data.name){
           $('.username').html(data.name); 
        }

        //$('#chat_model .close').trigger('click');

        $button = '<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>';

        if (data.status) {
             $('#chat_model .alert').attr('class', 'alert alert-success').html($button + data.msg);
        } else {
             $('#chat_model .alert').attr('class', 'alert alert-danger').html($button + data.msg);
        }
    });
    return false;
});
</script>