<?php foreach($messages as $msg){ ?>
<div class="chat-message clearfix" data-id="<?= $msg['msg_id'] ?>">
    <?php if($receiver['photo']){ ?>
        <img src="<?= base_url().'images/users/thumbs/'.$receiver['photo'] ?>" alt="" style="width: 37px;height:37px;">
    <?php }else{ ?>    
        <img src="images/customer-user.png" alt="" style="width: 37px;height:37px;">
    <?php } ?>
    <div class="chat-message-content clearfix">
        <!--<span class="chat-time"><?= format_time() ?></span>-->
        <span class="chat-time" data-livestamp="<?= $msg['timestamp'] ?>"></span> 
        <h5><?= $receiver['name'] ?></h5>
        <p><?= $msg['msg'] ?></p>
    </div> <!-- end chat-message-content -->
</div> <!-- end chat-message -->
<!-- <hr class="separator" /> -->
<?php } ?>