<div class="modal-dialog">
<div class="modal-content" id="agent_model">
    <div class="modal-header">
        <a data-dismiss="modal" class="close">×</a>
        <h4 class="modal-title"><?= $this->lang->line('assign_agent') ?></h4>
    </div>
    <div class="modal-body">
        
        <div class="alert alert-success hide"></div>
        
        <?php echo form_open('', 'id="agent_form"'); ?>
        
            <input type="hidden" name="customer_id" value="<?= $customer_id ?>" />
            
            <!-- All Departments -->
            <div class="control-group">
                <label class="control-label"><?= $this->lang->line('department') ?></label>
                <select name="department_id" class="form-control">
                    <option value=""><?= $this->lang->line('all') ?></option>
                    <?php foreach($departments as $department){ ?>
                    <option value="<?= $department['id'] ?>"><?= $department['department_name'] ?></option>
                    <?php } ?>
                </select>
                <p></p>
            </div>
            
            <!-- Type -->
            <div class="control-group">
                <label class="control-label"><?= $this->lang->line('type') ?></label>
                <select name="type" class="form-control">
                    <option value=""><?= $this->lang->line('all') ?></option>
                    <option value="online"><?= $this->lang->line('online') ?></option>
                    <option value="offline"><?= $this->lang->line('offline') ?></option>
                </select>
                <p class="help-block">
                    <?= $this->lang->line('assign_agent_help_text') ?>
                </p>
            </div>
            
            <!-- User list -->
            <div class="control-group">
                <label class="control-label"><?= $this->lang->line('agent') ?></label>
                <select name="agent_id" class="form-control">
                    <!--
                    <option value=""><?= $this->lang->line('all') ?></option>
                    -->
                    <?php foreach($agents as $agent){ ?>
                    <option value="<?= $agent['id'] ?>"><?= $agent['name'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </form>                    
    </div>
    <div class="modal-footer">
        <input type="button" class="btn btn-assign-agent btn-primary" value="<?= $this->lang->line('assign') ?>" />
        <a class="btn-remove-model btn btn-default"><?= $this->lang->line('cancel') ?></a>
    </div>
</div>
</div>