<?php

class AuthorizeModel extends CI_Model
{
    public function addOrder($customer_id, $course_id, $amount)
    {
        $mode = $this->session->userdata('checkout_mode');

        if ($mode == 'gift') {
            //update gift status
            $gift_id = $this->session->userdata('gift_id');
            $this->db->set('status', 1);
            $this->db->where('gift_id', $gift_id);
            $this->db->update('course_gifts');
        } else {

            //add order
            $data = array(
                'customer_id' => $customer_id,
                'course_id' => $course_id,
            );
            $this->db->insert('customer_to_course', $data);
        }

        //save history
        $instructor_id = $this->db->get_where('instructor_to_course',
                array('course_id' => $course_id))->row()->instructor_id;
        $commision = $this->setting_model->get('commision');
        $commision_amount = $amount * ($commision / 100);

        $data = array(
            'course_id' => $course_id,
            'customer_id' => $customer_id,
            'instructor_id' => $instructor_id,
            'amount' => $amount,
            'commision' => $commision,
            'instructor_amount' => $amount - $commision_amount,
            'commision_amount' => $commision_amount,
            'date' => date('Y-m-d'),
        );
        $this->db->insert('transaction_history', $data);

        //save customer credit
        $this->db->query('UPDATE `customers` SET `credit_balance` = '
            .'credit_balance + '.$data['instructor_amount'].' '
            ."WHERE `customer_id` = '".$instructor_id."'");
    }
}
