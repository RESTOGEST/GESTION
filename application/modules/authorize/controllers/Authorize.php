<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Authorize extends MX_Controller
{
    public function pay()
    {
        $service_id = $this->session->userdata('service_id');
        $user_id = $this->session->userdata('user_id');

        //get user
        $user = $this->db->get_where(
            'users',
            array('id' => $user_id)
         )->row_array();

        $this->session->set_userdata('comment', $this->input->post('comment', true));

        //price
        $price = $this->db->get_where(
            'services',
            array('service_id' => $service_id)
        )->row()->price;

        if ($price <= 0) {
            $price = $this->input->post('price', true);
        }

        //load third party plugin
        $this->load->plugin('AuthorizeNet');

        //access
        $login_id = config('auth_login_id');
        $trans_key = config('auth_trans_key');
        $is_testing = config('authorize_test');

        define('AUTHORIZENET_API_LOGIN_ID', $login_id);
        define('AUTHORIZENET_TRANSACTION_KEY', $trans_key);
        define('AUTHORIZENET_SANDBOX', $is_testing);
        define('AUTHORIZENET_MD5_SETTING', '');

        $transaction = new AuthorizeNetAIM();

        $auth_data = array(
            'amount' => round($price, 2),
            'card_num' => $this->input->post('cardNumber', true),
            'exp_date' => $this->input->post('expirationDate', true),
            'first_name' => $user['name'],
            'last_name' => '',
            'address' => '',
            'city' => '',
            'state' => '',
            'country' => '',
            'zip' => '',
            'email' => $user['email'],
            'card_code' => $this->input->post('securityCode', true),
        );

        $transaction->setFields($auth_data);

        $transaction->VERIFY_PEER = false;

        $response = $transaction->authorizeAndCapture();

        if ($response->approved) {

            //success action
            $data = array(
                'user_id' => $user_id,
                'service_id' => $service_id,
                'amount' => round($price, 2),
                'date_added' => date('Y-m-d'),
                'comment' => $this->session->userdata('comment'),
            );
            $this->db->insert('user_to_service', $data);

            //unset data
            $this->session->unset_userdata('comment');

            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('payment_success')
            );

            $this->session->set_flashdata('message', $msg);

            $return = array(
                          'status' => 1,
                          'msg' => $this->lang->line('payment_success'),
                      );

            //success msg
            echo json_encode($return);
        } else {

            $return = array(
                'status' => 0,
                'msg' => 'Error: '.$response->error_message,
            );

            //error msg
            echo json_encode($return);
        }
    }
}
