<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Features extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        checkLogin(); // session helper function
         
        $lang = $this->session->userdata('language');
        $this->lang->load('feature', $lang);
        $this->load->helper('time_format');
        $this->load->model('featureModel');
    }

    public function index()
    {
        $url = site_url('features/index?');

        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $data['sort'] = $this->input->get('sort', true);
        $data['order'] = $this->input->get('order', true);
        $data['title'] = $this->input->get('title', true);
        $data['product'] = $this->input->get('product', true);
        $data['status'] = $this->input->get('status', true);

        if (!$data['sort']) {
            $data['sort'] = 'title';
            $url .= '&sort=title';
        } else {
            $url .= '&sort='.$data['sort'];
        }

        if (!$data['order']) {
            $data['order'] = 'ASC';
            $url .= '&order=ASC';
        } else {
            $url .= '&order='.$data['order'];
        }

        if ($data['title']) {
            $url .= '&title='.$data['title'];
        }

        if ($data['product']) {
            $url .= '&product='.$data['product'];
        }

        if ($data['status']) {
            $url .= '&status='.$data['status'];
        }

        $this->load->helper('text');

        $filter_data = array(
            'sort' => $data['sort'],
            'order' => $data['order'],
            'title' => $this->input->get('title', true),
            'product' => $this->input->get('product', true),
            'status' => $this->input->get('status', true),
        );

        //pagination
        $limit = 5;
        $offset = ($page - 1) * $limit;
        $this->load->library('pagination');
        $config['base_url'] = $url;
        $config['total_rows'] = $this->featureModel->getTotal($filter_data);
        $config['per_page'] = $limit;
        $config['page_query_string'] = true;
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;

        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();
        $data['msg'] = $this->session->flashdata('msg');

        $data['my_votes'] = $this->featureModel->getMyVoted();

        $data['rows'] = $this->featureModel->get(
            false,
            $limit,
            $offset,
            $filter_data
        );

        $url = '';

        if ($data['order'] == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if ($data['title']) {
            $url .= '&title='.$data['title'];
        }

        if ($data['product']) {
            $url .= '&product='.$data['product'];
        }

        if ($data['status'] || ($data['status'] != '' && $data['status'] == 0)) {
            $url .= '&status='.$data['status'];
        }

        $data['sort_title'] = site_url('features/index?sort=f.title'.$url);
        $data['sort_product'] = site_url('features/index?sort=p.title'.$url);
        $data['sort_status'] = site_url('features/index?sort=f.status'.$url);
        $data['sort_votes'] = site_url('features/index?sort=f.votes'.$url);
        $data['sort_date_added'] =
            site_url('features/index?sort=f.date_added'.$url);

        $theme = $this->session->userdata('theme');

        if (file_exists(
                APPPATH.'views/themes/'.$theme.'/template/features/index'
            )) {
            $this->load->view(
                'themes/'.$theme.'/template/features/index',
                $data
            );
        } else {
            $this->load->view('themes/default/template/features/index', $data);
        }
    }

    public function addDiscussion()
    {
        $id = $this->featureModel->addDiscussion();

        $data['row'] = $this->featureModel->getDiscussion($id);

        $theme = $this->session->userdata('theme');

        if (file_exists(
               APPPATH.'views/themes/'.$theme.'/template/discussion/item'
            )) {
            $this->load->view(
                'themes/'.$theme.'/template/discussion/item',
                $data
            );
        } else {
            $this->load->view('themes/default/template/discussion/item', $data);
        }
    }

    public function discussion()
    {
        $feature_id = $this->input->get('feature_id', true);
        
        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $data['feature'] = $this->featureModel->getFeature($feature_id);

        if (empty($data['feature'])) {
            show_404();
        }

        //pagination
        $limit = 10;
        $offset = ($page - 1) * $limit;

        $this->load->library('pagination');

        $config['base_url'] = base_url(
                                  'features/discussion/?feature_id='.$feature_id
                              );

        $config['total_rows'] = $this->featureModel->getTotalDiscussions(
                                    $feature_id
                                );

        $config['per_page'] = $limit;
        $config['page_query_string'] = true;
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;

        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();
        $data['rows'] = $this->featureModel->getDiscussions(
            $feature_id,
            $offset,
            $limit
        );

        $data['user_id'] = $this->session->userdata('user_id');

        $theme = $this->session->userdata('theme');

        if (file_exists(
                APPPATH.'views/themes/'.$theme.'/template/features/discussion'
            )) {
            $this->load->view(
                'themes/'.$theme.'/template/discussion/index',
                $data
            );
        } else {
            $this->load->view(
                'themes/default/template/discussion/index',
                $data
            );
        }
    }

    public function insert()
    {
        $department = $this->session->userdata('department');
        if (!$department) {
            show_404();
        }

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules(
            'product',
            $this->lang->line('product'),
            ''
        );

        $this->form_validation->set_rules(
            'product_id',
            $this->lang->line('product'),
            'required'
        );

        $this->form_validation->set_rules(
            'title',
            $this->lang->line('title'),
            'required'
        );

        $this->form_validation->set_rules(
            'description',
            $this->lang->line('description'),
            'required'
        );

        if ($this->form_validation->run() == false) {

            $data['msg'] = validation_errors();
            
            $theme = $this->session->userdata('theme');

            if (file_exists(
                    APPPATH.'views/themes/'.$theme.'/template/features/insert')
                ) {
                $this->load->view(
                    'themes/'.$theme.'/template/features/insert',
                    $data
                );
            } else {
                $this->load->view(
                    'themes/default/template/features/insert',
                    $data
                );
            }
        } else {
            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('feature_insert_msg')
            );

            $this->session->set_flashdata('msg', $msg);
            redirect('features');
        }
    }

    public function view($id)
    {
        $data['row'] = $this->featureModel->get($id);
        
        $theme = $this->session->userdata('theme');

        if (file_exists(
                APPPATH.'views/themes/'.$theme.'/template/features/view'
            )) {
            $this->load->view(
                'themes/'.$theme.'/template/features/view',
                $data
            );
        } else {
            $this->load->view('themes/default/template/features/view', $data);
        }
    }

    public function productAutocomplete($q)
    {
        $this->db->like('title', $q);
        $result = $this->db->get_where('products')->result_array();
        echo json_encode($result);
    }

    public function edit($id)
    {
        $department = $this->session->userdata('department');
        if (!$department) {
            show_404();
        }

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules(
            'product',
            $this->lang->line('product'), 
            ''
        );
        
        $this->form_validation->set_rules(
            'product_id',
            $this->lang->line('product'),
            'required'
        );

        $this->form_validation->set_rules(
            'title',
            $this->lang->line('title'),
            'required'
        );
        
        $this->form_validation->set_rules(
            'description',
            $this->lang->line('description'),
            'required'
        );

        if ($this->form_validation->run() == false) {
            
            $data['msg'] = validation_errors();
            
            $data['row'] = $this->featureModel->get($id);

            $data['row']['product'] = $this->featureModel->getProductName(
                                          $data['row']['product_id']
                                      );

            $theme = $this->session->userdata('theme');

            if (file_exists(
                    APPPATH.'views/themes/'.$theme.'/template/features/edit'
                )) {
                $this->load->view(
                    'themes/'.$theme.'/template/features/edit',
                    $data
                );
            } else {
                $this->load->view(
                    'themes/default/template/features/edit',
                    $data
                );
            }
        } else {
            $this->featureModel->update($id);

            $msg = sprintf(
                $this->lang->line('msg_success'),
                $this->lang->line('feature_update_msg')
            );

            $this->session->set_flashdata('msg', $msg);

            redirect('features');
        }
    }

    public function vote($id)
    {
        $this->featureModel->vote($id);

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('feature_vote_msg')
        );

        $this->session->set_flashdata('msg', $msg);
        
        redirect('features');
    }

    public function complete($id)
    {
        $this->featureModel->complete($id);

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('feature_complete_msg')
        );

        $this->session->set_flashdata('msg', $msg);

        redirect('features');
    }

    public function incomplete($id)
    {
        $this->featureModel->incomplete($id);

        $msg = sprintf(
            $this->lang->line('msg_success'),
            $this->lang->line('feature_incomplete_msg')
        );

        $this->session->set_flashdata('msg', $msg);

        redirect('features');
    }

    public function delete($id)
    {
        $department = $this->session->userdata('department');
        
        if (!$department) {
            show_404();
        }

        $this->featureModel->delete($id);
    }
}
