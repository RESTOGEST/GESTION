<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class FeatureModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTotalDiscussions($feature_id)
    {
        $this->db->join('users u', 'u.id = d.user_id');
        $this->db->where(array('feature_id' => $feature_id));

        return $this->db->count_all_results('feature_discussion d');
    }

    public function addDiscussion()
    {
        $data = array(
            'user_id' => $this->session->userdata('user_id'),
            'comment' => $this->input->post('comment', true),
            'feature_id' => $this->input->post('feature_id', true),
        );
        $this->db->insert('feature_discussion', $data);

        return $this->db->insert_id();
    }

    public function getDiscussion($discussion_id)
    {
        $this->db->select('u.name as author, d.*');
        $this->db->join('users u', 'u.id = d.user_id');

        return $this->db->get_where('feature_discussion d',
                array('fd_id' => $discussion_id))->row_array();
    }

    public function getDiscussions($feature_id, $offset, $limit)
    {
        $this->db->select('u.name as author, d.*');
        $this->db->offset($offset);
        $this->db->limit($limit);
        $this->db->order_by('fd_id ASC');
        $this->db->join('users u', 'u.id = d.user_id');

        return $this->db->get_where('feature_discussion d',
                array('feature_id' => $feature_id))->result_array();
    }

    public function getProductName($product_id)
    {
        $row = $this->db->get_where('products',
                array('product_id' => $product_id))->row_array();
        if ($row) {
            return $row['title'];
        }
    }

    public function complete($id)
    {
        $this->db->where('feature_id', $id);
        $this->db->set('status', 1);
        $this->db->update('features');
    }

    public function incomplete($id)
    {
        $this->db->where('feature_id', $id);
        $this->db->set('status', 0);
        $this->db->update('features');
    }

    public function getTotal($filter_data = array())
    {
        if (!empty($filter_data['title'])) {
            $this->db->like('f.title', $filter_data['title']);
        }

        if (!empty($filter_data['product'])) {
            $this->db->like('p.title', $filter_data['product']);
        }

        if (!empty($filter_data['status'])) {
            $this->db->where('f.status', $filter_data['status']);
        }

        $this->db->join('products p', 'p.product_id = f.product_id');

        return $this->db->count_all_results('features f');
    }

    public function getMyVoted()
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $rows = $this->db->get('feature_vote_history')->result_array();
        $ids = array();
        foreach ($rows as $row) {
            $ids[] = $row['feature_id'];
        }

        return $ids;
    }

    public function vote($id)
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('feature_id', $id);
        $is_exist = $this->db->count_all_results('feature_vote_history');

        if (!$is_exist) {
            $data = array(
                'user_id' => $this->session->userdata('user_id'),
                'feature_id' => $id,
                'date_added' => date('Y-m-d'),
            );
            $this->db->insert('feature_vote_history', $data);

            $sql = 'update features set votes = votes + 1 where feature_id = '
                .$id;

            $this->db->query($sql);
        }
    }

    public function getFeature($id)
    {
        $this->db->select('p.title as product, f.*');
        $this->db->join('products p', 'p.product_id = f.product_id');

        return $this->db->get_where(
            'features f',
            array('feature_id' => $id)
        )->row_array();
    }

    public function get($id = 0, $limit = 10, $offset = 0, $filter_data = null)
    {
        if ($id) {
            return $this->db->get_where(
                'features',
                array('feature_id' => $id)
            )->row_array();
        } else {
            $this->db->select('f.*, p.title as product');

            if (!empty($filter_data['title'])) {
                $this->db->like('f.title', $filter_data['title']);
            }

            if (!empty($filter_data['product'])) {
                $this->db->like('p.title', $filter_data['product']);
            }

            if ($filter_data['status'] ||
                ($filter_data['status'] != '' && $filter_data['status'] == 0)) {
                $this->db->where('f.status', $filter_data['status']);
            }

            $arr_order = array(
                'DESC',
                'ASC',
            );

            $arr_sort = array(
                'f.title',
                'p.title',
                'f.status',
                'f.votes',
                'f.date_added',
            );

            if (in_array($filter_data['order'], $arr_order) &&
                in_array($filter_data['sort'], $arr_sort)
            ) {
                $sort_order = $filter_data['sort'].' '.$filter_data['order'];

                $this->db->order_by($sort_order);
            }

            $this->db->join('products p', 'p.product_id = f.product_id');

            return $this->db->get(
                    'features f', $limit, $offset
                )->result_array();
        }
    }

    public function insert()
    {
        $data = array(
            'user_id' => $this->session->userdata('user_id'),
            'product_id' => $this->input->post('product_id', true),
            'title' => $this->input->post('title', true),
            'description' => $this->input->post('description', true),
            'date_added' => date('Y-m-d'),
        );
        $this->db->insert('features', $data);
    }

    public function update($id)
    {
        $data = array(
            'title' => $this->input->post('title', true),
            'product_id' => $this->input->post('product_id', true),
            'description' => $this->input->post('description', true),
        );
        $this->db->where('feature_id', $id);
        $this->db->update('features', $data);
    }

    public function delete($id)
    {
        $this->db->delete('features', array('feature_id' => $id));
    }
}
