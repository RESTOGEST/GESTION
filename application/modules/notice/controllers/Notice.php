<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Notice extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (userdata('department') != 1) {
            die('Access Forbidden');
        }

        checkLogin(); // session helper function
         
        $lang = $this->session->userdata('notice');
        $this->lang->load('notice', $lang);
        $this->load->model('noticeModel');
    }

    public function index()
    {
        $page = $this->input->get('page', true);

        if (!$page) {
            $page = 1;
        }

        $q = $this->input->get('q', true);

        $this->load->helper('text');

        //pagination
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $this->load->library('pagination');
        $config['base_url'] = base_url('notice/index?q='.$q);
        $config['total_rows'] = $this->noticeModel->getTotal($q);
        $config['per_page'] = $limit;
        $config['page_query_string'] = true;
        $config['next_link'] = $this->lang->line('next').' &rarr;';
        $config['prev_link'] = '&larr; '.$this->lang->line('previous');
        $config['num_links'] = 5;
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();

        $data['rows'] = array();

        $rows = $this->noticeModel->get(false, $limit, $offset, $q);

        foreach ($rows as $row) {
            $row['users'] = $this->noticeModel->getUser($row['notice_id']);
            $data['rows'][] = $row;
        }

        $data['msg'] = $this->session->flashdata('msg');

        $theme = $this->session->userdata('theme');

        $template = '/template/notice/index';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    public function insert()
    {
        $department = $this->session->userdata('department');

        if (!$department) {
            show_404();
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('department_id', 'department_id', '');
        $this->form_validation->set_rules('user', 'user', '');
        $this->form_validation->set_rules('user_id', 'user_id', '');
        $this->form_validation->set_rules('content', 'Content', 'required');
        $this->form_validation->set_rules('sort_order', '', '');

        if ($this->form_validation->run() == false) {
            $data['departments'] = $this->noticeModel->getDepartments();
            $data['msg'] = validation_errors();

            if ($this->input->post('user_ids', true)) {
                
                $user_ids = $this->input->post('user_ids', true);

                $sql = 'select name, id from users WHERE id IN (';
                $sql .= implode(',', $user_ids).')';

                $data['users'] = $this->db->query($sql)->result_array();
            } else {
                $data['users'] = array();
            }

            $theme = $this->session->userdata('theme');

            $template = '/template/notice/insert';

            if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
                $this->load->view('themes/'.$theme.$template, $data);
            } else {
                $this->load->view('themes/default'.$template, $data);
            }
        } else {
            $this->noticeModel->insert();

            $msg = sprintf(
                $this->lang->line('msg_success'),
                'Success: Notice Inserted Successfully!'
            );

            $this->session->set_flashdata('msg', $msg);

            redirect('notice');
        }
    }

    public function view($id)
    {
        $data['row'] = $this->noticeModel->get($id);
        $theme = $this->session->userdata('theme');

        $template = '/template/notice/view';

        if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
            $this->load->view('themes/'.$theme.$template, $data);
        } else {
            $this->load->view('themes/default'.$template, $data);
        }
    }

    public function edit($id)
    {
        $department = $this->session->userdata('department');

        if (!$department) {
            show_404();
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('department_id', 'department_id', '');
        $this->form_validation->set_rules('user', 'user', '');
        $this->form_validation->set_rules('user_id', 'user_id', '');
        $this->form_validation->set_rules('content', 'Content', 'required');
        $this->form_validation->set_rules('sort_order', '', '');

        if ($this->form_validation->run() == false) {
            $data['departments'] = $this->noticeModel->getDepartments();
            $data['msg'] = validation_errors();
            $data['row'] = $this->noticeModel->get($id);

            if ($this->input->post('user_ids', true)) {
                
                $user_ids = $this->input->post('user_ids', true);

                $sql = 'select name, id from users WHERE id IN ('.
                    implode(',', $user_ids).')';

                $data['users'] = $this->db->query($sql)->result_array();
            } else {
                $data['users'] = $this->noticeModel->getUser($id);
            }

            $theme = $this->session->userdata('theme');

            $template = '/template/notice/edit';

            if (file_exists(APPPATH.'views/themes/'.$theme.$template)) {
                $this->load->view('themes/'.$theme.$template, $data);
            } else {
                $this->load->view('themes/default'.$template, $data);
            }
        } else {
            $this->noticeModel->update($id);

            $msg = sprintf(
                $this->lang->line('msg_success'),
                'Success: Notice Updated Successfully!'
            );
            $this->session->set_flashdata('msg', $msg);

            redirect('notice');
        }
    }

    public function delete($id)
    {
        $department = $this->session->userdata('department');

        if (!$department) {
            show_404();
        }

        $this->noticeModel->delete($id);
    }

    public function userAutocomplete($q, $department_id)
    {
        $this->db->where('department', $department_id);
        $this->db->like('name', $q);
        $this->db->limit(10);
        echo json_encode($this->db->get('users')->result_array());
    }
}
