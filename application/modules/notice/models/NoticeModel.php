<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class NoticeModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getUser($notice_id)
    {
        $this->db->select('u.id, u.name');
        $this->db->join('notice_to_user n2u', 'n2u.user_id = u.id');
        $this->db->where('n2u.notice_id = '.$notice_id);

        return $this->db->get('users u')->result_array();
    }

    public function getDepartments()
    {
        return $this->db->get('departments')->result_array();
    }

    public function getTotal($q = null)
    {
        if ($q) {
            $this->db->like('content', $q);

            return $this->db->count_all_results('notice');
        } else {
            return $this->db->count_all('notice');
        }
    }

    public function get($id = 0, $limit = 10, $offset = 0, $q = null)
    {
        if ($id) {
            return $this->db->get_where(
                'notice',
                array(
                    'notice_id' => $id,
                )
            )->row_array();
        } else {
            if ($q) {
                $this->db->like('content', $q);
            }

            $this->db->order_by('sort_order', 'DESC');
            $this->db->select('d.department_name as department, notice.*');
            $this->db->join(
                'departments d',
                'd.id = notice.department_id',
                'left'
            );

            return $this->db->get('notice', $limit, $offset)->result_array();
        }
    }

    public function insert()
    {
        if ($this->input->post('user_id', true)) {
            $user_id = $this->input->post('user_id', true);
        } else {
            $user_id = 0;
        }

        $data = array(
            'content' => $this->input->post('content', true),
            'department_id' => $this->input->post('department_id', true),
            'user_id' => $user_id,
            'sort_order' => $this->input->post('sort_order', true),
            'date_added' => date('Y-m-d'),
        );
        $this->db->insert('notice', $data);

        $notice_id = $this->db->insert_id();

        $user_ids = $this->input->post('user_ids', true);

        foreach ($user_ids as $user_id) {
            $this->db->insert('notice_to_user',
                array(
                'notice_id' => $notice_id,
                'user_id' => $user_id,
            ));
        }
    }

    public function update($id)
    {
        $data = array(
            'content' => $this->input->post('content', true),
            'department_id' => $this->input->post('department_id', true),
            'user_id' => $this->input->post('user_id', true),
            'sort_order' => $this->input->post('sort_order', true),
            'date_added' => date('Y-m-d'),
        );
        $this->db->where('notice_id', $id);
        $this->db->update('notice', $data);

        $user_ids = $this->input->post('user_ids', true);

        $this->db->delete('notice_to_user', array('notice_id' => $id));

        if($user_ids){
            foreach ($user_ids as $user_id) {
                $this->db->insert('notice_to_user',
                    array(
                    'notice_id' => $id,
                    'user_id' => $user_id,
                ));
            }
        }
    }

    public function delete($id)
    {
        $this->db->delete('notice', array('notice_id' => $id));
        $this->db->delete('notice_to_user', array('notice_id' => $id));
    }
}
