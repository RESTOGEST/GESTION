<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * test if the date is today
 * @param  a date string
 * @return bool
 */

function sendNotice($email, $subject, $message) {
    
    $CI = & get_instance();

    $CI->load->library('email');
          
    $config['protocol'] = config('smtp_protocol');
    $config['smtp_host'] = config('smtp_host');
    $config['smtp_user'] = config('smtp_user');
    $config['smtp_pass'] = config('smtp_pass');
    $config['smtp_port'] = config('smtp_port');
    $config['smtp_timeout'] = config('smtp_timeout');
    $config['charset']      = 'utf-8';
    $config['newline']      = "\r\n";
    $config['mailtype']     = 'html';
    $CI->email->initialize($config);

    $CI->email->from(config('site_email'), $CI->config->item('site_name'));
    $CI->email->to($email);
    
    $CI->email->subject($subject);
    $CI->email->message($message);

    $CI->email->send();		
}

?>