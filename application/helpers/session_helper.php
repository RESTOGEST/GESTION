<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Oturum kontrol?n? yapan, oturum a??k de?ilse istek yap?lan i?lemi iptal edip giri? sayfas?na y?nlendiren fonks.
function checkLogin() {
    
    $CI = & get_instance();

    $CI->load->model("userModel");

    $username = $CI->session->userdata('username');
    $userhash = $CI->session->userdata('userhash');
    
    if ($username == '' || $userhash == '') {
        redirect('user/login');
    } elseif ($CI->userModel->existsEmail($username) == 0) {
        redirect('user/login');
    } else {

        $userdata = $CI->userModel->userData($username);

        $hash = $CI->config->item('password_hash');

        $hash_db = md5($userdata->password . $hash);

        if ($userhash != $hash_db) {
            redirect('user/login');
        }
    }
}

// ?ye giri?i yap?lm?? m? yap?lmam?? m? sadece d?nd?r?r. Y?nlendirme yapmaz. 
function isLogin() {

    $CI = & get_instance();

    $CI->load->model("userModel");

    $username = $CI->session->userdata('username');
    $userhash = $CI->session->userdata('userhash');

    if ($username == '' || $userhash == '') {
        return 0;
    } else {
        
        $userdata = $CI->userModel->userData($username);

        if(isset($userdata->password)){
            $hash_db = md5($userdata->password
                . $CI->config->item('password_hash'));

            $exists = $CI->userModel->existsEmail($username);

            if ($CI->userModel->existsEmail($username) == 0) {
                return 0;
            } elseif ($userhash != $hash_db) {
                return 0;
            } elseif ($exists == 1 && $userhash == $hash_db) {
                return 1;
            }
        }else{
            return 0;
        }
    }
}

function userdata($field = 'id') {
    
    $CI = & get_instance();

    $CI->load->model("userModel");

    $username = $CI->session->userdata('username');

    $userdata = $CI->userModel->userData($username);

    if(isset($userdata->$field)){
        return $userdata->$field;
    }
}

function config($field = 'id') {
    $CI = & get_instance();

    $temp = $CI->db->query("SELECT value FROM settings WHERE name='".$field."'")->row_array();
    
    if($temp){
        return $temp['value'];
    }else{
        return null;
    }
}
