--
-- Database: `a1support`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachments_tmp`
--

DROP TABLE IF EXISTS `attachments_tmp`;
CREATE TABLE IF NOT EXISTS `attachments_tmp` (
`id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bugs`
--

DROP TABLE IF EXISTS `bugs`;
CREATE TABLE IF NOT EXISTS `bugs` (
`bug_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `votes` int(50) NOT NULL,
  `product_id` int(200) NOT NULL,
  `date_added` date NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 - Complete, 0 - Incomplete'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bugs`
--

INSERT INTO `bugs` (`bug_id`, `user_id`, `title`, `description`, `votes`, `product_id`, `date_added`, `status`) VALUES
(1, 14, 'Image Resize', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 3, '2015-10-08', 1),
(2, 14, 'Sound Effect', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 1, 6, '2015-10-16', 0),
(3, 14, 'Plugin Error', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 5, '2015-10-16', 0),
(4, 14, 'Website Down', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 13, '2015-10-16', 1),
(5, 14, 'After Effects', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 7, '2015-10-16', 0),
(6, 14, '3D Models', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 2, '2015-10-16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bug_discussion`
--

DROP TABLE IF EXISTS `bug_discussion`;
CREATE TABLE IF NOT EXISTS `bug_discussion` (
`fd_id` int(250) NOT NULL,
  `bug_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `comment` text NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bug_discussion`
--

INSERT INTO `bug_discussion` (`fd_id`, `bug_id`, `user_id`, `comment`, `datetime`) VALUES
(25, 1, 51, 'Image doesn''t resize as per image div', '2015-10-21 17:07:23'),
(26, 1, 61, 'Yeah We are working on that', '2015-10-21 17:09:21'),
(27, 1, 51, 'When can we expect this bug to resolve ?', '2015-10-21 17:09:55'),
(28, 1, 61, 'It is solved', '2015-10-21 17:11:05'),
(29, 1, 51, 'Ok', '2015-10-21 17:13:24'),
(30, 2, 53, 'The sound effect keeps on looping', '2015-10-21 17:18:25'),
(31, 2, 61, 'It will be fixed within 48 hrs', '2015-10-21 17:19:05'),
(32, 3, 36, 'Plugin shows 404 error after import', '2015-10-21 17:26:00'),
(33, 3, 55, 'It is permission issue', '2015-10-21 17:26:41'),
(34, 3, 35, 'Ok', '2015-10-21 17:27:26'),
(35, 5, 61, 'JWPlayer stops in between automatically', '2015-10-21 17:28:40'),
(36, 5, 35, 'We know and we are working on it', '2015-10-21 17:31:10');

-- --------------------------------------------------------

--
-- Table structure for table `bug_vote_history`
--

DROP TABLE IF EXISTS `bug_vote_history`;
CREATE TABLE IF NOT EXISTS `bug_vote_history` (
`vote_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `bug_id` int(250) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bug_vote_history`
--

INSERT INTO `bug_vote_history` (`vote_id`, `user_id`, `bug_id`, `date_added`) VALUES
(1, 14, 1, '2015-07-03'),
(2, 14, 2, '2015-09-14'),
(3, 14, 3, '2015-09-14'),
(4, 14, 5, '2015-10-16');

-- --------------------------------------------------------

--
-- Table structure for table `chat_minimize`
--

DROP TABLE IF EXISTS `chat_minimize`;
CREATE TABLE IF NOT EXISTS `chat_minimize` (
  `receiver_id` int(250) NOT NULL,
  `sender_id` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_minimize`
--

INSERT INTO `chat_minimize` (`receiver_id`, `sender_id`) VALUES
(14, 18),
(20, 14),
(21, 14),
(17, 14),
(14, 24),
(14, 25),
(14, 26),
(16, 32),
(14, 34),
(14, 41),
(38, 14),
(14, 38),
(37, 14),
(44, 16),
(39, 43),
(43, 47),
(14, 48),
(16, 47),
(14, 47),
(63, 14),
(47, 14),
(14, 51),
(51, 14),
(16, 14),
(56, 14);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
`id` int(11) NOT NULL,
  `department_name` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_name`) VALUES
(14, 'Accounting'),
(6, 'Hosting'),
(13, 'Tech'),
(12, 'Sales');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
`faq_id` int(250) NOT NULL,
  `question` varchar(250) NOT NULL,
  `answer` text NOT NULL,
  `datetime` datetime NOT NULL,
  `sort_order` int(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `question`, `answer`, `datetime`, `sort_order`) VALUES
(1, 'What is A1 Support ?', '<p><strong>A1 Support Desk&nbsp;</strong>is All In One Support Desk solution&nbsp;using which you can have support tickets, live concurrent chats, Knowledge Base using FAQ, Bug Reporting, Selling Customization Services, Verify Envato Purchase</p>', '2015-10-21 12:10:10', 8),
(2, 'How to Buy ?', '<p>You can buy A1 Support at codecanyon.net by envato</p>', '2015-10-22 12:50:21', 2),
(3, 'Sample FAQ Question', '<p>Sample FAQ Answer</p>', '2015-10-21 13:22:32', 3),
(4, 'How Does Features work ?', '<p>Similar to Bugs Customer Requests a New feature from already existing products&nbsp;and gives description about it.There is discussion attached to every feature created and people can discuss about it. There is also voting system. The feature getting maximum votes can be developed in next iteration or update of Product. When it has been developed admin will flag it as Complete</p>', '2015-10-21 12:59:22', 4),
(5, 'How Multiple Chat works ?', '<p>Multiple or Live Concurrent Chats opens a new chat window for every customer and creates canned messages to see what agent has spoken before.</p><p>In this way 1 agent can attend to many customers. Also Chat can be transferred to different agent.</p>', '2015-10-21 12:11:43', 5),
(6, 'How does Envato Purchase Code Verifier work ?', '<p>For Envato Purchase Code Verifier to work you have to set your API Key and Username. API Keys can be found or generated&nbsp;at <strong>Account &gt; Settings &gt; API Keys</strong></p>', '2015-10-21 13:15:52', 6),
(8, 'How does Bug Reporting work ?', '<p>Customer Reports a Bug and gives description about it.There is discussion attached to every bug reported and people can discuss about bugs. There is also voting system. The bug getting maximum votes can be addressed first and considered critical. When it is solved admin will flag it as solved</p>', '2015-10-21 12:18:35', 7);

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
CREATE TABLE IF NOT EXISTS `features` (
`feature_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `votes` int(50) NOT NULL,
  `product_id` int(200) NOT NULL,
  `date_added` date NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 - Complete, 0 - Incomplete'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`feature_id`, `user_id`, `title`, `description`, `votes`, `product_id`, `date_added`, `status`) VALUES
(1, 14, 'Themeforest Themes', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 13, '2015-10-16', 0),
(2, 14, 'Graphicriver Graphics', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 3, '2015-10-16', 1),
(3, 14, 'Photodune Photo', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 10, '2015-10-16', 1),
(4, 14, 'Audiojungle Clips', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 6, '2015-10-16', 0),
(5, 14, 'Codecanyon Script', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 5, '2015-10-16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `feature_discussion`
--

DROP TABLE IF EXISTS `feature_discussion`;
CREATE TABLE IF NOT EXISTS `feature_discussion` (
`fd_id` int(250) NOT NULL,
  `feature_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `comment` text NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feature_discussion`
--

INSERT INTO `feature_discussion` (`fd_id`, `feature_id`, `user_id`, `comment`, `datetime`) VALUES
(25, 1, 51, 'Theme needs to be compatible with WooCommerce 2.3.x', '2015-10-21 17:07:23'),
(26, 1, 61, 'Yeah We are working on that', '2015-10-21 17:09:21'),
(27, 1, 51, 'When can we expect the release ?', '2015-10-21 17:09:55'),
(28, 1, 61, 'At the end of this month', '2015-10-21 17:11:05'),
(29, 1, 51, 'Ok', '2015-10-21 17:13:24'),
(30, 2, 53, 'I want this Graphicriver graphic with Disco theme', '2015-10-21 17:18:25'),
(31, 2, 61, 'We have added additional 2 layouts one of which is Disco theme as asked here.', '2015-10-21 17:19:05'),
(32, 3, 36, 'Photo of small child with flower needed in this pack', '2015-10-21 17:26:00'),
(33, 3, 55, 'Yeah seconded. We want it.', '2015-10-21 17:26:41'),
(34, 3, 35, 'Added and is now available', '2015-10-21 17:27:26'),
(35, 5, 61, 'I think Paypal Payment Gateway integration would increase demand of this script', '2015-10-21 17:28:40'),
(36, 5, 35, 'Paypal, Stripe and Authorize coming in next release', '2015-10-21 17:31:10');

-- --------------------------------------------------------

--
-- Table structure for table `feature_vote_history`
--

DROP TABLE IF EXISTS `feature_vote_history`;
CREATE TABLE IF NOT EXISTS `feature_vote_history` (
`vote_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `feature_id` int(250) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
`id` int(10) unsigned NOT NULL,
  `ticket_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `files` text NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `created_time` int(10) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `ticket_id`, `user_id`, `message`, `files`, `ip_address`, `created_time`) VALUES
(10, 7, 16, 'asdsad', '59cb275151b16f4c9c95c6429550d5d6.png', '127.0.0.1', 1435908180),
(11, 1, 14, 'ydfryghzdreplplkdfrgkdofgdfhjfdjh;olkfghp', '49cb275151b16f4c9c95c6429550d5d6.png', '127.0.0.1', 1435913400),
(35, 15, 58, 'Hi, I have been upto you a few times asking about Shopify Ebay Integration.Please tell me your charges', '0', '93.107.92.61', 1439854080),
(14, 10, 14, 'By tomorrow evening', '0', '123.237.161.239', 1439296500),
(59, 20, 47, 'I''m facing problem in A1 Support', '0', '192.168.2.9', 1444296300),
(62, 20, 47, 'Ok I will wait. Please do it urgently. Following points needs to be considered<ol><li>Hello1</li><li>Hello2</li><li>Hello3</li></ol><a target="_blank" href="http://www.wowtech.co">http://www.wowtech.co</a>', '49cb275151b16f4c9c95c6429550d5d6.png', '192.168.2.9', 1444296540),
(50, 18, 37, 'Need Installation help with your theme. I have given my FTP details in Customer Details section', '', '127.0.0.1', 1443164520),
(51, 18, 41, 'We have successfully installed theme on your server. Please find attached screenshot', '49cb275151b16f4c9c95c6429550d5d6.png', '127.0.0.1', 1443164760),
(38, 15, 35, 'Hello Mark, We will tell you soon. Also if possible please provide more details as to what type of integration are you taking about so that we are clear on your requirements', '0', '123.237.161.239', 1440041280),
(48, 19, 16, 'Please go through attached screenshot', '49cb275151b16f4c9c95c6429550d5d6.png', '192.168.2.9', 1441883100),
(79, 17, 38, 'Yeah its been solved. Thank you', '0', '127.0.0.1', 1445547200),
(40, 17, 38, 'Hi, I bought your extension under the Order ID&nbsp;#2, However I am facing some errors on the system. Currently I am operating on OC Version 1.5.2.1 The Issue are as follows:Back-end Admin<ol><li>Under Point of Sale &gt; Settings , when I click on the &quot;Cancel Button&quot; i have an 404 error POS Interface.</li><li>When I Click &quot;Info&quot; it shows me an error saying &quot;Error: Could not load language pos/product!&quot; on all the products shown on the right</li><li>The Search function is not able to search for any products</li><li>I am not able to add to cart for products that has option set in the product when i click on the &quot;Cart&quot; Button.</li><li>When the product are added successfully, the front-end of the website prompts an error of &quot;Invalid argument supplied for foreach()&quot;</li><li>When I tired to update the order under &quot;Order List -&gt; Edit&quot; when I click on the submit button, nothing happens.</li><li>When I edit the orders from the order list I am not able to submit or place an order</li><li>I am not able to apply coupon in the order</li><li>I try to place an order under a registered customer when I tried to place an order.</li></ol>Kindly revert on the issues mentioned above, Hope to hear from you soon.Waiting for you urgent reply.', '0', '203.117.128.174', 1440060120),
(41, 15, 58, 'I want Shopify and Ebay inventory to remain in sync. Is that possible ?', '0', '89.101.224.159', 1440061260),
(42, 15, 58, 'any update im really under pressure for this now', '0', '89.101.224.159', 1440583680),
(78, 17, 40, 'We have solved the problem. Please Look and reply back', '', '127.0.0.1', 1445546400),
(45, 15, 43, 'It would cost $399 to make such an app. Also it will have to be hosted on your hosting.', '0', '89.101.224.159', 1440588060),
(46, 19, 16, 'I''m interested in your Magento theme.<br>Have certain queries though.<br><br>1. What will the duration of support ?<br>2. What if I want certain customization ? <br>', '0', '50.254.249.85', 1440594420),
(76, 19, 65, 'Ok. We will see and get back to you', '', '127.0.0.1', 1445411460),
(47, 17, 14, 'lkjlk', '49cb275151b16f4c9c95c6429550d5d6.png', '192.168.2.9', 1441881120),
(58, 19, 14, 'fghfgh', '', '127.0.0.1', 1443177900),
(65, 21, 48, 'WooCommerce Shipping is not getting calculated perfectly', '0', '192.168.2.7', 1444312620),
(66, 21, 48, 'I want per kg calculation. If I set $1 per kg and Shopping Cart has 10Kg, Shipping Cost should be $10', '', '192.168.2.7', 1444312680),
(67, 22, 16, 'Hello I''m getting 404 error. Please look at the attachments.', '49cb275151b16f4c9c95c6429550d5d6.png,59cb275151b16f4c9c95c6429550d5d6.png', '127.0.0.1', 1444647960),
(68, 23, 55, 'Sample Customer Comment', '', '127.0.0.1', 1444722180),
(69, 23, 60, 'Sample Support Reply', '', '127.0.0.1', 1444722300),
(70, 23, 55, 'Sample Customer Reply', '', '127.0.0.1', 1444722420),
(71, 23, 60, 'Sample Support Reply', '', '127.0.0.1', 1444722480),
(72, 24, 47, 'Sample Customer Comment with Attachment', '49cb275151b16f4c9c95c6429550d5d6.png', '127.0.0.1', 1444722540),
(80, 20, 40, 'It has been done. See now', '', '127.0.0.1', 1445546940),
(75, 24, 60, 'Sample Support Reply with Attachment', '59cb275151b16f4c9c95c6429550d5d6.png', '127.0.0.1', 1444735980),
(77, 22, 40, 'We have seen the attachments.Please provide FTP details', '', '127.0.0.1', 1445544360);

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
CREATE TABLE IF NOT EXISTS `notice` (
`notice_id` int(250) NOT NULL,
  `department_id` int(250) DEFAULT NULL,
  `user_id` int(250) NOT NULL,
  `content` text NOT NULL,
  `sort_order` int(50) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `department_id`, `user_id`, `content`, `sort_order`, `date_added`) VALUES
(1, 6, 0, 'Your Problem is Solved..', 3, '2015-10-16'),
(2, 6, 0, 'Scheduled maintenance from 21st October, 2015 4:00 am PDT to 8:00 am PDT', 0, '2015-10-21'),
(3, 0, 0, 'We will be on holidays from November 10th to October 15th, 2015', 1, '2015-10-16'),
(4, 0, 0, 'We are Sorry. Our site is under maintenance', 0, '2015-10-16');

-- --------------------------------------------------------

--
-- Table structure for table `notice_to_user`
--

DROP TABLE IF EXISTS `notice_to_user`;
CREATE TABLE IF NOT EXISTS `notice_to_user` (
`nu_id` int(11) NOT NULL,
  `notice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice_to_user`
--

INSERT INTO `notice_to_user` (`nu_id`, `notice_id`, `user_id`) VALUES
(12, 1, 42),
(15, 4, 55),
(16, 4, 57),
(17, 4, 51),
(18, 3, 51);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
`product_id` int(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `icon` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `link` varchar(250) NOT NULL,
  `sort_order` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `title`, `icon`, `description`, `price`, `link`, `sort_order`) VALUES
(2, '3D Ocean', 'd682694c2856e37649ffc4417fb074a2.png', 'Browse a massive collection of royalty free 3D models, stock 3D objects, and CG Textures for 3D modelling software like Cinema 4D, Maya, and 3ds Max', '50.00', 'http://3docean.net/', 1),
(3, 'Graphicriver', '4fb41db51bab2f0c6aa8b7331ab9b3b0.png', 'Browse premium design templates and stock graphics for logos, fonts, print design, web design, Photoshop, InDesign, Lightroom, icons, business cards, and more.', '25.00', 'http://graphicriver.net/', 6),
(5, 'Codecanyon', 'e37bfb636fa5128f1ed75c7d9dbbb420.png', 'Browse the largest script and code marketplace on the web. Find WordPress plugins, jQuery plugins, Javascript, CSS and more. Save time, Buy code.', '50.00', 'http://codecanyon.net/', 4),
(6, 'Audiojungle', 'd85b7637498244453280c8cfecb7a28a.png', 'Browse our massive collection of sound effects, royalty free music and stock audio. Add music to web, broadcast, video, presentations, and other projects.', '200.00', 'http://audiojungle.net/', 3),
(7, 'Videohive', '0e9898a9842bb83e0800a6278f5a73f1.png', 'Create a video with video templates.  Browse high quality after affects templates, apple motion videos, Cinema4D files, video clips, royalty free stock footage, and more.', '50.00', 'http://videohive.net/', 9),
(10, 'Photodune', 'a06fdd03be6d3683226f2434d858c0bf.png', 'Buy royalty-free stock photos from a global community of photographers. All photos are sold for commercial use on websites, presentations, videos, advertising, editorial and more.', '25.00', 'http://photodune.net/', 7),
(12, 'Envato', '365cf94c66fd3fbb09edad32656f1ab9.png', 'Over 3 million digital products created by a global community of designers, developers, photographers, illustrators & producers.', '45.00', 'http://www.envato.com/', 5),
(13, 'Themeforest', '7f0d56156a038d1d4111601bc11ad67b.png', 'The #1 marketplace for premium website templates, including themes for WordPress, Magento, Drupal, Joomla, and more.  Create a website, fast.', '200.00', 'http://themeforest.net/', 8),
(14, 'Activeden', '663bf18fb0622df61424a0dd76a43901.png', 'With thousands of Flash Components, Files and Templates, ActiveDen is the largest library of stock Flash online. Starting at just $2 and by a huge community of authors!', '400.00', 'http://activeden.net/', 2);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
`service_id` int(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `icon` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `sort_order` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `title`, `icon`, `description`, `price`, `sort_order`) VALUES
(3, 'Magento Development', 'ce4a53392c943f994383dca9fd789f61.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '25.00', 3),
(7, 'Opencart Development', '98aa29ca5d2d0efb2b70500a72f0011f.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '50.00', 4),
(8, 'Wordpress  Development', '9f4fc2967b0816abc008d48a91e31fb0.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '69.00', 8),
(11, 'Woo Commerce Development', '4f5bb1c7672e2e349fb968c73f211318.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '567.00', 7),
(12, 'Shopify Development', '92c958c809956a432c185c36b5a3daf1.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '456.00', 6),
(13, 'Android Development', '4df4564e112f37453f343502c6c738a6.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '78.00', 1),
(14, 'Drupal Development', 'ce5d71017fd40749e3346f2d64422cce.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '49.00', 2),
(15, 'PrestaShop Development', 'c173e2ebfd9466a0a921731c7c22af71.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '60.00', 5),
(16, 'Zencart Development', '5bee2dc077a642f8d406318d8cdee5a8.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '55.00', 9),
(17, 'Custom Services', '1aae9a71d34a8518692e46fb2dc47c3e.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'id', '1'),
(2, 'agent_max_chat', '5'),
(5, 'allowed_extensions', 'gif|jpg|jpeg|png|pdf|txt'),
(6, 'max_upload_files', '4'),
(7, 'max_upload_file_size', '3000'),
(8, 'theme', 'default'),
(22, 'auth_login_id', ''),
(23, 'auth_trans_key', ''),
(24, 'authorize_test', '1'),
(25, 'paypal_username', ''),
(26, 'paypal_password', ''),
(27, 'paypal_signature', ''),
(28, 'paypal_test', '1'),
(29, 'currency', 'USD'),
(36, 'envato_app_id', ''),
(37, 'envato_username', ''),
(48, 'smtp_protocol', 'smtp'),
(49, 'smtp_host', ''),
(50, 'smtp_user', ''),
(51, 'smtp_pass', ''),
(52, 'smtp_port', '465'),
(53, 'smtp_timeout', '10'),
(90, 'new_ticket_subject', '{{site_name}} Ticket #{{ticket_id}}'),
(91, 'new_ticket_template', '<p>Ticket #{{ticket_id}} Details&nbsp;</p><p>{{title}}</p><p>---------------------------------------------------------------<br />{{message}}</p><p>Generated from&nbsp;<br />{{ip_addresss}}</p><p>View Ticket</p><p>{{link}}&nbsp;</p><p>&nbsp;</p>'),
(92, 'ticket_status_change_subject', '{{site_name}} Ticket #{{ticket_id}}'),
(93, 'ticket_status_change_template', '<p>Status changed from&nbsp;{{old_status}} to&nbsp;{{new_status}}</p><p>View Ticket</p><p>{{link}}&nbsp;</p>'),
(108, 'online_check_time', '2'),
(109, 'online_update_time', '2500'),
(110, 'message_check_time', '2500'),
(111, 'notification_check_time', '2500'),
(148, 'site_name', 'A1 Support Desk'),
(149, 'site_email', 'admin@example.com'),
(150, 'tickets_per_page', '10'),
(151, 'language', 'english'),
(152, 'currency_sl', '$'),
(153, 'currency_sr', '');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
`id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `product_id` int(11) unsigned NULL,
  `service_id` int(11) unsigned NULL,
  `order_id` varchar(50) NULL,  
  `title` varchar(255) NOT NULL,
  `ftp_db_access` text NULL,
  `priority` enum('1','2','3') NOT NULL,
  `created_time` int(11) NOT NULL,
  `modified_time` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `agent_id` int(50) NOT NULL,
  `secret` varchar(250) NOT NULL,
  `ticket_status` int(1) NOT NULL COMMENT '1- open, 2 - pending, 3 - close'
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `department_id`, `user_id`, `title`, `ftp_db_access`, `priority`, `created_time`, `modified_time`, `ip_address`, `agent_id`, `secret`, `ticket_status`) VALUES
(19, 12, 42, 'Magento Theme Problem', '', '3', 1440594420, 1445411460, '50.254.249.85', 60, '', 1),
(18, 13, 37, 'Installation Request', '', '3', 1440586560, 1440586560, '60.241.129.171', 41, '', 3),
(17, 12, 38, 'Opencart Plugin Development', '', '2', 1440060120, 1445546400, '203.117.128.174', 42, '', 3),
(15, 13, 58, 'Shopify Ebay Integration', '', '3', 1439854080, 1439854080, '93.107.92.61', 43, '', 3),
(21, 12, 48, 'WooCommerce Shipping Development', '', '3', 1444312620, 1444312680, '192.168.2.7', 41, '', 2),
(20, 13, 47, 'Admin Not Opening', '', '2', 1444296300, 1445546940, '192.168.2.9', 43, '', 3),
(22, 13, 16, '404 Not Found Error', '', '2', 1444647960, 1445544360, '127.0.0.1', 42, '', 2),
(23, 6, 47, 'Upgrade VPS Plan', '', '1', 1444722180, 1444722480, '127.0.0.1', 43, '1325391493', 3),
(24, 6, 55, 'PHP Short Tag Error', '', '1', 1444722540, 1444735980, '127.0.0.1', 42, '1285796106', 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `photo` varchar(250) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `lostpw` varchar(50) NOT NULL,
  `register_time` int(11) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `department` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` datetime NOT NULL,
  `typing_momment` datetime NOT NULL,
  `total_active_chat` int(50) NOT NULL,
  `agent_id` int(50) NOT NULL,
  `chat_response` tinyint(1) NOT NULL COMMENT '0- no data, 1-like, 2-dislike',
  `is_chat_banned` tinyint(1) NOT NULL,
  `details` text NOT NULL,
  `play_sound` tinyint(1) NOT NULL,  
  `status` int(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `email`, `telephone`, `password`, `lostpw`, `register_time`, `ip_address`, `department`, `last_activity`, `typing_momment`, `total_active_chat`, `agent_id`, `chat_response`, `is_chat_banned`, `details`, `play_sound`, `status`) VALUES
(51, 'John', '', 'customer@example.com', '123456789', 'f4ad231214cb99a985dff0f056a36242', '', 1445406780, '', 0, '2015-10-23 04:50:25', '2015-10-23 04:45:50', 1, 40, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(55, 'Peter', '', 'peter@example.com', '', '', '', 1444632960, '127.0.0.1', 0, '2015-10-21 17:26:43', '2015-10-12 12:27:31', 0, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(56, 'Sam', '', 'sam@example.com', '898989898', '', '', 1444634220, '127.0.0.1', 0, '2015-10-17 16:38:37', '2015-10-17 16:24:29', 1, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(57, 'Deny', '', 'deny@example.com', '', '', '', 1444635300, '127.0.0.1', 0, '2015-10-12 13:08:39', '2015-10-12 13:06:06', 0, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 0),
(58, 'Mark', '', 'mark@example.com', '847684684684', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1444635540, '127.0.0.1', 0, '2015-10-12 13:11:46', '2015-10-12 13:11:16', 1, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(16, 'Robert', '', 'robert@example.com', '', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1435668480, '127.0.0.1', 0, '2015-10-13 18:45:08', '2015-10-13 18:25:06', 0, 14, 2, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(53, 'David', '', 'david@example.com', '', '', '', 1445406780, '', 0, '2015-10-21 17:19:57', '2015-10-10 11:14:36', 0, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 0),
(35, 'Brendan', '', 'agent@example.com', '', '2ec199f1e2de31576869a57488e919ad', '', 1439853780, '93.107.92.61', 14, '2015-10-21 17:32:18', '0000-00-00 00:00:00', 0, 28, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(36, 'Kevin Riley', '', 'kevin@example.com', '', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1439937000, '89.168.44.80', 0, '2015-10-21 17:26:03', '2015-09-28 18:27:29', 0, 14, 0, 1, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(37, 'Derek', '', 'derek@example.com', '', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1445406780, '', 0, '2015-10-23 04:50:30', '2015-10-23 04:45:58', 1, 40, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(38, 'Zidus', '', 'zidus@example.com', '', '42d8aa7cde9c78c4757862d84620c335', '', 1440057660, '203.117.128.174', 0, '2015-10-22 21:02:09', '0000-00-00 00:00:00', 0, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(39, 'Zach', '', 'zach@example.com', '', '', '', 1444713180, '', 0, '2015-09-30 18:15:36', '2015-09-30 17:03:52', 0, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 0),
(40, 'Betty', '', 'betty@example.com', '596868505', '', '86015a14dcfb53e84e252fe7be9da30c', 1445406780, '', 1, '2015-10-23 05:12:49', '2015-10-23 04:46:32', 2, 37, 2, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(41, 'Steven', '', 'steven@example.com', '364736476', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1440586320, '60.241.129.171', 12, '2015-10-17 18:23:33', '0000-00-00 00:00:00', 1, 16, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(42, 'Lewis Austin', '', 'lewis@example.com', '', 'd681ff0be5b4f6bc527db81fa7899534', '', 1440594360, '50.254.249.85', 6, '2015-08-26 13:28:19', '0000-00-00 00:00:00', 1, 0, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 0),
(28, 'Tom Enns', '', 'tom@example.com', '', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1439375700, '108.59.8.218', 0, '2015-10-17 19:09:45', '2015-10-17 19:08:33', 1, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(49, 'Joseph', '', 'joseph@example.com', '54645756756', '59c95189ac895fcc1c6e1c38d067e244', '', 1444308540, '192.168.2.7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(43, 'Nancy', '', 'nancy@example.com', '5756757', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1441879200, '192.168.2.9', 6, '2015-10-23 04:50:27', '2015-10-12 14:34:08', 0, 16, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(44, 'Paul', '', 'paul@example.com', '589896566', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1444713180, '', 0, '2015-09-28 11:25:00', '2015-09-28 11:15:00', 0, 16, 0, 1, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(45, 'Donal', '', 'donald@example.com', '', '', '', 1445406780, '', 0, '2015-09-26 15:57:57', '0000-00-00 00:00:00', 1, 16, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(50, 'Jeff', '', 'jeff@example.com', '34234893434', 'e10adc3949ba59abbe56e057f20f883e', '', 1444308600, '192.168.2.7', 0, '2015-10-09 16:11:14', '0000-00-00 00:00:00', 1, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(47, 'Laura', '', 'laura@example.com', '', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1445406780, '', 0, '2015-10-13 17:04:33', '2015-10-12 17:42:21', 1, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(48, 'Helen', '', 'helen@example.com', '344749892374', '154cc5b9fbc9ae2adae49ce009b61f67', '', 1445406780, '192.168.2.7', 0, '2015-10-08 19:31:57', '0000-00-00 00:00:00', 1, 14, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(60, 'Thomas', '', 'thomas@example.com', '364736476', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1444713060, '127.0.0.1', 13, '2015-10-16 16:01:33', '0000-00-00 00:00:00', 0, 51, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 1),
(61, 'Michael', '', 'michael@example.com', '364736476', 'cfe609497d9f6e9a4dc30e1ffe1331ec', '', 1444713180, '127.0.0.1', 13, '2015-10-21 17:34:07', '0000-00-00 00:00:00', 0, 0, 0, 0, '<p>FTP Details</p><p>www.example.com</p>', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_chatroom`
--

DROP TABLE IF EXISTS `user_chatroom`;
CREATE TABLE IF NOT EXISTS `user_chatroom` (
`chat_room_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `receiver_id` int(250) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_chatroom`
--

INSERT INTO `user_chatroom` (`chat_room_id`, `user_id`, `receiver_id`, `status`) VALUES
(23, 14, 28, 0),
(57, 40, 51, 0),
(58, 40, 37, 0),
(59, 40, 51, 0),
(60, 40, 37, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_messages`
--

DROP TABLE IF EXISTS `user_messages`;
CREATE TABLE IF NOT EXISTS `user_messages` (
`msg_id` int(200) NOT NULL,
  `chat_room_id` int(11) NOT NULL,
  `from_id` int(200) NOT NULL,
  `to_id` int(200) NOT NULL,
  `msg` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_notified` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_messages`
--

INSERT INTO `user_messages` (`msg_id`, `chat_room_id`, `from_id`, `to_id`, `msg`, `timestamp`, `is_notified`) VALUES
(1, 25, 28, 35, 'Hi', '2015-10-17 13:38:23', 1),
(2, 25, 35, 28, 'Hello,\r\nHow can i help you sir ?', '2015-10-17 13:39:23', 1),
(3, 25, 28, 35, 'I need to know more about A1 Support', '2015-10-17 13:40:01', 1),
(4, 25, 35, 28, 'A1 Support is an All-In-one Support Desk Solution with various features', '2015-10-17 13:41:23', 1),
(5, 25, 28, 35, 'Where can i buy it ?', '2015-10-17 13:42:01', 1),
(6, 25, 35, 28, 'You can buy A1 Support Desk on Codecanyon', '2015-10-17 13:43:23', 1),
(7, 26, 51, 60, 'Hi', '2015-10-17 13:08:23', 1),
(8, 26, 60, 51, 'Hello', '2015-10-17 13:09:23', 1),
(9, 26, 51, 60, 'Sample Customer Reply', '2015-10-17 13:10:01', 1),
(10, 26, 60, 51, 'Sample Agent Reply', '2015-10-17 13:11:23', 1),
(11, 26, 51, 60, 'Sample Customer Reply?', '2015-10-17 13:12:01', 1),
(12, 26, 60, 51, 'Sample Agent Reply', '2015-10-17 13:13:23', 1),
(17, 30, 40, 37, 'Hi', '2015-10-22 23:13:41', 1),
(18, 29, 51, 40, 'Hello', '2015-10-22 23:14:09', 1),
(19, 29, 51, 40, 'I want support with A1 Support', '2015-10-22 23:14:39', 1),
(20, 29, 40, 51, 'Ok', '2015-10-22 23:14:50', 1),
(21, 29, 40, 51, 'What Kind of support ?', '2015-10-22 23:14:57', 1),
(22, 30, 37, 40, 'Hello', '2015-10-22 23:15:13', 1),
(23, 30, 37, 40, 'I want to buy this A1 Support Desk', '2015-10-22 23:15:29', 1),
(24, 30, 37, 40, 'Can you guide me ?', '2015-10-22 23:15:34', 1),
(25, 29, 51, 40, 'Installation Support', '2015-10-22 23:15:51', 1),
(26, 30, 40, 37, 'Yeah You can buy it on CodeCanyon', '2015-10-22 23:16:18', 0),
(27, 29, 40, 51, 'Yeah We Do provide free installation', '2015-10-22 23:16:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_rating`
--

DROP TABLE IF EXISTS `user_rating`;
CREATE TABLE IF NOT EXISTS `user_rating` (
`rating_id` int(250) NOT NULL,
  `customer_id` int(250) NOT NULL,
  `agent_id` int(250) NOT NULL,
  `rating` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_rating`
--

INSERT INTO `user_rating` (`rating_id`, `customer_id`, `agent_id`, `rating`) VALUES
(4, 55, 14, 2),
(6, 56, 14, 2),
(7, 58, 14, 1),
(9, 28, 35, 1),
(10, 51, 60, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_to_service`
--

DROP TABLE IF EXISTS `user_to_service`;
CREATE TABLE IF NOT EXISTS `user_to_service` (
`id` int(200) NOT NULL,
  `user_id` int(200) NOT NULL,
  `service_id` int(200) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `comment` text NOT NULL,
  `date_added` date NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_to_service`
--

INSERT INTO `user_to_service` (`id`, `user_id`, `service_id`, `amount`, `comment`, `date_added`, `status`) VALUES
(1, 43, 3, '25.00', 'Customize as per PDF sent', '2015-10-17', 1),
(2, 60, 12, '456.00', 'Theme should be minimal', '2015-10-18', 0),
(3, 40, 17, '99.00', 'Opencart Customization of Bulk Import/Export', '2015-10-19', 1),
(4, 61, 11, '567.00', 'PSD to WooCommerce', '2015-10-20', 0),
(5, 41, 7, '50.00', 'Opencart Social Login Extension', '2015-10-20', 0),
(6, 51, 14, '49.00', 'Drupal News Website Creation', '2015-10-21', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attachments_tmp`
--
ALTER TABLE `attachments_tmp`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bugs`
--
ALTER TABLE `bugs`
 ADD PRIMARY KEY (`bug_id`);

--
-- Indexes for table `bug_discussion`
--
ALTER TABLE `bug_discussion`
 ADD PRIMARY KEY (`fd_id`);

--
-- Indexes for table `bug_vote_history`
--
ALTER TABLE `bug_vote_history`
 ADD PRIMARY KEY (`vote_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
 ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
 ADD PRIMARY KEY (`feature_id`);

--
-- Indexes for table `feature_discussion`
--
ALTER TABLE `feature_discussion`
 ADD PRIMARY KEY (`fd_id`);

--
-- Indexes for table `feature_vote_history`
--
ALTER TABLE `feature_vote_history`
 ADD PRIMARY KEY (`vote_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
 ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `notice_to_user`
--
ALTER TABLE `notice_to_user`
 ADD PRIMARY KEY (`nu_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
 ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_chatroom`
--
ALTER TABLE `user_chatroom`
 ADD PRIMARY KEY (`chat_room_id`);

--
-- Indexes for table `user_messages`
--
ALTER TABLE `user_messages`
 ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `user_rating`
--
ALTER TABLE `user_rating`
 ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `user_to_service`
--
ALTER TABLE `user_to_service`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attachments_tmp`
--
ALTER TABLE `attachments_tmp`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bugs`
--
ALTER TABLE `bugs`
MODIFY `bug_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bug_discussion`
--
ALTER TABLE `bug_discussion`
MODIFY `fd_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `bug_vote_history`
--
ALTER TABLE `bug_vote_history`
MODIFY `vote_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
MODIFY `faq_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
MODIFY `feature_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `feature_discussion`
--
ALTER TABLE `feature_discussion`
MODIFY `fd_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `feature_vote_history`
--
ALTER TABLE `feature_vote_history`
MODIFY `vote_id` int(250) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
MODIFY `notice_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `notice_to_user`
--
ALTER TABLE `notice_to_user`
MODIFY `nu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `product_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
MODIFY `service_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `user_chatroom`
--
ALTER TABLE `user_chatroom`
MODIFY `chat_room_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `user_messages`
--
ALTER TABLE `user_messages`
MODIFY `msg_id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user_rating`
--
ALTER TABLE `user_rating`
MODIFY `rating_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_to_service`
--
ALTER TABLE `user_to_service`
MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
