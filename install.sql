DROP TABLE IF EXISTS `attachments_tmp`;
CREATE TABLE IF NOT EXISTS `attachments_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `bugs`;
CREATE TABLE IF NOT EXISTS `bugs` (
  `bug_id` int(250) NOT NULL AUTO_INCREMENT,
  `user_id` int(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `votes` int(50) NOT NULL,
  `product_id` int(200) NOT NULL,
  `date_added` date NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 - Complete, 0 - Incomplete',
  PRIMARY KEY (`bug_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `bug_discussion`;
CREATE TABLE IF NOT EXISTS `bug_discussion` (
  `fd_id` int(250) NOT NULL AUTO_INCREMENT,
  `bug_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `comment` text NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `bug_vote_history`;
CREATE TABLE IF NOT EXISTS `bug_vote_history` (
  `vote_id` int(250) NOT NULL AUTO_INCREMENT,
  `user_id` int(250) NOT NULL,
  `bug_id` int(250) NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `chat_minimize`;
CREATE TABLE IF NOT EXISTS `chat_minimize` (
  `receiver_id` int(250) NOT NULL,
  `sender_id` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(250) NOT NULL AUTO_INCREMENT,
  `question` varchar(250) NOT NULL,
  `answer` text NOT NULL,
  `datetime` datetime NOT NULL,
  `sort_order` int(50) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `features`;
CREATE TABLE IF NOT EXISTS `features` (
  `feature_id` int(250) NOT NULL AUTO_INCREMENT,
  `user_id` int(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `votes` int(50) NOT NULL,
  `product_id` int(200) NOT NULL,
  `date_added` date NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 - Complete, 0 - Incomplete',
  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `feature_discussion`;
CREATE TABLE IF NOT EXISTS `feature_discussion` (
  `fd_id` int(250) NOT NULL AUTO_INCREMENT,
  `feature_id` int(250) NOT NULL,
  `user_id` int(250) NOT NULL,
  `comment` text NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `feature_vote_history`;
CREATE TABLE IF NOT EXISTS `feature_vote_history` (
  `vote_id` int(250) NOT NULL AUTO_INCREMENT,
  `user_id` int(250) NOT NULL,
  `feature_id` int(250) NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `files` text NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `created_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `notice`;
CREATE TABLE IF NOT EXISTS `notice` (
  `notice_id` int(250) NOT NULL AUTO_INCREMENT,
  `department_id` int(250) DEFAULT NULL,
  `user_id` int(250) NOT NULL,
  `content` text NOT NULL,
  `sort_order` int(50) NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `notice_to_user`;
CREATE TABLE IF NOT EXISTS `notice_to_user` (
  `nu_id` int(11) NOT NULL AUTO_INCREMENT,
  `notice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`nu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(250) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `icon` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `link` varchar(250) NOT NULL,
  `sort_order` int(100) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(250) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `icon` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `sort_order` int(100) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'id', '1'),
(2, 'agent_max_chat', '5'),
(5, 'allowed_extensions', 'gif|jpg|jpeg|png|pdf|txt'),
(6, 'max_upload_files', '4'),
(7, 'max_upload_file_size', '3000'),
(8, 'theme', 'default'),
(22, 'auth_login_id', ''),
(23, 'auth_trans_key', ''),
(24, 'authorize_test', '1'),
(25, 'paypal_username', ''),
(26, 'paypal_password', ''),
(27, 'paypal_signature', ''),
(28, 'paypal_test', '1'),
(29, 'currency', 'USD'),
(36, 'envato_app_id', ''),
(37, 'envato_username', ''),
(48, 'smtp_protocol', 'smtp'),
(49, 'smtp_host', ''),
(50, 'smtp_user', ''),
(51, 'smtp_pass', ''),
(52, 'smtp_port', '465'),
(53, 'smtp_timeout', '10'),
(90, 'new_ticket_subject', '{{site_name}} Ticket #{{ticket_id}}'),
(91, 'new_ticket_template', '<p>Ticket #{{ticket_id}} Details&nbsp;</p><p>{{title}}</p><p>---------------------------------------------------------------<br />{{message}}</p><p>Generated from&nbsp;<br />{{ip_addresss}}</p><p>View Ticket</p><p>{{link}}&nbsp;</p><p>&nbsp;</p>'),
(92, 'ticket_status_change_subject', '{{site_name}} Ticket #{{ticket_id}}'),
(93, 'ticket_status_change_template', '<p>Status changed from&nbsp;{{old_status}} to&nbsp;{{new_status}}</p><p>View Ticket</p><p>{{link}}&nbsp;</p>'),
(108, 'online_check_time', '2'),
(109, 'online_update_time', '2500'),
(110, 'message_check_time', '2500'),
(111, 'notification_check_time', '2500'),
(130, 'site_name', 'A1 Support Desk'),
(131, 'site_email', ''),
(132, 'tickets_per_page', '10'),
(133, 'language', 'english'),
(134, 'currency_sl', '$'),
(135, 'currency_sr', '');

DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `product_id` int(11) unsigned NULL,
  `service_id` int(11) unsigned NULL,
  `order_id` varchar(50) NULL,
  `title` varchar(255) NOT NULL,
  `ftp_db_access` text NULL,
  `priority` enum('1','2','3') NOT NULL,
  `created_time` int(11) NOT NULL,
  `modified_time` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `agent_id` int(50) NOT NULL,
  `secret` varchar(250) NOT NULL,
  `ticket_status` int(1) NOT NULL COMMENT '1- open, 2 - pending, 3 - close',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `photo` varchar(250) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `lostpw` varchar(50) NOT NULL,
  `register_time` int(11) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `department` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` datetime NOT NULL,
  `typing_momment` datetime NOT NULL,
  `total_active_chat` int(50) NOT NULL,
  `agent_id` int(50) NOT NULL,
  `chat_response` tinyint(1) NOT NULL COMMENT '0- no data, 1-like, 2-dislike',
  `is_chat_banned` tinyint(1) NOT NULL,
  `details` text NOT NULL,
  `play_sound` tinyint(1) NOT NULL,  
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `user_chatroom`;
CREATE TABLE IF NOT EXISTS `user_chatroom` (
  `chat_room_id` int(250) NOT NULL AUTO_INCREMENT,
  `user_id` int(250) NOT NULL,
  `receiver_id` int(250) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`chat_room_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `user_messages`;
CREATE TABLE IF NOT EXISTS `user_messages` (
  `msg_id` int(200) NOT NULL AUTO_INCREMENT,
  `chat_room_id` int(11) NOT NULL,
  `from_id` int(200) NOT NULL,
  `to_id` int(200) NOT NULL,
  `msg` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_notified` tinyint(1) NOT NULL,
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `user_rating`;
CREATE TABLE IF NOT EXISTS `user_rating` (
  `rating_id` int(250) NOT NULL AUTO_INCREMENT,
  `customer_id` int(250) NOT NULL,
  `agent_id` int(250) NOT NULL,
  `rating` tinyint(1) NOT NULL,
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `user_to_service`;
CREATE TABLE IF NOT EXISTS `user_to_service` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `user_id` int(200) NOT NULL,
  `service_id` int(200) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `comment` text NOT NULL,
  `date_added` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;